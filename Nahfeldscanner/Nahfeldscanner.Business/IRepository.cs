﻿// ///////////////////////////////////
// File: IRepository.cs
// Last Change: 30.01.2018  10:54
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Nahfeldscanner.Model.Database;
    using NHibernate.Criterion;



    public interface IRepository : IDisposable
    {
        #region Properties

        bool IsConnected { get; }

        string DatabasePath { get; }

        #endregion



        void RecreateDatabase(string databasePath);

        void LoadDatabase(string databasePath);

        void CloseDatabase();

        void SaveOrUpdate<T>(T t) where T : class;

        void InsertArray<T>(T[] t) where T : class;

        void BulkInsert(string databasePath, MeasurementValue[] measurementValues);

        void Delete<T>(T t) where T : class;

        long GetAmount<T>() where T : class;

        ICollection<T> GetAll<T>() where T : class;

        T GetById<T>(int id) where T : class;

        T GetEagerById<T>(int id, Func<T, object> property, params Func<T, object>[] otherProperties) where T : class;

        T GetByCriterion<T>(ICriterion criterion) where T : class;

        ICollection<T> GetCollectionByCriterion<T>(ICriterion criterion, Expression<Func<T, object>> orderProperty) where T : class;

        ICollection<T> GetCollectionByCriterion<T, U>(ICriterion criterion1,
                                                      Expression<Func<T, U>> combinationCriterion,
                                                      ICriterion criterion2,
                                                      Expression<Func<T, object>> orderProperty) where T : class;

        ICollection<MeasurementValue> GetMinMeasuremntValueCollectionByCriterion(ICriterion criterion, Expression<Func<MeasurementValue, object>> minProperty);

        ICollection<MeasurementValue> GetMaxMeasuremntValueCollectionByCriterion(ICriterion criterion, Expression<Func<MeasurementValue, object>> maxProperty);
    }
}