﻿// ///////////////////////////////////
// File: SqLiteRepository.cs
// Last Change: 30.01.2018  11:33
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Business
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.Model.Database;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;



    public class SqLiteRepository : IRepository
    {
        #region Fields

        private const int AmountCombinedValues = 100;

        private readonly ISessionManager _sessionManager;

        #endregion



        #region Constructors / Destructor

        public SqLiteRepository(ISessionManager sessionManager)
        {
            this._sessionManager = sessionManager;
        }

        #endregion



        #region IRepository Members

        public bool IsConnected
        {
            get { return this._sessionManager.IsConnected; }
        }

        public string DatabasePath
        {
            get { return this._sessionManager.DatabasePath; }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Free all unmanaged resources when object will be destroyed
        /// </summary>
        public void Dispose()
        {
            if (this._sessionManager != null)
            {
                this._sessionManager.Dispose();
            }
        }

        /// <summary>
        ///     Recreates all tables with content in the database.
        /// </summary>
        /// <param name="databasePath">server: database name, local database: database path</param>
        public void RecreateDatabase(string databasePath)
        {
            this._sessionManager.RecreateDatabase(databasePath);
        }

        /// <summary>
        ///     Loads the database and updates the database schema if necessary.
        /// </summary>
        /// <param name="databasePath">server: database name, local database: database path</param>
        public void LoadDatabase(string databasePath)
        {
            this._sessionManager.LoadDatabase(databasePath);
        }

        /// <summary>
        ///     Closes the connection.
        /// </summary>
        public void CloseDatabase()
        {
            this._sessionManager.CloseDatabase();
        }

        /// <summary>
        ///     Saves a new object or updates the existing one in the database.
        /// </summary>
        /// <typeparam name="T">class of the object</typeparam>
        /// <param name="t">the object itself</param>
        public void SaveOrUpdate<T>(T t) where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.SaveOrUpdate(t);
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new InvalidOperationException(e.Message, e.InnerException);
                    }
                }
            }
        }

        /// <summary>
        ///     Inserts a collection of objects.
        ///     No cascading for the children.
        /// </summary>
        /// <typeparam name="T">object class</typeparam>
        /// <param name="t">object array</param>
        public void InsertArray<T>(T[] t) where T : class
        {
            using (IStatelessSession statelessSession = this._sessionManager.OpenStatelessSession())
            {
                using (ITransaction transaction = statelessSession.BeginTransaction())
                {
                    try
                    {
                        foreach (T arrayItem in t)
                        {
                            statelessSession.Insert(arrayItem);
                        }

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new InvalidOperationException(e.Message, e.InnerException);
                    }
                }
            }
        }

        /// <summary>
        ///     Bulk inserts a collection of objects.
        ///     No cascading for the children.
        /// </summary>
        /// <param name="databasePath">path to the database</param>
        /// <param name="measurementValues">measurement value array</param>
        public void BulkInsert(string databasePath, MeasurementValue[] measurementValues)
        {
            IEnumerable<string> queries = CreateQueryList(measurementValues);

            using (SQLiteConnection connection = new SQLiteConnection(@"Data Source=" + databasePath))
            {
                connection.Open();

                using (SQLiteTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (string query in queries)
                        {
                            SQLiteCommand command = connection.CreateCommand();
                            command.CommandText = query;
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new InvalidOperationException(e.Message, e.InnerException);
                    }
                }

                connection.Close();
            }
        }

        /// <summary>
        ///     Deletes the passed object in the database.
        /// </summary>
        /// <typeparam name="T">object class</typeparam>
        /// <param name="t">object that should be deleted from database</param>
        public void Delete<T>(T t) where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        // necessary to check if object even exists in database
                        session.Update(t);
                        session.Delete(t);
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new InvalidOperationException(e.Message, e.InnerException);
                    }
                }
            }
        }

        /// <summary>
        ///     Gets the amount of rows in the desired table.
        /// </summary>
        /// <typeparam name="T">object class</typeparam>
        /// <returns>number of rows</returns>
        public long GetAmount<T>() where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT count(*) FROM "MeasurementValue"
                return session.CreateCriteria<T>().SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
            }
        }

        /// <summary>
        ///     Gets all objects from the specific class from the database.
        /// </summary>
        /// <typeparam name="T">object class</typeparam>
        /// <returns>all specific objects in the database</returns>
        public ICollection<T> GetAll<T>() where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT * FROM "MeasurementValue"
                return session.QueryOver<T>().List();
            }
        }

        /// <summary>
        ///     Gets the object with specific id.
        /// </summary>
        /// <typeparam name="T">object class</typeparam>
        /// <param name="id">id of the object</param>
        /// <returns>object with specific id</returns>
        public T GetById<T>(int id) where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT * FROM "Analysis" WHERE ID = 1
                return session.Get<T>(id);
            }
        }

        /// <summary>
        ///     Gets the object with specific id and loads also the desired reference or children.
        /// </summary>
        /// <typeparam name="T">object class</typeparam>
        /// <param name="id">id of the object</param>
        /// <param name="property">reference or children that should be loaded, too</param>
        /// <param name="otherProperties">other references or children that should be loaded, too</param>
        /// <returns>eager loaded object with specific id</returns>
        public T GetEagerById<T>(int id, Func<T, object> property, params Func<T, object>[] otherProperties) where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT * FROM "Analysis" WHERE ID = 1
                // SELECT * FROM "Measurement" WHERE Analysis_ID = 1
                T t = session.Get<T>(id);
                NHibernateUtil.Initialize(property(t));

                foreach (Func<T, object> prop in otherProperties)
                {
                    NHibernateUtil.Initialize(prop(t));
                }

                return t;
            }
        }

        /// <summary>
        ///     Gets first element that fulfills the criterion.
        /// </summary>
        /// <typeparam name="T">object class</typeparam>
        /// <param name="criterion">criteria for the elements</param>
        /// <returns>elements that fulfill the criteria</returns>
        public T GetByCriterion<T>(ICriterion criterion) where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT * FROM "Measurement" WHERE X = 1 LIMIT 1
                return session.QueryOver<T>().Where(criterion).Take(1).SingleOrDefault();
            }
        }

        /// <summary>
        ///     Gets all elements that fulfill the criteria.
        /// </summary>
        /// <typeparam name="T">object class</typeparam>
        /// <param name="criterion">criteria for the elements</param>
        /// <param name="orderProperty">elements will be ordered asc by this property</param>
        /// <returns>elements that fulfill the criteria</returns>
        public ICollection<T> GetCollectionByCriterion<T>(ICriterion criterion, Expression<Func<T, object>> orderProperty) where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT * FROM "Measurement" WHERE X = 1 ORDER BY Analysis_ID ASC
                return session.QueryOver<T>().Where(criterion).OrderBy(orderProperty).Asc.List();
            }
        }

        /// <summary>
        ///     Gets all elements that fulfill the joined criteria.
        /// </summary>
        /// <typeparam name="T">first object class</typeparam>
        /// <typeparam name="U">second object class</typeparam>
        /// <param name="criterion1">criteria for the first elements</param>
        /// <param name="combinationCriterion">reference to other element class</param>
        /// <param name="criterion2">criteria for the second elements</param>
        /// <param name="orderProperty">elements will be ordered asc by this property</param>
        /// <returns>elements that fulfill the combined criteria</returns>
        public ICollection<T> GetCollectionByCriterion<T, U>(ICriterion criterion1,
                                                             Expression<Func<T, U>> combinationCriterion,
                                                             ICriterion criterion2,
                                                             Expression<Func<T, object>> orderProperty) where T : class
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT mv.*, m.* FROM "MeasurementValue" mv inner join "Measurement" m on mv.Measurement_id=m.ID
                // WHERE mv.Frequency = 100000 and m.Analysis_id = 1 ORDER BY mv.Measurement_id ASC
                return session.QueryOver<T>().Where(criterion1).OrderBy(orderProperty).Asc().JoinQueryOver(combinationCriterion).Where(criterion2).List();
            }
        }

        /// <summary>
        ///     Gets all measurement values with min property value grouped by measurement.
        /// </summary>
        /// <param name="criterion">criterion for the elements</param>
        /// <param name="minProperty">property which should contain the min value</param>
        /// <returns></returns>
        public ICollection<MeasurementValue> GetMinMeasuremntValueCollectionByCriterion(ICriterion criterion, Expression<Func<MeasurementValue, object>> minProperty)
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT *, min(IntensityMaxDetector) FROM "MeasurementValue" WHERE X = 1 ORDER BY Analysis_ID ASC
                MeasurementValue measurementValue = null;

                return session.QueryOver<MeasurementValue>()
                              .SelectList(list => list.Select(x => x.ID).WithAlias(() => measurementValue.ID)
                                                      .Select(x => x.Frequency).WithAlias(() => measurementValue.Frequency)
                                                      .Select(x => x.IntensityMaxDetector).WithAlias(() => measurementValue.IntensityMaxDetector)
                                                      .Select(x => x.IntensityAVGDetector).WithAlias(() => measurementValue.IntensityAVGDetector)
                                                      .Select(x => x.IntensityQPDetector).WithAlias(() => measurementValue.IntensityQPDetector)
                                                      .SelectGroup(x => x.Measurement).WithAlias(() => measurementValue.Measurement)
                                                      .SelectMin(minProperty))
                              .Where(criterion)
                              .TransformUsing(Transformers.AliasToBean<MeasurementValue>())
                              .List();
            }
        }

        /// <summary>
        ///     Gets all measurement values with max property value grouped by measurement.
        /// </summary>
        /// <param name="criterion">criterion for the elements</param>
        /// <param name="maxProperty">property which should contain the max value</param>
        /// <returns></returns>
        public ICollection<MeasurementValue> GetMaxMeasuremntValueCollectionByCriterion(ICriterion criterion, Expression<Func<MeasurementValue, object>> maxProperty)
        {
            using (ISession session = this._sessionManager.OpenSession())
            {
                // example query:
                // SELECT *, min(IntensityMaxDetector) FROM "MeasurementValue" WHERE X = 1 ORDER BY Analysis_ID ASC
                MeasurementValue measurementValue = null;

                return session.QueryOver<MeasurementValue>()
                              .SelectList(list => list.Select(x => x.ID).WithAlias(() => measurementValue.ID)
                                                      .Select(x => x.Frequency).WithAlias(() => measurementValue.Frequency)
                                                      .Select(x => x.IntensityMaxDetector).WithAlias(() => measurementValue.IntensityMaxDetector)
                                                      .Select(x => x.IntensityAVGDetector).WithAlias(() => measurementValue.IntensityAVGDetector)
                                                      .Select(x => x.IntensityQPDetector).WithAlias(() => measurementValue.IntensityQPDetector)
                                                      .SelectGroup(x => x.Measurement).WithAlias(() => measurementValue.Measurement)
                                                      .SelectMax(maxProperty))
                              .Where(criterion)
                              .TransformUsing(Transformers.AliasToBean<MeasurementValue>())
                              .List();
            }
        }

        #endregion



        private static IEnumerable<string> CreateQueryList(IReadOnlyList<MeasurementValue> measurementValues)
        {
            int arrayLength = measurementValues.Count;

            // init all subqueries with the insert statement
            int numberOfQueries = arrayLength % AmountCombinedValues == 0 ? arrayLength / AmountCombinedValues : arrayLength / AmountCombinedValues + 1;
            List<string> queries = Enumerable.Repeat(GetInsertStatement(typeof(MeasurementValue)), numberOfQueries).ToList();

            // adds value statement to the subqueries (for each subquery multiple value statements combined and separated by comma)
            for (int i = 0; i < arrayLength; i++)
            {
                int listIndex = i / AmountCombinedValues;

                queries[listIndex] += GetMeasurementValueStatement(measurementValues[i]);

                // check if comma for separation needed
                if (i < arrayLength - 1 && (i + 1) % AmountCombinedValues != 0)
                {
                    queries[listIndex] += ",";
                }
            }

            return queries;
        }

        private static string GetInsertStatement(Type type)
        {
            return string.Format("INSERT INTO {0} VALUES ", type.Name);
        }

        private static string GetMeasurementValueStatement(MeasurementValue mv)
        {
            return string.Format(CultureInfo.InvariantCulture, "(null, {0}, {1}, {2}, {3}, {4})", mv.Frequency, mv.IntensityMaxDetector, mv.IntensityAVGDetector, mv.IntensityQPDetector, mv.Measurement.ID);
        }
    }
}