﻿// ///////////////////////////////////
// File: ISessionManager.cs
// Last Change: 20.01.2018  11:24
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.DataAccess
{
    using System;
    using NHibernate;



    public interface ISessionManager : IDisposable
    {
        #region Properties

        bool IsConnected { get; }

        string DatabasePath { get; }

        #endregion



        void RecreateDatabase(string databasePath);

        void LoadDatabase(string databasePath);

        ISession OpenSession();

        IStatelessSession OpenStatelessSession();

        void CloseDatabase();
    }
}