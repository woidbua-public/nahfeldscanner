﻿// ///////////////////////////////////
// File: SessionManager.cs
// Last Change: 22.01.2018  10:20
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.DataAccess
{
    using System.Reflection;
    using Nahfeldscanner.Model.Database;
    using NHibernate;



    public abstract class SessionManager : ISessionManager
    {
        #region Fields

        /// <summary>
        ///     Get the assembly informations about the models for creating or updating the database schema
        /// </summary>
        protected readonly Assembly assembly = typeof(Analysis).Assembly;

        /// <summary>
        ///     Contains the data to connect to the database.
        /// </summary>
        protected ISessionFactory _sessionFactory;

        #endregion



        #region ISessionManager Members

        /// <summary>
        ///     Free all unmanaged resources when object will be destroyed
        /// </summary>
        public void Dispose()
        {
            if (this._sessionFactory != null)
            {
                this._sessionFactory.Dispose();
            }
        }

        /// <summary>
        ///     Indicates whether it is connected to the database.
        /// </summary>
        public bool IsConnected
        {
            get
            {
                if (this._sessionFactory != null)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        ///     Saves the path to the database that was passed as a parameter.
        /// </summary>
        public string DatabasePath { get; private set; }

        /// <summary>
        ///     Recreates all tables with content in the database.
        /// </summary>
        /// <param name="databasePath">server: database name, local database: database path</param>
        public void RecreateDatabase(string databasePath)
        {
            this.CreateSchema(databasePath);
            this.DatabasePath = databasePath;
        }

        /// <summary>
        ///     Loads the database and updates the database schema if necessary.
        /// </summary>
        /// <param name="databasePath">server: database name, local database: database path</param>
        public void LoadDatabase(string databasePath)
        {
            this.LoadSchema(databasePath);
            this.DatabasePath = databasePath;
        }

        /// <summary>
        ///     Opens a session for CRUD commnands.
        ///     Note: stateless session doesn't handle any cascade operation on children
        /// </summary>
        /// <returns>session that is necessary for executing the CRUD commands</returns>
        public ISession OpenSession()
        {
            return this._sessionFactory.OpenSession();
        }

        /// <summary>
        ///     Opens a stateless session for bulk CRUD commands.
        /// </summary>
        /// <returns>stateless session that is necessary for executing the bulk CRUD commands</returns>
        public IStatelessSession OpenStatelessSession()
        {
            return this._sessionFactory.OpenStatelessSession();
        }

        /// <summary>
        ///     Closes the connection.
        /// </summary>
        public void CloseDatabase()
        {
            if (this.IsConnected)
            {
                this._sessionFactory.Close();
                this._sessionFactory.Dispose();
                this._sessionFactory = null;
                this.DatabasePath = null;
            }
        }

        #endregion



        /// <summary>
        ///     Creates the schema for the desired database.
        /// </summary>
        /// <param name="databasePath">server: database name, local database: database path</param>
        protected abstract void CreateSchema(string databasePath);

        /// <summary>
        ///     Updates the schema for the desired database.
        /// </summary>
        /// <param name="databasePath">server: database name, local database: database path</param>
        protected abstract void LoadSchema(string databasePath);
    }
}