﻿// ///////////////////////////////////
// File: SqLiteSessionManager.cs
// Last Change: 19.01.2018  14:04
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.DataAccess
{
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using NHibernate.Tool.hbm2ddl;



    public class SqLiteSessionManager : SessionManager
    {
        protected override void CreateSchema(string databasePath)
        {
            this._sessionFactory = Fluently.Configure()
                                           .Database(SQLiteConfiguration.Standard.AdoNetBatchSize(100).UsingFile(databasePath))
                                           .Mappings(m => m.FluentMappings.AddFromAssembly(this.assembly))
                                           .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, true))
                                           .BuildSessionFactory();
        }

        protected override void LoadSchema(string databasePath)
        {
            this._sessionFactory = Fluently.Configure()
                                           .Database(SQLiteConfiguration.Standard.AdoNetBatchSize(100).UsingFile(databasePath))
                                           .Mappings(m => m.FluentMappings.AddFromAssembly(this.assembly))
                                           .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                                           .BuildSessionFactory();
        }
    }
}