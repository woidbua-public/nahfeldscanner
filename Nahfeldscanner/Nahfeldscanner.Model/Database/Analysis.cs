﻿// ///////////////////////////////////
// File: Analysis.cs
// Last Change: 12.02.2018  14:14
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database
{
    using System.Collections.Generic;



    public class Analysis
    {
        #region Constructors / Destructor

        public Analysis()
        {
            this.Init(new Parameter());
        }

        public Analysis(Parameter parameter)
        {
            this.Init(parameter);
        }

        #endregion



        #region Properties

        public virtual int ID { get; set; }

        public virtual bool? FinishedAnalysis { get; set; }

        public virtual string StartTime { get; set; }

        public virtual string EndTime { get; set; }

        public virtual string Description { get; set; }

        public virtual Parameter Parameter { get; set; }

        public virtual IList<Measurement> Measurements { get; protected set; }

        #endregion



        /// <summary>
        ///     Adds the measurement to the list and sets the analysis reference
        ///     of the measurement object to itself.
        /// </summary>
        /// <param name="measurement">measurement that should be added to list</param>
        public virtual void AddMeasurement(Measurement measurement)
        {
            measurement.Analysis = this;
            this.Measurements.Add(measurement);
        }

        /// <summary>
        ///     Creates a new Analysis with the same description and reference to the same parameter.
        /// </summary>
        /// <returns>new analysis</returns>
        public virtual Analysis Clone()
        {
            return new Analysis(this.Parameter.Clone())
                   {
                           Description = this.Description
                   };
        }

        private void Init(Parameter parameter)
        {
            this.Parameter = parameter;
            this.Measurements = new List<Measurement>();
        }
    }
}