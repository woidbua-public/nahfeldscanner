﻿// ///////////////////////////////////
// File: AnalysisMap.cs
// Last Change: 19.01.2018  14:22
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database.Mapping
{
    using FluentNHibernate.Mapping;



    public class AnalysisMap : ClassMap<Analysis>
    {
        #region Constructors / Destructor

        public AnalysisMap()
        {
            this.Id(x => x.ID).Index("idx_analysis_primary_id");
            this.Map(x => x.FinishedAnalysis);
            this.Map(x => x.StartTime);
            this.Map(x => x.EndTime);
            this.Map(x => x.Description);

            // should not delete parameter when analysis will be deleted
            this.References(x => x.Parameter).Cascade.SaveUpdate().Not.LazyLoad();

            // deletes all measurement that are associated with this analysis
            this.HasMany(x => x.Measurements).Cascade.DeleteOrphan();
        }

        #endregion
    }
}