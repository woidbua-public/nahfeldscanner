﻿// ///////////////////////////////////
// File: MeasurementMap.cs
// Last Change: 19.01.2018  14:21
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database.Mapping
{
    using FluentNHibernate.Mapping;



    public class MeasurementMap : ClassMap<Measurement>
    {
        #region Constructors / Destructor

        public MeasurementMap()
        {
            this.Id(x => x.ID).Index("idx_measurement_primary_id");
            this.Map(x => x.X);
            this.Map(x => x.Y);
            this.References(x => x.Analysis).Index("idx_analysis_foreign_id");

            // deletes all measurement values that are associated with this measurement
            this.HasMany(x => x.MeasurementValues).Cascade.DeleteOrphan();
        }

        #endregion
    }
}