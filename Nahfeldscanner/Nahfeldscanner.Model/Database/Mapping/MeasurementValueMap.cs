﻿// ///////////////////////////////////
// File: MeasurementValueMap.cs
// Last Change: 19.01.2018  14:21
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database.Mapping
{
    using FluentNHibernate.Mapping;



    public class MeasurementValueMap : ClassMap<MeasurementValue>
    {
        #region Constructors / Destructor

        public MeasurementValueMap()
        {
            this.Id(x => x.ID);
            this.Map(x => x.Frequency).Index("idx_frequency");
            this.Map(x => x.IntensityMaxDetector).Index("idx_intensity_max");
            this.Map(x => x.IntensityAVGDetector).Index("idx_intensity_average");
            this.Map(x => x.IntensityQPDetector).Index("idx_intensity_quasipeak");
            this.References(x => x.Measurement).Index("idx_measurement_foreign_id");
        }

        #endregion
    }
}