﻿// ///////////////////////////////////
// File: ParameterMap.cs
// Last Change: 19.01.2018  14:21
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database.Mapping
{
    using FluentNHibernate.Mapping;



    public class ParameterMap : ClassMap<Parameter>
    {
        #region Constructors / Destructor

        public ParameterMap()
        {
            this.Id(x => x.ID).Index("idx_parameter_primary_id");
            this.Map(x => x.DoQuasipeakMeasurement);
            this.Map(x => x.OriginX);
            this.Map(x => x.OriginY);
            this.Map(x => x.XStart);
            this.Map(x => x.DeltaX);
            this.Map(x => x.XEnd);
            this.Map(x => x.YStart);
            this.Map(x => x.DeltaY);
            this.Map(x => x.YEnd);
            this.Map(x => x.FreqStart);
            this.Map(x => x.DeltaFreq);
            this.Map(x => x.FreqEnd);
            this.Map(x => x.ResBW);
            this.Map(x => x.MeasureTime);
            this.Map(x => x.Orientation);
            this.Map(x => x.SensorType);
            this.Map(x => x.SensorImageStream);
            this.Map(x => x.SensorName);
            this.HasMany(x => x.Analyses);
        }

        #endregion
    }
}