﻿// ///////////////////////////////////
// File: SensorMap.cs
// Last Change: 19.01.2018  14:21
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database.Mapping
{
    using FluentNHibernate.Mapping;



    public class SensorMap : ClassMap<Sensor>
    {
        #region Constructors / Destructor

        public SensorMap()
        {
            this.Id(x => x.ID);
            this.Map(x => x.SensorImageStream);
            this.Map(x => x.SensorType);
            this.Map(x => x.SensorName);
            this.Map(x => x.MinFrequency);
            this.Map(x => x.MaxFrequency);
        }

        #endregion
    }
}