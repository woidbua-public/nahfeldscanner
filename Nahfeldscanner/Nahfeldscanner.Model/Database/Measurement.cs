﻿// ///////////////////////////////////
// File: Measurement.cs
// Last Change: 22.01.2018  10:52
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database
{
    using System.Collections.Generic;



    public class Measurement
    {
        #region Constructors / Destructor

        public Measurement()
        {
            this.MeasurementValues = new List<MeasurementValue>();
        }

        #endregion



        #region Properties

        public virtual int ID { get; set; }

        public virtual double X { get; set; }

        public virtual double Y { get; set; }

        public virtual Analysis Analysis { get; set; }

        public virtual IList<MeasurementValue> MeasurementValues { get; set; }

        #endregion



        /// <summary>
        ///     Adds the measurement value to the list and sets the measurement reference
        ///     of the measurement value object to itself.
        /// </summary>
        /// <param name="measurementValue">measurement value that should be added to list</param>
        public virtual void AddMeasurementValue(MeasurementValue measurementValue)
        {
            measurementValue.Measurement = this;
            this.MeasurementValues.Add(measurementValue);
        }
    }
}