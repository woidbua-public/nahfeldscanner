﻿// ///////////////////////////////////
// File: MeasurementValue.cs
// Last Change: 22.01.2018  14:13
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database
{
    public class MeasurementValue
    {
        #region Properties

        public virtual int ID { get; set; }

        public virtual double Frequency { get; set; }

        public virtual double IntensityMaxDetector { get; set; }

        public virtual double IntensityAVGDetector { get; set; }

        public virtual double IntensityQPDetector { get; set; }

        public virtual Measurement Measurement { get; set; }

        #endregion
    }
}