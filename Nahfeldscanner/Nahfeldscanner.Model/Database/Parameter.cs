﻿// ///////////////////////////////////
// File: Parameter.cs
// Last Change: 01.02.2018  14:18
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.Model.Properties;



    public class Parameter
    {
        #region Constructors / Destructor

        /// <summary>
        ///     Creates a new Parameter and sets the property values to the saved default values.
        /// </summary>
        public Parameter()
        {
            this.Analyses = new List<Analysis>();

            this.DoQuasipeakMeasurement = Settings.Default.DoQuasipeakMeasurement;
            this.OriginX = Settings.Default.OriginX;
            this.OriginY = Settings.Default.OriginY;
            this.XStart = Settings.Default.XStart;
            this.DeltaX = Settings.Default.DeltaX;
            this.XEnd = Settings.Default.XEnd;
            this.YStart = Settings.Default.YStart;
            this.DeltaY = Settings.Default.DeltaY;
            this.YEnd = Settings.Default.YEnd;
            this.FreqStart = Settings.Default.FreqStart;
            this.ResBW = (ResBW)Settings.Default.ResBWIndex;
            this.DeltaFreq = (double)this.ResBW / 4; // is always a quarter of the resolution bandwidth
            this.FreqEnd = Settings.Default.FreqEnd;
            this.MeasureTime = Settings.Default.MeasureTime;
            this.Orientation = (Orientation)Settings.Default.OrientationIndex;
            this.SensorType = (SensorType)Settings.Default.SensorTypeIndex;
            this.SensorName = Settings.Default.SensorName;

            // tries to convert to saved sensor image stream. If not possible, set null
            try
            {
                this.SensorImageStream = Convert.FromBase64String(Settings.Default.SensorImageStream);
            }
            catch
            {
                this.SensorImageStream = null;
            }
        }

        #endregion



        #region Properties

        public virtual int ID { get; set; }

        public virtual bool DoQuasipeakMeasurement { get; set; }

        public virtual double? OriginX { get; set; }

        public virtual double? OriginY { get; set; }

        public virtual double? XStart { get; set; }

        public virtual double? DeltaX { get; set; }

        public virtual double? XEnd { get; set; }

        public virtual double? YStart { get; set; }

        public virtual double? DeltaY { get; set; }

        public virtual double? YEnd { get; set; }

        public virtual double? FreqStart { get; set; }

        public virtual double? DeltaFreq { get; set; }

        public virtual double? FreqEnd { get; set; }

        public virtual ResBW? ResBW { get; set; }

        public virtual int? MeasureTime { get; set; }

        public virtual Orientation? Orientation { get; set; }

        public virtual SensorType? SensorType { get; set; }

        public virtual byte[] SensorImageStream { get; set; }

        public virtual string SensorName { get; set; }

        public virtual IList<Analysis> Analyses { get; set; }

        #endregion



        /// <summary>
        ///     Adds the analysis to the list and sets the parameter reference
        ///     of the analysis object to itself.
        /// </summary>
        /// <param name="analysis">analysis that should be added to list</param>
        public virtual void AddAnalysis(Analysis analysis)
        {
            analysis.Parameter = this;
            this.Analyses.Add(analysis);
        }

        /// <summary>
        ///     Clears all property values.
        ///     The reference to the analyses stay preserved.
        /// </summary>
        public virtual void Clear()
        {
            this.DoQuasipeakMeasurement = false;
            this.OriginX = null;
            this.OriginY = null;
            this.XStart = null;
            this.DeltaX = null;
            this.XEnd = null;
            this.YStart = null;
            this.DeltaY = null;
            this.YEnd = null;
            this.FreqStart = null;
            this.DeltaFreq = null;
            this.FreqEnd = null;
            this.ResBW = null;
            this.MeasureTime = null;
            this.Orientation = null;
            this.SensorType = null;
            this.SensorImageStream = null;
            this.SensorName = null;
        }

        /// <summary>
        ///     Indicates whether all parameter values are set
        /// </summary>
        /// <returns>true: all properties set, false: some properties not set</returns>
        public virtual bool ValuesSet()
        {
            bool isValid = true;

            foreach (PropertyInfo propertyInfo in this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.DeclaringType == this.GetType()))
            {
                // Sensor Image Stream can be empty (when no picture was added to the selected sensor)
                if ((propertyInfo.GetValue(this) == null && propertyInfo.Name != "SensorImageStream") || propertyInfo.GetValue(this) is string && string.IsNullOrEmpty((string)propertyInfo.GetValue(this)))
                {
                    isValid = false;
                }
            }

            return isValid;
        }

        /// <summary>
        ///     Copies all parameter properties to the new parameter.
        ///     Does not include the analyses references.
        /// </summary>
        /// <returns>new parameter with copied property values</returns>
        public virtual Parameter Clone()
        {
            return new Parameter
                   {
                           DoQuasipeakMeasurement = this.DoQuasipeakMeasurement,
                           OriginX = this.OriginX,
                           OriginY = this.OriginY,
                           XStart = this.XStart,
                           DeltaX = this.DeltaX,
                           XEnd = this.XEnd,
                           YStart = this.YStart,
                           DeltaY = this.DeltaY,
                           YEnd = this.YEnd,
                           FreqStart = this.FreqStart,
                           DeltaFreq = this.DeltaFreq,
                           FreqEnd = this.FreqEnd,
                           ResBW = this.ResBW,
                           MeasureTime = this.MeasureTime,
                           Orientation = this.Orientation,
                           SensorType = this.SensorType,
                           SensorImageStream = this.SensorImageStream,
                           SensorName = this.SensorName
                   };
        }

        /// <summary>
        ///     Copies all position parameter properties to the new parameter.
        ///     Does not include the analyses references.
        /// </summary>
        /// <returns>new parameter with copied property values</returns>
        public virtual Parameter ClonePositions()
        {
            return new Parameter
                   {
                           OriginX = this.OriginX,
                           OriginY = this.OriginY,
                           XStart = this.XStart,
                           DeltaX = this.DeltaX,
                           XEnd = this.XEnd,
                           YStart = this.YStart,
                           DeltaY = this.DeltaY,
                           YEnd = this.YEnd
                   };
        }
    }
}