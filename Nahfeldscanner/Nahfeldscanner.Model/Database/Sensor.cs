﻿// ///////////////////////////////////
// File: Sensor.cs
// Last Change: 19.02.2018  11:40
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Database
{
    using Nahfeldscanner.Model.Enum;



    public class Sensor
    {
        #region Constructors / Destructor

        public Sensor()
        {
            this.SensorType = Enum.SensorType.E_Field;
        }

        public Sensor(string sensorName, SensorType? sensorType, byte[] sensorImageStream, double? minFrequency = null, double? maxFrequency = null)
        {
            this.SensorName = sensorName;
            this.SensorType = sensorType;
            this.SensorImageStream = sensorImageStream;
            this.MinFrequency = minFrequency;
            this.MaxFrequency = maxFrequency;
        }

        #endregion



        #region Properties

        public virtual int ID { get; set; }

        public virtual byte[] SensorImageStream { get; set; }

        public virtual SensorType? SensorType { get; set; }

        public virtual string SensorName { get; set; }

        public virtual double? MinFrequency { get; set; }

        public virtual double? MaxFrequency { get; set; }

        #endregion
    }
}