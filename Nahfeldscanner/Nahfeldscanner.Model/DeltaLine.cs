﻿// ///////////////////////////////////
// File: DeltaLine.cs
// Last Change: 19.01.2018  14:23
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model
{
    public class DeltaLine
    {
        #region Constructors / Destructor

        public DeltaLine(double xLength, double yLength, double xStart, double yStart)
        {
            this.XLength = xLength;
            this.YLength = yLength;
            this.XStart = xStart;
            this.YStart = yStart;
        }

        #endregion



        #region Properties

        public double XLength { get; set; }

        public double YLength { get; set; }

        public double XStart { get; set; }

        public double YStart { get; set; }

        #endregion
    }
}