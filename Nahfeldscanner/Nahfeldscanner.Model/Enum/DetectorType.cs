﻿// ///////////////////////////////////
// File: DetectorType.cs
// Last Change: 30.10.2017  11:51
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Enum
{
    using System.ComponentModel;
    using Nahfeldscanner.Model.Converter;



    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum DetectorType
    {
        [Description("Max Peak")]
        Max_Peak,

        [Description("Average")]
        Average,

        [Description("Quasipeak")]
        Quasipeak
    }
}