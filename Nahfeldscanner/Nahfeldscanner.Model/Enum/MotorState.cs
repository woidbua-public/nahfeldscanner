﻿// ///////////////////////////////////
// File: MotorState.cs
// Last Change: 30.10.2017  11:51
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Enum
{
    using System.ComponentModel;
    using Nahfeldscanner.Model.Converter;



    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum MotorState
    {
        [Description("Arduino Disconnected")]
        Disconnected,

        [Description("Not Initialized")]
        Alarm,

        [Description("Is Running")]
        Home,

        [Description("Ready")]
        Idle,

        [Description("Is Running")]
        Run
    }
}