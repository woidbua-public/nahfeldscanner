﻿// ///////////////////////////////////
// File: Orientation.cs
// Last Change: 11.12.2017  13:30
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Enum
{
    using System.ComponentModel;
    using Nahfeldscanner.Model.Converter;



    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum Orientation
    {
        [Description("X")]
        X,

        [Description("Y")]
        Y
    }
}