﻿// ///////////////////////////////////
// File: ResBW.cs
// Last Change: 30.10.2017  11:51
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Enum
{
    using System.ComponentModel;
    using Nahfeldscanner.Model.Converter;



    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum ResBW
    {
        [Description("10 Hz")]
        BW_10_Hz = 10,

        [Description("100 Hz")]
        BW_100_Hz = 100,

        [Description("200 Hz")]
        BW_200_Hz = 200,

        [Description("1 kHz")]
        BW_1_kHz = 1000,

        [Description("9 kHz")]
        BW_9_kHz = 9000,

        [Description("10 kHz")]
        BW_10_kHz = 10000,

        [Description("100 kHz")]
        BW_100_kHz = 100000,

        [Description("120 kHz")]
        BW_120_kHz = 120000
    }
}