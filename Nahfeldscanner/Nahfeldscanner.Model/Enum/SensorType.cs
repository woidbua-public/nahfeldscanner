﻿// ///////////////////////////////////
// File: SensorType.cs
// Last Change: 15.12.2017  08:58
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model.Enum
{
    using System.ComponentModel;
    using Nahfeldscanner.Model.Converter;



    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum SensorType
    {
        [Description("E-Field")]
        E_Field,

        [Description("H-Field")]
        H_Field
    }
}