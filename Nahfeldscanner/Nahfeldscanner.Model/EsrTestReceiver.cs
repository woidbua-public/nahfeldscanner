﻿// ///////////////////////////////////
// File: EsrTestReceiver.cs
// Last Change: 04.02.2018  09:40
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Model
{
    using System;
    using Ivi.Driver;
    using Nahfeldscanner.Model.Properties;
    using RohdeSchwarz.RsEmi;
    using AmplitudeUnits = Ivi.SpecAn.AmplitudeUnits;



    public class EsrTestReceiver : IDisposable
    {
        #region Fields

        private const string defaultWindow = "Win0";
        private const string defaultRange = "R1";
        private const string defaultMarker = "M1";
        private const string traceMax = "TR1";
        private const string traceAVG = "TR2";
        private const string traceQuasipeak = "TR4";
        private const TraceResultType bargraphMax = TraceResultType.Trace1;
        private const TraceResultType bargraphAVG = TraceResultType.Trace2;
        private const TraceResultType bargraphQuasipeak = TraceResultType.Trace4;

        private readonly string connectionString = "TCPIP::" + Settings.Default.TestReceiverIP + "::INSTR";

        private RsEmi driver;
        private IRsEmiTestReceiverMeasurement testReceiver;
        private IRsEmiTestReceiverMeasurementFrequency testReceiverFrequency;
        private IRsEmiTestReceiverMeasurementFrequencyScanRange testReceiverFrequencyRange;
        private IRsEmiTestReceiverMeasurementLowLevel testReceiverMeasurementLowLevel;
        private IRsEmiTestReceiverMeasurementMarker testReceiverMarker;

        #endregion



        #region Properties

        /// <summary>
        ///     Indicates whether the driver is connected to the test receiver.
        /// </summary>
        public bool IsConnected
        {
            get
            {
                if (this.driver == null)
                {
                    return false;
                }

                return true;
            }
        }

        #endregion



        #region IDisposable Members

        public void Dispose()
        {
            if (this.driver != null)
            {
                this.driver.Dispose();
            }
        }

        #endregion



        /// <summary>
        ///     Connects to the test receiver, enables the display and sets the
        ///     frequency mode to time domain.
        /// </summary>
        public void Connect()
        {
            // Connection string, IDQuery, Reset
            this.driver = new RsEmi(this.connectionString, true, true);

            this.testReceiver = this.driver.TestReceiverMeasurement[defaultWindow];
            this.testReceiverFrequency = this.testReceiver.Configuration.Frequency;
            this.testReceiverFrequencyRange = this.testReceiver.Configuration.FrequencyScan.Range[defaultRange];
            this.testReceiverMeasurementLowLevel = this.testReceiver.Measurement.LowLevel;
            this.testReceiverMarker = this.testReceiver.Configuration.Marker[defaultMarker];

            this.WriteToInstrument("SYST:DISP:UPD ON");
            this.testReceiver.ReceiverMode();
            this.testReceiverFrequency.FrequencyMode = FrequencyMode.TimeDomain;
        }

        /// <summary>
        ///     Resets the test receiver.
        /// </summary>
        public void Reset()
        {
            this.driver.Utility.Reset();
        }

        /// <summary>
        ///     Close the connection to the test receiver.
        /// </summary>
        public void Close()
        {
            try
            {
                this.driver.Close();
            }
            catch
            {
                // do nothing
            }
            finally
            {
                this.driver.Dispose();
                this.driver = null;
            }
        }

        public void SetStartStopFrequency(double startFrequency, double stopFrequency)
        {
            this.driver.Frequency.ConfigureStartStop(startFrequency, stopFrequency);
            this.testReceiverFrequencyRange.Start = startFrequency;
            this.testReceiverFrequencyRange.Stop = stopFrequency;
        }

        public void SetResolutionBandwidth(double resolutionBW)
        {
            this.testReceiverFrequencyRange.Bandwidth = resolutionBW;
        }

        /// <summary>
        ///     Deactivates the automatic bandwidth resolution.
        /// </summary>
        public void DeactivateCisprRbwCoupling()
        {
            this.testReceiver.Configuration.Selection.ResolutionBandwidthAuto = false;
        }

        public void SetSweepTime(int milliseconds)
        {
            this.driver.SweepCoupling.SweepTime = PrecisionTimeSpan.FromMilliseconds(milliseconds);
        }

        public void SetDetectorModes()
        {
            BargraphDetector[] bargraphDetectors =
            {
                    BargraphDetector.MaxPeak,
                    BargraphDetector.Average,
                    BargraphDetector.QuasiPeak
            };

            this.testReceiver.Configuration.Display.ConfigureBargraphDetectorSelection(bargraphDetectors);
            this.testReceiver.Configuration.Display.Trace[traceMax].BargraphTraceMode = BargraphTraceMode.Write;
            this.testReceiver.Configuration.Display.Trace[traceAVG].BargraphTraceMode = BargraphTraceMode.Write;
            this.testReceiver.Configuration.Display.Trace[traceQuasipeak].BargraphTraceMode = BargraphTraceMode.Write;
            this.WriteToInstrument("INIT:CONT OFF");
        }

        public void SetDetectorModesWithoutQuasipeak()
        {
            BargraphDetector[] bargraphDetectors =
            {
                    BargraphDetector.MaxPeak,
                    BargraphDetector.Average
            };

            this.testReceiver.Configuration.Display.ConfigureBargraphDetectorSelection(bargraphDetectors);
            this.testReceiver.Configuration.Display.Trace[traceMax].BargraphTraceMode = BargraphTraceMode.Write;
            this.testReceiver.Configuration.Display.Trace[traceAVG].BargraphTraceMode = BargraphTraceMode.Write;
            this.WriteToInstrument("INIT:CONT OFF");
        }

        public void SetDCCoupling()
        {
            this.testReceiver.Configuration.Amplitude.RFInput.Coupling = RFInputCoupling.DC;
        }

        /// <summary>
        ///     Sends the command to the test receiver.
        /// </summary>
        /// <param name="command">the command that should be executed</param>
        public void WriteToInstrument(string command)
        {
            this.driver.UtilityFunctions.WriteToInstrument(command);
        }

        /// <summary>
        ///     Executes one spectrum scan.
        /// </summary>
        public void StartSingleRun()
        {
            // Deactivates continuous scan
            this.WriteToInstrument("INIT2:CONT OFF");

            // Starts measurement and waits until it's finished
            this.WriteToInstrument("INIT2;*WAI");
        }

        /// <summary>
        ///     Searches the max intensity in the executed scan and saves the
        ///     found intensity with its frequency into the parameter.
        /// </summary>
        /// <param name="frequency">receives the frequency value where the max intensity occurs</param>
        /// <param name="intensity">receives the max intensity value</param>
        public void GetMaxPeak(out double frequency, out double intensity)
        {
            this.testReceiverMarker.Search.Peak();
            frequency = this.testReceiverMarker.Position;
            intensity = this.testReceiverMarker.Amplitude;
        }

        /// <summary>
        ///     Returns the used amplitude unit.
        /// </summary>
        /// <returns>amplitude unit</returns>
        public AmplitudeUnits GetAmplitudeUnit()
        {
            return this.driver.Level.AmplitudeUnits;
        }

        /// <summary>
        ///     Returns the used resolution bandwidth.
        /// </summary>
        /// <returns>bandwidth</returns>
        public double GetResolutionBandwidth()
        {
            return this.testReceiverFrequencyRange.Bandwidth;
        }

        /// <summary>
        ///     Sets the amplitude unit.
        /// </summary>
        /// <param name="amplitudeUnit">desired amplitude unit</param>
        public void SetAmplitudeUnit(AmplitudeUnits amplitudeUnit)
        {
            this.driver.Level.AmplitudeUnits = amplitudeUnit;
        }

        /// <summary>
        ///     Returns the delta frequency.
        /// </summary>
        /// <returns>delta frequency</returns>
        public double GetStepSize()
        {
            return this.testReceiverFrequencyRange.StepSize;
        }

        /// <summary>
        ///     Returns the measured max detector intensities.
        /// </summary>
        /// <returns>max detector intensities</returns>
        public double[] GetMaxMeasurementValues()
        {
            return this.testReceiverMeasurementLowLevel.FetchReceiverTraceData(bargraphMax);
        }

        /// <summary>
        ///     Returns the measured avg detector intensities.
        /// </summary>
        /// <returns>avg detector intensities</returns>
        public double[] GetAVGMeasurementValues()
        {
            return this.testReceiverMeasurementLowLevel.FetchReceiverTraceData(bargraphAVG);
        }

        /// <summary>
        ///     Returns the measured quasipeak detector intensities.
        /// </summary>
        /// <returns>quasipeak detector intensities</returns>
        public double[] GetQuasipeakMeasurementValues()
        {
            return this.testReceiverMeasurementLowLevel.FetchReceiverTraceData(bargraphQuasipeak);
        }
    }
}