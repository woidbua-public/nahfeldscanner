﻿// ///////////////////////////////////
// File: SqLiteRepositoryTest.cs
// Last Change: 01.02.2018  14:20
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.Business
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using FluentAssertions;
    using log4net.Config;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.Model.Database;
    using NHibernate;
    using NHibernate.Criterion;



    [TestClass]
    public class SqLiteRepositoryTest
    {
        #region Fields

        private const int AmountAnalysis = 2;
        private const int AmountMeasurement = 100;
        private const int AmountMeasurementValue = 1000;

        private const int AmountBulkSaveOrUpdate = 100;
        private const int AmountBulkInsert = 1000;
        private const int AmountSensor = 10;

        private const int XMax = 25;

        private SqLiteRepository _sqLiteRespository;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            XmlConfigurator.Configure();
            this._sqLiteRespository = new SqLiteRepository(new SqLiteSessionManager());

            DatabaseFactory.InitDatabase(this._sqLiteRespository,
                                         AmountAnalysis, AmountMeasurement, AmountMeasurementValue, AmountSensor, XMax);
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this._sqLiteRespository.Dispose();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void NotConnectedAtStartup()
        {
            // Arrange
            this._sqLiteRespository = new SqLiteRepository(new SqLiteSessionManager());

            // Assert
            this._sqLiteRespository.IsConnected.Should().BeFalse();
        }

        [TestMethod]
        public void IsConnectedAfterDatabaseCreation()
        {
            // Act
            this.RecreateDatabase();

            // Assert
            this._sqLiteRespository.IsConnected.Should().BeTrue();
        }

        [TestMethod]
        public void SetsDatabasePathAfterDatabaseCreation()
        {
            // Act
            this.RecreateDatabase();

            // Assert
            this._sqLiteRespository.DatabasePath.Should().Be(DatabaseFactory.SqLiteFilePath);
        }

        [TestMethod]
        public void IsConnectedAfterDatabaseLoaded()
        {
            // Act
            this.LoadDatabase();

            // Assert
            this._sqLiteRespository.IsConnected.Should().BeTrue();
        }

        [TestMethod]
        public void SetsDatabasePathAfterDatabaseLoaded()
        {
            // Act
            this.LoadDatabase();

            // Assert
            this._sqLiteRespository.DatabasePath.Should().Be(DatabaseFactory.SqLiteFilePath);
        }

        [TestMethod]
        public void NotConnectedAfterDatabaseClosed()
        {
            // Act
            this._sqLiteRespository.CloseDatabase();

            // Assert
            this._sqLiteRespository.IsConnected.Should().BeFalse();
        }

        [TestMethod]
        public void EmptyFilePathAfterDatabaseClosed()
        {
            // Act
            this._sqLiteRespository.CloseDatabase();

            // Assert
            this._sqLiteRespository.DatabasePath.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void SavesParameterInDatabase()
        {
            // Act
            Action sqLiteAction = () => this._sqLiteRespository.SaveOrUpdate(new Parameter());

            // Assert
            sqLiteAction.ShouldNotThrow<Exception>();
        }

        [TestMethod]
        public void ThrowsExceptionWhenParameterCouldNotBeSavedOrUpdated()
        {
            // Act
            Action sqLiteAction = () => this._sqLiteRespository.SaveOrUpdate((Parameter)null);

            // Assert
            sqLiteAction.ShouldThrow<Exception>();
        }

        [TestMethod]
        public void MeasureTimeByInsertingMultipleParametersViaSaveOrUpdate()
        {
            // Arrange
            Parameter[] parameters = new Parameter[AmountBulkSaveOrUpdate];

            for (int i = 0; i < AmountBulkSaveOrUpdate; i++)
            {
                parameters[i] = new Parameter();
            }

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < AmountBulkSaveOrUpdate; i++)
            {
                this._sqLiteRespository.SaveOrUpdate(parameters[i]);
            }

            stopwatch.Stop();

            // Assert
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void MeasureTimeByInsertingMultipleMeasurementValuesViaInsertArray()
        {
            // Arrange
            MeasurementValue[] measurementValues = new MeasurementValue[AmountBulkInsert];

            for (int i = 0; i < AmountBulkInsert; i++)
            {
                measurementValues[i] = new MeasurementValue();
            }

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            this._sqLiteRespository.InsertArray(measurementValues);

            stopwatch.Stop();

            // Assert
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void MeasureTimeByInsertingMultipleMeasurmentValuesViaBulkInsert()
        {
            // Arrange
            MeasurementValue[] measurementValues = new MeasurementValue[AmountBulkInsert];

            for (int i = 0; i < AmountBulkInsert; i++)
            {
                measurementValues[i] = new MeasurementValue { Measurement = new Measurement { ID = 1 } };
            }

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            this._sqLiteRespository.BulkInsert(DatabaseFactory.SqLiteFilePath, measurementValues);

            stopwatch.Stop();

            // Assert
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void ThrowExceptionWhenParameterArrayCouldNotBeInsertedToDatabase()
        {
            // Arrange
            Parameter[] parameters = { null, new Parameter() };

            // Act
            Action sqLiteAction = () => this._sqLiteRespository.InsertArray(parameters);

            // Assert
            sqLiteAction.ShouldThrow<Exception>();
        }

        [TestMethod]
        public void MeasureTimeForGettingNumberOfRows()
        {
            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            long amount = this._sqLiteRespository.GetAmount<MeasurementValue>();

            stopwatch.Stop();

            // Assert
            amount.Should().Be(AmountAnalysis * AmountMeasurement * AmountMeasurementValue);
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void MeasureTimeForGettingAllAnalyses()
        {
            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            ICollection<Analysis> analyses = this._sqLiteRespository.GetAll<Analysis>();

            stopwatch.Stop();

            // Assert
            analyses.Count.Should().Be(AmountAnalysis);
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void DeletesParameterFromDatabase()
        {
            // Arrange
            Parameter sqLiteParameter = new Parameter();

            this._sqLiteRespository.SaveOrUpdate(sqLiteParameter);

            // Act
            this._sqLiteRespository.Delete(sqLiteParameter);

            // Assert
            this._sqLiteRespository.GetById<Parameter>(sqLiteParameter.ID).Should().BeNull();
        }

        [TestMethod]
        public void ThrowsExceptionWhenParameterCouldNotBeDeletedFromDatabase()
        {
            // Arrange
            Parameter sqLiteParameter = new Parameter();

            // Act
            Action sqLiteAction = () => this._sqLiteRespository.Delete(sqLiteParameter);

            // Assert
            sqLiteAction.ShouldThrow<Exception>();
        }

        [TestMethod]
        public void DeletesAnalysisAndMeasurementReferences()
        {
            // Arrange
            Analysis analysis = ModelFactory.GetDefaultAnalysis();
            analysis.Parameter = ModelFactory.GetDefaultParameter();
            Measurement measurement1 = ModelFactory.GetDefaultMeasurement();
            Measurement measurement2 = ModelFactory.GetDefaultMeasurement();
            MeasurementValue measurementValue1 = ModelFactory.GetDefaultMeasurementValue();
            MeasurementValue measurementValue2 = ModelFactory.GetDefaultMeasurementValue();
            MeasurementValue measurementValue3 = ModelFactory.GetDefaultMeasurementValue();
            MeasurementValue measurementValue4 = ModelFactory.GetDefaultMeasurementValue();

            this._sqLiteRespository.SaveOrUpdate(analysis);
            analysis.AddMeasurement(measurement1);
            analysis.AddMeasurement(measurement2);
            this._sqLiteRespository.SaveOrUpdate(measurement1);
            this._sqLiteRespository.SaveOrUpdate(measurement2);
            measurement1.AddMeasurementValue(measurementValue1);
            measurement1.AddMeasurementValue(measurementValue2);
            measurement2.AddMeasurementValue(measurementValue3);
            measurement2.AddMeasurementValue(measurementValue4);
            this._sqLiteRespository.SaveOrUpdate(measurementValue1);
            this._sqLiteRespository.SaveOrUpdate(measurementValue2);
            this._sqLiteRespository.SaveOrUpdate(measurementValue3);
            this._sqLiteRespository.SaveOrUpdate(measurementValue4);

            // Act
            this._sqLiteRespository.Delete(analysis);

            // Assert
            this._sqLiteRespository.GetById<Analysis>(analysis.ID).Should().BeNull();
            this._sqLiteRespository.GetById<Parameter>(analysis.Parameter.ID).Should().NotBeNull();
            this._sqLiteRespository.GetById<Measurement>(measurement1.ID).Should().BeNull();
            this._sqLiteRespository.GetById<Measurement>(measurement2.ID).Should().BeNull();
            this._sqLiteRespository.GetById<MeasurementValue>(measurementValue1.ID).Should().BeNull();
            this._sqLiteRespository.GetById<MeasurementValue>(measurementValue2.ID).Should().BeNull();
            this._sqLiteRespository.GetById<MeasurementValue>(measurementValue3.ID).Should().BeNull();
            this._sqLiteRespository.GetById<MeasurementValue>(measurementValue4.ID).Should().BeNull();
        }

        [TestMethod]
        public void DeletesMeasurementAndMeasurementValueReferences()
        {
            // Arrange
            Measurement measurement = ModelFactory.GetDefaultMeasurement();
            MeasurementValue measurementValue1 = ModelFactory.GetDefaultMeasurementValue();
            MeasurementValue measurementValue2 = ModelFactory.GetDefaultMeasurementValue();

            this._sqLiteRespository.SaveOrUpdate(measurement);
            measurement.AddMeasurementValue(measurementValue1);
            measurement.AddMeasurementValue(measurementValue2);
            this._sqLiteRespository.SaveOrUpdate(measurementValue1);
            this._sqLiteRespository.SaveOrUpdate(measurementValue2);

            // Act
            this._sqLiteRespository.Delete(measurement);

            // Assert
            this._sqLiteRespository.GetById<Measurement>(measurement.ID).Should().BeNull();
            this._sqLiteRespository.GetById<MeasurementValue>(measurementValue1.ID).Should().BeNull();
            this._sqLiteRespository.GetById<MeasurementValue>(measurementValue2.ID).Should().BeNull();
        }

        [TestMethod]
        public void GetsParameterWithSpecificId()
        {
            // Act
            Parameter sqLiteParameter = this._sqLiteRespository.GetById<Parameter>(1);

            // Assert
            sqLiteParameter.Should().NotBeNull();

            sqLiteParameter.ID.Should().Be(1);
        }

        [TestMethod]
        public void MeasureTimeForGettingMeasurementValueWithSpecificId()
        {
            // Arrange
            const int MeasurementValueId = AmountMeasurementValue / 4 * 3;

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            MeasurementValue measurementValue = this._sqLiteRespository.GetById<MeasurementValue>(MeasurementValueId);

            stopwatch.Stop();

            // Assert
            measurementValue.Should().NotBeNull();
            measurementValue.ID.Should().Be(MeasurementValueId);
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void GetLazyLoadedAnalysisById()
        {
            // Act
            Analysis analysis = this._sqLiteRespository.GetById<Analysis>(1);

            // Assert
            analysis.Should().NotBeNull();
            NHibernateUtil.IsInitialized(analysis.Parameter).Should().BeTrue();
            NHibernateUtil.IsInitialized(analysis.Measurements).Should().BeFalse();
        }

        [TestMethod]
        public void GetLazyLoadedMeasurementById()
        {
            // Act
            Measurement measurement = this._sqLiteRespository.GetById<Measurement>(1);

            // Assert
            measurement.Should().NotBeNull();
            NHibernateUtil.IsInitialized(measurement.Analysis).Should().BeFalse();
            NHibernateUtil.IsInitialized(measurement.MeasurementValues).Should().BeFalse();
        }

        [TestMethod]
        public void GetEagerLoadedAnalysisById()
        {
            // Act
            Analysis analysis = this._sqLiteRespository.GetEagerById<Analysis>(1, x => x.Measurements);

            // Assert
            analysis.Should().NotBeNull();
            NHibernateUtil.IsInitialized(analysis.Parameter).Should().BeTrue();
            NHibernateUtil.IsInitialized(analysis.Measurements).Should().BeTrue();
            analysis.Measurements.Count.Should().Be(AmountMeasurement);
        }

        [TestMethod]
        public void GetEagerLoadedMeasurementById()
        {
            // Act
            Measurement measurement = this._sqLiteRespository.GetEagerById<Measurement>(1, x => x.Analysis, x => x.MeasurementValues);

            // Assert
            measurement.Should().NotBeNull();
            NHibernateUtil.IsInitialized(measurement.Analysis).Should().BeTrue();
            NHibernateUtil.IsInitialized(measurement.MeasurementValues).Should().BeTrue();
            measurement.MeasurementValues.Count.Should().Be(AmountMeasurementValue);
        }

        [TestMethod]
        public void MeasureTimeByGettingMeasurementValueByCriterion()
        {
            // Arrange
            const int Frequency = AmountMeasurementValue / 4 * 3;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            ICriterion criterion = Restrictions.Where<MeasurementValue>(x => x.Frequency == Frequency);

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            MeasurementValue measurementValue = this._sqLiteRespository.GetByCriterion<MeasurementValue>(criterion);
            stopwatch.Stop();

            // Assert
            measurementValue.Frequency.Should().Be(Frequency);
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void MeasureTimeByGettingMeasurementValuesByCriterion()
        {
            // Arrange
            const int Frequency = AmountMeasurementValue / 4 * 3;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            ICriterion criterion = Restrictions.Where<MeasurementValue>(x => x.Frequency == Frequency);

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            ICollection<MeasurementValue> measurementValues = this._sqLiteRespository.GetCollectionByCriterion<MeasurementValue>(criterion, x => x.Measurement.ID);
            stopwatch.Stop();

            // Assert
            measurementValues.Count.Should().Be(AmountAnalysis * AmountMeasurement);
            measurementValues.ElementAt(0).Frequency.Should().Be(Frequency);
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void MeasureTimeByGettingMeasurementValueByCombinedCriterion()
        {
            // Arrange
            const int Frequency = AmountMeasurementValue / 4 * 3;
            const int AnalysisId = 2;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            ICriterion measurementValueCriterion = Restrictions.Where<MeasurementValue>(x => x.Frequency == Frequency);
            ICriterion measurementCriterion = Restrictions.Where<Measurement>(x => x.Analysis.ID == AnalysisId);

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            ICollection<MeasurementValue> measurementValues = this._sqLiteRespository.GetCollectionByCriterion<MeasurementValue, Measurement>(measurementValueCriterion, x => x.Measurement, measurementCriterion, x => x.Measurement.ID);
            stopwatch.Stop();

            // Assert
            measurementValues.Count.Should().Be(AmountMeasurement);
            measurementValues.ElementAt(0).Frequency.Should().Be(Frequency);
            measurementValues.ElementAt(0).Measurement.Analysis.ID.Should().Be(AnalysisId);
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void MeasureTimeByGettingMinMeasurementValueCollectionByCriterion()
        {
            // Arrange
            ICriterion measurementValueCriterion = Restrictions.And(Restrictions.Le(Projections.Property<MeasurementValue>(x => x.Measurement.ID), AmountMeasurement),
                                                                    Restrictions.Ge(Projections.Property<MeasurementValue>(x => x.Measurement.ID), 1));

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            ICollection<MeasurementValue> measurementValues = this._sqLiteRespository.GetMinMeasuremntValueCollectionByCriterion(measurementValueCriterion, x => x.IntensityMaxDetector);
            stopwatch.Stop();

            // Assert
            measurementValues.Count.Should().Be(AmountMeasurement);

            for (int i = 0; i < AmountMeasurement; i++)
            {
                measurementValues.ElementAt(i).Measurement.ID.Should().Be(i + 1);
            }

            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        [TestMethod]
        public void MeasureTimeByGettingMaxMeasurementValueCollectionByCriterion()
        {
            // Arrange
            ICriterion measurementValueCriterion = Restrictions.And(Restrictions.Le(Projections.Property<MeasurementValue>(x => x.Measurement.ID), 20),
                                                                    Restrictions.Ge(Projections.Property<MeasurementValue>(x => x.Measurement.ID), 1));

            // Act
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            ICollection<MeasurementValue> measurementValues = this._sqLiteRespository.GetMaxMeasuremntValueCollectionByCriterion(measurementValueCriterion, x => x.IntensityMaxDetector);
            stopwatch.Stop();

            // Assert
            measurementValues.Count.Should().Be(20);
            Trace.WriteLine("SQLite: " + stopwatch.Elapsed.TotalMilliseconds + " ms");
        }

        #endregion



        private void RecreateDatabase()
        {
            this._sqLiteRespository.RecreateDatabase(DatabaseFactory.SqLiteFilePath);
        }

        private void LoadDatabase()
        {
            this._sqLiteRespository.LoadDatabase(DatabaseFactory.SqLiteFilePath);
        }
    }
}