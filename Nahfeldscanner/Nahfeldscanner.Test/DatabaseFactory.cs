﻿// ///////////////////////////////////
// File: DatabaseFactory.cs
// Last Change: 26.01.2018  13:21
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test
{
    using System;
    using System.IO;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Properties;
    using ModelProeprties = Nahfeldscanner.Model.Properties;



    public static class DatabaseFactory
    {
        #region Fields

        private const string nameDatabase = "testDatabase.db";
        private const string nameFolder = "Downloads";

        private static readonly string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        private static readonly string pathDownloads = Path.Combine(pathUser, nameFolder);
        private static readonly string pathDatabase = Path.Combine(pathDownloads, nameDatabase);

        #endregion



        #region Properties

        public static string TestFolderPath
        {
            get { return pathDownloads; }
        }

        public static string SqLiteFilePath
        {
            get { return pathDatabase; }
        }

        #endregion



        /// <summary>
        ///     TODO: löschen
        /// </summary>
        public static void CreateTestFile()
        {
            DeleteTestSqLiteDatabase();

            FileStream fileStream = File.Create(SqLiteFilePath);
            fileStream.Close();
        }

        public static void DeleteTestSqLiteDatabase()
        {
            if (File.Exists(SqLiteFilePath))
            {
                File.Delete(SqLiteFilePath);
            }
        }

        public static void SetDefaultSqLiteDatabaseFilePathInSettings()
        {
            Settings.Default.DatabaseFilePath = SqLiteFilePath;
            Settings.Default.Save();
        }

        public static void ClearDatabaseFilePathInSettings()
        {
            Settings.Default.DatabaseFilePath = string.Empty;
            Settings.Default.Save();
        }

        public static void SetDefaultParameterSettings()
        {
            ModelProeprties.Settings.Default.DoQuasipeakMeasurement = ModelFactory.DefaultDoQuasipeakMeasurement;
            ModelProeprties.Settings.Default.OriginX = ModelFactory.DefaultOriginX;
            ModelProeprties.Settings.Default.OriginY = ModelFactory.DefaultOriginY;
            ModelProeprties.Settings.Default.XStart = ModelFactory.DefaultXStart;
            ModelProeprties.Settings.Default.DeltaX = ModelFactory.DefaultDeltaX;
            ModelProeprties.Settings.Default.XEnd = ModelFactory.DefaultXEnd;
            ModelProeprties.Settings.Default.YStart = ModelFactory.DefaultYStart;
            ModelProeprties.Settings.Default.DeltaY = ModelFactory.DefaultDeltaY;
            ModelProeprties.Settings.Default.YEnd = ModelFactory.DefaultYEnd;
            ModelProeprties.Settings.Default.FreqStart = ModelFactory.DefaultFreqStart;
            ModelProeprties.Settings.Default.ResBWIndex = (int)ModelFactory.DefaultResBW;
            ModelProeprties.Settings.Default.FreqEnd = ModelFactory.DefaultFreqEnd;
            ModelProeprties.Settings.Default.MeasureTime = ModelFactory.DefaultMeasureTime;
            ModelProeprties.Settings.Default.OrientationIndex = (int)ModelFactory.DefaultOrientation;
            ModelProeprties.Settings.Default.SensorTypeIndex = (int)ModelFactory.DefaultSensorType;
            ModelProeprties.Settings.Default.SensorImageStream = Convert.ToBase64String(ModelFactory.DefaultSensorImageStream);
            ModelProeprties.Settings.Default.SensorName = ModelFactory.DefaultSensorName;
            ModelProeprties.Settings.Default.Save();
        }

        public static void SetDefaultParameterSettings(string sensorName)
        {
            ModelProeprties.Settings.Default.DoQuasipeakMeasurement = ModelFactory.DefaultDoQuasipeakMeasurement;
            ModelProeprties.Settings.Default.OriginX = ModelFactory.DefaultOriginX;
            ModelProeprties.Settings.Default.OriginY = ModelFactory.DefaultOriginY;
            ModelProeprties.Settings.Default.XStart = ModelFactory.DefaultXStart;
            ModelProeprties.Settings.Default.DeltaX = ModelFactory.DefaultDeltaX;
            ModelProeprties.Settings.Default.XEnd = ModelFactory.DefaultXEnd;
            ModelProeprties.Settings.Default.YStart = ModelFactory.DefaultYStart;
            ModelProeprties.Settings.Default.DeltaY = ModelFactory.DefaultDeltaY;
            ModelProeprties.Settings.Default.YEnd = ModelFactory.DefaultYEnd;
            ModelProeprties.Settings.Default.FreqStart = ModelFactory.DefaultFreqStart;
            ModelProeprties.Settings.Default.ResBWIndex = (int)ModelFactory.DefaultResBW;
            ModelProeprties.Settings.Default.FreqEnd = ModelFactory.DefaultFreqEnd;
            ModelProeprties.Settings.Default.MeasureTime = ModelFactory.DefaultMeasureTime;
            ModelProeprties.Settings.Default.OrientationIndex = (int)ModelFactory.DefaultOrientation;
            ModelProeprties.Settings.Default.SensorTypeIndex = (int)ModelFactory.DefaultSensorType;
            ModelProeprties.Settings.Default.SensorImageStream = Convert.ToBase64String(ModelFactory.DefaultSensorImageStream);
            ModelProeprties.Settings.Default.SensorName = sensorName;
            ModelProeprties.Settings.Default.Save();
        }

        public static void ClearDefaultParameterSettings()
        {
            ModelProeprties.Settings.Default.DoQuasipeakMeasurement = !ModelFactory.DefaultDoQuasipeakMeasurement;
            ModelProeprties.Settings.Default.OriginX = 0;
            ModelProeprties.Settings.Default.OriginY = 0;
            ModelProeprties.Settings.Default.XStart = 0;
            ModelProeprties.Settings.Default.DeltaX = 0;
            ModelProeprties.Settings.Default.XEnd = 0;
            ModelProeprties.Settings.Default.YStart = 0;
            ModelProeprties.Settings.Default.DeltaY = 0;
            ModelProeprties.Settings.Default.YEnd = 0;
            ModelProeprties.Settings.Default.FreqStart = 0;
            ModelProeprties.Settings.Default.ResBWIndex = 999;
            ModelProeprties.Settings.Default.FreqEnd = 0;
            ModelProeprties.Settings.Default.MeasureTime = 0;
            ModelProeprties.Settings.Default.OrientationIndex = 999;
            ModelProeprties.Settings.Default.SensorTypeIndex = 999;
            ModelProeprties.Settings.Default.SensorImageStream = null;
            ModelProeprties.Settings.Default.SensorName = null;
            ModelProeprties.Settings.Default.Save();
        }

        public static void InitDatabase(IRepository repository, int amountAnalysis, int amountMeasurement, int amountMeasurementValue, int amountSensor, int xMax)
        {
            repository.LoadDatabase(SqLiteFilePath);

            if (repository.GetAmount<Analysis>() != amountAnalysis ||
                repository.GetAmount<Measurement>() != (long)amountMeasurement * amountAnalysis ||
                repository.GetAmount<MeasurementValue>() != (long)amountMeasurementValue * amountMeasurement * amountAnalysis ||
                repository.GetAmount<Sensor>() != amountSensor)
            {
                repository.RecreateDatabase(SqLiteFilePath);
                AddSampleData(repository, SqLiteFilePath, amountAnalysis, amountMeasurement, amountMeasurementValue, amountSensor, xMax);
            }
        }

        public static void AddSampleData(IRepository repository, string databasePath, int amountAnalysis, int amountMeasurement, int amountMeasurementValue, int amountSensor, int xMax)
        {
            repository.RecreateDatabase(databasePath);

            int xEnd = amountMeasurement > xMax ? xMax : amountMeasurement;
            int yEnd = amountMeasurement % xMax > 0 ? amountMeasurement / xMax + 1 : amountMeasurement / xMax;

            Parameter parameter = CreateParameter((int)xEnd, (int)yEnd, amountMeasurementValue);
            repository.SaveOrUpdate(parameter);

            Analysis[] analyses = AddAnalyses(repository, parameter, amountAnalysis);
            Measurement[] measurements = AddMeasurements(repository, analyses, amountMeasurement, xMax);
            AddMeasurementValues(repository, measurements, amountMeasurementValue);
            AddSensors(repository, amountSensor);
        }

        private static Parameter CreateParameter(int xEnd, int yEnd, double freqEnd)
        {
            return new Parameter
                   {
                           DoQuasipeakMeasurement = true,
                           XStart = 1,
                           DeltaX = 1,
                           XEnd = xEnd,
                           YStart = 1,
                           DeltaY = 1,
                           YEnd = yEnd,
                           FreqStart = 1,
                           DeltaFreq = 1,
                           FreqEnd = freqEnd
                   };
        }

        // ReSharper disable once UnusedMember.Local
        private static Analysis[] AddAnalyses(IRepository repository, int amount)
        {
            Analysis[] analyses = new Analysis[amount];
            Parameter[] parameters = new Parameter[amount];

            for (int aIndex = 0; aIndex < amount; aIndex++)
            {
                Analysis analysis = CreateAnalysis();
                parameters[aIndex] = analysis.Parameter;
                analyses[aIndex] = analysis;
            }

            repository.InsertArray(parameters);
            repository.InsertArray(analyses);
            return analyses;
        }

        private static Analysis[] AddAnalyses(IRepository repository, Parameter parameter, int amount)
        {
            Analysis[] analyses = new Analysis[amount];

            for (int aIndex = 0; aIndex < amount; aIndex++)
            {
                Analysis analysis = CreateAnalysis();
                parameter.AddAnalysis(analysis);
                analyses[aIndex] = analysis;
            }

            repository.InsertArray(analyses);
            return analyses;
        }

        private static Analysis CreateAnalysis()
        {
            return new Analysis();
        }

        private static Measurement[] AddMeasurements(IRepository repository, Analysis[] analyses, int amount, int xMax)
        {
            int analysesLength = analyses.Length;

            Measurement[] measurements = new Measurement[amount * analysesLength];

            for (int aIndex = 0; aIndex < analysesLength; aIndex++)
            {
                for (int mIndex = 0; mIndex < amount; mIndex++)
                {
                    int x = mIndex % xMax + 1;
                    int y = mIndex / xMax + 1;

                    Measurement measurement = CreateMeasurement(x, y);
                    analyses[aIndex].AddMeasurement(measurement);
                    measurements[aIndex * amount + mIndex] = measurement;
                }
            }

            repository.InsertArray(measurements);
            return measurements;
        }

        private static Measurement CreateMeasurement(double x, double y)
        {
            return new Measurement
                   {
                           X = x,
                           Y = y
                   };
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private static MeasurementValue[] AddMeasurementValues(IRepository repository, Measurement[] measurements, int amount)
        {
            int measurementLength = measurements.Length;

            MeasurementValue[] measurementValues = new MeasurementValue[amount * measurementLength];
            Random rand = new Random();

            for (int mIndex = 0; mIndex < measurementLength; mIndex++)
            {
                for (int mvIndex = 0; mvIndex < amount; mvIndex++)
                {
                    MeasurementValue measurementValue = CreateMeasurementValue(mvIndex + 1, rand.NextDouble() * 100, rand.NextDouble() * 100, rand.NextDouble() * 100);
                    measurements[mIndex].AddMeasurementValue(measurementValue);
                    measurementValues[mIndex * amount + mvIndex] = measurementValue;
                }
            }

            repository.BulkInsert(SqLiteFilePath, measurementValues);
            return measurementValues;
        }

        private static MeasurementValue CreateMeasurementValue(double frequency, double intMax, double intAvg, double intQP)
        {
            return new MeasurementValue
                   {
                           Frequency = frequency,
                           IntensityMaxDetector = intMax,
                           IntensityAVGDetector = intAvg,
                           IntensityQPDetector = intQP
                   };
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private static Sensor[] AddSensors(IRepository repository, int amount)
        {
            Func<int, string> setSensorName = x => ModelFactory.DefaultSensorName.Substring(0, ModelFactory.DefaultSensorName.Length - 1) + x;
            Func<int, SensorType> setSensorType = x => x % 2 == 0 ? SensorType.E_Field : SensorType.H_Field;

            Sensor[] sensors = new Sensor[amount];

            for (int i = 0; i < amount; i++)
            {
                sensors[i] = CreateSensor(setSensorName(i + 1), setSensorType(i));
            }

            repository.InsertArray(sensors);
            return sensors;
        }

        private static Sensor CreateSensor(string sensorName, SensorType sensorType)
        {
            return new Sensor(sensorName, sensorType, ModelFactory.DefaultSensorImageStream, 1e6, 1e9);
        }
    }
}