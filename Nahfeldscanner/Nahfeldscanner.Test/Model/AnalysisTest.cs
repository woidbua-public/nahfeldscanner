﻿// ///////////////////////////////////
// File: AnalysisTest.cs
// Last Change: 22.01.2018  10:53
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.Model
{
    using System;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Model.Database;



    [TestClass]
    public class AnalysisTest
    {
        #region Fields

        private Analysis _analysis;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this._analysis = new Analysis();
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this._analysis = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void ParameterNotNullAfterCreation()
        {
            // Assert
            this._analysis.Parameter.Should().NotBeNull();
        }

        [TestMethod]
        public void MeasurementListNotNullAfterCreation()
        {
            // Assert
            this._analysis.Measurements.Should().NotBeNull();
        }

        [TestMethod]
        public void AddsMeasurementWithReferenceToAnalysis()
        {
            // Arrange
            const int AnalysisId = 2;
            this._analysis.ID = AnalysisId;

            // Act
            this._analysis.AddMeasurement(new Measurement());

            // Assert
            this._analysis.Measurements.Count.Should().Be(1);
            this._analysis.Measurements[0].Analysis.Should().NotBeNull();
            this._analysis.Measurements[0].Analysis.ID.Should().Be(2);
        }

        [TestMethod]
        public void ClonesAnalysis()
        {
            // Arrange
            const int AnalysisId = 2;
            const int ParameterId = 5;
            const string Description = "testString";

            this._analysis.ID = AnalysisId;
            this._analysis.Parameter.ID = ParameterId;
            this._analysis.Description = Description;

            // Act
            Analysis analysis = this._analysis.Clone();

            // Assert
            analysis.Description.Should().Be(Description);
            analysis.Parameter.ID.Should().Be(0);
            analysis.ID.Should().NotBe(AnalysisId);
        }

        #endregion
    }
}