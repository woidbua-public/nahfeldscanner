﻿// ///////////////////////////////////
// File: ObjectEqualityComparer.cs
// Last Change: 22.01.2018  11:19
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.Model.Database
{
    using System;
    using System.Collections;
    using System.Linq;
    using Nahfeldscanner.Model.Database;



    // ReSharper disable CompareOfFloatsByEqualityOperator
    public class ObjectEqualityComparer : IEqualityComparer
    {
        #region IEqualityComparer Members

        public new bool Equals(object obj1, object obj2)
        {
            if (obj1 == null || obj2 == null)
            {
                return false;
            }

            Analysis analysis1 = obj1 as Analysis;
            Analysis analysis2 = obj2 as Analysis;

            if (analysis1 != null && analysis2 != null)
            {
                return this.AreEqual(analysis1, analysis2);
            }

            Parameter parameter1 = obj1 as Parameter;
            Parameter parameter2 = obj2 as Parameter;

            if (parameter1 != null && parameter2 != null)
            {
                return this.AreEqual(parameter1, parameter2);
            }

            Measurement measurement1 = obj1 as Measurement;
            Measurement measurement2 = obj2 as Measurement;

            if (measurement1 != null && measurement2 != null)
            {
                return this.AreEqual(measurement1, measurement2);
            }

            MeasurementValue measurementValue1 = obj1 as MeasurementValue;
            MeasurementValue measurementValue2 = obj2 as MeasurementValue;

            if (measurementValue1 != null && measurementValue2 != null)
            {
                return this.AreEqual(measurementValue1, measurementValue2);
            }

            Sensor sensor1 = obj1 as Sensor;
            Sensor sensor2 = obj2 as Sensor;

            if (sensor1 != null && sensor2 != null)
            {
                return this.AreEqual(sensor1, sensor2);
            }

            return obj1.Equals(obj2);
        }

        public int GetHashCode(object obj)
        {
            throw new NotImplementedException();
        }

        #endregion



        private bool AreEqual(Analysis analysis1, Analysis analysis2)
        {
            if (analysis1.ID == analysis2.ID &&
                analysis1.StartTime == analysis2.StartTime &&
                analysis1.EndTime == analysis2.EndTime &&
                analysis1.Description == analysis2.Description &&
                analysis1.FinishedAnalysis == analysis2.FinishedAnalysis)
            {
                return true;
            }

            return false;
        }

        private bool AreEqual(Measurement measurement1, Measurement measurement2)
        {
            if (measurement1.ID == measurement2.ID &&
                measurement1.X == measurement2.X &&
                measurement1.Y == measurement2.Y)
            {
                return true;
            }

            return false;
        }

        private bool AreEqual(MeasurementValue measurementValue1, MeasurementValue measurementValue2)
        {
            if (measurementValue1.ID == measurementValue2.ID &&
                measurementValue1.Frequency == measurementValue2.Frequency &&
                measurementValue1.IntensityMaxDetector == measurementValue2.IntensityMaxDetector &&
                measurementValue1.IntensityAVGDetector == measurementValue2.IntensityAVGDetector &&
                measurementValue1.IntensityQPDetector == measurementValue2.IntensityQPDetector)
            {
                return true;
            }

            return false;
        }

        private bool AreEqual(Parameter parameter1, Parameter parameter2)
        {
            if (parameter1.ID == parameter2.ID &&
                parameter1.DoQuasipeakMeasurement == parameter2.DoQuasipeakMeasurement &&
                parameter1.OriginX == parameter2.OriginX &&
                parameter1.OriginY == parameter2.OriginY &&
                parameter1.XStart == parameter2.XStart &&
                parameter1.DeltaX == parameter2.DeltaX &&
                parameter1.XEnd == parameter2.XEnd &&
                parameter1.YStart == parameter2.YStart &&
                parameter1.DeltaY == parameter2.DeltaY &&
                parameter1.YEnd == parameter2.YEnd &&
                parameter1.FreqStart == parameter2.FreqStart &&
                parameter1.DeltaFreq == parameter2.DeltaFreq &&
                parameter1.FreqEnd == parameter2.FreqEnd &&
                parameter1.ResBW == parameter2.ResBW &&
                parameter1.MeasureTime == parameter2.MeasureTime &&
                parameter1.Orientation == parameter2.Orientation &&
                parameter1.SensorImageStream.SequenceEqual(parameter2.SensorImageStream) &&
                parameter1.SensorType == parameter2.SensorType &&
                parameter1.SensorName == parameter2.SensorName)
            {
                return true;
            }

            return false;
        }

        private bool AreEqual(Sensor sensor1, Sensor sensor2)
        {
            if (sensor1.ID == sensor2.ID &&
                sensor1.SensorType == sensor2.SensorType &&
                sensor1.SensorImageStream.SequenceEqual(sensor2.SensorImageStream) &&
                sensor1.SensorName == sensor2.SensorName)
            {
                return true;
            }

            return false;
        }
    }
}