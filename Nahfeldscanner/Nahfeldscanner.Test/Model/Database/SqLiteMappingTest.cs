﻿// ///////////////////////////////////
// File: SqLiteMappingTest.cs
// Last Change: 22.01.2018  10:30
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.Model.Database
{
    using System;
    using System.Collections.Generic;
    using FluentNHibernate.Testing;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.Model.Database;



    [TestClass]
    public class SqLiteMappingTest
    {
        #region Fields

        private SqLiteSessionManager sessionManager;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this.sessionManager = new SqLiteSessionManager();
            this.sessionManager.RecreateDatabase(DatabaseFactory.SqLiteFilePath);
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this.sessionManager.Dispose();
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void CanMapAnalysis()
        {
            // Assert
            new PersistenceSpecification<Analysis>(this.sessionManager.OpenSession(), new ObjectEqualityComparer())
                    .CheckProperty(a => a.ID, ModelFactory.DefaultID)
                    .CheckProperty(a => a.FinishedAnalysis, ModelFactory.DefaultFinishedAnalysis)
                    .CheckProperty(a => a.StartTime, ModelFactory.DefaultStartTime)
                    .CheckProperty(a => a.EndTime, ModelFactory.DefaultEndTime)
                    .CheckProperty(a => a.Description, ModelFactory.DefaultDescription)
                    .CheckReference(a => a.Parameter, ModelFactory.GetDefaultParameter())
                    .CheckList(a => a.Measurements, new List<Measurement> { ModelFactory.GetDefaultMeasurement() })
                    .VerifyTheMappings();
        }

        [TestMethod]
        public void CanMapMeasurement()
        {
            // Arrange
            new PersistenceSpecification<Measurement>(this.sessionManager.OpenSession(), new ObjectEqualityComparer())
                    .CheckProperty(m => m.ID, ModelFactory.DefaultID)
                    .CheckProperty(m => m.X, ModelFactory.DefaultX)
                    .CheckProperty(m => m.Y, ModelFactory.DefaultY)
                    .CheckReference(m => m.Analysis, ModelFactory.GetDefaultAnalysis())
                    .CheckList(m => m.MeasurementValues, new List<MeasurementValue> { ModelFactory.GetDefaultMeasurementValue() })
                    .VerifyTheMappings();
        }

        [TestMethod]
        public void CanMapMeasurementValue()
        {
            new PersistenceSpecification<MeasurementValue>(this.sessionManager.OpenSession(), new ObjectEqualityComparer())
                    .CheckProperty(m => m.ID, ModelFactory.DefaultID)
                    .CheckProperty(m => m.IntensityMaxDetector, ModelFactory.DefaultIntensityMaxDetector)
                    .CheckProperty(m => m.IntensityAVGDetector, ModelFactory.DefaultIntensityAVGDetector)
                    .CheckProperty(m => m.IntensityQPDetector, ModelFactory.DefaultIntensityQPDetector)
                    .CheckReference(m => m.Measurement, ModelFactory.GetDefaultMeasurement())
                    .VerifyTheMappings();
        }

        [TestMethod]
        public void CanMapParameter()
        {
            // Assert
            new PersistenceSpecification<Parameter>(this.sessionManager.OpenSession(), new ObjectEqualityComparer())
                    .CheckProperty(p => p.ID, ModelFactory.DefaultID + 1) // +1 because of parameter from analysis
                    .CheckProperty(p => p.DoQuasipeakMeasurement, true)
                    .CheckProperty(p => p.OriginX, ModelFactory.DefaultOriginX)
                    .CheckProperty(p => p.OriginY, ModelFactory.DefaultOriginY)
                    .CheckProperty(p => p.XStart, ModelFactory.DefaultXStart)
                    .CheckProperty(p => p.DeltaX, ModelFactory.DefaultDeltaX)
                    .CheckProperty(p => p.XEnd, ModelFactory.DefaultXEnd)
                    .CheckProperty(p => p.YStart, ModelFactory.DefaultYStart)
                    .CheckProperty(p => p.DeltaY, ModelFactory.DefaultDeltaY)
                    .CheckProperty(p => p.YEnd, ModelFactory.DefaultYEnd)
                    .CheckProperty(p => p.FreqStart, ModelFactory.DefaultFreqStart)
                    .CheckProperty(p => p.DeltaFreq, ModelFactory.DefaultDeltaFreq)
                    .CheckProperty(p => p.FreqEnd, ModelFactory.DefaultFreqEnd)
                    .CheckProperty(p => p.ResBW, ModelFactory.DefaultResBW)
                    .CheckProperty(p => p.MeasureTime, ModelFactory.DefaultMeasureTime)
                    .CheckProperty(p => p.Orientation, ModelFactory.DefaultOrientation)
                    .CheckProperty(p => p.SensorType, ModelFactory.DefaultSensorType)
                    .CheckProperty(p => p.SensorImageStream, ModelFactory.DefaultSensorImageStream)
                    .CheckProperty(p => p.SensorName, ModelFactory.DefaultSensorName)
                    .CheckList(p => p.Analyses, new List<Analysis> { ModelFactory.GetDefaultAnalysis() })
                    .VerifyTheMappings();
        }

        [TestMethod]
        public void CanMapSensor()
        {
            new PersistenceSpecification<Sensor>(this.sessionManager.OpenSession(), new ObjectEqualityComparer())
                    .CheckProperty(s => s.ID, ModelFactory.DefaultID)
                    .CheckProperty(s => s.SensorImageStream, ModelFactory.DefaultSensorImageStream)
                    .CheckProperty(s => s.SensorType, ModelFactory.DefaultSensorType)
                    .CheckProperty(s => s.SensorName, ModelFactory.DefaultSensorName)
                    .VerifyTheMappings();
        }

        #endregion
    }
}