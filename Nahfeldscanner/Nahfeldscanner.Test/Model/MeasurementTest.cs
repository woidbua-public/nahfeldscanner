﻿// ///////////////////////////////////
// File: MeasurementTest.cs
// Last Change: 22.01.2018  10:53
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.Model
{
    using System;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Model.Database;



    [TestClass]
    public class MeasurementTest
    {
        #region Fields

        private Measurement _measurement;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this._measurement = new Measurement();
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this._measurement = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void MeasurementValueListInitializedAfterCreation()
        {
            // Assert
            this._measurement.MeasurementValues.Should().NotBeNull();
        }

        [TestMethod]
        public void AddsMeasurementValueWithReferenceToMeasurement()
        {
            // Arrange
            const int MeasurementId = 2;
            this._measurement.ID = MeasurementId;

            // Act
            this._measurement.AddMeasurementValue(new MeasurementValue());

            // Assert
            this._measurement.MeasurementValues.Count.Should().Be(1);
            this._measurement.MeasurementValues[0].Measurement.Should().NotBeNull();
            this._measurement.MeasurementValues[0].Measurement.ID.Should().Be(2);
        }

        #endregion
    }
}