﻿// ///////////////////////////////////
// File: ParameterTest.cs
// Last Change: 23.12.2017  13:29
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.Model
{
    using System;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Properties;



    [TestClass]
    public class ParameterTest
    {
        private Parameter _parameter;

        [TestInitialize]
        public void Init()
        {
            this._parameter = new Parameter();
        }

        [TestCleanup]
        public void Cleanup()
        {
            this._parameter = null;
            GC.Collect();
        }

        [TestMethod]
        public void ParameterSetAfterInit()
        {
            // Assert
            this._parameter.ID.Should().Be(0);
            this._parameter.DoQuasipeakMeasurement.Should().BeTrue();
            this._parameter.OriginX.Should().NotBeNull();
            this._parameter.OriginY.Should().NotBeNull();
            this._parameter.XStart.Should().NotBeNull();
            this._parameter.DeltaX.Should().NotBeNull();
            this._parameter.XEnd.Should().NotBeNull();
            this._parameter.YStart.Should().NotBeNull();
            this._parameter.DeltaY.Should().NotBeNull();
            this._parameter.YEnd.Should().NotBeNull();
            this._parameter.FreqStart.Should().NotBeNull();
            this._parameter.DeltaFreq.Should().NotBeNull();
            this._parameter.FreqEnd.Should().NotBeNull();
            this._parameter.ResBW.Should().NotBeNull();
            this._parameter.MeasureTime.Should().NotBeNull();
            this._parameter.Orientation.Should().NotBeNull();
            this._parameter.SensorType.Should().NotBeNull();
            this._parameter.SensorImageStream.Should().NotBeNull();
            this._parameter.SensorName.Should().NotBeNull();
        }

        [TestMethod]
        public void ClearsParameterValues()
        {
            // Act
            this._parameter.Clear();

            // Assert
            this._parameter.ID.Should().Be(0);
            this._parameter.DoQuasipeakMeasurement.Should().BeFalse();
            this._parameter.OriginX.Should().BeNull();
            this._parameter.OriginY.Should().BeNull();
            this._parameter.XStart.Should().BeNull();
            this._parameter.DeltaX.Should().BeNull();
            this._parameter.XEnd.Should().BeNull();
            this._parameter.YStart.Should().BeNull();
            this._parameter.DeltaY.Should().BeNull();
            this._parameter.YEnd.Should().BeNull();
            this._parameter.FreqStart.Should().BeNull();
            this._parameter.DeltaFreq.Should().BeNull();
            this._parameter.FreqEnd.Should().BeNull();
            this._parameter.ResBW.Should().BeNull();
            this._parameter.MeasureTime.Should().BeNull();
            this._parameter.Orientation.Should().BeNull();
            this._parameter.SensorType.Should().BeNull();
            this._parameter.SensorImageStream.Should().BeNull();
            this._parameter.SensorName.Should().BeNull();
        }

        [TestMethod]
        public void IsValidWhenAllPropertiesHaveValues()
        {
            // Arrange
            Settings.Default.SensorName = "testname";
            Settings.Default.Save();

            // Assert
            this._parameter.ValuesSet().Should().Be(true);
        }

        [TestMethod]
        public void IsNotValidWhenAPropertyHasNoValue()
        {
            // Act
            this._parameter.XStart = null;

            // Assert
            this._parameter.ValuesSet().Should().Be(false);
        }

        [TestMethod]
        public void CreatesCloneParameter()
        {
            // Act
            Parameter parameter = this._parameter.Clone();

            // Assert
            parameter.ValuesSet().Should().BeTrue();
        }
    }
}