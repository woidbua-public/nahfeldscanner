﻿// ///////////////////////////////////
// File: ModelFactory.cs
// Last Change: 17.01.2018  11:43
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test
{
    using System.Collections.Generic;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;



    public static class ModelFactory
    {
        #region Fields

        public static int DefaultID = 1;

        public static string DefaultStartTime = "05.12.2015 06:12:54";
        public static string DefaultEndTime = "05.12.2015 06:45:23";
        public static string DefaultDescription = "E-Feldsonde um 45° gedreht";
        public static bool DefaultFinishedAnalysis = true;

        public static bool DefaultDoQuasipeakMeasurement = true;
        public static double DefaultOriginX = 15;
        public static double DefaultOriginY = 20;
        public static double DefaultXStart = 3;
        public static double DefaultDeltaX = 1;
        public static double DefaultXEnd = 10;
        public static double DefaultYStart = 12;
        public static double DefaultDeltaY = 2;
        public static double DefaultYEnd = 20;
        public static double DefaultFreqStart = 10.0;
        public static double DefaultDeltaFreq = 2.5;
        public static double DefaultFreqEnd = 20.0;
        public static ResBW DefaultResBW = ResBW.BW_10_Hz;
        public static int DefaultMeasureTime = 5;
        public static Orientation DefaultOrientation = Orientation.Y;
        public static byte[] DefaultSensorImageStream = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        public static string DefaultSensorName = "Sensor 2";
        public static SensorType DefaultSensorType = SensorType.H_Field;

        public static double DefaultX = 3;
        public static double DefaultY = 20;

        public static List<double> DefaultFrequencies = new List<double> { 10.0, 12.5, 15.0, 17.5, 20.0 };
        public static double DefaultFrequency = 100100;
        public static double DefaultIntensityMaxDetector = 20.0;
        public static double DefaultIntensityAVGDetector = 20.5;
        public static double DefaultIntensityQPDetector = 21.0;

        // TODO: delete
        public static double DefaultMaxDetectorMinFrequency = DefaultFrequencies[4];
        public static double DefaultMaxDetectorMaxFrequency = DefaultFrequencies[1];
        public static double DefaultAVGDetectorMinFrequency = DefaultFrequencies[3];
        public static double DefaultAVGDetectorMaxFrequency = DefaultFrequencies[0];
        public static double DefaultQuasipeakDetectorMinFrequency = DefaultFrequencies[1];
        public static double DefaultQuasipeakDetectorMaxFrequency = DefaultFrequencies[4];

        public static double[] DefaultIntensitiesMaxDetector = { 10.23, 22.54, 10.0, 5.5, 3.25 };
        public static double DefaultMaxDetectorMinIntensity = DefaultIntensitiesMaxDetector[4];
        public static double DefaultMaxDetectorMaxIntensity = DefaultIntensitiesMaxDetector[1];

        public static double[] DefaultIntensitiesAVGDetector = { 94, 32.5, 35.15, 27.2, 53.23 };
        public static double DefaultAVGDetectorMinIntensity = DefaultIntensitiesAVGDetector[3];
        public static double DefaultAVGDetectorMaxIntensity = DefaultIntensitiesAVGDetector[0];

        public static double[] DefaultIntensitiesQuasipeakDetector = { 55.55, 11.11, 33.33, 22.22, 66.66 };
        public static double DefaultQuasipeakDetectorMinIntensity = DefaultIntensitiesQuasipeakDetector[1];
        public static double DefaultQuasipeakDetectorMaxIntensity = DefaultIntensitiesQuasipeakDetector[4];

        #endregion



        public static Analysis GetFilledAnalysis()
        {
            Analysis analysis = GetDefaultAnalysis();
            Parameter parameter = GetDefaultParameter();
            Measurement measurement = GetDefaultMeasurement();
            MeasurementValue measurementValue = GetDefaultMeasurementValue();

            measurement.AddMeasurementValue(measurementValue);
            analysis.AddMeasurement(measurement);
            analysis.Parameter = parameter;

            return analysis;
        }

        public static Analysis GetDefaultAnalysis()
        {
            return new Analysis
                   {
                           FinishedAnalysis = DefaultFinishedAnalysis,
                           StartTime = DefaultStartTime,
                           EndTime = DefaultEndTime,
                           Description = DefaultDescription
                   };
        }

        public static Measurement GetDefaultMeasurement()
        {
            return new Measurement
                   {
                           X = DefaultX,
                           Y = DefaultY
                   };
        }

        public static MeasurementValue GetDefaultMeasurementValue()
        {
            return new MeasurementValue
                   {
                           Frequency = DefaultFrequency,
                           IntensityMaxDetector = DefaultIntensityMaxDetector,
                           IntensityAVGDetector = DefaultIntensityAVGDetector,
                           IntensityQPDetector = DefaultIntensityQPDetector
                   };
        }

        public static Parameter GetDefaultParameter()
        {
            return new Parameter
                   {
                           DoQuasipeakMeasurement = DefaultDoQuasipeakMeasurement,
                           OriginX = DefaultOriginX,
                           OriginY = DefaultOriginY,
                           XStart = DefaultXStart,
                           DeltaX = DefaultDeltaX,
                           XEnd = DefaultXEnd,
                           YStart = DefaultYStart,
                           DeltaY = DefaultDeltaY,
                           YEnd = DefaultYEnd,
                           FreqStart = DefaultFreqStart,
                           ResBW = DefaultResBW,
                           DeltaFreq = DefaultDeltaFreq,
                           FreqEnd = DefaultFreqEnd,
                           MeasureTime = DefaultMeasureTime,
                           Orientation = DefaultOrientation,
                           SensorType = DefaultSensorType,
                           SensorImageStream = DefaultSensorImageStream,
                           SensorName = DefaultSensorName
                   };
        }

        public static Sensor GetDefaultSensor()
        {
            return new Sensor
                   {
                           SensorImageStream = DefaultSensorImageStream,
                           SensorType = DefaultSensorType,
                           SensorName = DefaultSensorName
                   };
        }
    }
}