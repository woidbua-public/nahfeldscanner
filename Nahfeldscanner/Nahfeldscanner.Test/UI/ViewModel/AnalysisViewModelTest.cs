﻿// ///////////////////////////////////
// File: AnalysisViewModelTest.cs
// Last Change: 24.01.2018  08:21
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.UI.ViewModel;



    [TestClass]
    public class AnalysisViewModelTest
    {
        #region Fields

        private SqLiteRepository _repository;
        private AnalysisViewModel _analysisViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this._repository = new SqLiteRepository(new SqLiteSessionManager());
            this._analysisViewModel = new AnalysisViewModel(this._repository, new Analysis());
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this._analysisViewModel = null;
            this._repository = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void PropertyCount()
        {
            // Assert
            this._analysisViewModel.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(prop => prop.DeclaringType == this._analysisViewModel.GetType()).ToList().Count.Should().Be(7);
        }

        [TestMethod]
        public void CanSetId()
        {
            // Arrange
            const int ID = 5;

            // Act
            this._analysisViewModel.ID = ID;

            // Assert
            this._analysisViewModel.ID.Should().Be(ID);
        }

        [TestMethod]
        public void GetDefaultAnalysisObjectProperties()
        {
            // Act
            this._analysisViewModel = new AnalysisViewModel(this._repository, ModelFactory.GetDefaultAnalysis());

            // Assert
            this._analysisViewModel.ID.Should().Be(0);
            this._analysisViewModel.StartTime.Should().Be(ModelFactory.DefaultStartTime);
            this._analysisViewModel.EndTime.Should().Be(ModelFactory.DefaultEndTime);
            this._analysisViewModel.Description.Should().Be(ModelFactory.DefaultDescription);
            this._analysisViewModel.FinishedAnalysis.Should().Be(ModelFactory.DefaultFinishedAnalysis);
            this._analysisViewModel.ParameterViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void GetNewAnalysisObjectproperties()
        {
            // Assert
            this._analysisViewModel.ID.Should().Be(0);
            this._analysisViewModel.StartTime.Should().BeNull();
            this._analysisViewModel.EndTime.Should().BeNull();
            this._analysisViewModel.Description.Should().BeNullOrEmpty();
            this._analysisViewModel.FinishedAnalysis.Should().BeNull();
            this._analysisViewModel.ParameterViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void RaisesPropertyChangedEvents()
        {
            // Arrange
            const string StartTime = "23.10.2017 08:51:34";
            const string EndTime = "23.10.2017 08:56:10";
            const string Description = "new description";

            this._analysisViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._analysisViewModel.StartTime = StartTime;
            this._analysisViewModel.EndTime = EndTime;
            this._analysisViewModel.Description = Description;
            this._analysisViewModel.FinishedAnalysis = true;

            // Assert
            this._analysisViewModel.StartTime.Should().Be(StartTime);
            this._analysisViewModel.EndTime.Should().Be(EndTime);
            this._analysisViewModel.Description.Should().Be(Description);
            this._analysisViewModel.FinishedAnalysis.Should().Be(true);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.StartTime);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.EndTime);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.Description);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.FinishedAnalysis);
        }

        [TestMethod]
        public void CanNotEdit()
        {
            // Assert
            this._analysisViewModel.CanEdit.Should().BeFalse();
        }

        [TestMethod]
        public void CanEdit()
        {
            // Act
            this._analysisViewModel.CanEdit = true;

            // Assert
            this._analysisViewModel.CanEdit.Should().BeTrue();
        }

        [TestMethod]
        public void RaisesPropertyChangedWhenEditStateChanges()
        {
            // Arrange
            this._analysisViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._analysisViewModel.CanEdit = true;

            // Assert
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.CanEdit);
        }

        [TestMethod]
        public void UpdatesAllProeprties()
        {
            // Arrange
            int counter = 0;
            this._analysisViewModel.PropertyChanged += (sender, e) => counter++;
            this._analysisViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._analysisViewModel.UpdateProperties();

            // Assert
            counter.Should().Be(7);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.CanEdit);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.ID);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.StartTime);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.EndTime);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.Description);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.FinishedAnalysis);
            this._analysisViewModel.ShouldRaisePropertyChangeFor(x => x.ParameterViewModel);
        }

        #endregion
    }
}