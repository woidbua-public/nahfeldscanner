﻿// ///////////////////////////////////
// File: BindableViewModelBaseTest.cs
// Last Change: 30.10.2017  11:57
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.ComponentModel;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.UI.ViewModel;



    [TestClass]
    public class BindableViewModelBaseTest
    {
        private TestClass testClass;

        private class TestClass : BindableViewModelBase
        {
            private string _firstName;

            public string FirstName
            {
                get { return this._firstName; }
                set { this.SetProperty(ref this._firstName, value); }
            }

            public string LastName
            {
                get { return this.PrivateLastName; }
                set { this.SetProperty(() => this.PrivateLastName = value, () => this.PrivateLastName == value); }
            }

            private string PrivateLastName { get; set; }

            public bool ChangesFirstName(string firstName)
            {
                return this.SetProperty(ref this._firstName, firstName);
            }

            public bool ChangesLastName(string lastName)
            {
                return this.SetProperty(() => this.PrivateLastName = lastName, () => this.PrivateLastName == lastName);
            }
        }

        [TestInitialize]
        public void Init()
        {
            this.testClass = new TestClass();
        }

        [TestCleanup]
        public void Cleanup()
        {
            this.testClass = null;
            GC.Collect();
        }

        [TestMethod]
        public void ChangesPropertyValue()
        {
            // Arrange
            string firstName = "Testname";
            string lastName = "Meier";

            // Act
            this.testClass.FirstName = firstName;
            this.testClass.LastName = lastName;

            // Assert
            this.testClass.FirstName.Should().Be(firstName);
            this.testClass.LastName.Should().Be(lastName);
        }

        [TestMethod]
        public void RaisesPropertyChangedWhenPropertyValueChanged()
        {
            // Arrange
            string firstName = "Testname";
            string lastName = "Meier";

            this.testClass.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this.testClass.FirstName = firstName;
            this.testClass.LastName = lastName;

            // Assert
            this.testClass.ShouldRaisePropertyChangeFor(x => x.FirstName);
            this.testClass.ShouldRaisePropertyChangeFor(x => x.LastName);
        }

        [TestMethod]
        public void DoesNotRaisesPropertyChangedWhenPropertyValueChangedToSameValue()
        {
            // Arrange
            string firstName = "Testname";
            string lastName = "Meier";

            this.testClass.FirstName = firstName;
            this.testClass.LastName = lastName;

            this.testClass.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this.testClass.FirstName = firstName;
            this.testClass.LastName = lastName;

            // Assert
            this.testClass.ShouldNotRaisePropertyChangeFor(x => x.FirstName);
            this.testClass.ShouldNotRaisePropertyChangeFor(x => x.LastName);
        }

        [TestMethod]
        public void ReturnsTrueWhenPropertyChanged()
        {
            // Arrange
            string firstName = "Testname";
            string lastName = "Meier";

            // Act
            bool changedFirstName = this.testClass.ChangesFirstName(firstName);
            bool changedLastName = this.testClass.ChangesFirstName(lastName);

            // Assert
            changedFirstName.Should().Be(true);
            changedLastName.Should().Be(true);
        }

        [TestMethod]
        public void ReturnsFalseWhenPropertyDidNotChange()
        {
            // Arrange
            string firstName = "Testname";
            string lastName = "Meier";

            this.testClass.ChangesFirstName(firstName);
            this.testClass.ChangesLastName(lastName);

            // Act
            bool changedFirstName = this.testClass.ChangesFirstName(firstName);
            bool changedLastName = this.testClass.ChangesLastName(lastName);

            // Assert
            changedFirstName.Should().Be(false);
            changedLastName.Should().Be(false);
        }
    }
}