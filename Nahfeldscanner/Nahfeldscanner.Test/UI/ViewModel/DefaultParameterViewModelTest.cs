﻿// ///////////////////////////////////
// File: DefaultParameterViewModelTest.cs
// Last Change: 15.02.2018  15:10
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.ComponentModel;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.ViewModel;
    using ModelProperties = Nahfeldscanner.Model.Properties.Settings;



    [TestClass]
    public class DefaultParameterViewModelTest
    {
        #region Fields

        private const int AmountAnalysis = 1;
        private const int AmountMeasurement = 1;
        private const int AmountMeasurementValue = 1;
        private const int AmountSensor = 10;
        private const int XMax = 5;

        private SqLiteRepository _repository;
        private DefaultParameterViewModel _parameterViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this._repository = new SqLiteRepository(new SqLiteSessionManager());

            DatabaseFactory.InitDatabase(this._repository,
                                         AmountAnalysis, AmountMeasurement, AmountMeasurementValue, AmountSensor, XMax);

            DatabaseFactory.SetDefaultParameterSettings();
            this._parameterViewModel = new DefaultParameterViewModel(this._repository);
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this._repository = null;
            this._parameterViewModel = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void InitSensorsAndSetSelectedToDefaultSensor()
        {
            // Assert
            this._parameterViewModel.SelectedSensorViewModel.Should().NotBeNull();
            this._parameterViewModel.SelectedSensorViewModel.SensorName.Should().Be(ModelFactory.DefaultSensorName);
        }

        [TestMethod]
        public void InitSensorsAndSetSelectedToFirstElement()
        {
            // Arrange
            DatabaseFactory.ClearDefaultParameterSettings();

            // Act
            this._parameterViewModel = new DefaultParameterViewModel(this._repository);

            // Assert
            this._parameterViewModel.SelectedSensorViewModel.Should().NotBeNull();
            this._parameterViewModel.SelectedSensorViewModel.SensorName.Should().Be(ModelFactory.DefaultSensorName.Substring(0, ModelFactory.DefaultSensorName.Length - 1) + "1");
        }

        [TestMethod]
        public void InitSensorsAndClearSensorDataWhenNoSensorInDatabase()
        {
            // Arrange
            DatabaseFactory.InitDatabase(this._repository,
                                         AmountAnalysis, AmountMeasurement, AmountMeasurementValue, 0, XMax);

            // Act
            this._parameterViewModel = new DefaultParameterViewModel(this._repository);

            // Assert
            this._parameterViewModel.SelectedSensorViewModel.Should().BeNull();
            this._parameterViewModel.SensorName.Should().BeNullOrEmpty();
            this._parameterViewModel.SensorType.Should().BeNull();
            this._parameterViewModel.SensorImageStream.Should().BeNull();
        }

        [TestMethod]
        public void ReloadsSensors()
        {
            // Arrange
            int deletedSensorViewModelId = this._parameterViewModel.Sensors[0].ID;
            SensorViewModel newSensorViewModel = new SensorViewModel(new Sensor(null, SensorType.E_Field, null));

            this._parameterViewModel.Sensors.RemoveAt(0);
            this._parameterViewModel.Sensors.Add(newSensorViewModel);

            // Act
            this._parameterViewModel.ReloadSensorsCommand.Execute(null);

            // Assert
            this._parameterViewModel.Sensors.Should().HaveCount(AmountSensor);
            this._parameterViewModel.Sensors.Should().Contain(x => x.ID == deletedSensorViewModelId);
        }

        [TestMethod]
        public void SetStepsToMinValue()
        {
            // Act
            this._parameterViewModel.XSteps = 1;
            this._parameterViewModel.YSteps = 1;

            // Assert
            this._parameterViewModel.XSteps.Should().Be(2);
            this._parameterViewModel.YSteps.Should().Be(2);
            this._parameterViewModel.DeltaX.Should().Be(ModelFactory.DefaultXEnd - ModelFactory.DefaultXStart);
            this._parameterViewModel.DeltaY.Should().Be(ModelFactory.DefaultYEnd - ModelFactory.DefaultYStart);
        }

        [TestMethod]
        public void RoundsValues()
        {
            // Arrange
            const double OriginX = 5.123;
            const double OriginY = 10.2034;
            const double XStart = 21.230;
            const double XEnd = 27.2340243;
            const double YStart = 31.102319;
            const double YEnd = 40.890123;

            DatabaseFactory.SetDefaultParameterSettings();

            // Act
            this._parameterViewModel.OriginX = OriginX;
            this._parameterViewModel.OriginY = OriginY;
            this._parameterViewModel.RelativeXEnd = XEnd;
            this._parameterViewModel.RelativeXStart = XStart;
            this._parameterViewModel.RelativeYEnd = YEnd;
            this._parameterViewModel.RelativeYStart = YStart;

            // Assert
            this._parameterViewModel.OriginX.Should().Be(Math.Round(OriginX, 1));
            this._parameterViewModel.OriginY.Should().Be(Math.Round(OriginY, 1));
            this._parameterViewModel.RelativeXStart.Should().Be(Math.Round(XStart, 1));
            this._parameterViewModel.RelativeXEnd.Should().Be(Math.Round(XEnd, 1));
            this._parameterViewModel.RelativeYStart.Should().Be(Math.Round(YStart, 1));
            this._parameterViewModel.RelativeYEnd.Should().Be(Math.Round(YEnd, 1));
        }

        [TestMethod]
        public void SetsOriginValueToMinValues()
        {
            // Arrange
            const int Value = -100;
            this._parameterViewModel.OriginX = -Value;
            this._parameterViewModel.OriginY = -Value;

            // Act
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;

            // Assert
            this._parameterViewModel.OriginX.Should().Be(Settings.Default.XMin);
            this._parameterViewModel.OriginY.Should().Be(Settings.Default.YMin);
        }

        [TestMethod]
        public void SetsOriginValueToMaxValues()
        {
            // Arrange
            const int Value = 500;

            // Act
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;

            // Assert
            this._parameterViewModel.OriginX.Should().Be(Settings.Default.XMax);
            this._parameterViewModel.OriginY.Should().Be(Settings.Default.YMax);
        }

        [TestMethod]
        public void SetsOriginValues()
        {
            // Arrange
            const int Value = 50;

            // Act
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;

            // Assert
            this._parameterViewModel.OriginX.Should().Be(Value);
            this._parameterViewModel.OriginY.Should().Be(Value);
        }

        [TestMethod]
        public void ChangesStartEndValuesWhenOriginChanges()
        {
            // Arrange
            const double Value = 10;
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;
            this._parameterViewModel.RelativeXStart = -Value;
            this._parameterViewModel.RelativeYStart = -Value;
            this._parameterViewModel.RelativeXEnd = -Value;
            this._parameterViewModel.RelativeYEnd = -Value;

            // Act
            this._parameterViewModel.OriginX = 0;
            this._parameterViewModel.OriginY = 0;

            // Assert
            this._parameterViewModel.RelativeXStart.Should().Be(0);
            this._parameterViewModel.RelativeYStart.Should().Be(0);
            this._parameterViewModel.RelativeYEnd.Should().Be(1);
            this._parameterViewModel.RelativeYEnd.Should().Be(1);
        }

        [TestMethod]
        public void ChangesEndValuesWhenOriginChanges()
        {
            // Arrange
            const double Value = 100;
            this._parameterViewModel.RelativeXEnd = Settings.Default.XMax;
            this._parameterViewModel.RelativeYEnd = Settings.Default.YMax;
            this._parameterViewModel.RelativeXStart = Settings.Default.XMax;
            this._parameterViewModel.RelativeYStart = Settings.Default.YMax;

            // Act
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;

            // Assert
            this._parameterViewModel.RelativeXStart.Should().Be(Settings.Default.XMax - 1 - Value);
            this._parameterViewModel.RelativeYStart.Should().Be(Settings.Default.YMax - 1 - Value);
            this._parameterViewModel.RelativeXEnd.Should().Be(Settings.Default.XMax - Value);
            this._parameterViewModel.RelativeYEnd.Should().Be(Settings.Default.YMax - Value);
        }

        [TestMethod]
        public void RecalculatesXStepsWhenXStartChanges()
        {
            // Arrange
            double XStart = ModelFactory.DefaultXStart / 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeXStart = XStart;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XSteps);
            this._parameterViewModel.XSteps.Should().Be(Math.Round((ModelFactory.DefaultXEnd - XStart) / this._parameterViewModel.DeltaX) + 1);
        }

        [TestMethod]
        public void RaisesPropertyChangedForXDifferenceWhenXStartChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeXStart = 10;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XDifference);
        }

        [TestMethod]
        public void RecalculatesXStepsWhenDeltaXChanges()
        {
            // Arrange
            const int DeltaX = 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.DeltaX = DeltaX;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XSteps);
            this._parameterViewModel.XSteps.Should().Be(Math.Round((ModelFactory.DefaultXEnd - ModelFactory.DefaultXStart) / DeltaX) + 1);
            this._parameterViewModel.DeltaX.Should().Be((ModelFactory.DefaultXEnd - ModelFactory.DefaultXStart) / (this._parameterViewModel.XSteps - 1));
        }

        [TestMethod]
        public void RecalculatesDeltaXWhenXStepsChanges()
        {
            // Arrange
            const int XSteps = 3;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.XSteps = XSteps;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaX);
            this._parameterViewModel.DeltaX.Should().Be((ModelFactory.DefaultXEnd - ModelFactory.DefaultXStart) / (XSteps - 1));
        }

        [TestMethod]
        public void RecalculatesXStepsWhenXEndChanges()
        {
            // Arrange
            double XEnd = ModelFactory.DefaultXEnd * 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeXEnd = XEnd;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XSteps);
            this._parameterViewModel.XSteps.Should().Be(Math.Round((XEnd - ModelFactory.DefaultXStart) / this._parameterViewModel.DeltaX) + 1);
        }

        [TestMethod]
        public void RaisesPropertyChangedForXDifferenceWhenXEndChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeXEnd = ModelFactory.DefaultXEnd * 3;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XDifference);
        }

        [TestMethod]
        public void RecalculatesYStepsWhenYStartChanges()
        {
            // Arrange
            double YStart = ModelFactory.DefaultYStart / 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeYStart = YStart;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YSteps);
            this._parameterViewModel.YSteps.Should().Be(Math.Round((ModelFactory.DefaultYEnd - YStart) / this._parameterViewModel.DeltaY) + 1);
        }

        [TestMethod]
        public void RaisesPropertyChangedForYDifferenceWhenYStartChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeYStart = 10;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YDifference);
        }

        [TestMethod]
        public void RecalculatesYStepsWhenDeltaYChanges()
        {
            // Arrange
            const int DeltaY = 3;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.DeltaY = DeltaY;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YSteps);
            this._parameterViewModel.YSteps.Should().Be(Math.Round((ModelFactory.DefaultYEnd - ModelFactory.DefaultYStart) / DeltaY) + 1);
            this._parameterViewModel.DeltaY.Should().Be((ModelFactory.DefaultYEnd - ModelFactory.DefaultYStart) / (this._parameterViewModel.YSteps - 1));
        }

        [TestMethod]
        public void RecalculatesDeltaYWhenYStepsChanges()
        {
            // Arrange
            const int YSteps = 4;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.YSteps = YSteps;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaY);
            this._parameterViewModel.DeltaY.Should().Be((ModelFactory.DefaultYEnd - ModelFactory.DefaultYStart) / (YSteps - 1));
        }

        [TestMethod]
        public void RecalculatesYStepsWhenYEndChanges()
        {
            // Arrange
            double YEnd = ModelFactory.DefaultYEnd * 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeYEnd = YEnd;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YSteps);
            this._parameterViewModel.YSteps.Should().Be(Math.Round((YEnd - ModelFactory.DefaultYStart) / this._parameterViewModel.DeltaY) + 1);
        }

        [TestMethod]
        public void RaisesPropertyChangedForYDifferenceWhenYEndChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeYEnd = 10;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YDifference);
        }

        [TestMethod]
        public void RecalculatesDeltaFreqWhenResolutionBWChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.ResBW = ResBW.BW_200_Hz;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaFreq);
            this._parameterViewModel.DeltaFreq.Should().Be((double)ResBW.BW_200_Hz / 4);
        }

        [TestMethod]
        public void SetsDeltaValuesToMinValue()
        {
            // Act
            this._parameterViewModel.DeltaX = 0.2;
            this._parameterViewModel.DeltaY = 0.2;

            // Assert
            this._parameterViewModel.DeltaX.Should().Be(1);
            this._parameterViewModel.DeltaY.Should().Be(1);
        }

        [TestMethod]
        public void SetsDeltaValuesToMaxDifference()
        {
            // Act
            this._parameterViewModel.DeltaX = 500;
            this._parameterViewModel.DeltaY = 500;

            // Assert
            this._parameterViewModel.DeltaX.Should().Be(this._parameterViewModel.XDifference);
            this._parameterViewModel.DeltaY.Should().Be(this._parameterViewModel.YDifference);
        }

        [TestMethod]
        public void SetsFreqStartToMinFreq()
        {
            // Act
            this._parameterViewModel.FreqStart = 0;

            // Assert
            this._parameterViewModel.FreqStart.Should().Be(1);
        }

        [TestMethod]
        public void SetsFreqStartToMaxFreq()
        {
            // Act
            this._parameterViewModel.FreqStart = 9999999999;

            // Assert
            this._parameterViewModel.FreqStart.Should().Be(this._parameterViewModel.FreqEnd / 1e1);
        }

        [TestMethod]
        public void SetsFreqEndToMinFreq()
        {
            // Act
            this._parameterViewModel.FreqEnd = 0;

            // Assert
            this._parameterViewModel.FreqEnd.Should().Be(this._parameterViewModel.FreqStart * 1e1);
        }

        [TestMethod]
        public void SetsMinValueForMeasureTime()
        {
            // Act
            this._parameterViewModel.MeasureTime = 0;

            // Assert
            this._parameterViewModel.MeasureTime.Should().Be(1);
        }

        [TestMethod]
        public void NoChangesAfterInit()
        {
            // Assert
            this._parameterViewModel.ResetChangesCommand.CanExecute(null).Should().BeFalse();
            this._parameterViewModel.SaveChangesCommand.CanExecute(null).Should().BeFalse();
        }

        [TestMethod]
        public void RecognizesChangedValues()
        {
            // Act
            this._parameterViewModel.OriginX = 100;

            // Assert
            this._parameterViewModel.ResetChangesCommand.CanExecute(null).Should().BeTrue();
            this._parameterViewModel.SaveChangesCommand.CanExecute(null).Should().BeTrue();
        }

        [TestMethod]
        public void ResetsDefaultValues()
        {
            // Arrange
            const double OriginX = 200;
            const double OriginY = 150;

            this._parameterViewModel.OriginX = OriginX;
            this._parameterViewModel.OriginY = OriginY;

            // Act
            this._parameterViewModel.ResetChangesCommand.Execute(null);

            // Assert
            this._parameterViewModel.OriginX.Should().Be(ModelProperties.Default.OriginX);
            this._parameterViewModel.OriginY.Should().Be(ModelProperties.Default.OriginY);
        }

        #endregion
    }
}