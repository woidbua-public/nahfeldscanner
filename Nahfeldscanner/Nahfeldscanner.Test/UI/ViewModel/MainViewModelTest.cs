﻿// ///////////////////////////////////
// File: MainViewModelTest.cs
// Last Change: 24.01.2018  12:54
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.ComponentModel;
    using FluentAssertions;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using Nahfeldscanner.UI.ViewModel;



    [TestClass]
    public class MainViewModelTest
    {
        #region Fields

        private string oldPath = string.Empty;

        private Mock<IRepository> mockRepository;
        private Mock<IDialogService> mockDialogService;
        private MainViewModel mainViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this.oldPath = Settings.Default.DatabaseFilePath;
            Settings.Default.DatabaseFilePath = string.Empty;

            this.mockRepository = new Mock<IRepository>();
            this.mockDialogService = new Mock<IDialogService>();
            this.mainViewModel = new MainViewModel(this.mockRepository.Object, this.mockDialogService.Object);
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this.mockRepository = null;
            this.mockDialogService = null;
            this.mainViewModel = null;

            DatabaseFactory.DeleteTestSqLiteDatabase();

            Settings.Default.DatabaseFilePath = this.oldPath;
            Settings.Default.Save();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void TriesToConnectAtStartup()
        {
            // Arrange
            DatabaseFactory.CreateTestFile();
            DatabaseFactory.SetDefaultSqLiteDatabaseFilePathInSettings();

            // Act
            this.mainViewModel = new MainViewModel(this.mockRepository.Object, this.mockDialogService.Object);

            // Assert
            this.mockRepository.Verify(x => x.LoadDatabase(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void DoesNotThrowExceptionWhenEmptyDatabasePathAtStartup()
        {
            // Arrange
            this.mockRepository.Setup(x => x.LoadDatabase(It.IsAny<string>())).Throws<Exception>();

            // Act
            Action action = () => this.mainViewModel = new MainViewModel(this.mockRepository.Object, this.mockDialogService.Object);

            // Assert
            action.ShouldNotThrow<Exception>();
        }

        [TestMethod]
        public void LoadsDatabase()
        {
            // Arrange
            this.SetupDatabaseForLoading();

            // Act
            this.mainViewModel.LoadDatabaseCommand.Execute(null);

            // Assert
            this.mockRepository.Verify(x => x.LoadDatabase(DatabaseFactory.SqLiteFilePath), Times.Once);
        }

        [TestMethod]
        public void DoesNotLoadDatabaseByEmptyPath()
        {
            // Arrange
            this.mockDialogService.Setup(x => x.ShowDatabaseFileDialog()).Returns(string.Empty);
            DatabaseFactory.CreateTestFile();

            // Act
            this.mainViewModel.LoadDatabaseCommand.Execute(null);

            // Assert
            this.mainViewModel.Title.Should().Be(Resources.Application_Title);
        }

        [TestMethod]
        public void ShowMessageWhenDatabaseCouldNotBeLoaded()
        {
            // Arrange
            this.SetupDatabaseForLoading();
            this.mockRepository.Setup(x => x.LoadDatabase(It.IsAny<string>())).Throws<Exception>();

            // Act
            this.mainViewModel.LoadDatabaseCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Exception_Message_CouldNotLoadDatabase, It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void ShowMessageWithInnerExceptionWhenDatabaseCouldNotBeLoaded()
        {
            // Arrange
            NullReferenceException nullReferenceException = new NullReferenceException("NullReferenceException occured.");
            Exception exception = new Exception("Exception occured.", nullReferenceException);

            this.SetupDatabaseForLoading();
            this.mockRepository.Setup(x => x.LoadDatabase(It.IsAny<string>())).Throws(exception);

            // Act
            this.mainViewModel.LoadDatabaseCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Exception_Message_CouldNotLoadDatabase, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void CreatesDatabase()
        {
            // Arrange
            this.SetupDatabaseForCreation();

            // Act
            this.mainViewModel.CreateDatabaseCommand.Execute(null);

            // Assert
            this.mockRepository.Verify(x => x.RecreateDatabase(DatabaseFactory.SqLiteFilePath), Times.Once);
        }

        [TestMethod]
        public void DoesNotCreateDatabaseByEmptyPath()
        {
            // Arrange
            this.mockDialogService.Setup(x => x.ShowSaveDatabaseFileDialog()).Returns(string.Empty);

            // Act
            this.mainViewModel.CreateDatabaseCommand.Execute(null);

            // Assert
            this.mockRepository.Verify(x => x.RecreateDatabase(It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void ShowMessageWhenDatabaseCouldNotBeCreated()
        {
            // Arrange
            this.SetupDatabaseForCreation();
            this.mockRepository.Setup(x => x.RecreateDatabase(It.IsAny<string>())).Throws<Exception>();

            // Act
            this.mainViewModel.CreateDatabaseCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Exception_Message_CouldNotCreateDatabase, It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void ShowMessageWithInnerExceptionWhenDatabaseCouldNotBeCreated()
        {
            // Arrange
            NullReferenceException nullReferenceException = new NullReferenceException("NullReferenceException occured.");
            Exception exception = new Exception("Exception occured.", nullReferenceException);

            this.SetupDatabaseForCreation();
            this.mockRepository.Setup(x => x.RecreateDatabase(It.IsAny<string>())).Throws(exception);

            // Act
            this.mainViewModel.CreateDatabaseCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Exception_Message_CouldNotCreateDatabase, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void ApplicationTitleWithoutLoadedPath()
        {
            // Assert
            this.mainViewModel.Title.Should().Be(Resources.Application_Title);
        }

        [TestMethod]
        public void ApplicationTitleWithLoadedPath()
        {
            // Act
            this.SetupDatabaseForLoading();

            // Assert
            this.mainViewModel.Title.Should().Be(Resources.Application_Title + " - " + DatabaseFactory.SqLiteFilePath);
        }

        [TestMethod]
        public void RaisePropertyChangedWhenDatabaseCreated()
        {
            // Arrange
            this.SetupDatabaseForCreation();

            int counter = 0;
            this.mainViewModel.MonitorEvents<INotifyPropertyChanged>();
            this.mainViewModel.PropertyChanged += (sender, e) => counter++;

            // Act
            this.mainViewModel.CreateDatabaseCommand.Execute(null);

            // Assert
            counter.Should().Be(2);
            this.mainViewModel.ShouldRaisePropertyChangeFor(x => x.Title);
            this.mainViewModel.ShouldRaisePropertyChangeFor(x => x.IsConnected);
        }

        [TestMethod]
        public void RaisePropertyChangedWhenDatabaseLoaded()
        {
            // Arrange
            this.SetupDatabaseForLoading();

            int counter = 0;
            this.mainViewModel.MonitorEvents<INotifyPropertyChanged>();
            this.mainViewModel.PropertyChanged += (sender, e) => counter++;

            // Act
            this.mainViewModel.LoadDatabaseCommand.Execute(null);

            // Assert
            counter.Should().Be(2);
            this.mainViewModel.ShouldRaisePropertyChangeFor(x => x.Title);
            this.mainViewModel.ShouldRaisePropertyChangeFor(x => x.IsConnected);
        }

        [TestMethod]
        public void WorkspacesInitializedAfterCreation()
        {
            // Assert
            this.mainViewModel.WorkspaceViewModels.Count.Should().Be(4);
            this.mainViewModel.CurrentWorkspaceViewModel.Should().NotBeNull();
            this.mainViewModel.CurrentWorkspaceViewModel.Should().BeOfType<SetupViewModel>();
        }

        [TestMethod]
        public void ChangesToScanViewModelOnNotificationMessage()
        {
            // Act
            Messenger.Default.Send(new NotificationMessage(Resources.Messenger_LoadScanVMForMainVM));

            // Assert
            this.mainViewModel.CurrentWorkspaceViewModel.Should().BeOfType<ScanViewModel>();
        }

        [TestMethod]
        public void CanChangeWorkspaceAfterInit()
        {
            // Assert
            this.mainViewModel.CanChangeWorkspace.Should().BeTrue();
        }

        [TestMethod]
        public void CanNotChangeWorkspaceWhenNotificationMessageReceived()
        {
            // Act
            Messenger.Default.Send(new NotificationMessage<bool>(false, Resources.Messenger_ChangeWorkspaceEnableStateForMainVM));

            // Assert
            this.mainViewModel.CanChangeWorkspace.Should().BeFalse();
        }

        [TestMethod]
        public void CanChangeWorkspaceWhenNotificationMessageReceived()
        {
            // Act
            Messenger.Default.Send(new NotificationMessage<bool>(false, Resources.Messenger_ChangeWorkspaceEnableStateForMainVM));
            Messenger.Default.Send(new NotificationMessage<bool>(true, Resources.Messenger_ChangeWorkspaceEnableStateForMainVM));

            // Assert
            this.mainViewModel.CanChangeWorkspace.Should().BeTrue();
        }

        [TestMethod]
        public void RaisesPropertyChangedWhenWorkspaceChangeEnableStateChanges()
        {
            // Arrange
            this.mainViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            Messenger.Default.Send(new NotificationMessage<bool>(false, Resources.Messenger_ChangeWorkspaceEnableStateForMainVM));

            // Assert
            this.mainViewModel.ShouldRaisePropertyChangeFor(x => x.CanChangeWorkspace);
        }

        [TestMethod]
        public void SetOriginWhenWorkspaceChangesToSetup()
        {
            // Arrange
            const double OriginX = 10;
            const double OriginY = 15;

            this.mainViewModel.CurrentWorkspaceViewModel = this.mainViewModel.WorkspaceViewModels[3];
            ((SetupViewModel)this.mainViewModel.WorkspaceViewModels[0]).ParameterViewModel.OriginX = OriginX;
            ((SetupViewModel)this.mainViewModel.WorkspaceViewModels[0]).ParameterViewModel.OriginY = OriginY;
            Messenger.Default.Send(new NotificationMessage<Tuple<double?, double?>>(new Tuple<double?, double?>(OriginX + 10, OriginY - 10), Resources.Messenger_SetOriginForSerialPortVM));

            // Act
            this.mainViewModel.CurrentWorkspaceViewModel = this.mainViewModel.WorkspaceViewModels[0];

            // Assert
            ((SetupViewModel)this.mainViewModel.CurrentWorkspaceViewModel).SerialPortViewModel.OriginX.Should().Be(OriginX);
            ((SetupViewModel)this.mainViewModel.CurrentWorkspaceViewModel).SerialPortViewModel.OriginY.Should().Be(OriginY);
        }

        [TestMethod]
        public void SetOriginWhenWorkspaceChangesToScan()
        {
            // Arrange
            const double OriginX = 10;
            const double OriginY = 15;

            this.mainViewModel.CurrentWorkspaceViewModel = this.mainViewModel.WorkspaceViewModels[3];
            ((ScanViewModel)this.mainViewModel.WorkspaceViewModels[1]).CreateNewAnalysisCommand.RelayCommand.Execute(null);
            ((ScanViewModel)this.mainViewModel.WorkspaceViewModels[1]).ParameterViewModel.OriginX = OriginX;
            ((ScanViewModel)this.mainViewModel.WorkspaceViewModels[1]).ParameterViewModel.OriginY = OriginY;
            Messenger.Default.Send(new NotificationMessage<Tuple<double?, double?>>(new Tuple<double?, double?>(OriginX + 10, OriginY - 10), Resources.Messenger_SetOriginForSerialPortVM));

            // Act
            this.mainViewModel.CurrentWorkspaceViewModel = this.mainViewModel.WorkspaceViewModels[1];

            // Assert
            ((ScanViewModel)this.mainViewModel.CurrentWorkspaceViewModel).SerialPortViewModel.OriginX.Should().Be(OriginX);
            ((ScanViewModel)this.mainViewModel.CurrentWorkspaceViewModel).SerialPortViewModel.OriginY.Should().Be(OriginY);
        }

        [TestMethod]
        public void SendsClearLoadedAnalysisMesage()
        {
            // Arrange
            NotificationMessage notificationMessage = null;
            this.SetupDatabaseForLoading();

            Messenger.Default.Register<NotificationMessage>(this, x => notificationMessage = x);

            // Act
            this.mainViewModel.LoadDatabaseCommand.Execute(null);

            // Assert
            notificationMessage.Should().NotBeNull();
            notificationMessage.Notification.Should().Be(Resources.Messenger_ClearLoadedAnalysisForScanVM);
        }

        #endregion



        private void SetupDatabaseForLoading()
        {
            this.mockDialogService.Setup(x => x.ShowDatabaseFileDialog()).Returns(DatabaseFactory.SqLiteFilePath);
            this.mockRepository.Setup(x => x.DatabasePath).Returns(DatabaseFactory.SqLiteFilePath);

            DatabaseFactory.CreateTestFile();
        }

        private void SetupDatabaseForCreation()
        {
            this.mockDialogService.Setup(x => x.ShowSaveDatabaseFileDialog()).Returns(DatabaseFactory.SqLiteFilePath);
            this.mockRepository.Setup(x => x.DatabasePath).Returns(DatabaseFactory.SqLiteFilePath);
        }
    }
}