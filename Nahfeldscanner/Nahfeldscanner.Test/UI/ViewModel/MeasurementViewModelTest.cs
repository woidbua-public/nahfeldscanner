﻿// ///////////////////////////////////
// File: MeasurementViewModelTest.cs
// Last Change: 23.01.2018  10:32
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.ViewModel;



    [TestClass]
    public class MeasurementViewModelTest
    {
        #region Fields

        private MeasurementViewModel _measurementViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this._measurementViewModel = new MeasurementViewModel(ModelFactory.GetDefaultMeasurement(), ModelFactory.GetDefaultMeasurementValue(),
                                                                  ModelFactory.GetDefaultMeasurementValue(), ModelFactory.GetDefaultMeasurementValue(),
                                                                  DetectorType.Max_Peak, 0, 0);
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this._measurementViewModel = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void PropertiesInitializedAfterCreation()
        {
            // Assert
            this._measurementViewModel.Id.Should().Be(0);
            this._measurementViewModel.X.Should().Be(ModelFactory.DefaultX);
            this._measurementViewModel.Y.Should().Be(ModelFactory.DefaultY);
            this._measurementViewModel.CurrentFrequency.Should().Be(ModelFactory.DefaultFrequency);
            this._measurementViewModel.CurrentIntensity.Should().Be(ModelFactory.DefaultIntensityMaxDetector);
            this._measurementViewModel.MinFrequency.Should().Be(ModelFactory.DefaultFrequency);
            this._measurementViewModel.MinIntensity.Should().Be(ModelFactory.DefaultIntensityMaxDetector);
            this._measurementViewModel.MaxFrequency.Should().Be(ModelFactory.DefaultFrequency);
            this._measurementViewModel.MaxIntensity.Should().Be(ModelFactory.DefaultIntensityMaxDetector);
        }

        [TestMethod]
        public void RaisesPropertyChangedWhenCurrentValuesChange()
        {
            // Arrange
            const double Frequency = 1234;
            const double Intensity = 1132;
            MeasurementValue measurementValue = new MeasurementValue
                                                {
                                                        Frequency = Frequency,
                                                        IntensityMaxDetector = 2345235,
                                                        IntensityAVGDetector = Intensity,
                                                        IntensityQPDetector = 1234141
                                                };

            List<string> changedProperties = new List<string>();
            this._measurementViewModel.PropertyChanged += (sender, args) => changedProperties.Add(args.PropertyName);

            // Act
            this._measurementViewModel.SetCurrentMeasurementValue(measurementValue, DetectorType.Average);

            // Assert
            changedProperties.Should().HaveCount(2);
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<MeasurementViewModel>(x => x.CurrentFrequency));
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<MeasurementViewModel>(x => x.CurrentIntensity));
            this._measurementViewModel.CurrentFrequency.Should().Be(Frequency);
            this._measurementViewModel.CurrentIntensity.Should().Be(Intensity);
        }

        [TestMethod]
        public void RaisesPropertyChangedWhenMinMeasurementValueChanges()
        {
            // Arrange
            const double Frequency = 1234;
            const double Intensity = 1132;
            MeasurementValue measurementValue = new MeasurementValue
                                                {
                                                    Frequency = Frequency,
                                                        IntensityMaxDetector = 6546,
                                                        IntensityAVGDetector = Intensity,
                                                        IntensityQPDetector = 2345235
                                                };

            List<string> changedProperties = new List<string>();
            this._measurementViewModel.PropertyChanged += (sender, args) => changedProperties.Add(args.PropertyName);

            // Act
            this._measurementViewModel.SetMinMeasurementValue(measurementValue, DetectorType.Average);

            // Assert
            changedProperties.Should().HaveCount(2);
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<MeasurementViewModel>(x => x.MinFrequency));
            this._measurementViewModel.MinFrequency.Should().Be(Frequency);
            this._measurementViewModel.MinIntensity.Should().Be(Intensity);
        }

        [TestMethod]
        public void RaisesPropertyChangedWhenMaxMeasurementValueChanges()
        {
            // Arrange
            const double Frequency = 1234;
            const double Intensity = 1132;
            MeasurementValue measurementValue = new MeasurementValue
                                                {
                                                        Frequency = Frequency,
                                                        IntensityMaxDetector = 6546,
                                                        IntensityAVGDetector = Intensity,
                                                        IntensityQPDetector = 2345235
                                                };

            List<string> changedProperties = new List<string>();
            this._measurementViewModel.PropertyChanged += (sender, args) => changedProperties.Add(args.PropertyName);

            // Act
            this._measurementViewModel.SetMaxMeasurementValue(measurementValue, DetectorType.Average);

            // Assert
            changedProperties.Should().HaveCount(2);
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<MeasurementViewModel>(x => x.MaxFrequency));
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<MeasurementViewModel>(x => x.MaxIntensity));
        }

        [TestMethod]
        public void SelectMinMeasurementValue()
        {
            // Arrange
            const double MinFrequency = 23;
            const double MinMaxIntensity = 1;
            const double MinAVGIntensity = 2;
            const double MinQPIntensity = 3;

            MeasurementValue measurementValue = new MeasurementValue
                                                {
                                                        Frequency = MinFrequency,
                                                        IntensityMaxDetector = MinMaxIntensity,
                                                        IntensityAVGDetector = MinAVGIntensity,
                                                        IntensityQPDetector = MinQPIntensity
                                                };

            this._measurementViewModel.SetMinMeasurementValue(measurementValue, DetectorType.Average);

            // Act
            this._measurementViewModel.SelectMinMeasurementValue();

            // Assert
            this._measurementViewModel.CurrentFrequency.Should().Be(this._measurementViewModel.MinFrequency);
            this._measurementViewModel.CurrentIntensity.Should().Be(this._measurementViewModel.MinIntensity);
        }

        [TestMethod]
        public void SelectMaxMeasurementValue()
        {
            // Arrange
            const double MaxFrequency = 23;
            const double MaxMaxIntensity = 1000;
            const double MaxAVGIntensity = 342;
            const double MaxQPIntensity = 35235;

            MeasurementValue measurementValue = new MeasurementValue
                                                {
                                                        Frequency = MaxFrequency,
                                                        IntensityMaxDetector = MaxMaxIntensity,
                                                        IntensityAVGDetector = MaxAVGIntensity,
                                                        IntensityQPDetector = MaxQPIntensity
                                                };

            this._measurementViewModel.SetMaxMeasurementValue(measurementValue, DetectorType.Quasipeak);

            // Act
            this._measurementViewModel.SelectMaxMeasurementValue();

            // Assert
            this._measurementViewModel.CurrentFrequency.Should().Be(this._measurementViewModel.MaxFrequency);
            this._measurementViewModel.CurrentIntensity.Should().Be(this._measurementViewModel.MaxIntensity);
        }

        #endregion
    }
}