﻿// ///////////////////////////////////
// File: ParameterViewModelTest.cs
// Last Change: 24.01.2018  12:30
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using FluentAssertions;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.ViewModel;
    using ModelProperty = Nahfeldscanner.Model.Properties;



    // ReSharper disable PossibleInvalidOperationException
    [TestClass]
    public class ParameterViewModelTest
    {
        #region Fields

        private const int AmountAnalysis = 1;
        private const int AmountMeasurement = 1;
        private const int AmountMeasurementValue = 1;
        private const int AmountSensor = 10;
        private const int XMax = 5;

        private SqLiteRepository _repository;
        private ParameterViewModel _parameterViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this._repository = new SqLiteRepository(new SqLiteSessionManager());

            DatabaseFactory.InitDatabase(this._repository,
                                         AmountAnalysis, AmountMeasurement, AmountMeasurementValue, AmountSensor, XMax);

            DatabaseFactory.SetDefaultParameterSettings();

            this._parameterViewModel = new ParameterViewModel(this._repository, ModelFactory.GetDefaultParameter());
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this._repository = null;
            this._parameterViewModel = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void NumberOfProperties()
        {
            this._parameterViewModel.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(prop => prop.DeclaringType == this._parameterViewModel.GetType()).ToList().Count.Should().Be(36);
        }

        [TestMethod]
        public void ReturnsTrueWhenAllParameterValuesSet()
        {
            // Arrange
            ModelProperty.Settings.Default.SensorName = "test";
            ModelProperty.Settings.Default.Save();

            // Assert
            this._parameterViewModel.ValuesSet.Should().BeTrue();
        }

        [TestMethod]
        public void ReturnsFalseWhenNotAllParameterValuesSet()
        {
            // Act
            this._parameterViewModel.RelativeXStart = null;

            // Assert
            this._parameterViewModel.ValuesSet.Should().BeFalse();
        }

        [TestMethod]
        public void InitSensorsAndSetSelectedToDefaultSensor()
        {
            // Assert
            this._parameterViewModel.SelectedSensorViewModel.Should().NotBeNull();
            this._parameterViewModel.SelectedSensorViewModel.SensorName.Should().Be(ModelFactory.DefaultSensorName);
        }

        [TestMethod]
        public void InitSensorsAndSetSelectedToFirstElement()
        {
            // Arrange
            Parameter parameter = ModelFactory.GetDefaultParameter();
            parameter.SensorName = null;

            // Act
            this._parameterViewModel = new ParameterViewModel(this._repository, parameter);

            // Assert
            this._parameterViewModel.SelectedSensorViewModel.Should().NotBeNull();
            this._parameterViewModel.SelectedSensorViewModel.SensorName.Should().Be(ModelFactory.DefaultSensorName.Substring(0, ModelFactory.DefaultSensorName.Length - 1) + "1");
        }

        [TestMethod]
        public void InitSensorsAndClearSensorDataWhenNoSensorInDatabase()
        {
            // Arrange
            DatabaseFactory.InitDatabase(this._repository,
                                         AmountAnalysis, AmountMeasurement, AmountMeasurementValue, 0, XMax);

            // Act
            this._parameterViewModel = new ParameterViewModel(this._repository, ModelFactory.GetDefaultParameter());

            // Assert
            this._parameterViewModel.SelectedSensorViewModel.Should().BeNull();
            this._parameterViewModel.SensorName.Should().BeNullOrEmpty();
            this._parameterViewModel.SensorType.Should().BeNull();
            this._parameterViewModel.SensorImageStream.Should().BeNull();
        }

        [TestMethod]
        public void ReloadsSensors()
        {
            // Arrange
            int deletedSensorViewModelId = this._parameterViewModel.Sensors[0].ID;
            SensorViewModel newSensorViewModel = new SensorViewModel(new Sensor(null, SensorType.E_Field, null));

            this._parameterViewModel.Sensors.RemoveAt(0);
            this._parameterViewModel.Sensors.Add(newSensorViewModel);

            // Act
            this._parameterViewModel.ReloadSensorsCommand.Execute(null);

            // Assert
            this._parameterViewModel.Sensors.Should().HaveCount(AmountSensor);
            this._parameterViewModel.Sensors.Should().Contain(x => x.ID == deletedSensorViewModelId);
        }

        [TestMethod]
        public void GetDefaultParameterObjectProperties()
        {
            // Assert
            this._parameterViewModel.CanEdit.Should().Be(false);
            this._parameterViewModel.ID.Should().Be(0);
            this._parameterViewModel.DoQuasipeakMeasurement.Should().Be(ModelProperty.Settings.Default.DoQuasipeakMeasurement);
            this._parameterViewModel.OriginX.Should().Be(ModelProperty.Settings.Default.OriginX);
            this._parameterViewModel.OriginY.Should().Be(ModelProperty.Settings.Default.OriginY);
            this._parameterViewModel.AbsoluteXStart.Should().Be(ModelProperty.Settings.Default.XStart + ModelProperty.Settings.Default.OriginX);
            this._parameterViewModel.RelativeXStart.Should().Be(ModelProperty.Settings.Default.XStart);
            this._parameterViewModel.DeltaX.Should().Be(ModelProperty.Settings.Default.DeltaX);
            this._parameterViewModel.XSteps.Should().Be(this._parameterViewModel.XDifference / this._parameterViewModel.DeltaX + 1);
            this._parameterViewModel.XDifference.Should().Be(this._parameterViewModel.RelativeXEnd - this._parameterViewModel.RelativeXStart);
            this._parameterViewModel.AbsoluteXEnd.Should().Be(ModelProperty.Settings.Default.XEnd + ModelProperty.Settings.Default.OriginX);
            this._parameterViewModel.RelativeXEnd.Should().Be(ModelProperty.Settings.Default.XEnd);
            this._parameterViewModel.AbsoluteYStart.Should().Be(ModelProperty.Settings.Default.YStart + ModelProperty.Settings.Default.OriginY);
            this._parameterViewModel.RelativeYStart.Should().Be(ModelProperty.Settings.Default.YStart);
            this._parameterViewModel.DeltaY.Should().Be(ModelProperty.Settings.Default.DeltaY);
            this._parameterViewModel.YSteps.Should().Be(this._parameterViewModel.YDifference / this._parameterViewModel.DeltaY + 1);
            this._parameterViewModel.YDifference.Should().Be(this._parameterViewModel.RelativeYEnd - this._parameterViewModel.RelativeYStart);
            this._parameterViewModel.AbsoluteYEnd.Should().Be(ModelProperty.Settings.Default.YEnd + ModelProperty.Settings.Default.OriginY);
            this._parameterViewModel.RelativeYEnd.Should().Be(ModelProperty.Settings.Default.YEnd);
            this._parameterViewModel.FreqStart.Should().Be(ModelProperty.Settings.Default.FreqStart);
            this._parameterViewModel.AbsoluteDeltaXLines.Count.Should().Be((int)this._parameterViewModel.XSteps);
            this._parameterViewModel.AbsoluteDeltaYLines.Count.Should().Be((int)this._parameterViewModel.YSteps);
            this._parameterViewModel.DeltaFreq.Should().Be((double)this._parameterViewModel.ResBW / 4);
            this._parameterViewModel.FreqEnd.Should().Be(ModelProperty.Settings.Default.FreqEnd);
            this._parameterViewModel.ResBW.Should().Be((ResBW)ModelProperty.Settings.Default.ResBWIndex);
            this._parameterViewModel.MeasureTime.Should().Be(ModelProperty.Settings.Default.MeasureTime);
            this._parameterViewModel.Orientation.Should().Be((Orientation)ModelProperty.Settings.Default.OrientationIndex);
            this._parameterViewModel.SensorType.Should().Be((SensorType)ModelProperty.Settings.Default.SensorTypeIndex);
            this._parameterViewModel.SensorImageStream.ShouldBeEquivalentTo(Convert.FromBase64String(ModelProperty.Settings.Default.SensorImageStream));
            this._parameterViewModel.SensorName.Should().Be(ModelProperty.Settings.Default.SensorName);
        }

        [TestMethod]
        public void RaisesPropertyChangedEvents()
        {
            // Arrange
            List<string> propertyNames = new List<string>();
            double originX = 5;
            double originY = 10;
            double xStart = 21;
            double deltaX = 2;
            double xSteps = 4;
            double xEnd = 27;
            double yStart = 31;
            double deltaY = 3;
            double ySteps = 4;
            double yEnd = 40;
            double freqStart = 100;
            double deltaFreq = (double)ResBW.BW_100_Hz / 4;
            double freqEnd = 1000;
            ResBW resBW = ResBW.BW_100_Hz;
            int measureTime = 3;
            Orientation orientation = Orientation.X;
            byte[] sensorImageStream = { 4, 5, 1, 6, 3, 7, 123, 64 };
            string sensorName = ModelFactory.DefaultSensorName.Substring(0, ModelFactory.DefaultSensorName.Length - 1) + "4";

            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();
            this._parameterViewModel.PropertyChanged += (sender, e) => propertyNames.Add(e.PropertyName);

            // Act
            this._parameterViewModel.CanEdit = true;
            this._parameterViewModel.DoQuasipeakMeasurement = false;
            this._parameterViewModel.OriginX = originX;
            this._parameterViewModel.OriginY = originY;
            this._parameterViewModel.RelativeXEnd = xEnd;
            this._parameterViewModel.RelativeXStart = xStart;
            this._parameterViewModel.DeltaX = deltaX;
            this._parameterViewModel.RelativeYEnd = yEnd;
            this._parameterViewModel.RelativeYStart = yStart;
            this._parameterViewModel.DeltaY = deltaY;
            this._parameterViewModel.FreqEnd = freqEnd;
            this._parameterViewModel.FreqStart = freqStart;
            this._parameterViewModel.ResBW = resBW;
            this._parameterViewModel.MeasureTime = measureTime;
            this._parameterViewModel.Orientation = orientation;
            this._parameterViewModel.SelectedSensorViewModel = new SensorViewModel(new Sensor(sensorName, SensorType.E_Field, sensorImageStream));

            // Assert
            this._parameterViewModel.CanEdit.Should().BeTrue();
            this._parameterViewModel.DoQuasipeakMeasurement.Should().BeFalse();
            this._parameterViewModel.OriginX.Should().Be(originX);
            this._parameterViewModel.OriginY.Should().Be(originY);
            this._parameterViewModel.AbsoluteXStart.Should().Be(xStart + originX);
            this._parameterViewModel.RelativeXStart.Should().Be(xStart);
            this._parameterViewModel.DeltaX.Should().Be(deltaX);
            this._parameterViewModel.XSteps.Should().Be(xSteps);
            this._parameterViewModel.XDifference.Should().Be(xEnd - xStart);
            this._parameterViewModel.AbsoluteXEnd.Should().Be(xEnd + originX);
            this._parameterViewModel.RelativeXEnd.Should().Be(xEnd);
            this._parameterViewModel.AbsoluteDeltaXLines.Count.Should().Be((int)this._parameterViewModel.XSteps);
            this._parameterViewModel.AbsoluteYStart.Should().Be(yStart + originY);
            this._parameterViewModel.RelativeYStart.Should().Be(yStart);
            this._parameterViewModel.DeltaY.Should().Be(deltaY);
            this._parameterViewModel.YSteps.Should().Be(ySteps);
            this._parameterViewModel.YDifference.Should().Be(yEnd - yStart);
            this._parameterViewModel.AbsoluteYEnd.Should().Be(yEnd + originY);
            this._parameterViewModel.RelativeYEnd.Should().Be(yEnd);
            this._parameterViewModel.AbsoluteDeltaYLines.Count.Should().Be((int)this._parameterViewModel.YSteps);
            this._parameterViewModel.FreqStart.Should().Be(freqStart);
            this._parameterViewModel.DeltaFreq.Should().Be(deltaFreq);
            this._parameterViewModel.FreqEnd.Should().Be(freqEnd);
            this._parameterViewModel.ResBW.Should().Be(resBW);
            this._parameterViewModel.MeasureTime.Should().Be(measureTime);
            this._parameterViewModel.Orientation.Should().Be(orientation);
            this._parameterViewModel.SensorType.Should().Be(SensorType.E_Field);
            this._parameterViewModel.SensorImageStream.Should().Equal(sensorImageStream);
            this._parameterViewModel.SensorName.Should().Be(sensorName);

            propertyNames.Count.Should().Be(53);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.CanEdit);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DoQuasipeakMeasurement);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.OriginX);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.OriginY);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteXStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.RelativeXStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaX);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XSteps);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XDifference);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteXEnd);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.RelativeXEnd);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteYStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.RelativeYStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaY);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YSteps);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YDifference);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteYEnd);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.RelativeYEnd);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.FreqStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaFreq);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.FreqEnd);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.ResBW);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.MeasureTime);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.Orientation);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.SelectedSensorViewModel);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.SensorType);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.SensorImageStream);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.SensorName);
        }

        [TestMethod]
        public void RoundsValues()
        {
            // Arrange
            const double OriginX = 5.123;
            const double OriginY = 10.2034;
            const double XStart = 21.230;
            const double XEnd = 27.2340243;
            const double YStart = 31.102319;
            const double YEnd = 40.890123;

            DatabaseFactory.SetDefaultParameterSettings();

            // Act
            this._parameterViewModel.OriginX = OriginX;
            this._parameterViewModel.OriginY = OriginY;
            this._parameterViewModel.RelativeXEnd = XEnd;
            this._parameterViewModel.RelativeXStart = XStart;
            this._parameterViewModel.RelativeYEnd = YEnd;
            this._parameterViewModel.RelativeYStart = YStart;

            // Assert
            this._parameterViewModel.OriginX.Should().Be(Math.Round(OriginX, 1));
            this._parameterViewModel.OriginY.Should().Be(Math.Round(OriginY, 1));
            this._parameterViewModel.RelativeXStart.Should().Be(Math.Round(XStart, 1));
            this._parameterViewModel.RelativeXEnd.Should().Be(Math.Round(XEnd, 1));
            this._parameterViewModel.RelativeYStart.Should().Be(Math.Round(YStart, 1));
            this._parameterViewModel.RelativeYEnd.Should().Be(Math.Round(YEnd, 1));
        }

        [TestMethod]
        public void ClearsValues()
        {
            // Act
            this._parameterViewModel.Clear();

            // Assert
            this._parameterViewModel.OriginX.Should().BeNull();
            this._parameterViewModel.OriginY.Should().BeNull();
            this._parameterViewModel.DoQuasipeakMeasurement.Should().BeFalse();
            this._parameterViewModel.AbsoluteXStart.Should().BeNull();
            this._parameterViewModel.RelativeXStart.Should().BeNull();
            this._parameterViewModel.DeltaX.Should().BeNull();
            this._parameterViewModel.XSteps.Should().BeNull();
            this._parameterViewModel.XDifference.Should().BeNull();
            this._parameterViewModel.AbsoluteXEnd.Should().BeNull();
            this._parameterViewModel.RelativeXEnd.Should().BeNull();
            this._parameterViewModel.AbsoluteDeltaXLines.Should().BeEmpty();
            this._parameterViewModel.AbsoluteYStart.Should().BeNull();
            this._parameterViewModel.RelativeYStart.Should().BeNull();
            this._parameterViewModel.DeltaY.Should().BeNull();
            this._parameterViewModel.YSteps.Should().BeNull();
            this._parameterViewModel.YDifference.Should().BeNull();
            this._parameterViewModel.AbsoluteYEnd.Should().BeNull();
            this._parameterViewModel.RelativeYEnd.Should().BeNull();
            this._parameterViewModel.AbsoluteDeltaYLines.Should().BeEmpty();
            this._parameterViewModel.FreqStart.Should().BeNull();
            this._parameterViewModel.FreqEnd.Should().BeNull();
            this._parameterViewModel.MeasureTime.Should().BeNull();
            this._parameterViewModel.Orientation.Should().BeNull();
            this._parameterViewModel.SensorType.Should().BeNull();
            this._parameterViewModel.SensorImageStream.Should().BeNull();
            this._parameterViewModel.SensorName.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void SetStepsToMinValue()
        {
            // Act
            this._parameterViewModel.XSteps = 1;
            this._parameterViewModel.YSteps = 1;

            // Assert
            this._parameterViewModel.XSteps.Should().Be(2);
            this._parameterViewModel.YSteps.Should().Be(2);
            this._parameterViewModel.DeltaX.Should().Be(ModelFactory.DefaultXEnd - ModelFactory.DefaultXStart);
            this._parameterViewModel.DeltaY.Should().Be(ModelFactory.DefaultYEnd - ModelFactory.DefaultYStart);
        }

        [TestMethod]
        public void SetsOriginValueToNull()
        {
            // Act
            this._parameterViewModel.OriginX = null;
            this._parameterViewModel.OriginY = null;

            // Assert
            this._parameterViewModel.OriginX.Should().BeNull();
            this._parameterViewModel.OriginY.Should().BeNull();
        }

        [TestMethod]
        public void SetsOriginValueToMinValues()
        {
            // Arrange
            const int Value = -100;
            this._parameterViewModel.OriginX = -Value;
            this._parameterViewModel.OriginY = -Value;

            // Act
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;

            // Assert
            this._parameterViewModel.OriginX.Should().Be(Settings.Default.XMin);
            this._parameterViewModel.OriginY.Should().Be(Settings.Default.YMin);
        }

        [TestMethod]
        public void SetsOriginValueToMaxValues()
        {
            // Arrange
            const int Value = 500;

            // Act
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;

            // Assert
            this._parameterViewModel.OriginX.Should().Be(Settings.Default.XMax);
            this._parameterViewModel.OriginY.Should().Be(Settings.Default.YMax);
        }

        [TestMethod]
        public void SetsOriginValues()
        {
            // Arrange
            const int Value = 50;

            // Act
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;

            // Assert
            this._parameterViewModel.OriginX.Should().Be(Value);
            this._parameterViewModel.OriginY.Should().Be(Value);
        }

        [TestMethod]
        public void ChangesStartEndValuesWhenOriginChanges()
        {
            // Arrange
            const double Value = 10;
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;
            this._parameterViewModel.RelativeXStart = -Value;
            this._parameterViewModel.RelativeYStart = -Value;
            this._parameterViewModel.RelativeXEnd = -Value;
            this._parameterViewModel.RelativeYEnd = -Value;

            // Act
            this._parameterViewModel.OriginX = 0;
            this._parameterViewModel.OriginY = 0;

            // Assert
            this._parameterViewModel.RelativeXStart.Should().Be(0);
            this._parameterViewModel.RelativeYStart.Should().Be(0);
            this._parameterViewModel.RelativeYEnd.Should().Be(1);
            this._parameterViewModel.RelativeYEnd.Should().Be(1);
        }

        [TestMethod]
        public void ChangesEndValuesWhenOriginChanges()
        {
            // Arrange
            const double Value = 100;
            this._parameterViewModel.RelativeXEnd = Settings.Default.XMax;
            this._parameterViewModel.RelativeYEnd = Settings.Default.YMax;
            this._parameterViewModel.RelativeXStart = Settings.Default.XMax;
            this._parameterViewModel.RelativeYStart = Settings.Default.YMax;

            // Act
            this._parameterViewModel.OriginX = Value;
            this._parameterViewModel.OriginY = Value;

            // Assert
            this._parameterViewModel.RelativeXStart.Should().Be(Settings.Default.XMax - 1 - Value);
            this._parameterViewModel.RelativeYStart.Should().Be(Settings.Default.YMax - 1 - Value);
            this._parameterViewModel.RelativeXEnd.Should().Be(Settings.Default.XMax - Value);
            this._parameterViewModel.RelativeYEnd.Should().Be(Settings.Default.YMax - Value);
        }

        [TestMethod]
        public void RecalculatesXStepsWhenXStartChanges()
        {
            // Arrange
            double XStart = ModelFactory.DefaultXStart / 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeXStart = XStart;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XSteps);
            this._parameterViewModel.XSteps.Should().Be(Math.Round((ModelFactory.DefaultXEnd - XStart) / (double)this._parameterViewModel.DeltaX) + 1);
        }

        [TestMethod]
        public void RaisesPropertyChangedForXDifferenceWhenXStartChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeXStart = 10;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XDifference);
        }

        [TestMethod]
        public void RecalculatesXStepsWhenDeltaXChanges()
        {
            // Arrange
            const int DeltaX = 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.DeltaX = DeltaX;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XSteps);
            this._parameterViewModel.XSteps.Should().Be(Math.Round((ModelFactory.DefaultXEnd - ModelFactory.DefaultXStart) / DeltaX) + 1);
            this._parameterViewModel.DeltaX.Should().Be((ModelFactory.DefaultXEnd - ModelFactory.DefaultXStart) / (this._parameterViewModel.XSteps - 1));
        }

        [TestMethod]
        public void RecalculatesDeltaXWhenXStepsChanges()
        {
            // Arrange
            const int XSteps = 3;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.XSteps = XSteps;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaX);
            this._parameterViewModel.DeltaX.Should().Be((ModelFactory.DefaultXEnd - ModelFactory.DefaultXStart) / (XSteps - 1));
        }

        [TestMethod]
        public void RecalculatesXStepsWhenXEndChanges()
        {
            // Arrange
            double XEnd = ModelFactory.DefaultXEnd * 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeXEnd = XEnd;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XSteps);
            this._parameterViewModel.XSteps.Should().Be(Math.Round((XEnd - ModelFactory.DefaultXStart) / (double)this._parameterViewModel.DeltaX) + 1);
        }

        [TestMethod]
        public void RaisesPropertyChangedForXDifferenceWhenXEndChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeXEnd = ModelFactory.DefaultXEnd * 3;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.XDifference);
        }

        [TestMethod]
        public void RecalculatesYStepsWhenYStartChanges()
        {
            // Arrange
            double YStart = ModelFactory.DefaultYStart / 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeYStart = YStart;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YSteps);
            this._parameterViewModel.YSteps.Should().Be(Math.Round((ModelFactory.DefaultYEnd - YStart) / (double)this._parameterViewModel.DeltaY) + 1);
        }

        [TestMethod]
        public void RaisesPropertyChangedForYDifferenceWhenYStartChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeYStart = 10;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YDifference);
        }

        [TestMethod]
        public void RecalculatesYStepsWhenDeltaYChanges()
        {
            // Arrange
            const int DeltaY = 3;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.DeltaY = DeltaY;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YSteps);
            this._parameterViewModel.YSteps.Should().Be(Math.Round((ModelFactory.DefaultYEnd - ModelFactory.DefaultYStart) / DeltaY) + 1);
            this._parameterViewModel.DeltaY.Should().Be((ModelFactory.DefaultYEnd - ModelFactory.DefaultYStart) / (this._parameterViewModel.YSteps - 1));
        }

        [TestMethod]
        public void RecalculatesDeltaYWhenYStepsChanges()
        {
            // Arrange
            const int YSteps = 4;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.YSteps = YSteps;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaY);
            this._parameterViewModel.DeltaY.Should().Be((ModelFactory.DefaultYEnd - ModelFactory.DefaultYStart) / (YSteps - 1));
        }

        [TestMethod]
        public void RecalculatesYStepsWhenYEndChanges()
        {
            // Arrange
            double YEnd = ModelFactory.DefaultYEnd * 2;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeYEnd = YEnd;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YSteps);
            this._parameterViewModel.YSteps.Should().Be(Math.Round((YEnd - ModelFactory.DefaultYStart) / (double)this._parameterViewModel.DeltaY) + 1);
        }

        [TestMethod]
        public void RaisesPropertyChangedForYDifferenceWhenYEndChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.RelativeYEnd = 10;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.YDifference);
        }

        [TestMethod]
        public void RecalculatesDeltaFreqWhenResolutionBWChanges()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.ResBW = ResBW.BW_200_Hz;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaFreq);
            this._parameterViewModel.DeltaFreq.Should().Be((double)ResBW.BW_200_Hz / 4);
        }

        [TestMethod]
        public void SetsDeltaFreqToNullWhenNoResBWWasSelected()
        {
            // Arrange
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.ResBW = null;

            // Assert
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.DeltaFreq);
            this._parameterViewModel.DeltaFreq.Should().Be(null);
        }

        [TestMethod]
        public void LimitsRelativeStartPositionsWhenEndPositionIsNull()
        {
            // Arrange
            this._parameterViewModel.RelativeXEnd = null;
            this._parameterViewModel.RelativeYEnd = null;

            // Act
            this._parameterViewModel.RelativeXStart = Settings.Default.XMax + 100;
            this._parameterViewModel.RelativeYStart = Settings.Default.YMax + 100;

            // Assert
            this._parameterViewModel.RelativeXStart.Should().Be(Settings.Default.XMax - 1 - this._parameterViewModel.OriginX);
            this._parameterViewModel.RelativeYStart.Should().Be(Settings.Default.YMax - 1 - this._parameterViewModel.OriginY);
        }

        [TestMethod]
        public void SetsDeltaValuesToMinValue()
        {
            // Act
            this._parameterViewModel.DeltaX = 0.2;
            this._parameterViewModel.DeltaY = 0.2;

            // Assert
            this._parameterViewModel.DeltaX.Should().Be(1);
            this._parameterViewModel.DeltaY.Should().Be(1);
        }

        [TestMethod]
        public void SetsDeltaValuesToMaxDifference()
        {
            // Act
            this._parameterViewModel.DeltaX = 500;
            this._parameterViewModel.DeltaY = 500;

            // Assert
            this._parameterViewModel.DeltaX.Should().Be(this._parameterViewModel.XDifference);
            this._parameterViewModel.DeltaY.Should().Be(this._parameterViewModel.YDifference);
        }

        [TestMethod]
        public void SetsRelativePositionsToNull()
        {
            // Act
            this._parameterViewModel.RelativeXStart = null;
            this._parameterViewModel.RelativeYStart = null;
            this._parameterViewModel.RelativeXEnd = null;
            this._parameterViewModel.RelativeYEnd = null;

            // Assert
            this._parameterViewModel.RelativeXStart.Should().BeNull();
            this._parameterViewModel.RelativeYStart.Should().BeNull();
            this._parameterViewModel.RelativeXEnd.Should().BeNull();
            this._parameterViewModel.RelativeYEnd.Should().BeNull();
        }

        [TestMethod]
        public void LimitsRelativeEndPositionsWhenStartPositionIsNull()
        {
            // Arrange
            this._parameterViewModel.RelativeXStart = null;
            this._parameterViewModel.RelativeYStart = null;

            // Act
            this._parameterViewModel.RelativeXEnd = -100;
            this._parameterViewModel.RelativeYEnd = -100;

            // Assert
            this._parameterViewModel.RelativeXEnd.Should().Be(Settings.Default.XMin + 1 - this._parameterViewModel.OriginX);
            this._parameterViewModel.RelativeYEnd.Should().Be(Settings.Default.YMin + 1 - this._parameterViewModel.OriginY);
        }

        [TestMethod]
        public void CanSetId()
        {
            // Arrange
            const int Value = 4;

            // Act
            this._parameterViewModel.ID = Value;

            // Assert
            this._parameterViewModel.ID.Should().Be(Value);
        }

        [TestMethod]
        public void ChangesXStartAndXEndDependingOnOriginXChange()
        {
            // Arrange
            const int OriginX = 100;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.OriginX = OriginX;

            // Assert
            this._parameterViewModel.RelativeXStart.Should().Be(ModelFactory.DefaultXStart - OriginX + ModelFactory.DefaultOriginX);
            this._parameterViewModel.RelativeXEnd.Should().Be(ModelFactory.DefaultXEnd - OriginX + ModelFactory.DefaultOriginX);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.RelativeXStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteXStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.RelativeXEnd);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteXEnd);
        }

        [TestMethod]
        public void ChangesYStartAndYEndDependingOnOriginYChange()
        {
            // Arrange
            const int OriginY = 150;
            this._parameterViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this._parameterViewModel.OriginY = 100;
            this._parameterViewModel.OriginY = OriginY;

            // Assert
            this._parameterViewModel.RelativeYStart.Should().Be(ModelFactory.DefaultYStart - OriginY + ModelFactory.DefaultOriginY);
            this._parameterViewModel.RelativeYEnd.Should().Be(ModelFactory.DefaultYEnd - OriginY + ModelFactory.DefaultOriginY);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.RelativeYStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteYStart);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.RelativeYEnd);
            this._parameterViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteYEnd);
        }

        [TestMethod]
        public void SendsOriginValuesForSerialPortViewModelWhenOriginXChanges()
        {
            // Arrange
            const int OriginX = 100;
            NotificationMessage<Tuple<double?, double?>> notification = null;
            Messenger.Default.Register<NotificationMessage<Tuple<double?, double?>>>(this, x => notification = x);

            // Act
            this._parameterViewModel.OriginX = OriginX;

            // Assert
            notification.Should().NotBeNull();
            notification.Notification.Should().Be(Resources.Messenger_SetOriginForSerialPortVM);
            notification.Content.Item1.Should().Be(OriginX);
            notification.Content.Item2.Should().Be(ModelFactory.DefaultOriginY);
        }

        [TestMethod]
        public void SendsOriginValuesForSerialPortViewModelWhenOriginYChanges()
        {
            // Arrange
            const int OriginY = 150;
            NotificationMessage<Tuple<double?, double?>> notification = null;
            Messenger.Default.Register<NotificationMessage<Tuple<double?, double?>>>(this, x => notification = x);

            // Act
            this._parameterViewModel.OriginY = OriginY;

            // Assert
            notification.Should().NotBeNull();
            notification.Notification.Should().Be(Resources.Messenger_SetOriginForSerialPortVM);
            notification.Content.Item1.Should().Be(ModelFactory.DefaultOriginX);
            notification.Content.Item2.Should().Be(OriginY);
        }

        [TestMethod]
        public void CreatesDeltaLines()
        {
            // Assert
            this._parameterViewModel.AbsoluteDeltaXLines.Should().NotBeEmpty();
            this._parameterViewModel.AbsoluteDeltaYLines.Should().NotBeEmpty();
        }

        [TestMethod]
        public void SetsFreqStartToMinFreq()
        {
            // Act
            this._parameterViewModel.FreqStart = 0;

            // Assert
            this._parameterViewModel.FreqStart.Should().Be(1);
        }

        [TestMethod]
        public void SetsFreqStartToMaxFreq()
        {
            // Act
            this._parameterViewModel.FreqStart = 9999999999;

            // Assert
            this._parameterViewModel.FreqStart.Should().Be(this._parameterViewModel.FreqEnd / 1e1);
        }

        [TestMethod]
        public void SetsFreqEndToMinFreq()
        {
            // Act
            this._parameterViewModel.FreqEnd = 0;

            // Assert
            this._parameterViewModel.FreqEnd.Should().Be(this._parameterViewModel.FreqStart * 1e1);
        }

        [TestMethod]
        public void SetsFreqEndToMinFreqWhenStartFreqIsNull()
        {
            // Arrange
            this._parameterViewModel.FreqStart = null;

            // Act
            this._parameterViewModel.FreqEnd = 0;

            // Assert
            this._parameterViewModel.FreqEnd.Should().Be(1e1);
        }

        [TestMethod]
        public void SetsMinValueForMeasureTime()
        {
            // Act
            this._parameterViewModel.MeasureTime = 0;

            // Assert
            this._parameterViewModel.MeasureTime.Should().Be(1);
        }

        #endregion
    }
}