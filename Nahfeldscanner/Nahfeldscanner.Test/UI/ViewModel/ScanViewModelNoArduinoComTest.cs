﻿// ///////////////////////////////////
// File: ScanViewModelNoArduinoComTest.cs
// Last Change: 30.01.2018  11:07
// Author: Andre Multerer
// ///////////////////////////////////



using ModelProperties = Nahfeldscanner.Model.Properties;



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using FluentAssertions;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using Nahfeldscanner.UI.State;
    using Nahfeldscanner.UI.ViewModel;
    using NHibernate.Criterion;



    [TestClass]
    public class ScanViewModelNoArduinoComTest
    {
        #region Fields

        private Mock<IRepository> mockRepository;
        private Mock<IDialogService> mockDialogService;
        private SerialPortViewModel serialPortViewModel;
        private ScanViewModel scanViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this.mockRepository = new Mock<IRepository>();
            this.mockDialogService = new Mock<IDialogService>();
            this.serialPortViewModel = new SerialPortViewModel(this.mockDialogService.Object);
            this.scanViewModel = new ScanViewModel(this.mockRepository.Object, this.mockDialogService.Object, this.serialPortViewModel,
                                                   Resources.Workspace_Title_Scan, Resources.img_scan);
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this.mockRepository = null;
            this.mockDialogService = null;
            this.serialPortViewModel.Dispose();
            this.serialPortViewModel = null;
            this.scanViewModel = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void SerialPortInitializedAfterCreation()
        {
            this.scanViewModel.SerialPortViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void ViewModelsNotInitializedAfterCreation()
        {
            // Assert
            this.scanViewModel.AnalysisViewModel.Should().BeNull();
            this.scanViewModel.ParameterViewModel.Should().BeNull();
            this.scanViewModel.SpectrumViewModel.Should().BeNull();
        }

        [TestMethod]
        public void InEmptyStateAfterCreation()
        {
            // Assert
            this.scanViewModel.CurrentState.Should().BeOfType<ScanEmptyState>();
        }

        [TestMethod]
        public void InitializedCommands()
        {
            // Assert
            this.scanViewModel.CRUDCommands.Should().NotBeNull();
            this.scanViewModel.CRUDCommands.Count.Should().Be(3);
            this.scanViewModel.TestExecutionCommands.Should().NotBeNull();
            this.scanViewModel.TestExecutionCommands.Count.Should().Be(3);
        }

        [TestMethod]
        public void ProgressHasDefaultValuesAfterCreation()
        {
            // Assert
            this.scanViewModel.IsProgressVisible.Should().BeFalse();
            this.scanViewModel.MeasurementProgress.Should().Be(0);
        }

        [TestMethod]
        public void CreatesNewAnalysis()
        {
            // Act
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.scanViewModel.AnalysisViewModel.ID.Should().Be(0);
            this.scanViewModel.AnalysisViewModel.StartTime.Should().BeNull();
            this.scanViewModel.AnalysisViewModel.EndTime.Should().BeNull();
            this.scanViewModel.ParameterViewModel.RelativeXStart.Should().Be(ModelProperties.Settings.Default.XStart);
            this.scanViewModel.ParameterViewModel.RelativeXEnd.Should().Be(ModelProperties.Settings.Default.XEnd);
        }

        [TestMethod]
        public void ChangesToCreationState()
        {
            // Act
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.scanViewModel.CurrentState.Should().BeOfType<ScanCreationState>();
        }

        [TestMethod]
        public void CanNotEditAnalysisAndParameterWhenNotInCreationState()
        {
            // Arrange
            this.mockRepository.Setup(x => x.GetEagerById(It.IsAny<int>(), It.IsAny<Func<Analysis, object>>())).Returns(ModelFactory.GetDefaultAnalysis());

            // Act
            Messenger.Default.Send(new NotificationMessage<int>(1, Resources.Messenger_LoadAnalysisForScanVM));

            // Assert
            this.scanViewModel.AnalysisViewModel.CanEdit.Should().BeFalse();
            this.scanViewModel.ParameterViewModel.CanEdit.Should().BeFalse();
        }

        [TestMethod]
        public void CanEditAnalysisAndParameterInCreationState()
        {
            // Act
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.scanViewModel.AnalysisViewModel.CanEdit.Should().BeTrue();
            this.scanViewModel.ParameterViewModel.CanEdit.Should().BeTrue();
        }

        [TestMethod]
        public void LoadsAnalysisFromMessage()
        {
            // Arrange
            this.SetupRepository();

            // Act
            Messenger.Default.Send(new NotificationMessage<int>(1, Resources.Messenger_LoadAnalysisForScanVM));

            // Assert
            this.scanViewModel.AnalysisViewModel.Description.Should().Be(ModelFactory.DefaultDescription);
            this.scanViewModel.ParameterViewModel.RelativeXStart.Should().Be(ModelFactory.DefaultXStart);
            this.scanViewModel.ParameterViewModel.RelativeXEnd.Should().Be(ModelFactory.DefaultXEnd);
            this.scanViewModel.SpectrumViewModel.MeasurementViewModels.Count.Should().Be(1);
        }

        [TestMethod]
        public void LoadsNewAnalysisWithSentParameter()
        {
            // Act
            Messenger.Default.Send(new NotificationMessage<Parameter>(ModelFactory.GetDefaultParameter(),
                                                                      Resources.Messenger_CreateNewAnalysisWithLoadedParameterForScanVM));

            // Assert
            this.scanViewModel.AnalysisViewModel.Description.Should().BeNullOrEmpty();
            this.scanViewModel.ParameterViewModel.RelativeXStart.Should().Be(ModelFactory.DefaultXStart);
            this.scanViewModel.ParameterViewModel.RelativeXEnd.Should().Be(ModelFactory.DefaultXEnd);
        }

        [TestMethod]
        public void CreatesAnalysisForRepeatingFinishedAnalysis()
        {
            // Arrange
            this.LoadFilledAnalysis();

            // Act
            this.scanViewModel.RepeatAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.scanViewModel.AnalysisViewModel.StartTime.Should().BeNull();
            this.scanViewModel.AnalysisViewModel.EndTime.Should().BeNull();
            this.scanViewModel.AnalysisViewModel.Description.Should().Be(ModelFactory.DefaultDescription);
            this.scanViewModel.ParameterViewModel.RelativeXStart.Should().Be(ModelFactory.DefaultXStart);
            this.scanViewModel.ParameterViewModel.RelativeXEnd.Should().Be(ModelFactory.DefaultXEnd);
            this.scanViewModel.SpectrumViewModel.Should().BeNull();
        }

        [TestMethod]
        public void DeletesAnalysisInCreationState()
        {
            // Arrange
            this.mockDialogService.Setup(x => x.ShowDialogYesNo(Resources.Message_Title_Attention, Resources.Message_ShouldDeleteAnalysis)).Returns(Task.FromResult(true));
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);

            // Act
            this.scanViewModel.DeleteAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.scanViewModel.AnalysisViewModel.Should().BeNull();
            this.scanViewModel.ParameterViewModel.Should().BeNull();
            this.scanViewModel.SpectrumViewModel.Should().BeNull();
        }

        [TestMethod]
        public void ChangesToEmptyStateWhenAnalysisWillBeDeleted()
        {
            // Arrange
            this.mockDialogService.Setup(x => x.ShowDialogYesNo(Resources.Message_Title_Attention, Resources.Message_ShouldDeleteAnalysis)).Returns(Task.FromResult(true));
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);

            // Act
            this.scanViewModel.DeleteAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.scanViewModel.CurrentState.Should().BeOfType<ScanEmptyState>();
        }

        [TestMethod]
        public void DoesNotDeleteAnalysisFromDatabaseWhenInCreationState()
        {
            // Arrange
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);

            // Act
            this.scanViewModel.DeleteAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.mockRepository.Verify(x => x.Delete(It.IsAny<Analysis>()), Times.Never);
        }

        [TestMethod]
        public void DeleteAnalysisFromDatabaseWhenNotInCreationState()
        {
            // Arrange
            this.mockDialogService.Setup(x => x.ShowDialogYesNo(Resources.Message_Title_Attention, Resources.Message_ShouldDeleteAnalysis)).Returns(Task.FromResult(true));
            this.LoadFilledAnalysis();

            // Act
            this.scanViewModel.DeleteAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.mockRepository.Verify(x => x.Delete(It.IsAny<Analysis>()), Times.Once);
        }

        [TestMethod]
        public void ClearsLoadedAnalysisWhenDeleted()
        {
            // Arrange
            this.mockDialogService.Setup(x => x.ShowDialogYesNo(Resources.Message_Title_Attention, Resources.Message_ShouldDeleteAnalysis)).Returns(Task.FromResult(true));
            this.LoadFilledAnalysis();

            // Act
            this.scanViewModel.DeleteAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.scanViewModel.AnalysisViewModel.Should().BeNull();
            this.scanViewModel.ParameterViewModel.Should().BeNull();
            this.scanViewModel.SpectrumViewModel.Should().BeNull();
        }

        [TestMethod]
        public void CanNotStartAnalysisWhenNotConnected()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(false);
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);

            // Act
            this.scanViewModel.StartAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_NoDatabaseConnection), Times.Once);
        }

        [TestMethod]
        public void CanNotStartAnalysisWhenParameterHasMissingValues()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);
            this.scanViewModel.ParameterViewModel.RelativeXStart = null;

            // Act
            this.scanViewModel.StartAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_ParameterHasEmptyValues), Times.Once);
        }

        [TestMethod]
        public void SendsMainMenuIsEnabledWhenCurrentStateChangesToLoadedState()
        {
            // Arrange
            NotificationMessage<bool> notificationMessage = null;
            Messenger.Default.Register<NotificationMessage<bool>>(this, x => notificationMessage = x);

            // Act
            this.LoadFilledAnalysis();

            // Assert
            notificationMessage.Should().NotBeNull();
            notificationMessage.Notification.Should().Be(Resources.Messenger_ChangeWorkspaceEnableStateForMainVM);
            notificationMessage.Content.Should().BeTrue();
        }

        [TestMethod]
        public void SendsMainMenuIsNotEnabledWhenCurrentStateChangesToCreationState()
        {
            // Arrange
            NotificationMessage<bool> notificationMessage = null;
            Messenger.Default.Register<NotificationMessage<bool>>(this, x => notificationMessage = x);

            // Act
            this.scanViewModel.CreateNewAnalysisCommand.RelayCommand.Execute(null);

            // Assert
            notificationMessage.Should().NotBeNull();
            notificationMessage.Notification.Should().Be(Resources.Messenger_ChangeWorkspaceEnableStateForMainVM);
            notificationMessage.Content.Should().BeFalse();
        }

        #endregion



        private void LoadFilledAnalysis()
        {
            this.SetupRepository();
            Messenger.Default.Send(new NotificationMessage<int>(1, Resources.Messenger_LoadAnalysisForScanVM));
        }

        private void SetupRepository()
        {
            MeasurementValue measurementValue = ModelFactory.GetDefaultMeasurementValue();
            measurementValue.Measurement = ModelFactory.GetDefaultMeasurement();

            this.mockRepository.Setup(x => x.GetEagerById(It.IsAny<int>(), It.IsAny<Func<Analysis, object>>())).Returns(ModelFactory.GetFilledAnalysis());
            this.mockRepository.Setup(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<MeasurementValue, Measurement>>>(), It.IsAny<ICriterion>(), It.IsAny<Expression<Func<MeasurementValue, object>>>())).Returns(new Collection<MeasurementValue> { measurementValue });
            this.mockRepository.Setup(x => x.GetMinMeasuremntValueCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<MeasurementValue, object>>>())).Returns(new Collection<MeasurementValue> { measurementValue });
            this.mockRepository.Setup(x => x.GetMaxMeasuremntValueCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<MeasurementValue, object>>>())).Returns(new Collection<MeasurementValue> { measurementValue });
        }
    }
}