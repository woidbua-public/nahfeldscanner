﻿// ///////////////////////////////////
// File: SearchViewModelTest.cs
// Last Change: 17.11.2017  09:04
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using FluentAssertions;
    using GalaSoft.MvvmLight.Messaging;
    using log4net.Config;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using Nahfeldscanner.UI.ViewModel;
    using NHibernate.Criterion;



    [TestClass]
    public class SearchViewModelTest
    {
        private Mock<IRepository> mockRepository;
        private Mock<IDialogService> mockDialogService;
        private SearchViewModel searchViewModel;

        [TestInitialize]
        public void Init()
        {
            XmlConfigurator.Configure();
            this.mockRepository = new Mock<IRepository>();
            this.mockDialogService = new Mock<IDialogService>();
            this.searchViewModel = new SearchViewModel(this.mockRepository.Object, this.mockDialogService.Object,
                                                       Resources.Workspace_Title_Search, Resources.img_search);
        }

        [TestCleanup]
        public void Cleanup()
        {
            this.mockRepository = null;
            this.searchViewModel = null;
            GC.Collect();
        }

        [TestMethod]
        public void InitializedFoundAnalysisList()
        {
            // Assert
            this.searchViewModel.FoundAnalyses.Should().NotBeNull();
        }

        [TestMethod]
        public void SearchAnalysisViewModelNotNullAfterInitialization()
        {
            // Assert
            this.searchViewModel.SearchAnalysisViewModel.Should().NotBeNull();
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void SelectedAnalysisNullAfterInitialization()
        {
            // Assert
            this.searchViewModel.SelectedAnalysisViewModel.Should().BeNull();
        }

        [TestMethod]
        public void SearchAnalysisParameterIsClearedAfterInit()
        {
            // Assert
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeXStart.Should().BeNull();
        }

        [TestMethod]
        public void LoadsSearchedAnalysisViewModels()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, Parameter>>>(), It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, object>>>()))
                .Returns(new List<Analysis> { ModelFactory.GetDefaultAnalysis() });

            // Act
            this.searchViewModel.SearchAnalysisCommand.Execute(null);

            // Assert
            this.searchViewModel.FoundAnalyses.Count.Should().Be(1);
        }

        [TestMethod]
        public void SetsSelectedAnalysisViewModel()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, Parameter>>>(), It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, object>>>()))
                .Returns(new List<Analysis> { ModelFactory.GetDefaultAnalysis() });

            this.searchViewModel.SearchAnalysisCommand.Execute(null);

            // Act
            this.searchViewModel.SelectedAnalysisViewModel = this.searchViewModel.FoundAnalyses[0];

            // Assert
            this.searchViewModel.SelectedAnalysisViewModel.Should().Be(this.searchViewModel.FoundAnalyses[0]);
        }

        [TestMethod]
        public void ShowsMessageWhenSearchCouldNotBeStartedBecauseNoDatabaseConnection()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(false);
            this.mockRepository.Setup(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, Parameter>>>(), It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, object>>>()))
                .Returns(new List<Analysis> { ModelFactory.GetDefaultAnalysis() });

            // Act
            this.searchViewModel.SearchAnalysisCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_NoDatabaseConnection), Times.Once);
        }

        [TestMethod]
        public void SendsShortCriterionMessageWhenAnalysisIdWasSelected()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, object>>>())).Returns(new List<Analysis> { ModelFactory.GetDefaultAnalysis() });

            this.searchViewModel.SearchAnalysisViewModel.ID = 10;

            // Act
            this.searchViewModel.SearchAnalysisCommand.Execute(null);

            // Asert
            this.mockRepository.Verify(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, object>>>()), Times.Once);
        }

        [TestMethod]
        public void ClearsFields()
        {
            // Arrange
            this.searchViewModel.SearchAnalysisViewModel.Description = "some text";
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeXStart = 100;
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeYStart = 100;
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeXEnd = 200;
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeYEnd = 200;

            // Act
            this.searchViewModel.ClearFieldsCommand.Execute(null);

            // Assert
            this.searchViewModel.SearchAnalysisViewModel.Description.Should().BeNullOrEmpty();
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeXStart.Should().BeNull();
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeYStart.Should().BeNull();
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeXEnd.Should().BeNull();
            this.searchViewModel.SearchAnalysisViewModel.ParameterViewModel.RelativeYEnd.Should().BeNull();
        }

        [TestMethod]
        public void SendsLoadSelectedAnalysisMessage()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, Parameter>>>(), It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, object>>>()))
                .Returns(new List<Analysis> { ModelFactory.GetDefaultAnalysis() });

            this.searchViewModel.SearchAnalysisCommand.Execute(null);
            this.searchViewModel.SelectedAnalysisViewModel = this.searchViewModel.FoundAnalyses[0];

            NotificationMessage<int> intNotificationMessage = null;
            NotificationMessage notificationMessage = null;

            Messenger.Default.Register<NotificationMessage<int>>(this, x => intNotificationMessage = x);
            Messenger.Default.Register<NotificationMessage>(this, x => notificationMessage = x);

            // Act
            this.searchViewModel.SendLoadSelectedAnalysisMessageCommand.Execute(null);

            // Assert
            intNotificationMessage.Should().NotBeNull();
            notificationMessage.Should().NotBeNull();
            intNotificationMessage.Notification.Should().Be(Resources.Messenger_LoadAnalysisForScanVM);
            notificationMessage.Notification.Should().Be(Resources.Messenger_LoadScanVMForMainVM);
        }

        [TestMethod]
        public void RemovesDeletedAnalysisFromFoundList()
        {
            // Arrange
            Analysis analysis1 = ModelFactory.GetDefaultAnalysis();
            Analysis analysis2 = ModelFactory.GetDefaultAnalysis();
            analysis1.ID = 1;
            analysis2.ID = 2;

            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, Parameter>>>(), It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, object>>>()))
                .Returns(new List<Analysis> { analysis1, analysis2 });

            this.searchViewModel.SearchAnalysisCommand.Execute(null);

            // Act
            Messenger.Default.Send(new NotificationMessage<int>(2, Resources.Messenger_RemoveDeletedAnalysisFromSearchListForSearchVM));

            // Assert
            this.searchViewModel.FoundAnalyses.Count.Should().Be(1);
            this.searchViewModel.FoundAnalyses[0].ID.Should().Be(1);
        }

        [TestMethod]
        public void ClearsAllFoundAnalyses()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetCollectionByCriterion(It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, Parameter>>>(), It.IsAny<ICriterion>(), It.IsAny<Expression<Func<Analysis, object>>>()))
                .Returns(new List<Analysis> { ModelFactory.GetDefaultAnalysis(), ModelFactory.GetDefaultAnalysis() });

            this.searchViewModel.SearchAnalysisCommand.Execute(null);

            // Act
            Messenger.Default.Send(new NotificationMessage(Resources.Messenger_ClearFoundAnalysesForSearchVM));

            // Assert
            this.searchViewModel.FoundAnalyses.Count.Should().Be(0);
        }
    }
}