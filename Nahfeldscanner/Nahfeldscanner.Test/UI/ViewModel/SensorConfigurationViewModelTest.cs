﻿// ///////////////////////////////////
// File: SensorConfigurationViewModelTest.cs
// Last Change: 24.01.2018  13:35
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using FluentAssertions;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using Nahfeldscanner.UI.ViewModel;
    using NHibernate.Criterion;



    [TestClass]
    public class SensorConfigurationViewModelTest
    {
        #region Fields

        private Mock<IRepository> mockRepository;
        private Mock<IDialogService> mockDialogService;
        private SensorConfigurationViewModel sensorConfigurationViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this.mockRepository = new Mock<IRepository>();
            this.mockDialogService = new Mock<IDialogService>();
            this.sensorConfigurationViewModel = new SensorConfigurationViewModel(this.mockRepository.Object, this.mockDialogService.Object);
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this.sensorConfigurationViewModel = null;
            this.mockRepository = null;
            this.mockDialogService = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void PropertyCount()
        {
            // Assert
            this.sensorConfigurationViewModel.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(prop => prop.DeclaringType == this.sensorConfigurationViewModel.GetType()).ToList().Count.Should().Be(5);
        }

        [TestMethod]
        public void NewSensorShouldNotBeNullAfterInit()
        {
            // Assert
            this.sensorConfigurationViewModel.NewSensorViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void SelectedSensorIsNullAfterInit()
        {
            // Assert
            this.sensorConfigurationViewModel.SelectedSensorViewModel.Should().BeNull();
        }

        [TestMethod]
        public void NoSensorInListWhenRepositoryEmpty()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetAll<Sensor>()).Returns(new List<Sensor>());

            // Act
            this.sensorConfigurationViewModel = new SensorConfigurationViewModel(this.mockRepository.Object, this.mockDialogService.Object);

            // Assert
            this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(0);
        }

        [TestMethod]
        public void SensorsInListWhenRepositoryNotEmpty()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetAll<Sensor>()).Returns(new List<Sensor> { ModelFactory.GetDefaultSensor() });

            // Act
            Messenger.Default.Send(new NotificationMessage(Resources.Messenger_LoadSensorsForSensorConfigurationVM));

            // Assert
            this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(1);
        }

        [TestMethod]
        public void LoadsImageToNewSensor()
        {
            // Arrange
            const string ImagePath = "..\\..\\Resources\\img_arrowUp.png";
            this.mockDialogService.Setup(x => x.ShowImageFileDialog()).Returns(ImagePath);

            // Act
            this.sensorConfigurationViewModel.LoadImageCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorImageStream.Should().NotBeNull();
        }

        [TestMethod]
        public void DoesNotLoadImageToNewSensorWhenCancelled()
        {
            // Arrange
            this.mockDialogService.Setup(x => x.ShowImageFileDialog()).Returns(string.Empty);

            // Act
            this.sensorConfigurationViewModel.LoadImageCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorImageStream.Should().BeNull();
        }

        [TestMethod]
        public void ShowMessageWhenImageCouldNotBeLoadedToNewSensor()
        {
            const string ImagePath = "img_arrowUp.png";
            this.mockDialogService.Setup(x => x.ShowImageFileDialog()).Returns(ImagePath);

            // Act
            this.sensorConfigurationViewModel.LoadImageCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorImageStream.Should().BeNullOrEmpty();
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Message_Title_Attention, It.IsAny<string>()), Times.Once());
        }

        [TestMethod]
        public void CanNotAddSensorWhenDisconnectedFromDatabase()
        {
            // Act
            this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_NoDatabaseConnection), Times.Once);
        }

        [TestMethod]
        public void CanNotAddSensorWhenNoSensorName()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);

            // Act
            this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);

            // Assert
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_SensorNameRequired), Times.Once);
        }

        [TestMethod]
        public void AddsNewSensorToList()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetById<Sensor>(It.IsAny<int>())).Returns(ModelFactory.GetDefaultSensor());
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorName = "TestSensor";

            // Act
            this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(1);
        }

        [TestMethod]
        public void DoNotAddNewSensorWhenExistsAlready()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetById<Sensor>(It.IsAny<int>())).Returns(ModelFactory.GetDefaultSensor());
            this.mockRepository.Setup(x => x.GetByCriterion<Sensor>(It.IsAny<ICriterion>())).Returns(ModelFactory.GetDefaultSensor());
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorName = "TestSensor";

            // Act
            this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(0);
        }

        [TestMethod]
        public void ShowMessageWhenSensorCouldNotBeAddedToDatabase()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.SaveOrUpdate(It.IsAny<Sensor>())).Throws(new Exception());
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorName = "TestSensor";

            // Act
            this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(0);
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_CouldNotSaveSensorToDatabase));
        }

        [TestMethod]
        public void CreateNewSensorWhenOldSensorWasAddedSuccessfully()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetById<Sensor>(It.IsAny<int>())).Returns(ModelFactory.GetDefaultSensor());
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorName = "TestSensor";

            // Act
            this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(1);
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorName.Should().BeNullOrEmpty();
        }

        /* not working because of async method */
        //[TestMethod]
        //public void DeletesSensor()
        //{
        //    // Arrange
        //    this.mockRepository.Setup(x => x.IsConnected).Returns(true);
        //    this.mockRepository.Setup(x => x.GetById<Sensor>(It.IsAny<int>())).Returns(ModelFactory.GetDefaultSensor());
        //    this.mockDialogService.Setup(x => x.ShowDialogYesNo(It.IsAny<string>(), It.IsAny<string>())).Returns(new Task<bool>(() => true));
        //    this.sensorConfigurationViewModel.NewSensorViewModel.SensorName = "TestSensor";
        //    this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);

        //    // Act
        //    this.sensorConfigurationViewModel.SensorViewModels[0].SendDeleteRequestCommand.Execute(null);

        //    // Assert
        //    //this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(0);
        //    this.mockRepository.Verify(x => x.Delete(It.IsAny<Sensor>()), Times.Once);
        //}

        [TestMethod]
        public void DoNotDeleteSensorWhenSensorNotFoundInRepository()
        {
            // Arrange
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetById<Sensor>(It.IsAny<int>())).Returns(ModelFactory.GetDefaultSensor());
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorName = "TestSensor";
            this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);
            this.mockRepository.Setup(x => x.GetById<Sensor>(It.IsAny<int>())).Returns((Sensor)null);

            // Act
            this.sensorConfigurationViewModel.SensorViewModels[0].SendDeleteRequestCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(1);
            this.mockRepository.Verify(x => x.Delete(It.IsAny<Sensor>()), Times.Never);
        }

        [TestMethod]
        public void ShowMessageWhenSensorCouldNotBeRemovedBecauseOfRepositoryException()
        {
            this.mockRepository.Setup(x => x.IsConnected).Returns(true);
            this.mockRepository.Setup(x => x.GetById<Sensor>(It.IsAny<int>())).Returns(ModelFactory.GetDefaultSensor());
            this.sensorConfigurationViewModel.NewSensorViewModel.SensorName = "TestSensor";
            this.sensorConfigurationViewModel.AddSensorCommand.Execute(null);

            this.mockRepository.Setup(x => x.GetById<Sensor>(It.IsAny<int>())).Throws(new Exception());

            // Act
            this.sensorConfigurationViewModel.SensorViewModels[0].SendDeleteRequestCommand.Execute(null);

            // Assert
            this.sensorConfigurationViewModel.SensorViewModels.Count.Should().Be(1);
            this.mockDialogService.Verify(x => x.ShowMessage(Resources.Message_Title_Attention, It.IsAny<string>()), Times.Once);
        }

        #endregion
    }
}