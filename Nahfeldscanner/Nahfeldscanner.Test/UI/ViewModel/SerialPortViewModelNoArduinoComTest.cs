﻿// ///////////////////////////////////
// File: SerialPortViewModelNoArduinoComTest.cs
// Last Change: 03.11.2017  12:20
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using FluentAssertions;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using Nahfeldscanner.UI.ViewModel;



    [TestClass]
    public class SerialPortViewModelNoArduinoComTest
    {
        private const string comPort = "COM1";

        private Mock<IDialogService> mockDialogService;
        private SerialPortViewModel serialPortViewModel;

        [TestInitialize]
        public void Init()
        {
            this.mockDialogService = new Mock<IDialogService>();
            this.serialPortViewModel = new SerialPortViewModel(this.mockDialogService.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            this.mockDialogService = null;
            this.serialPortViewModel.Dispose();
            this.serialPortViewModel = null;
            GC.Collect();
        }

        [TestMethod]
        public void NumberOfProperties()
        {
            // Assert
            this.serialPortViewModel.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.DeclaringType == this.serialPortViewModel.GetType()).ToList().Count.Should().Be(31);
        }

        [TestMethod]
        public void IsInitialized()
        {
            // Assert
            this.serialPortViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void GetSelectedPortNameAfterCreation()
        {
            // Assert
            this.serialPortViewModel.SelectedPortName.Should().Be(comPort);
        }

        [TestMethod]
        public void DoesNotChangedSelectedPortNameWhenItDoesNotExist()
        {
            // Act
            this.serialPortViewModel.SelectedPortName = "COM2";

            // Assert
            this.serialPortViewModel.SelectedPortName.Should().Be(comPort);
        }

        [TestMethod]
        public void DoesNotChangeSelectedPortNameWhenNewValueIsEmpty()
        {
            // Act
            this.serialPortViewModel.SelectedPortName = string.Empty;

            // Assert
            this.serialPortViewModel.SelectedPortName.Should().Be(comPort);
        }

        [TestMethod]
        public void NotConnectedAfterCreation()
        {
            // Assert
            this.serialPortViewModel.MotorState.Should().Be(MotorState.Disconnected);
            this.serialPortViewModel.IsOpen.Should().BeFalse();
            this.serialPortViewModel.IsConnected.Should().BeFalse();
            this.serialPortViewModel.IsInitialized.Should().BeFalse();
            this.serialPortViewModel.IsIdle.Should().BeFalse();
            this.serialPortViewModel.IsRunning.Should().BeFalse();
        }

        [TestMethod]
        public void CurrentPositionsShouldBeNullWhenNotConnected()
        {
            // Assert
            this.serialPortViewModel.AbsoluteCurrentX.Should().BeNull();
            this.serialPortViewModel.AbsoluteCurrentY.Should().BeNull();
            this.serialPortViewModel.RelativeCurrentX.Should().BeNull();
            this.serialPortViewModel.RelativeCurrentY.Should().BeNull();
        }

        [TestMethod]
        public void GoToPositionsHaveDefaultValuesAfterCreation()
        {
            // Assert
            this.serialPortViewModel.GoToX.Should().Be(0);
            this.serialPortViewModel.GoToY.Should().Be(0);
        }

        [TestMethod]
        public void OriginPositionsHaveDefaultValuesAfterCreation()
        {
            // Assert
            this.serialPortViewModel.OriginX.Should().Be(0);
            this.serialPortViewModel.OriginY.Should().Be(0);
        }

        [TestMethod]
        public void StepSizeHasSavedSettingsValueAfterCreation()
        {
            // Assert
            this.serialPortViewModel.StepSize.Should().Be(Settings.Default.StepSize);
        }

        [TestMethod]
        public void ChangesStepSize()
        {
            // Arrange
            double oldStepSize = Settings.Default.StepSize;
            double newStepSize = 100;

            // Act
            this.serialPortViewModel.StepSize = newStepSize;

            // Assert
            this.serialPortViewModel.StepSize.Should().Be(newStepSize);
            this.serialPortViewModel.StepSize = oldStepSize;
        }

        [TestMethod]
        public void BaudRateShouldHaveSavedSettingsValue()
        {
            // Assert
            this.serialPortViewModel.BaudRate.Should().Be(Settings.Default.BaudRate);
        }

        [TestMethod]
        public void SetsOriginPositionsToMinValueWhenValueLessThanMinValue()
        {
            // Act
            this.serialPortViewModel.OriginX = -10;
            this.serialPortViewModel.OriginY = -10;

            // Assert
            this.serialPortViewModel.OriginX.Should().Be(Settings.Default.XMin);
            this.serialPortViewModel.OriginY.Should().Be(Settings.Default.YMin);
        }

        [TestMethod]
        public void SetsOriginPositionsToMaxValueWhenValueGreaterThanMaxValue()
        {
            // Act
            this.serialPortViewModel.OriginX = 500;
            this.serialPortViewModel.OriginY = 500;

            // Assert
            this.serialPortViewModel.OriginX.Should().Be(Settings.Default.XMax);
            this.serialPortViewModel.OriginY.Should().Be(Settings.Default.YMax);
        }

        [TestMethod]
        public void SetsOriginPositions()
        {
            // Arrange
            int xPosition = 10;
            int yPosition = 5;

            // Act
            this.serialPortViewModel.OriginX = xPosition;
            this.serialPortViewModel.OriginY = yPosition;

            // Assert
            this.serialPortViewModel.OriginX.Should().Be(xPosition);
            this.serialPortViewModel.OriginY.Should().Be(yPosition);
        }

        [TestMethod]
        public void RaisesPropertyChangedEventsWhenOriginPositionsChange()
        {
            // Arrange
            int counter = 0;
            int xPosition = 10;
            int yPosition = 5;

            this.serialPortViewModel.MonitorEvents<INotifyPropertyChanged>();
            this.serialPortViewModel.PropertyChanged += (sender, e) => counter++;

            // Act
            this.serialPortViewModel.OriginX = xPosition;
            this.serialPortViewModel.OriginY = yPosition;

            // Assert
            counter.Should().Be(4);
            this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.OriginX);
            this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.OriginY);
        }

        [TestMethod]
        public void SetsGoToPositionsToMinValueWhenValueLessThanMinValue()
        {
            // Arrange
            int value = 10;
            this.serialPortViewModel.OriginX = value;
            this.serialPortViewModel.OriginY = value;

            // Act
            this.serialPortViewModel.GoToX = -20;
            this.serialPortViewModel.GoToY = -30;

            // Assert
            this.serialPortViewModel.GoToX.Should().Be(-value);
            this.serialPortViewModel.GoToY.Should().Be(-value);
        }

        [TestMethod]
        public void SetsGoToPositionsToMaxValueWhenValueGreaterThanMaxValue()
        {
            // Arrange
            int value = 10;
            this.serialPortViewModel.OriginX = value;
            this.serialPortViewModel.OriginY = value;

            // Act
            this.serialPortViewModel.GoToX = 500;
            this.serialPortViewModel.GoToY = 500;

            // Assert
            this.serialPortViewModel.GoToX.Should().Be(220);
            this.serialPortViewModel.GoToY.Should().Be(285);
        }

        [TestMethod]
        public void SetsGoToPositions()
        {
            // Arrange
            int xPosition = 10;
            int yPosition = 5;

            // Act
            this.serialPortViewModel.GoToX = xPosition;
            this.serialPortViewModel.GoToY = yPosition;

            // Assert
            this.serialPortViewModel.GoToX.Should().Be(xPosition);
            this.serialPortViewModel.GoToY.Should().Be(yPosition);
        }

        [TestMethod]
        public void RecalculatesGoToMinValueWhenOriginChanges()
        {
            // Arrange
            this.serialPortViewModel.GoToX = 230;
            this.serialPortViewModel.GoToY = 295;

            // Act
            this.serialPortViewModel.OriginX = 30;
            this.serialPortViewModel.OriginY = 95;

            // Assert
            this.serialPortViewModel.GoToX.Should().Be(200);
            this.serialPortViewModel.GoToY.Should().Be(200);
        }

        [TestMethod]
        public void RaisesPropertyChangedEventsWhenGoToPositionsChange()
        {
            // Arrange
            int counter = 0;
            int xPosition = 100;
            int yPosition = 50;
            this.serialPortViewModel.MonitorEvents<INotifyPropertyChanged>();
            this.serialPortViewModel.PropertyChanged += (sender, e) => counter++;

            // Act
            this.serialPortViewModel.GoToX = xPosition;
            this.serialPortViewModel.GoToY = yPosition;

            // Assert
            counter.Should().Be(2);
            this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.GoToX);
            this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.GoToY);
        }

        [TestMethod]
        public void SetsAbsoluteTargetPositionsToMinValueWhenValueLessThanMinValue()
        {
            // Act
            this.serialPortViewModel.AbsoluteTargetX = -10;
            this.serialPortViewModel.AbsoluteTargetY = -10;

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(Settings.Default.XMin);
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(Settings.Default.YMin);
        }

        [TestMethod]
        public void SetsAbsoluteTargetPositionsToMaxValueWhenValueGreaterThanMaxValue()
        {
            // Act
            this.serialPortViewModel.AbsoluteTargetX = 500;
            this.serialPortViewModel.AbsoluteTargetY = 500;

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(Settings.Default.XMax);
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(Settings.Default.YMax);
        }

        [TestMethod]
        public void SetsAbsoluteTargetPositions()
        {
            // Arrange
            int xPosition = 10;
            int yPosition = 5;

            // Act
            this.serialPortViewModel.AbsoluteTargetX = xPosition;
            this.serialPortViewModel.AbsoluteTargetY = yPosition;

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(xPosition);
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(yPosition);
        }

        [TestMethod]
        public void RaisesPropertyChangedEventsWhenAbsoluteTargetPositionsChange()
        {
            // Arrange
            int counter = 0;
            int xPosition = 10;
            int yPosition = 5;

            this.serialPortViewModel.MonitorEvents<INotifyPropertyChanged>();
            this.serialPortViewModel.PropertyChanged += (sender, e) => counter++;

            // Act
            this.serialPortViewModel.AbsoluteTargetX = xPosition;
            this.serialPortViewModel.AbsoluteTargetY = yPosition;

            // Assert
            counter.Should().Be(2);
            this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteTargetX);
            this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.AbsoluteTargetY);
        }

        [TestMethod]
        public void UpdatesPortNames()
        {
            // Arrange
            this.serialPortViewModel.MonitorEvents<INotifyPropertyChanged>();

            // Act
            this.serialPortViewModel.UpdatePortNamesCommand.Execute(null);

            // Assert
            if (this.serialPortViewModel.PortNames.Count == 0)
            {
                // no com port when outside docking station
                return;
            }
            this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.SelectedPortName);
        }

        [TestMethod]
        public void OpensConnection()
        {
            // Arrange
            this.serialPortViewModel.UpdatePortNamesCommand.Execute(null);
            if (this.serialPortViewModel.PortNames.Count == 0)
            {
                // skip test, because no com port connected (outside docking station)
                return;
            }

            // Act
            this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);

            // Assert
            this.serialPortViewModel.IsOpen.Should().BeTrue();
        }

        [TestMethod]
        public void ClosesConnection()
        {
            // Arrange
            this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);

            // Act
            this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);

            // Assert
            Thread.Sleep(2 * Settings.Default.ThreadDelay);
            this.serialPortViewModel.IsOpen.Should().BeFalse();
            this.serialPortViewModel.MotorState.Should().Be(MotorState.Disconnected);
        }

        [TestMethod]
        public void DoesNotChangeAbsoluteTargetPositionsOnHomeSequenceWhenNotConnected()
        {
            // Arrange
            int position = 20;
            this.serialPortViewModel.AbsoluteTargetX = position;
            this.serialPortViewModel.AbsoluteTargetY = position;

            // Act
            this.serialPortViewModel.GoHomeCommand.Execute(null);

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(position);
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(position);
        }

        [TestMethod]
        public void DoesNotChangeAbsoluteTargetPositionsToGoToPositionWhenNotConnected()
        {
            // Arrange
            this.serialPortViewModel.GoToX = 100;
            this.serialPortViewModel.GoToY = 100;

            // Act
            this.serialPortViewModel.GoToCommand.Execute(null);

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(0);
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(0);
        }

        [TestMethod]
        public void DoesNotMoveUpWhenNotConnected()
        {
            // Act
            this.serialPortViewModel.MoveUpCommand.Execute(null);

            // Assert
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(0);
        }

        [TestMethod]
        public void DoesNotMoveRightWhenNotConnected()
        {
            // Act
            this.serialPortViewModel.MoveRightCommand.Execute(null);

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(0);
        }

        [TestMethod]
        public void DoesNotMoveDownWhenNotConnected()
        {
            // Arrange
            this.serialPortViewModel.AbsoluteTargetY = 1;

            // Act
            this.serialPortViewModel.MoveDownCommand.Execute(null);

            // Assert
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(1);
        }

        [TestMethod]
        public void DoesNotMoveLeftWhenNotConnected()
        {
            // Arrange
            this.serialPortViewModel.AbsoluteTargetX = 1;

            // Act
            this.serialPortViewModel.MoveLeftCommand.Execute(null);

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(1);
        }

        [TestMethod]
        public void DoesNotSetAbsoluteTargetPositionsToOriginWhenNotConnected()
        {
            // Arrange
            this.serialPortViewModel.OriginX = 20;
            this.serialPortViewModel.OriginY = 20;

            // Act
            this.serialPortViewModel.GoOriginCommand.Execute(null);

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(0);
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(0);
        }

        [TestMethod]
        public void DoesNotSetOriginToCurrentPositionWhenNotConnected()
        {
            // Act
            Action action = () => this.serialPortViewModel.SetOriginToCurrentPositionCommand.Execute(null);

            // Assert
            action.ShouldNotThrow<Exception>();
            this.serialPortViewModel.OriginX.Should().Be(0);
            this.serialPortViewModel.OriginY.Should().Be(0);
        }

        [TestMethod]
        public void DoesNotMoveToAbsolutePositionWhenNotConnected()
        {
            // Act
            this.serialPortViewModel.MoveToAbsolutePosition(100, 100);

            // Assert
            this.serialPortViewModel.AbsoluteTargetX.Should().Be(0);
            this.serialPortViewModel.AbsoluteTargetY.Should().Be(0);
        }

        [TestMethod]
        public void SetsOriginViaMessengerWhenValuesNotNull()
        {
            // Act
            int value = 100;
            Messenger.Default.Send(new NotificationMessage<Tuple<double?, double?>>(new Tuple<double?, double?>(value, value), Resources.Messenger_SetOriginForSerialPortVM));

            // Assert
            this.serialPortViewModel.OriginX.Should().Be(value);
            this.serialPortViewModel.OriginY.Should().Be(value);
        }

        [TestMethod]
        public void DoesNotSetOriginViaMessengerWhenValuesNull()
        {
            // Act
            int value = 100;
            Messenger.Default.Send(new NotificationMessage<Tuple<double?, double?>>(new Tuple<double?, double?>(null, value), Resources.Messenger_SetOriginForSerialPortVM));

            // Assert
            this.serialPortViewModel.OriginX.Should().Be(0);
            this.serialPortViewModel.OriginY.Should().Be(0);
        }
    }
}