﻿// ///////////////////////////////////
// File: SerialPortViewModelWithArduinoComTest.cs
// Last Change: 30.10.2017  11:51
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;



    [TestClass]
    public class SerialPortViewModelWithArduinoComTest
    {
        //#region Fields

        //private const string comPort = "COM7";

        //private Mock<IDialogService> mockDialogService;
        //private SerialPortViewModel serialPortViewModel;

        //#endregion Fields

        //#region Methods

        //[TestInitialize]
        //public void Init()
        //{
        //    this.mockDialogService = new Mock<IDialogService>();
        //    this.serialPortViewModel = new SerialPortViewModel(this.mockDialogService.Object);
        //    this.serialPortViewModel.SelectedPortName = this.serialPortViewModel.PortNames[0];
        //}

        //[TestCleanup]
        //public void Cleanup()
        //{
        //    this.mockDialogService = null;
        //    this.serialPortViewModel.Dispose();
        //    this.serialPortViewModel = null;
        //    GC.Collect();
        //}

        //[TestMethod]
        //public void IsInitialized()
        //{
        //    // Assert
        //    this.serialPortViewModel.Should().NotBeNull();
        //}

        //[TestMethod]
        //public void GetsPortNamesAfterCreation()
        //{
        //    // Assert
        //    this.serialPortViewModel.PortNames.Count.Should().Be(1);
        //}

        //[TestMethod]
        //public void GetSelectedPortNameAfterCreation()
        //{
        //    // Assert
        //    this.serialPortViewModel.SelectedPortName.Should().Be(comPort);
        //}

        //[TestMethod]
        //public void DoesNotChangedSelectedPortNameWhenItDoesNotExist()
        //{
        //    // Act
        //    this.serialPortViewModel.SelectedPortName = "COM2";

        //    // Assert
        //    this.serialPortViewModel.SelectedPortName.Should().Be(comPort);
        //}

        //[TestMethod]
        //public void DoesNotChangeSelectedPortNameWhenNewValueIsEmpty()
        //{
        //    // Act
        //    this.serialPortViewModel.SelectedPortName = string.Empty;

        //    // Assert
        //    this.serialPortViewModel.SelectedPortName.Should().Be(comPort);
        //}

        //[TestMethod]
        //public void NotConnectedAfterCreation()
        //{
        //    // Assert
        //    this.serialPortViewModel.MotorState.Should().Be(MotorState.Disconnected);
        //    this.serialPortViewModel.IsOpen.Should().BeFalse();
        //    this.serialPortViewModel.IsConnected.Should().BeFalse();
        //    this.serialPortViewModel.IsInitialized.Should().BeFalse();
        //    this.serialPortViewModel.IsIdle.Should().BeFalse();
        //    this.serialPortViewModel.IsRunning.Should().BeFalse();
        //}

        //[TestMethod]
        //public void CurrentPositionsShouldBeNullWhenNotConnected()
        //{
        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().BeNull();
        //    this.serialPortViewModel.CurrentY.Should().BeNull();
        //}

        //[TestMethod]
        //public void TargetPositionsHaveDefaultValuesAfterCreation()
        //{
        //    // Assert
        //    this.serialPortViewModel.TargetX.Should().Be(0);
        //    this.serialPortViewModel.TargetY.Should().Be(0);
        //}

        //[TestMethod]
        //public void OriginPositionsHaveSavedSettingsValuesAfterCreation()
        //{
        //    // Assert
        //    this.serialPortViewModel.OriginX.Should().Be(Settings.Default.OriginX);
        //    this.serialPortViewModel.OriginY.Should().Be(Settings.Default.OriginY);
        //}

        //[TestMethod]
        //public void StepSizeHasSavedSettingsValueAfterCreation()
        //{
        //    // Assert
        //    this.serialPortViewModel.StepSize.Should().Be(Settings.Default.StepSize);
        //}

        //[TestMethod]
        //public void ChangesStepSize()
        //{
        //    // Arrange
        //    double oldStepSize = Settings.Default.StepSize;
        //    double newStepSize = 100;

        //    // Act
        //    this.serialPortViewModel.StepSize = newStepSize;

        //    // Assert
        //    this.serialPortViewModel.StepSize.Should().Be(newStepSize);
        //    this.serialPortViewModel.StepSize = oldStepSize;
        //}

        //[TestMethod]
        //public void BaudRateShouldHaveSavedSettingsValue()
        //{
        //    // Assert
        //    this.serialPortViewModel.BaudRate.Should().Be(Settings.Default.BaudRate);
        //}

        //[TestMethod]
        //public void SetsTargetPositionsToMinValueWhenValueLessThanMinValue()
        //{
        //    // Act
        //    this.serialPortViewModel.TargetX = -10;
        //    this.serialPortViewModel.TargetY = -10;

        //    // Assert
        //    this.serialPortViewModel.TargetX.Should().Be(Settings.Default.XMin);
        //    this.serialPortViewModel.TargetY.Should().Be(Settings.Default.YMin);
        //}

        //[TestMethod]
        //public void SetsTargetPositionsToMaxValueWhenValueGreaterThanMaxValue()
        //{
        //    // Act
        //    this.serialPortViewModel.TargetX = 500;
        //    this.serialPortViewModel.TargetY = 500;

        //    // Assert
        //    this.serialPortViewModel.TargetX.Should().Be(Settings.Default.XMax);
        //    this.serialPortViewModel.TargetY.Should().Be(Settings.Default.YMax);
        //}

        //[TestMethod]
        //public void SetsTargetPositions()
        //{
        //    // Arrange
        //    int xPosition = 100;
        //    int yPosition = 50;

        //    // Act
        //    this.serialPortViewModel.TargetX = xPosition;
        //    this.serialPortViewModel.TargetY = yPosition;

        //    // Assert
        //    this.serialPortViewModel.TargetX.Should().Be(xPosition);
        //    this.serialPortViewModel.TargetY.Should().Be(yPosition);
        //}

        //[TestMethod]
        //public void RaisesPropertyChangedEventsWhenTargetPositionsChange()
        //{
        //    // Arrange
        //    int counter = 0;
        //    int xPosition = 100;
        //    int yPosition = 50;
        //    this.serialPortViewModel.MonitorEvents<INotifyPropertyChanged>();
        //    this.serialPortViewModel.PropertyChanged += (sender, e) => counter++;

        //    // Act
        //    this.serialPortViewModel.TargetX = xPosition;
        //    this.serialPortViewModel.TargetY = yPosition;

        //    // Assert
        //    counter.Should().Be(2);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.TargetX);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.TargetY);
        //}

        //[TestMethod]
        //public void SetsOriginPositionsToMinValueWhenValueLessThanMinValue()
        //{
        //    // Arrange
        //    double oldOriginX = Settings.Default.OriginX;
        //    double oldOriginY = Settings.Default.OriginY;

        //    // Act
        //    this.serialPortViewModel.OriginX = -10;
        //    this.serialPortViewModel.OriginY = -10;

        //    // Assert
        //    this.serialPortViewModel.OriginX.Should().Be(Settings.Default.XMin);
        //    this.serialPortViewModel.OriginY.Should().Be(Settings.Default.YMin);

        //    this.serialPortViewModel.OriginX = oldOriginX;
        //    this.serialPortViewModel.OriginY = oldOriginY;
        //}

        //[TestMethod]
        //public void SetsOriginPositionsToMaxValueWhenValueGreaterThanMaxValue()
        //{
        //    // Arrange
        //    double oldOriginX = Settings.Default.OriginX;
        //    double oldOriginY = Settings.Default.OriginY;

        //    // Act
        //    this.serialPortViewModel.OriginX = 500;
        //    this.serialPortViewModel.OriginY = 500;

        //    // Assert
        //    this.serialPortViewModel.OriginX.Should().Be(Settings.Default.XMax);
        //    this.serialPortViewModel.OriginY.Should().Be(Settings.Default.YMax);

        //    this.serialPortViewModel.OriginX = oldOriginX;
        //    this.serialPortViewModel.OriginY = oldOriginY;
        //}

        //[TestMethod]
        //public void SetsOriginPositions()
        //{
        //    // Arrange
        //    double oldOriginX = Settings.Default.OriginX;
        //    double oldOriginY = Settings.Default.OriginY;

        //    int xPosition = 100;
        //    int yPosition = 50;

        //    // Act
        //    this.serialPortViewModel.OriginX = xPosition;
        //    this.serialPortViewModel.OriginY = yPosition;

        //    // Assert
        //    this.serialPortViewModel.OriginX.Should().Be(xPosition);
        //    this.serialPortViewModel.OriginY.Should().Be(yPosition);

        //    this.serialPortViewModel.OriginX = oldOriginX;
        //    this.serialPortViewModel.OriginY = oldOriginY;
        //}

        //[TestMethod]
        //public void RaisesPropertyChangedEventsWhenOriginPositionsChange()
        //{
        //    // Arrange
        //    double oldOriginX = Settings.Default.OriginX;
        //    double oldOriginY = Settings.Default.OriginY;

        //    int counter = 0;
        //    int xPosition = 100;
        //    int yPosition = 50;

        //    this.serialPortViewModel.MonitorEvents<INotifyPropertyChanged>();
        //    this.serialPortViewModel.PropertyChanged += (sender, e) => counter++;

        //    // Act
        //    this.serialPortViewModel.OriginX = xPosition;
        //    this.serialPortViewModel.OriginY = yPosition;

        //    // Assert
        //    counter.Should().Be(2);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.OriginX);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.OriginY);

        //    this.serialPortViewModel.OriginX = oldOriginX;
        //    this.serialPortViewModel.OriginY = oldOriginY;
        //}

        //[TestMethod]
        //public void UpdatesPortNames()
        //{
        //    // Arrange
        //    this.serialPortViewModel.MonitorEvents<INotifyPropertyChanged>();

        //    // Act
        //    this.serialPortViewModel.UpdatePortNamesCommand.Execute(null);

        //    // Assert
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.SelectedPortName);
        //}

        //[TestMethod]
        //public void OpensConnection()
        //{
        //    // Act
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);

        //    // Assert
        //    this.serialPortViewModel.IsOpen.Should().BeTrue();
        //}

        //[TestMethod]
        //public void ClosesConnection()
        //{
        //    // Arrange
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);

        //    // Act
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);

        //    // Assert
        //    Thread.Sleep(100);
        //    this.serialPortViewModel.IsOpen.Should().BeFalse();
        //    this.serialPortViewModel.MotorState.Should().Be(MotorState.Disconnected);
        //}

        //[TestMethod]
        //public void DoesNotChangeTargetPositionsOnHomeSequenceWhenNotConnected()
        //{
        //    // Arrange
        //    int position = 50;
        //    this.serialPortViewModel.TargetX = position;
        //    this.serialPortViewModel.TargetY = position;

        //    // Act
        //    this.serialPortViewModel.GoHomeCommand.Execute(null);

        //    // Assert
        //    this.serialPortViewModel.TargetX.Should().Be(position);
        //    this.serialPortViewModel.TargetY.Should().Be(position);
        //}

        //[TestMethod]
        //public void DoesNotSetTargetPositionsToOriginWhenNotConnected()
        //{
        //    // Arrange
        //    double oldOriginX = Settings.Default.OriginX;
        //    double oldOriginY = Settings.Default.OriginY;

        //    this.serialPortViewModel.OriginX = 50;
        //    this.serialPortViewModel.OriginY = 50;

        //    // Act
        //    this.serialPortViewModel.GoOriginCommand.Execute(null);

        //    // Assert
        //    this.serialPortViewModel.TargetX.Should().Be(0);
        //    this.serialPortViewModel.TargetY.Should().Be(0);

        //    this.serialPortViewModel.OriginX = oldOriginX;
        //    this.serialPortViewModel.OriginY = oldOriginY;
        //}

        //[TestMethod]
        //public void DoesNotSetOriginToCurrentPositionWhenNotConnected()
        //{
        //    // Act
        //    Action action = () => this.serialPortViewModel.SetOriginToCurrentPositionCommand.Execute(null);

        //    // Assert
        //    action.ShouldNotThrow<Exception>();
        //}

        //[TestMethod]
        //public void GetsCurrentPositions()
        //{
        //    // Act
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().NotBeNull();
        //    this.serialPortViewModel.CurrentY.Should().NotBeNull();
        //}

        //[TestMethod]
        //public void IsConnectedShouldBeTrue()
        //{
        //    // Act
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    // Assert
        //    this.serialPortViewModel.IsConnected.Should().BeTrue();
        //}

        //[TestMethod]
        //public void ChangesMotorStateWhenConnected()
        //{
        //    // Arrange
        //    this.serialPortViewModel.MonitorEvents<INotifyPropertyChanged>();

        //    // Act
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    // Assert
        //    this.serialPortViewModel.MotorState.Should().NotBe(MotorState.Disconnected);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.IsOpen);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.IsConnected);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.IsInitialized);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.IsIdle);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.IsRunning);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.CurrentX);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.CurrentY);
        //    this.serialPortViewModel.ShouldRaisePropertyChangeFor(x => x.SelectedPortName);
        //}

        //[TestMethod]
        //public void GoesHome()
        //{
        //    // Arrange
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);

        //    // Act
        //    Thread.Sleep(Settings.Default.LongThreadDelay);
        //    this.serialPortViewModel.GoHomeCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().Be(0);
        //    this.serialPortViewModel.CurrentY.Should().Be(0);
        //    this.serialPortViewModel.TargetX.Should().Be(0);
        //    this.serialPortViewModel.TargetY.Should().Be(0);
        //}

        //[TestMethod]
        //public void GoesTarget()
        //{
        //    // Arrange
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);

        //        while (this.serialPortViewModel.IsIdle == false)
        //        {
        //            Thread.Sleep(Settings.Default.ThreadDelay);
        //        }
        //    }

        //    // Act
        //    this.serialPortViewModel.TargetX = 10;
        //    this.serialPortViewModel.TargetY = 10;

        //    this.serialPortViewModel.GoToCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().BeApproximately(this.serialPortViewModel.TargetX, 0.051);
        //    this.serialPortViewModel.CurrentY.Should().BeApproximately(this.serialPortViewModel.TargetY, 0.051);
        //}

        //[TestMethod]
        //public void GoesOrigin()
        //{
        //    // Arrange
        //    double oldOriginX = Settings.Default.OriginX;
        //    double oldOriginY = Settings.Default.OriginY;
        //    int originX = 20;
        //    int originY = 20;

        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);

        //        while (this.serialPortViewModel.IsIdle == false)
        //        {
        //            Thread.Sleep(Settings.Default.ThreadDelay);
        //        }
        //    }

        //    // Act
        //    this.serialPortViewModel.OriginX = originX;
        //    this.serialPortViewModel.OriginY = originY;

        //    this.serialPortViewModel.GoOriginCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().BeApproximately(originX, 0.051);
        //    this.serialPortViewModel.CurrentY.Should().BeApproximately(originY, 0.051);

        //    this.serialPortViewModel.OriginX = oldOriginX;
        //    this.serialPortViewModel.OriginY = oldOriginY;
        //}

        //[TestMethod]
        //public void MovesUp()
        //{
        //    // Arrange
        //    double oldStepSize = this.serialPortViewModel.StepSize;
        //    double stepSize = 10;

        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);
        //    }
        //    else
        //    {
        //        this.serialPortViewModel.TargetX = 0;
        //        this.serialPortViewModel.TargetY = 0;
        //        this.serialPortViewModel.GoToCommand.Execute(null);
        //    }

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    this.serialPortViewModel.StepSize = stepSize;

        //    // Act
        //    this.serialPortViewModel.MoveUpCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().Be(0);
        //    this.serialPortViewModel.CurrentY.Should().BeApproximately(stepSize, 0.051);
        //    this.serialPortViewModel.StepSize = oldStepSize;
        //}

        //[TestMethod]
        //public void MovesRight()
        //{
        //    // Arrange
        //    double oldStepSize = this.serialPortViewModel.StepSize;
        //    double stepSize = 10;

        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);
        //    }
        //    else
        //    {
        //        this.serialPortViewModel.TargetX = 0;
        //        this.serialPortViewModel.TargetY = 0;
        //        this.serialPortViewModel.GoToCommand.Execute(null);
        //    }

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    this.serialPortViewModel.StepSize = stepSize;

        //    // Act
        //    this.serialPortViewModel.MoveRightCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().BeApproximately(stepSize, 0.051);
        //    this.serialPortViewModel.CurrentY.Should().Be(0);
        //    this.serialPortViewModel.StepSize = oldStepSize;
        //}

        //[TestMethod]
        //public void MovesDown()
        //{
        //    // Arrange
        //    double oldStepSize = this.serialPortViewModel.StepSize;
        //    double stepSize = 10;

        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);
        //    }
        //    else
        //    {
        //        this.serialPortViewModel.TargetX = 0;
        //        this.serialPortViewModel.TargetY = 0;
        //        this.serialPortViewModel.GoToCommand.Execute(null);
        //    }

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    this.serialPortViewModel.StepSize = stepSize;

        //    this.serialPortViewModel.MoveUpCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Act
        //    this.serialPortViewModel.MoveDownCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().Be(0);
        //    this.serialPortViewModel.CurrentY.Should().Be(0);
        //    this.serialPortViewModel.StepSize = oldStepSize;
        //}

        //[TestMethod]
        //public void MovesLeft()
        //{
        //    // Arrange
        //    double oldStepSize = this.serialPortViewModel.StepSize;
        //    double stepSize = 10;

        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);
        //    }
        //    else
        //    {
        //        this.serialPortViewModel.TargetX = 0;
        //        this.serialPortViewModel.TargetY = 0;
        //        this.serialPortViewModel.GoToCommand.Execute(null);
        //    }

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    this.serialPortViewModel.StepSize = stepSize;

        //    this.serialPortViewModel.MoveRightCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Act
        //    this.serialPortViewModel.MoveLeftCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Assert
        //    this.serialPortViewModel.CurrentX.Should().Be(0);
        //    this.serialPortViewModel.CurrentY.Should().Be(0);
        //    this.serialPortViewModel.StepSize = oldStepSize;
        //}

        //[TestMethod]
        //public void SetsOriginToCurrentPosition()
        //{
        //    // Arrange
        //    double oldOriginX = this.serialPortViewModel.OriginX;
        //    double oldOriginY = this.serialPortViewModel.OriginY;
        //    double position = 15;

        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);
        //    }
        //    else
        //    {
        //        this.serialPortViewModel.TargetX = 0;
        //        this.serialPortViewModel.TargetY = 0;
        //        this.serialPortViewModel.GoToCommand.Execute(null);
        //    }

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    this.serialPortViewModel.MoveToAbsolutePosition(position, position);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Act
        //    this.serialPortViewModel.SetOriginToCurrentPositionCommand.Execute(null);

        //    // Assert
        //    this.serialPortViewModel.OriginX.Should().BeApproximately(position, 0.051);
        //    this.serialPortViewModel.OriginY.Should().BeApproximately(position, 0.051);
        //    this.serialPortViewModel.OriginX = oldOriginX;
        //    this.serialPortViewModel.OriginY = oldOriginY;
        //}

        //[TestMethod]
        //public void IsIdleShouldReturnTrue()
        //{
        //    // Act
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);
        //    }

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Assert
        //    this.serialPortViewModel.IsIdle.Should().BeTrue();

        //}

        //[TestMethod]
        //public void IsRunningShouldReturnTrue()
        //{
        //    // Arrange
        //    double position = 50;
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    if (this.serialPortViewModel.IsInitialized == false)
        //    {
        //        this.serialPortViewModel.GoHomeCommand.Execute(null);
        //        Thread.Sleep(Settings.Default.LongThreadDelay);
        //    }
        //    else
        //    {
        //        this.serialPortViewModel.TargetX = 0;
        //        this.serialPortViewModel.TargetY = 0;
        //        this.serialPortViewModel.GoToCommand.Execute(null);
        //    }

        //    while (this.serialPortViewModel.IsIdle == false)
        //    {
        //        Thread.Sleep(Settings.Default.ThreadDelay);
        //    }

        //    // Act
        //    this.serialPortViewModel.MoveToAbsolutePosition(position, position);
        //    Thread.Sleep(Settings.Default.ThreadDelay);

        //    // Assert
        //    this.serialPortViewModel.IsRunning.Should().BeTrue();

        //}

        //[TestMethod]
        //public void ShowsErrorMessageWhenCommandReturnedError()
        //{
        //    // Arrange
        //    this.serialPortViewModel.OpenCloseConnectionCommand.Execute(null);
        //    Thread.Sleep(Settings.Default.LongThreadDelay);

        //    // Act
        //    this.serialPortViewModel.MoveToAbsolutePosition(10, 10);
        //    Thread.Sleep(2 * Settings.Default.LongThreadDelay);

        //    // Assert
        //    this.mockDialogService.Verify(x => x.ShowMessage(Resources.Exception_Message_SerialErrorState, It.IsAny<string>()), Times.Once);
        //}

        //#endregion Methods
    }
}