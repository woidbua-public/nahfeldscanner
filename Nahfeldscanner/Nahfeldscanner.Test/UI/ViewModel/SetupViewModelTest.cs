﻿// ///////////////////////////////////
// File: SetupViewModelTest.cs
// Last Change: 24.01.2018  14:19
// Author: Andre Multerer
// ///////////////////////////////////



using ModelProperties = Nahfeldscanner.Model.Properties;



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.Threading;
    using FluentAssertions;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using Nahfeldscanner.UI.ViewModel;



    [TestClass]
    public class SetupViewModelTest
    {
        #region Fields

        private Mock<IRepository> mockRepository;
        private Mock<IDialogService> mockDialogService;
        private Mock<SerialPortViewModel> mockSerialPortViewModel;
        private SetupViewModel setupViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this.mockRepository = new Mock<IRepository>();
            this.mockDialogService = new Mock<IDialogService>();
            this.mockSerialPortViewModel = new Mock<SerialPortViewModel>(this.mockDialogService.Object);
            this.setupViewModel = new SetupViewModel(this.mockRepository.Object, this.mockSerialPortViewModel.Object,
                                                     Resources.Workspace_Title_Setup, Resources.img_setup);
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this.mockRepository = null;
            this.mockDialogService = null;
            this.mockSerialPortViewModel.Object.Dispose();
            this.mockSerialPortViewModel = null;
            this.setupViewModel = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void SerialPortViewModelInitialized()
        {
            // Assert
            this.setupViewModel.SerialPortViewModel.Should().NotBeNull();
            this.setupViewModel.SerialPortViewModel.Should().Be(this.mockSerialPortViewModel.Object);
        }

        [TestMethod]
        public void ParameterViewModelInitialized()
        {
            // Assert
            this.setupViewModel.ParameterViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void CanNotSetOriginWhenCurrentPositionIsNull()
        {
            // Arrange
            this.mockSerialPortViewModel.Setup(x => x.AbsoluteCurrentX).Returns((double?)null);
            this.mockSerialPortViewModel.Setup(x => x.AbsoluteCurrentY).Returns((double?)null);

            // Act
            this.setupViewModel.SetOriginToCurrentPositionCommand.Execute(null);

            // Assert
            this.setupViewModel.ParameterViewModel.OriginX.Should().Be(ModelFactory.DefaultOriginX);
            this.setupViewModel.ParameterViewModel.OriginY.Should().Be(ModelFactory.DefaultOriginY);
        }

        [TestMethod]
        public void SetsOriginWhenCurrentPositionIsNotNull()
        {
            // Arrange
            const int Value = 100;
            this.mockSerialPortViewModel.Setup(x => x.AbsoluteCurrentX).Returns(Value);
            this.mockSerialPortViewModel.Setup(x => x.AbsoluteCurrentY).Returns(Value);

            // Act
            this.setupViewModel.SetOriginToCurrentPositionCommand.Execute(null);

            // Assert
            this.setupViewModel.ParameterViewModel.OriginX.Should().Be(Value);
            this.setupViewModel.ParameterViewModel.OriginY.Should().Be(Value);
        }

        [TestMethod]
        public void CanNotSetStartPositionsWhenCurrentPositionIsNull()
        {
            // Arrange
            this.mockSerialPortViewModel.Setup(x => x.RelativeCurrentX).Returns((double?)null);
            this.mockSerialPortViewModel.Setup(x => x.RelativeCurrentY).Returns((double?)null);

            // Act
            this.setupViewModel.SetStartPositionToCurrentPositionCommand.Execute(null);

            // Assert
            this.setupViewModel.ParameterViewModel.RelativeXStart.Should().Be(ModelProperties.Settings.Default.XStart);
            this.setupViewModel.ParameterViewModel.RelativeYStart.Should().Be(ModelProperties.Settings.Default.YStart);
        }

        [TestMethod]
        public void SetsStartPositionsWhenCurrentPositionIsNotNull()
        {
            // Arrange
            double XValue = ModelFactory.DefaultXStart / 2;
            this.mockSerialPortViewModel.Setup(x => x.RelativeCurrentX).Returns(XValue);
            this.mockSerialPortViewModel.Setup(x => x.RelativeCurrentY).Returns(XValue);

            // Act
            this.setupViewModel.SetStartPositionToCurrentPositionCommand.Execute(null);

            // Assert
            this.setupViewModel.ParameterViewModel.RelativeXStart.Should().Be(XValue);
            this.setupViewModel.ParameterViewModel.RelativeYStart.Should().Be(XValue);
        }

        [TestMethod]
        public void CanNotSetEndPositionsWhenCurrentPositionIsNull()
        {
            // Arrange
            this.mockSerialPortViewModel.Setup(x => x.RelativeCurrentX).Returns((double?)null);
            this.mockSerialPortViewModel.Setup(x => x.RelativeCurrentY).Returns((double?)null);

            // Act
            this.setupViewModel.SetEndPositionToCurrentPositionCommand.Execute(null);

            // Assert
            this.setupViewModel.ParameterViewModel.RelativeXEnd.Should().Be(ModelProperties.Settings.Default.XEnd);
            this.setupViewModel.ParameterViewModel.RelativeYEnd.Should().Be(ModelProperties.Settings.Default.YEnd);
        }

        [TestMethod]
        public void SetsEndPositionsWhenCurrentPositionIsNotNull()
        {
            // Arrange
            const double xValue = 120;
            this.mockSerialPortViewModel.Setup(x => x.RelativeCurrentX).Returns(xValue);
            this.mockSerialPortViewModel.Setup(x => x.RelativeCurrentY).Returns(xValue);

            // Act
            this.setupViewModel.SetEndPositionToCurrentPositionCommand.Execute(null);

            // Assert
            this.setupViewModel.ParameterViewModel.RelativeXEnd.Should().Be(xValue);
            this.setupViewModel.ParameterViewModel.RelativeYEnd.Should().Be(xValue);
        }

        [TestMethod]
        public void SendsParameterAndWorkspaceChangeMessage()
        {
            // Arrange
            NotificationMessage workspaceNotification = null;
            NotificationMessage<Parameter> parameterNotification = null;

            Messenger.Default.Register<NotificationMessage>(this, notification => workspaceNotification = notification);
            Messenger.Default.Register<NotificationMessage<Parameter>>(this, notification => parameterNotification = notification);

            // Act
            this.setupViewModel.SendParameterAndSwitchToScanViewModelCommand.Execute(null);

            // Assert
            workspaceNotification.Should().NotBeNull();
            parameterNotification.Should().NotBeNull();
            workspaceNotification.Notification.Should().Be(Resources.Messenger_LoadScanVMForMainVM);
            parameterNotification.Notification.Should().Be(Resources.Messenger_CreateNewAnalysisWithLoadedParameterForScanVM);
        }

        [TestMethod]
        public void CreatesNewParameterWithSameValuesAfterSending()
        {
            // Arrange
            double RelativeXStart = ModelFactory.DefaultXStart / 2;
            this.setupViewModel.ParameterViewModel.RelativeXStart = RelativeXStart;

            // Act
            this.setupViewModel.SendParameterAndSwitchToScanViewModelCommand.Execute(null);

            // Assert
            this.setupViewModel.ParameterViewModel.RelativeXStart.Should().Be(RelativeXStart);
        }

        [TestMethod]
        public void SerialPortOriginsDoNotChangeAfterNewParameterCreation()
        {
            // Arrange
            const double OriginX = 43;
            const double OriginY = 44;

            this.mockSerialPortViewModel.SetupAllProperties();

            this.setupViewModel.ParameterViewModel.OriginX = OriginX;
            this.setupViewModel.ParameterViewModel.OriginY = OriginY;

            // Act
            this.setupViewModel.SendParameterAndSwitchToScanViewModelCommand.Execute(null);

            // Assert
            this.setupViewModel.SerialPortViewModel.OriginX.Should().Be(OriginX);
            this.setupViewModel.SerialPortViewModel.OriginY.Should().Be(OriginY);
        }

        [TestMethod]
        public void DisconnectsWhenNewParameterWasSent()
        {
            // Arrange
            this.setupViewModel.SerialPortViewModel.OpenCloseConnectionCommand.Execute(null);

            // Act
            this.setupViewModel.SendParameterAndSwitchToScanViewModelCommand.Execute(null);

            // Assert
            Thread.Sleep(2 * Settings.Default.LongThreadDelay);
            this.setupViewModel.SerialPortViewModel.IsOpen.Should().BeFalse();
        }

        [TestMethod]
        public void CanChangeTestReceiverIP()
        {
            // Arrange
            string testIP = "100.100.100.100";
            string oldIP = ModelProperties.Settings.Default.TestReceiverIP;

            // Act
            this.setupViewModel.TestReceiverIP = testIP;

            // Assert
            this.setupViewModel.TestReceiverIP.Should().Be(testIP);

            this.setupViewModel.TestReceiverIP = oldIP;
        }

        #endregion
    }
}