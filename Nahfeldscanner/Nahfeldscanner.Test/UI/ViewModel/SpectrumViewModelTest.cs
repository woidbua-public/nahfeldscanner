﻿// ///////////////////////////////////
// File: SpectrumViewModelTest.cs
// Last Change: 30.01.2018  09:17
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.Test.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.ViewModel;
    using NHibernate.Criterion;



    [TestClass]
    public class SpectrumViewModelTest
    {
        #region Fields

        private const int AmountAnalysis = 1;
        private const int AmountMeasurement = 500;
        private const int AmountMeasurementValue = 100;
        private const int AmountSensor = 10;
        private const int XMax = 25;

        private SqLiteRepository _repository;
        private SpectrumViewModel _spectrumViewModel;

        #endregion



        #region Test Initialize

        [TestInitialize]
        public void Init()
        {
            this._repository = new SqLiteRepository(new SqLiteSessionManager());

            DatabaseFactory.InitDatabase(this._repository,
                                         AmountAnalysis, AmountMeasurement, AmountMeasurementValue, AmountSensor, XMax);

            this._spectrumViewModel = new SpectrumViewModel(this._repository, this._repository.GetEagerById<Analysis>(1, x => x.Measurements));
        }

        #endregion



        #region Test Cleanup

        [TestCleanup]
        public void Cleanup()
        {
            this._spectrumViewModel = null;
            GC.Collect();
        }

        #endregion



        #region Test Methods

        [TestMethod]
        public void SelectedFrequencyIndexOnStartIndexAfterInit()
        {
            // Assert
            this._spectrumViewModel.SelectedFrequencyIndex.Should().Be(0);
        }

        [TestMethod]
        public void SelectedDetectorTypeOnMaxDetectorAfterInit()
        {
            // Assert
            this._spectrumViewModel.SelectedDetectorType.Should().Be(DetectorType.Max_Peak);
        }

        [TestMethod]
        public void InitializesMaxFrequencyIndex()
        {
            // Assert
            this._spectrumViewModel.MaxFrequencyIndex.Should().Be(AmountMeasurementValue - 1);
        }

        [TestMethod]
        public void InitializesAllMeasurementViewModels()
        {
            // Assert
            this._spectrumViewModel.MeasurementViewModels.Count.Should().Be(AmountMeasurement);

            for (int i = 0; i < AmountMeasurement; i++)
            {
                this._spectrumViewModel.MeasurementViewModels[i].Id.Should().Be(i + 1);
                this._spectrumViewModel.MeasurementViewModels[i].CurrentFrequency.Should().Be(1);
            }
        }

        [TestMethod]
        public void InitializesMeasurementDimensions()
        {
            // Assert
            this._spectrumViewModel.XMeasurementSize.Should().NotBe(0);
            this._spectrumViewModel.YMeasurementSize.Should().NotBe(0);
            this._spectrumViewModel.XOverallMeasurementSize.Should().NotBe(0);
            this._spectrumViewModel.YOverallMeasurementSize.Should().NotBe(0);
        }

        [TestMethod]
        public void SelectedMeasurementViewModelNullAfterInit()
        {
            // Assert
            this._spectrumViewModel.SelectedMeasurementViewModel.Should().BeNull();
        }

        [TestMethod]
        public void DiagramMeasurementViewModelNullAfterInit()
        {
            // Assert
            this._spectrumViewModel.DiagramMeasurementViewModel.Should().BeNull();
        }

        [TestMethod]
        public void OverallMinMaxMeasurementValuesSetAfterInit()
        {
            // Assert
            this._spectrumViewModel.OverallMinMeasurementViewModel.Should().NotBeNull();
            this._spectrumViewModel.OverallMaxMeasurementViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void ChangesMinAndMaxMeasurementValuesWhenDetectorTypeChanges()
        {
            // Arrange
            ICriterion criterion = Restrictions.And(Restrictions.Le(Projections.Property<MeasurementValue>(x => x.Measurement.ID), AmountMeasurement),
                                                                    Restrictions.Ge(Projections.Property<MeasurementValue>(x => x.Measurement.ID), 1));

            ICollection<MeasurementValue> minMeasurementValues = this._repository.GetMinMeasuremntValueCollectionByCriterion(criterion, x => x.IntensityAVGDetector);
            ICollection<MeasurementValue> maxMeasurementValues = this._repository.GetMaxMeasuremntValueCollectionByCriterion(criterion, x => x.IntensityAVGDetector);

            // Act
            this._spectrumViewModel.SelectedDetectorType = DetectorType.Average;

            // Assert
            for (int i = 0; i < AmountMeasurement; i++)
            {
                this._spectrumViewModel.MeasurementViewModels[i].MinFrequency.Should().Be(minMeasurementValues.ElementAt(i).Frequency);
                this._spectrumViewModel.MeasurementViewModels[i].MinIntensity.Should().Be(minMeasurementValues.ElementAt(i).IntensityAVGDetector);
                this._spectrumViewModel.MeasurementViewModels[i].MaxFrequency.Should().Be(maxMeasurementValues.ElementAt(i).Frequency);
                this._spectrumViewModel.MeasurementViewModels[i].MaxIntensity.Should().Be(maxMeasurementValues.ElementAt(i).IntensityAVGDetector);
            }
        }

        [TestMethod]
        public void ChangesOverallMinMaxValuesWhenDetectorTypeChanges()
        {
            // Arrange
            List<string> changedProperties = new List<string>();
            this._spectrumViewModel.PropertyChanged += (sender, args) => changedProperties.Add(args.PropertyName);

            // Act
            this._spectrumViewModel.SelectedDetectorType = DetectorType.Average;

            // Assert
            changedProperties.Should().HaveCount(5);
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<SpectrumViewModel>(x => x.SelectedDetectorType));
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<SpectrumViewModel>(x => x.OverallMaxMeasurementViewModel));
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<SpectrumViewModel>(x => x.OverallMaxMeasurementViewModel));
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<SpectrumViewModel>(x => x.MinIntensityIndicator));
            changedProperties.Should().Contain(TestFunctions.GetPropertyName<SpectrumViewModel>(x => x.MaxIntensityIndicator));
        }

        [TestMethod]
        public void ChangesCurrentMeasurementValuesWhenSelectedFrequencyIndexChanges()
        {
            // Arrange
            const int NewFrequency = 25;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            ICriterion measurementValueCriterion = Restrictions.Where<MeasurementValue>(x => x.Frequency == NewFrequency);
            ICriterion measurementCriterion = Restrictions.Where<Measurement>(x => x.Analysis.ID == 1);

            ICollection<MeasurementValue> currentMeasurementValues =
                    this._repository.GetCollectionByCriterion<MeasurementValue, Measurement>(measurementValueCriterion,
                                                                                             x => x.Measurement,
                                                                                             measurementCriterion,
                                                                                             x => x.Measurement.ID);

            // Act
            this._spectrumViewModel.SelectedFrequencyIndex = NewFrequency - 1;

            // Assert
            for (int i = 0; i < this._spectrumViewModel.MeasurementViewModels.Count; i++)
            {
                this._spectrumViewModel.MeasurementViewModels[i].CurrentFrequency.Should().Be(NewFrequency);
                this._spectrumViewModel.MeasurementViewModels[i].CurrentIntensity.Should().Be(currentMeasurementValues.ElementAt(i).IntensityMaxDetector);
            }
        }

        [TestMethod]
        public void IncrementsSelectedFrequencyIndex()
        {
            // Act
            this._spectrumViewModel.SelectGreaterIndexCommand.Execute(null);

            // Assert
            this._spectrumViewModel.SelectedFrequencyIndex.Should().Be(1);
        }

        [TestMethod]
        public void DoesNotIncrementSelectedFrequencyIndexWhenMaxFrequencyIndexReached()
        {
            // Arrange
            this._spectrumViewModel.SelectedFrequencyIndex = this._spectrumViewModel.MaxFrequencyIndex;

            // Act
            this._spectrumViewModel.SelectGreaterIndexCommand.Execute(null);

            // Assert
            this._spectrumViewModel.SelectedFrequencyIndex.Should().Be(this._spectrumViewModel.MaxFrequencyIndex);
        }

        [TestMethod]
        public void DecrementsSelectedFrequencyIndex()
        {
            // Arrange
            this._spectrumViewModel.SelectedFrequencyIndex = 1;

            // Act
            this._spectrumViewModel.SelectSmallerIndexCommand.Execute(null);

            // Assert
            this._spectrumViewModel.SelectedFrequencyIndex.Should().Be(0);
        }

        [TestMethod]
        public void DoesNotDecrementSelectedFrequencyIndexWhenMinFrequencyIndexReached()
        {
            // Act
            this._spectrumViewModel.SelectSmallerIndexCommand.Execute(null);

            // Assert
            this._spectrumViewModel.SelectedFrequencyIndex.Should().Be(0);
        }

        [TestMethod]
        public void SelectsMeasurementWhereOverallMinIntensityOccuredAndSetsFrequencyIndex()
        {
            // Act
            this._spectrumViewModel.SelectOverallMinIntensityCommand.Execute(null);

            // Assert
            this._spectrumViewModel.SelectedMeasurementViewModel.Should().Be(this._spectrumViewModel.OverallMinMeasurementViewModel);
            this._spectrumViewModel.SelectedFrequency.Should().Be(this._spectrumViewModel.OverallMinMeasurementViewModel.MinFrequency);
        }

        [TestMethod]
        public void SelectsMeasurementWhereOverallMaxIntensityOccuredAndSetsFrequencyIndex()
        {
            // Act
            this._spectrumViewModel.SelectOverallMaxIntensityCommand.Execute(null);

            // Assert
            this._spectrumViewModel.SelectedMeasurementViewModel.Should().Be(this._spectrumViewModel.OverallMaxMeasurementViewModel);
            this._spectrumViewModel.SelectedFrequency.Should().Be(this._spectrumViewModel.OverallMaxMeasurementViewModel.MaxFrequency);
        }

        [TestMethod]
        public void SetsFrequencyWhereMinIntensityOfSelectedMeasurementIs()
        {
            // Arrange
            this._spectrumViewModel.SelectedMeasurementViewModel = this._spectrumViewModel.MeasurementViewModels[0];

            // Act
            this._spectrumViewModel.SelectFrequencyIndexOfSelectedMeasurementWhereMinIntensityCommand.Execute(null);

            // Assert
            this._spectrumViewModel.SelectedFrequency.Should().Be(this._spectrumViewModel.MeasurementViewModels[0].MinFrequency);
        }

        [TestMethod]
        public void SetsFrequencyWhereMaxIntensityOfSelectedMeasurementIs()
        {
            // Arrange
            this._spectrumViewModel.SelectedMeasurementViewModel = this._spectrumViewModel.MeasurementViewModels[0];

            // Act
            this._spectrumViewModel.SelectFrequencyIndexOfSelectedMeasurementWhereMaxIntensityCommand.Execute(null);

            // Assert
            this._spectrumViewModel.SelectedFrequency.Should().Be(this._spectrumViewModel.MeasurementViewModels[0].MaxFrequency);
        }

        [TestMethod]
        public void SetsAllCurrentMeasurementValuesOfMeasurementsToMinMeasurementValues()
        {
            // Act
            this._spectrumViewModel.SelectAllMinIntensitiesCommand.Execute(null);

            // Assert
            foreach (MeasurementViewModel measurementViewModel in this._spectrumViewModel.MeasurementViewModels)
            {
                measurementViewModel.CurrentFrequency.Should().Be(measurementViewModel.MinFrequency);
                measurementViewModel.CurrentIntensity.Should().Be(measurementViewModel.MinIntensity);
            }
        }

        [TestMethod]
        public void SetsAllCurrentMeasurementValuesOfMeasurementsToMaxMeasurementValues()
        {
            // Act
            this._spectrumViewModel.SelectAllMaxIntensitiesCommand.Execute(null);

            // Assert
            foreach (MeasurementViewModel measurementViewModel in this._spectrumViewModel.MeasurementViewModels)
            {
                measurementViewModel.CurrentFrequency.Should().Be(measurementViewModel.MaxFrequency);
                measurementViewModel.CurrentIntensity.Should().Be(measurementViewModel.MaxIntensity);
            }
        }

        [TestMethod]
        public void SetsDiagramMeasurementViewModelWhenSelectedMeasurementViewModelChanges()
        {
            // Act
            this._spectrumViewModel.SelectedMeasurementViewModel = this._spectrumViewModel.MeasurementViewModels[0];

            // Assert
            this._spectrumViewModel.DiagramMeasurementViewModel.Should().NotBeNull();
        }

        [TestMethod]
        public void SetsTrackerDataPointsInDiagramMeasurementViewModelWhenSelectedFrequencyIndexChanges()
        {
            // Arrange
            this._spectrumViewModel.SelectedMeasurementViewModel = this._spectrumViewModel.MeasurementViewModels[0];

            // Act
            this._spectrumViewModel.SelectedFrequencyIndex = AmountMeasurementValue - 1;

            // Assert
            this._spectrumViewModel.DiagramMeasurementViewModel.TrackerDataPoints[0].X.Should().Be(this._spectrumViewModel.SelectedFrequency);
            this._spectrumViewModel.DiagramMeasurementViewModel.TrackerDataPoints[1].X.Should().Be(this._spectrumViewModel.SelectedFrequency);
        }

        #endregion
    }
}