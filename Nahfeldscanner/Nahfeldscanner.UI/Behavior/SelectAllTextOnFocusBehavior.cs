﻿// ///////////////////////////////////
// File: SelectAllTextOnFocusBehavior.cs
// Last Change: 08.11.2017  09:15
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Behavior
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Interactivity;



    public class SelectAllTextOnFocusBehavior : Behavior<TextBox>
    {
        public static readonly DependencyProperty SelectTextOnFocusProperty = DependencyProperty
                .RegisterAttached("SelectTextOnFocus",
                                  typeof(bool),
                                  typeof(SelectAllTextOnFocusBehavior),
                                  new FrameworkPropertyMetadata(false, GotFocus));

        public static void SetSelectTextOnFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(SelectTextOnFocusProperty, value);
        }

        private static void GotFocus(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextBox textbox = d as TextBox;

            if (null == textbox)
            {
                return;
            }

            if ((e.NewValue as bool?).GetValueOrDefault((false)))
            {
                textbox.GotKeyboardFocus += OnKeyboardFocusSelectText;
            }
            else
            {
                textbox.GotKeyboardFocus -= OnKeyboardFocusSelectText;
            }
        }

        private static void OnKeyboardFocusSelectText(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (e.KeyboardDevice.IsKeyDown(Key.Tab))
            {
                ((TextBox)sender).SelectAll();
            }
        }
    }
}