﻿// ///////////////////////////////////
// File: BoolToVisibilityConverter.cs
// Last Change: 15.02.2018  12:24
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;



    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool && (bool)value)
            {
                return Visibility.Visible;
            }

            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility && (Visibility)value == Visibility.Visible)
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}