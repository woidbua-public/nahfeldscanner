﻿// ///////////////////////////////////
// File: ByteArrayToBitmapImageConverter.cs
// Last Change: 15.02.2018  12:25
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Windows.Data;
    using System.Windows.Media.Imaging;



    [ValueConversion(typeof(byte[]), typeof(BitmapImage))]
    public class ByteArrayToBitmapImageConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte[] byteArray = value as byte[];

            if (byteArray == null || byteArray.Length == 0)
            {
                return null;
            }

            BitmapImage image = new BitmapImage();

            using (MemoryStream ms = new MemoryStream(byteArray))
            {
                try
                {
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = ms;
                    image.EndInit();
                }
                catch
                {
                    // do nothing when image could not be created
                    return null;
                }
            }

            return image;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BitmapImage bitmapImage = value as BitmapImage;

            if (bitmapImage == null)
            {
                return null;
            }

            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));

            byte[] data;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                encoder.Save(memoryStream);
                data = memoryStream.ToArray();
            }

            return data;
        }

        #endregion
    }
}