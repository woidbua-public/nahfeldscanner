﻿// ///////////////////////////////////
// File: DoubleMultiplyConverter.cs
// Last Change: 15.02.2018  12:25
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;



    [ValueConversion(typeof(double), typeof(double))]
    internal class DoubleMultiplyConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;

            if (doubleValue == null)
            {
                return Binding.DoNothing;
            }

            if (parameter == null)
            {
                return doubleValue;
            }

            double doubleParameterValue;
            double.TryParse(parameter as string, out doubleParameterValue);

            return doubleParameterValue * doubleValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}