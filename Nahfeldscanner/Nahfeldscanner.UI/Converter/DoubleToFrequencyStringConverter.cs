﻿// ///////////////////////////////////
// File: DoubleToFrequencyStringConverter.cs
// Last Change: 15.01.2018  08:57
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Windows.Data;



    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleToFrequencyStringConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;

            if (doubleValue == null)
            {
                return string.Empty;
            }

            if (doubleValue < 1E3)
            {
                return string.Format("{0} Hz", doubleValue);
            }

            if (doubleValue >= 1E3 && doubleValue < 1E6)
            {
                return string.Format("{0} kHz", doubleValue / 1E3);
            }

            if (doubleValue >= 1E6 && doubleValue < 1E9)
            {
                return string.Format("{0} MHz", doubleValue / 1E6);
            }

            if (doubleValue >= 1E9)
            {
                return string.Format("{0} GHz", doubleValue / 1E9);
            }

            return string.Format("{0} Hz", doubleValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double doubleValue = 0;
            string stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue))
            {
                return doubleValue;
            }

            Regex regex = new Regex(@"^-?\d+(?:[\.\,]\d+)?");
            Match match = regex.Match(stringValue);

            if (match.Success)
            {
                doubleValue = double.Parse(match.Value.Replace(',', '.'), CultureInfo.InvariantCulture);
            }

            if (stringValue.EndsWith("k", StringComparison.OrdinalIgnoreCase))
            {
                doubleValue *= 1E3;
            }
            else if (stringValue.EndsWith("M", StringComparison.OrdinalIgnoreCase))
            {
                doubleValue *= 1E6;
            }
            else if (stringValue.EndsWith("G", StringComparison.OrdinalIgnoreCase))
            {
                doubleValue *= 1E9;
            }

            return doubleValue;
        }

        #endregion
    }
}