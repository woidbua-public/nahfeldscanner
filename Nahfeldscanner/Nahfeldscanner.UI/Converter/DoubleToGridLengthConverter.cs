﻿// ///////////////////////////////////
// File: DoubleToGridLengthConverter.cs
// Last Change: 16.02.2018  08:44
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;



    public class DoubleToGridLengthConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType == typeof(GridLength))
            {
                if (value is double)
                {
                    return new GridLength((double)value);
                }

                return GridLength.Auto;
            }

            if (targetType == typeof(double))
            {
                if (value is GridLength)
                {
                    return ((GridLength)value).Value;
                }

                return double.NaN;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return this.Convert(value, targetType, parameter, culture);
        }

        #endregion
    }
}