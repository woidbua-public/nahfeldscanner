﻿// ///////////////////////////////////
// File: DoubleToIntensityFractionStringMultiConverter.cs
// Last Change: 15.02.2018  12:27
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;



    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleToIntensityFractionStringMultiConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            double? minValue = value[0] as double?;
            double? maxValue = value[1] as double?;

            if (minValue == null || maxValue == null)
            {
                return Binding.DoNothing;
            }

            if (minValue > maxValue)
            {
                double? tempValue = maxValue;
                maxValue = minValue;
                minValue = tempValue;
            }

            if (parameter == null)
            {
                return Binding.DoNothing;
            }

            double doubleParameterValue;
            double.TryParse(parameter as string, out doubleParameterValue);

            return Math.Round((double)minValue + ((double)maxValue - (double)minValue) * doubleParameterValue, 2).ToString(CultureInfo.CurrentCulture) + " dBm";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}