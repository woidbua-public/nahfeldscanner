﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nahfeldscanner.UI.Converter
{
    using System.Globalization;
    using System.Windows.Data;



    [ValueConversion(typeof(double?), typeof(string))]
    public class DoubleToLimitDimensionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;

            if (doubleValue == null)
            {
                return string.Empty;
            }

            if (parameter != null)
            {
                return string.Format("{0} = {1} mm", parameter, Math.Round((double)doubleValue, 1));
            }

            return string.Format("{0} mm", Math.Round((double)doubleValue, 1));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
