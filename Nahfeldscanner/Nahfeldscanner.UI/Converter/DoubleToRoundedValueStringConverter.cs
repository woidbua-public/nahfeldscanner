﻿// ///////////////////////////////////
// File: DoubleToRoundedValueStringConverter.cs
// Last Change: 13.11.2017  15:07
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Windows.Data;



    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleToRoundedValueStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;

            if (doubleValue == null)
            {
                return string.Empty;
            }

            return string.Format("{0}", doubleValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double doubleValue = 0;
            string stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue))
            {
                return doubleValue;
            }

            Regex regex = new Regex(@"^-?\d+(?:[\.\,]\d+)?");
            Match match = regex.Match(stringValue);

            if (match.Success)
            {
                doubleValue = double.Parse(match.Value.Replace(',', '.'), CultureInfo.InvariantCulture);
            }

            return doubleValue;
        }
    }
}