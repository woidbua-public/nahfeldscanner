﻿// ///////////////////////////////////
// File: DoubleToSolidColorBrushConverter.cs
// Last Change: 15.02.2018  09:39
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media;



    [ValueConversion(typeof(double), typeof(SolidColorBrush))]
    public class DoubleToSolidColorBrushConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value[0] as double?;
            double? minValue = value[1] as double?;
            double? maxValue = value[2] as double?;

            if (doubleValue == null || minValue == null || maxValue == null)
            {
                return Binding.DoNothing;
            }

            if (minValue > maxValue)
            {
                double? tempValue = maxValue;
                maxValue = minValue;
                minValue = tempValue;
            }

            doubleValue = doubleValue < minValue ? minValue : doubleValue;
            doubleValue = doubleValue > maxValue ? maxValue : doubleValue;

            double deltaFaktor = Math.Abs((double)maxValue - (double)minValue) / 100;

            if (deltaFaktor.Equals(0.0))
            {
                return Binding.DoNothing;
            }

            doubleValue = Math.Abs((double)doubleValue - (double)minValue) / deltaFaktor;

            byte r = System.Convert.ToByte(2.55 * doubleValue);
            byte g = System.Convert.ToByte(2.55 * (100 - doubleValue));
            const byte b = 0;

            return new SolidColorBrush(Color.FromArgb(255, r, g, b));
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}