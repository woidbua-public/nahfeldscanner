﻿// ///////////////////////////////////
// File: EnumDetectorTypeToRestrictedEnumDetectorType.cs
// Last Change: 24.11.2017  12:26
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Data;
    using Nahfeldscanner.Model.Enum;



    [ValueConversion(typeof(bool), typeof(Enum))]
    public class EnumDetectorTypeToRestrictedEnumDetectorType : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool? boolValue = value as bool?;

            if (boolValue == false)
            {
                return Enum.GetValues(typeof(DetectorType)).Cast<DetectorType>().Where(x => x != DetectorType.Quasipeak);
            }

            return Enum.GetValues(typeof(DetectorType));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}