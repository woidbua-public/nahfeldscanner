﻿// ///////////////////////////////////
// File: HeightToMinimizedHeightConverter.cs
// Last Change: 20.12.2017  15:07
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Drawing;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media.Imaging;



    [ValueConversion(typeof(Bitmap), typeof(BitmapImage))]
    public class HeightToMinimizedHeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;

            if (doubleValue == null)
            {
                return Binding.DoNothing;
            }

            if (parameter == null)
            {
                return doubleValue;
            }

            double doubleParameterValue;
            double.TryParse(parameter as string, out doubleParameterValue);

            return doubleValue - doubleParameterValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}