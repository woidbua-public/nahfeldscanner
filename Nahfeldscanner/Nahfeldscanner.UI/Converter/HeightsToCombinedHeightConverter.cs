﻿// ///////////////////////////////////
// File: HeightsToCombinedHeightConverter.cs
// Last Change: 23.12.2017  09:29
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;



    [ValueConversion(typeof(int), typeof(int))]
    public class HeightsToCombinedHeightConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double height = 0;

            foreach (object value in values)
            {
                double? intValue = value as double?;

                if (intValue == null)
                {
                    continue;
                }

                height += (double)intValue;
            }

            return height;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}