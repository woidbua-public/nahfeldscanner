﻿// ///////////////////////////////////
// File: NullableBoolToBitmapImageConverter.cs
// Last Change: 08.11.2017  08:37
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media.Imaging;



    [ValueConversion(typeof(bool?), typeof(BitmapImage))]
    public class NullableBoolToBitmapImageConverter : IValueConverter
    {
        public BitmapImage TrueImage { get; set; }

        public BitmapImage FalseImage { get; set; }

        public BitmapImage NullImage { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool? result = value as bool?;

            if (result == true)
            {
                return this.TrueImage;
            }

            if (result == false)
            {
                return this.FalseImage;
            }

            return this.NullImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}