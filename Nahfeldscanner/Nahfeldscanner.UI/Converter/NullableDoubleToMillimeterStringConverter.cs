﻿// ///////////////////////////////////
// File: NullableDoubleToMillimeterStringConverter.cs
// Last Change: 13.11.2017  14:26
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Windows.Data;



    [ValueConversion(typeof(double?), typeof(string))]
    public class NullableDoubleToMillimeterStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;

            if (doubleValue == null)
            {
                return string.Empty;
            }

            if (parameter != null)
            {
                int intValue;
                int.TryParse((string)parameter, out intValue);

                if (intValue != 0)
                {
                    return string.Format("{0} mm", Math.Round((double)doubleValue, intValue));
                }
            }

            return string.Format("{0} mm", Math.Round((double)doubleValue, 1));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = null;
            string stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue))
            {
                return doubleValue;
            }

            Regex regex = new Regex(@"^-?\d+(?:[\.\,]\d+)?");
            Match match = regex.Match(stringValue);

            if (match.Success)
            {
                doubleValue = double.Parse(match.Value.Replace(',', '.'), CultureInfo.InvariantCulture);
            }

            return doubleValue;
        }
    }
}