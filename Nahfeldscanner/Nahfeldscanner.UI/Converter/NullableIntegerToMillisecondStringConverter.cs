﻿// ///////////////////////////////////
// File: NullableIntegerToMillisecondStringConverter.cs
// Last Change: 08.11.2017  08:39
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Windows.Data;



    [ValueConversion(typeof(int?), typeof(string))]
    internal class NullableIntegerToMillisecondStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? intValue = value as int?;

            if (intValue == null)
            {
                return string.Empty;
            }

            return string.Format("{0} ms", intValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? intValue = null;
            string stringValue = value as string;

            if (string.IsNullOrEmpty(stringValue))
            {
                return intValue;
            }

            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(stringValue);

            if (match.Success)
            {
                intValue = int.Parse(match.Value, CultureInfo.InvariantCulture);
            }

            return intValue;
        }
    }
}