﻿// ///////////////////////////////////
// File: NullablePositionToCanvasDimensionConverter.cs
// Last Change: 08.11.2017  08:48
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using Nahfeldscanner.UI.Properties;



    [ValueConversion(typeof(double?), typeof(double?))]
    public class NullablePositionToCanvasDimensionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;

            if (doubleValue == null)
            {
                return Binding.DoNothing;
            }

            if (parameter != null)
            {
                double parameterValue;
                double.TryParse((string)parameter, out parameterValue);
                return (double)doubleValue * Settings.Default.MeasurementCanvasDimensionFactor - parameterValue / 2;
            }

            return (double)doubleValue * Settings.Default.MeasurementCanvasDimensionFactor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}