﻿// ///////////////////////////////////
// File: NullablePositionToCanvasDimensionPlusStrokeSizeConverter.cs
// Last Change: 08.11.2017  08:55
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using Nahfeldscanner.UI.Properties;



    [ValueConversion(typeof(double), typeof(double))]
    public class NullablePositionToCanvasDimensionPlusStrokeSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;

            if (doubleValue == null)
            {
                return Binding.DoNothing;
            }

            return (double)doubleValue * Settings.Default.MeasurementCanvasDimensionFactor + Settings.Default.StrokeSize;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}