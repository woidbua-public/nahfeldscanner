﻿// ///////////////////////////////////
// File: SensorDescriptionConverter.cs
// Last Change: 19.02.2018  11:48
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;



    public class SensorDescriptionConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string sensorName = (string)values[0];
            string minFreq = this.ConvertToFrequencyString(values[1] as double?);
            string maxFreq = this.ConvertToFrequencyString(values[2] as double?);

            if (string.IsNullOrEmpty(minFreq))
            {
                minFreq = "???";
            }

            if (string.IsNullOrEmpty(maxFreq))
            {
                maxFreq = "???";
            }

            return string.Format("{0} ({1} - {2})", sensorName, minFreq, maxFreq);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        private string ConvertToFrequencyString(double? value)
        {
            double? doubleValue = value;

            if (doubleValue == null)
            {
                return string.Empty;
            }

            if (doubleValue < 1E3)
            {
                return string.Format("{0} Hz", doubleValue);
            }

            if (doubleValue >= 1E3 && doubleValue < 1E6)
            {
                return string.Format("{0} kHz", doubleValue / 1E3);
            }

            if (doubleValue >= 1E6 && doubleValue < 1E9)
            {
                return string.Format("{0} MHz", doubleValue / 1E6);
            }

            if (doubleValue >= 1E9)
            {
                return string.Format("{0} GHz", doubleValue / 1E9);
            }

            return string.Format("{0} Hz", doubleValue);
        }
    }
}