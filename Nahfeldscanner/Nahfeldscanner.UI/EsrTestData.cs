﻿// ///////////////////////////////////
// File: EsrTestData.cs
// Last Change: 22.01.2018  11:24
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI
{
    using System;
    using System.Collections.Generic;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;



    // TODO: delete test class
    public static class EsrTestData
    {
        #region Fields

        private const double xStart = 10;
        private const double deltaX = 2;
        private const double xEnd = 30;
        private const double yStart = 0;
        private const double deltaY = 1;
        private const double yEnd = 20;
        private const double freqStart = 1000000;
        private const double deltaFreq = 1000000;
        private const double freqEnd = 100000000;

        #endregion



        public static Analysis GetAnalysis()
        {
            Analysis analysis = new Analysis();
            analysis.StartTime = "23.10.2017 08:51:34";
            analysis.EndTime = "23.10.2017 08:56:10";
            analysis.Description = "Messung mit E-Feld-Sonde";
            analysis.FinishedAnalysis = true;
            analysis.Parameter = GetParameter();

            foreach (var measurement in GetMeasurements())
            {
                analysis.AddMeasurement(measurement);
            }

            return analysis;
        }

        private static Parameter GetParameter()
        {
            Parameter parameter = new Parameter
                                  {
                                          XStart = xStart,
                                          DeltaX = deltaX,
                                          XEnd = xEnd,
                                          YStart = yStart,
                                          DeltaY = deltaY,
                                          YEnd = yEnd,
                                          FreqStart = freqStart,
                                          DeltaFreq = deltaFreq,
                                          FreqEnd = freqEnd,
                                          ResBW = ResBW.BW_100_Hz,
                                          MeasureTime = 5,
                                  };

            return parameter;
        }

        private static List<Measurement> GetMeasurements()
        {
            Random random = new Random();
            List<Measurement> measurements = new List<Measurement>();

            for (int x = (int)xStart; x <= (int)xEnd; x += (int)deltaX)
            {
                for (int y = (int)yStart; y <= (int)yEnd; y += (int)deltaY)
                {
                    Measurement measurement = new Measurement
                                              {
                                                      X = x,
                                                      Y = y,

                                                      // TODO: uncomment
                                                      //IntensitiesMaxDetector = GetIntensities(random),
                                                      //IntensitiesAVGDetector = GetIntensities(random),
                                                      //IntensitiesQuasipeakDetector = GetIntensities(random)
                                              };

                    measurements.Add(measurement);
                }
            }

            return measurements;
        }

        private static double[] GetIntensities(Random random)
        {
            int numberOfFrequencies = Convert.ToInt32((freqEnd - freqStart) / deltaFreq + 1);
            List<double> intensities = new List<double>();

            for (int i = 0; i < numberOfFrequencies; i++)
            {
                intensities.Add(random.NextDouble() * 100);
            }

            return intensities.ToArray();
        }
    }
}