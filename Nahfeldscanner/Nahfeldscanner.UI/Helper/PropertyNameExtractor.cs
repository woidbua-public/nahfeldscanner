﻿// ///////////////////////////////////
// File: PropertyNameExtractor.cs
// Last Change: 19.02.2018  12:09
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Helper
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;



    public static class PropertyNameExtractor
    {
        public static string GetPropertyName<TPropertySource>(Expression<Func<TPropertySource, object>> expression)
        {
            var lambda = expression as LambdaExpression;
            MemberExpression memberExpression;

            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = lambda.Body as UnaryExpression;
                memberExpression = unaryExpression.Operand as MemberExpression;
            }
            else
            {
                memberExpression = lambda.Body as MemberExpression;
            }

            if (memberExpression != null)
            {
                var propertyInfo = memberExpression.Member as PropertyInfo;

                if (propertyInfo != null)
                {
                    return propertyInfo.Name;
                }
            }

            return null;
        }
    }
}