﻿// ///////////////////////////////////
// File: DialogService.cs
// Last Change: 13.12.2017  12:41
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Service
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using MahApps.Metro.Controls;
    using MahApps.Metro.Controls.Dialogs;
    using Application = System.Windows.Application;



    public class DialogService : IDialogService
    {
        public async Task ShowMessage(string title, string message)
        {
            MetroWindow metroWindow = Application.Current.MainWindow as MetroWindow;
            await metroWindow.ShowMessageAsync(title, message);
        }

        public async Task ShowMessage(string title, string message, string detailedMessage)
        {
            MetroWindow metroWindow = Application.Current.MainWindow as MetroWindow;
            await metroWindow.ShowMessageAsync(title, message + "\n\n" + detailedMessage);
        }

        public async Task<bool> ShowDialogYesNo(string title, string message)
        {
            MetroWindow metroWindow = Application.Current.MainWindow as MetroWindow;

            MetroDialogSettings settings = new MetroDialogSettings
                                           {
                                               AffirmativeButtonText = "Yes",
                                               NegativeButtonText = "No"
                                           };

            MessageDialogResult result = await metroWindow.ShowMessageAsync(title, message, MessageDialogStyle.AffirmativeAndNegative, settings);

            if (result == MessageDialogResult.Affirmative)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> ShowCustomDialog(string title, string message, string button1, string button2)
        {
            MetroWindow metroWindow = Application.Current.MainWindow as MetroWindow;

            MetroDialogSettings settings = new MetroDialogSettings
                                           {
                                               AffirmativeButtonText = button1,
                                               NegativeButtonText = button2
                                           };

            MessageDialogResult result = await metroWindow.ShowMessageAsync(title, message, MessageDialogStyle.AffirmativeAndNegative, settings);

            if (result == MessageDialogResult.Affirmative)
            {
                return true;
            }

            return false;
        }

        public string ShowImageFileDialog()
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Title = @"Geben Sie das gewünschte Bild an.";
                ofd.Filter = @"Image Files(*.BMP;*.JPG;*.JPEG;*.GIF)|*.BMP;*.JPG;*.JPEG;*.GIF";
                ofd.InitialDirectory = Environment.UserName;
                ofd.Multiselect = false;
                ofd.RestoreDirectory = true;

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    return ofd.FileName;
                }

                return null;
            }
        }

        public string ShowDatabaseFileDialog()
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Title = @"Geben Sie den Pfad zur Datenbank an.";
                ofd.Filter = @"db file (*.db)|*.db";
                ofd.InitialDirectory = Environment.UserName;
                ofd.Multiselect = false;
                ofd.RestoreDirectory = true;

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    return ofd.FileName;
                }

                return null;
            }
        }

        public string ShowSaveDatabaseFileDialog()
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = @"Geben Sie den Pfad und den Dateinamen der neuen Datenbank an.";
                sfd.InitialDirectory = Environment.UserName;
                sfd.Filter = @"Database file (*.db)|*.db";

                if (sfd.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(sfd.FileName))
                {
                    return sfd.FileName;
                }

                return null;
            }
        }
    }
}