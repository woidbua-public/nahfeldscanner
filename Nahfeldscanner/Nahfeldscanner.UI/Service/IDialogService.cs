﻿// ///////////////////////////////////
// File: IDialogService.cs
// Last Change: 13.12.2017  12:42
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.Service
{
    using System.Threading.Tasks;



    public interface IDialogService
    {
        Task ShowMessage(string title, string message);

        Task ShowMessage(string title, string message, string detailedMessage);

        Task<bool> ShowDialogYesNo(string title, string message);

        Task<bool> ShowCustomDialog(string title, string message, string button1, string button2);

        string ShowImageFileDialog();

        string ShowDatabaseFileDialog();

        string ShowSaveDatabaseFileDialog();
    }
}