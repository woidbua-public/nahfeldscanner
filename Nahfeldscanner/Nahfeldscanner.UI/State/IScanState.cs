﻿namespace Nahfeldscanner.UI.State
{
    public interface IScanState
    {
        bool CanCreate();

        bool CanRepeat();

        bool CanDelete();

        bool CanRun();

        bool CanPause();

        bool CanStop();
    }
}