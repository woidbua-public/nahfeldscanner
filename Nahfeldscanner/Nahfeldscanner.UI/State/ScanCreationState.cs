﻿namespace Nahfeldscanner.UI.State
{
    public class ScanCreationState : IScanState
    {
        public bool CanCreate()
        {
            return false;
        }

        public bool CanRepeat()
        {
            return false;
        }

        public bool CanDelete()
        {
            return true;
        }

        public bool CanRun()
        {
            return true;
        }

        public bool CanPause()
        {
            return false;
        }

        public bool CanStop()
        {
            return false;
        }
    }
}