﻿// ///////////////////////////////////
// File: ScanEditState.cs
// Last Change: 16.02.2018  08:57
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.State
{
    internal class ScanEditState : IScanState
    {
        #region IScanState Members

        public bool CanCreate()
        {
            return false;
        }

        public bool CanRepeat()
        {
            return false;
        }

        public bool CanDelete()
        {
            return false;
        }

        public bool CanRun()
        {
            return false;
        }

        public bool CanPause()
        {
            return false;
        }

        public bool CanStop()
        {
            return false;
        }

        #endregion
    }
}