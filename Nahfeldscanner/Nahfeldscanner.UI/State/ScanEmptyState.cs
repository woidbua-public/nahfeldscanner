﻿namespace Nahfeldscanner.UI.State
{
    public class ScanEmptyState : IScanState
    {
        public bool CanCreate()
        {
            return true;
        }

        public bool CanRepeat()
        {
            return false;
        }

        public bool CanDelete()
        {
            return false;
        }

        public bool CanRun()
        {
            return false;
        }

        public bool CanPause()
        {
            return false;
        }

        public bool CanStop()
        {
            return false;
        }
    }
}