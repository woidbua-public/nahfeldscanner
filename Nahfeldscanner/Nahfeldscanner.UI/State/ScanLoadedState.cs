﻿namespace Nahfeldscanner.UI.State
{
    public class ScanLoadedState : IScanState
    {
        public bool CanCreate()
        {
            return true;
        }

        public bool CanRepeat()
        {
            return true;
        }

        public bool CanDelete()
        {
            return true;
        }

        public bool CanRun()
        {
            return false;
        }

        public bool CanPause()
        {
            return false;
        }

        public bool CanStop()
        {
            return false;
        }
    }
}