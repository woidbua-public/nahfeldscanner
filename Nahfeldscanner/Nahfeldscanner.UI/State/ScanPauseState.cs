﻿namespace Nahfeldscanner.UI.State
{
    public class ScanPauseState : IScanState
    {
        public bool CanCreate()
        {
            return false;
        }

        public bool CanRepeat()
        {
            return false;
        }

        public bool CanDelete()
        {
            return false;
        }

        public bool CanRun()
        {
            return true;
        }

        public bool CanPause()
        {
            return false;
        }

        public bool CanStop()
        {
            return true;
        }
    }
}