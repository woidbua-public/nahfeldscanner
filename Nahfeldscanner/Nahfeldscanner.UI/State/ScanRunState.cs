﻿namespace Nahfeldscanner.UI.State
{
    public class ScanRunState : IScanState
    {
        public bool CanCreate()
        {
            return false;
        }

        public bool CanRepeat()
        {
            return false;
        }

        public bool CanDelete()
        {
            return false;
        }

        public bool CanRun()
        {
            return false;
        }

        public bool CanPause()
        {
            return true;
        }

        public bool CanStop()
        {
            return true;
        }
    }
}