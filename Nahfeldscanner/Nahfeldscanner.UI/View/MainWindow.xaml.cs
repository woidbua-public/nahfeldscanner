﻿// ///////////////////////////////////
// File: MainWindow.xaml.cs
// Last Change: 06.11.2017  09:49
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.View
{
    using MahApps.Metro.Controls;



    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            this.InitializeComponent();
        }
    }
}