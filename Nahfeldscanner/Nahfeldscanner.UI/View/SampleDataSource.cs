﻿// ///////////////////////////////////
// File: SampleDataSource.cs
// Last Change: 20.12.2017  13:34
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.View
{
    using System.Collections.ObjectModel;
    using System.Drawing;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using Nahfeldscanner.UI.ViewModel;



    public static class SampleDataSource
    {
        public static SensorViewModel SensorViewModel
        {
            get
            {
                Bitmap bitmap = Resources.img_home;
                ImageConverter imageConverter = new ImageConverter();

                Sensor sensor = new Sensor();
                sensor.SensorType = SensorType.E_Field;
                sensor.SensorName = "Testsensor XWE423";
                sensor.SensorImageStream = (byte[])imageConverter.ConvertTo(bitmap, typeof(byte[]));

                return new SensorViewModel(sensor);
            }
        }

        public static SensorConfigurationViewModel SensorConfigurationViewModel
        {
            get
            {
                SensorConfigurationViewModel sensorConfigurationViewModel = new SensorConfigurationViewModel(new SqLiteRepository(new SqLiteSessionManager()), new DialogService())
                                                                            {
                                                                                SelectedSensorViewModel = SensorViewModel,
                                                                                NewSensorViewModel = SensorViewModel,
                                                                                SensorViewModels = new ObservableCollection<SensorViewModel>() { SensorViewModel, SensorViewModel, SensorViewModel }
                                                                            };
                return sensorConfigurationViewModel;
            }
        }
    }
}