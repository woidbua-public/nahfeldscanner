﻿using System.Windows.Controls;

namespace Nahfeldscanner.UI.View.User_Control
{
    /// <summary>
    /// Interaction logic for AnalysisView.xaml
    /// </summary>
    public partial class AnalysisView : UserControl
    {
        public AnalysisView()
        {
            InitializeComponent();
        }
    }
}