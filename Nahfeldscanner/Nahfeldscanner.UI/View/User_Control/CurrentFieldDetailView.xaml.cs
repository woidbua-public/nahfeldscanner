﻿// ///////////////////////////////////
// File: CurrentFieldDetailView.xaml.cs
// Last Change: 15.02.2018  10:12
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.View.User_Control
{
    using System.Windows.Controls;



    /// <summary>
    ///     Interaction logic for CurrentFieldDetailView.xaml
    /// </summary>
    public partial class CurrentFieldDetailView : UserControl
    {
        #region Constructors / Destructor

        public CurrentFieldDetailView()
        {
            InitializeComponent();
        }

        #endregion
    }
}