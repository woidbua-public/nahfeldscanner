﻿// ///////////////////////////////////
// File: ParameterView.xaml.cs
// Last Change: 15.12.2017  11:18
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.View.User_Control
{
    using System.Windows.Controls;



    /// <summary>
    ///     Interaction logic for ParameterView.xaml
    /// </summary>
    public partial class ParameterView : UserControl
    {
        public ParameterView()
        {
            this.InitializeComponent();
        }
    }
}