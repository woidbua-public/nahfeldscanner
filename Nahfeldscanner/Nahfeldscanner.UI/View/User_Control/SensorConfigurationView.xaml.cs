﻿// ///////////////////////////////////
// File: SensorConfigurationView.xaml.cs
// Last Change: 23.12.2017  11:55
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.View.User_Control
{
    using System.Windows.Controls;



    /// <summary>
    ///     Interaction logic for SensorConfigurationView.xaml
    /// </summary>
    public partial class SensorConfigurationView : UserControl
    {
        public SensorConfigurationView()
        {
            this.InitializeComponent();
        }
    }
}