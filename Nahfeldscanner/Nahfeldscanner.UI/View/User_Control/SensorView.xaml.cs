﻿// ///////////////////////////////////
// File: SensorView.xaml.cs
// Last Change: 19.02.2018  12:31
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.View.User_Control
{
    using System.Windows.Controls;



    /// <summary>
    ///     Interaction logic for SensorView.xaml
    /// </summary>
    public partial class SensorView : UserControl
    {
        #region Constructors / Destructor

        public SensorView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}