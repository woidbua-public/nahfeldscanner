﻿// ///////////////////////////////////
// File: SetupView.xaml.cs
// Last Change: 13.11.2017  13:02
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.View.User_Control
{
    using System.Windows.Controls;



    /// <summary>
    ///     Interaction logic for ArduinoConfigurationView.xaml
    /// </summary>
    public partial class SetupView : UserControl
    {
        public SetupView()
        {
            this.InitializeComponent();
        }
    }
}