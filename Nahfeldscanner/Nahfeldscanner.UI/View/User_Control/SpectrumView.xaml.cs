﻿// ///////////////////////////////////
// File: SpectrumView.xaml.cs
// Last Change: 15.02.2018  08:33
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.View.User_Control
{
    using System.Windows.Controls;



    /// <summary>
    ///     Interaction logic for SpectrumView.xaml
    /// </summary>
    public partial class SpectrumView : UserControl
    {
        #region Constructors / Destructor

        public SpectrumView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}