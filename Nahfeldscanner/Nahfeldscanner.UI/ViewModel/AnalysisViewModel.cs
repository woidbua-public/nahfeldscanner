﻿// ///////////////////////////////////
// File: AnalysisViewModel.cs
// Last Change: 24.01.2018  08:19
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System.Linq;
    using System.Reflection;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;



    public class AnalysisViewModel : BindableViewModelBase
    {
        #region Fields

        private readonly Analysis analysis;

        private bool _canEdit;
        private PropertyInfo[] _propertyInfos;

        #endregion



        #region Constructors / Destructor

        public AnalysisViewModel(IRepository repository, Analysis analysis)
        {
            this.analysis = analysis;
            this.ParameterViewModel = new ParameterViewModel(repository, this.analysis.Parameter);
        }
    #endregion



        #region Properties

        public bool CanEdit
        {
            get { return this._canEdit; }
            set { this.SetProperty(() => this._canEdit = value, () => this._canEdit == value); }
        }

        public int ID
        {
            get { return this.analysis.ID; }
            set { this.SetProperty(() => this.analysis.ID = value, () => this.analysis.ID == value); }
        }

        public string StartTime
        {
            get { return this.analysis.StartTime; }
            set { this.SetProperty(() => this.analysis.StartTime = value, () => this.analysis.StartTime == value); }
        }

        public string EndTime
        {
            get { return this.analysis.EndTime; }
            set { this.SetProperty(() => this.analysis.EndTime = value, () => this.analysis.EndTime == value); }
        }

        public string Description
        {
            get { return this.analysis.Description; }
            set { this.SetProperty(() => this.analysis.Description = value, () => this.analysis.Description == value); }
        }

        public bool? FinishedAnalysis
        {
            get { return this.analysis.FinishedAnalysis; }
            set { this.SetProperty(() => this.analysis.FinishedAnalysis = value, () => this.analysis.FinishedAnalysis == value); }
        }

        public ParameterViewModel ParameterViewModel { get; private set; }

        private PropertyInfo[] PropertyInfos
        {
            get
            {
                if (this._propertyInfos == null)
                {
                    this._propertyInfos = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                              .Where(prop => prop.DeclaringType == this.GetType()).ToArray();
                }

                return this._propertyInfos;
            }
        }

        #endregion



        public void UpdateProperties()
        {
            foreach (PropertyInfo propertyInfo in this.PropertyInfos)
            {
                this.RaisePropertyChanged(propertyInfo.Name);
            }
        }
    }
}