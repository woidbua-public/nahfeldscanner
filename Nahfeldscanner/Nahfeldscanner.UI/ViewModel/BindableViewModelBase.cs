﻿// ///////////////////////////////////
// File: BindableViewModelBase.cs
// Last Change: 24.01.2018  08:22
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Runtime.CompilerServices;
    using GalaSoft.MvvmLight;



    public abstract class BindableViewModelBase : ViewModelBase
    {
        protected virtual bool SetProperty<T>(ref T storage, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (Equals(storage, newValue))
            {
                return false;
            }

            storage = newValue;
            this.RaisePropertyChanged(propertyName);
            return true;
        }

        protected virtual bool SetProperty(Action action, Func<bool> equal, [CallerMemberName] string propertyName = null)
        {
            if (equal())
            {
                return false;
            }

            action();
            this.RaisePropertyChanged(propertyName);
            return true;
        }
    }
}