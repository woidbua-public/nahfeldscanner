﻿// ///////////////////////////////////
// File: CommandViewModel.cs
// Last Change: 24.01.2018  08:22
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using GalaSoft.MvvmLight.Command;



    public class CommandViewModel : BindableViewModelBase
    {
        #region Constructors / Destructor

        public CommandViewModel(string displayName, RelayCommand relayCommand)
        {
            this.DisplayName = displayName;
            this.RelayCommand = relayCommand;
        }

        #endregion



        #region Properties

        public string DisplayName { get; private set; }

        public RelayCommand RelayCommand { get; private set; }

        #endregion
    }
}