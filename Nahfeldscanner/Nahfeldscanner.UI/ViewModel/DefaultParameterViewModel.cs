﻿// ///////////////////////////////////
// File: DefaultParameterViewModel.cs
// Last Change: 19.02.2018  12:03
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Helper;
    using Nahfeldscanner.UI.Markup;
    using Nahfeldscanner.UI.Properties;
    using ModelSettings = Nahfeldscanner.Model.Properties.Settings;



    public class DefaultParameterViewModel : BindableViewModelBase, IDataErrorInfo
    {
        #region Fields

        private readonly IRepository repository;

        private PropertyInfo[] _propertyInfos;

        private double _xSteps;
        private double _ySteps;

        private SensorViewModel _selectedSensorViewModel;

        private RelayCommand _resetChangesCommand;
        private RelayCommand _saveChangesCommand;
        private RelayCommand _reloadSensorsCommand;

        #endregion



        #region Constructors / Destructor

        public DefaultParameterViewModel(IRepository repository)
        {
            this.repository = repository;
            this.Sensors = new ObservableCollection<SensorViewModel>();

            this.InitSensors();

            this.CalculateXSteps();
            this.CalculateYSteps();

            // set explicitly to false after steps were calculated
            this.ValuesChanged = false;
        }

        #endregion



        #region Properties

        public bool DoQuasipeakMeasurement
        {
            get { return ModelSettings.Default.DoQuasipeakMeasurement; }
            set
            {
                if (this.SetProperty(() => ModelSettings.Default.DoQuasipeakMeasurement = value, () => ModelSettings.Default.DoQuasipeakMeasurement == value))
                {
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double OriginX
        {
            get { return ModelSettings.Default.OriginX; }
            set
            {
                bool changed;
                double roundedValue = this.GetOneDigitRoundedValue(value);
                double deltaX = ModelSettings.Default.OriginX - roundedValue;

                if (roundedValue < Settings.Default.XMin)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.OriginX = Settings.Default.XMin, () => ModelSettings.Default.OriginX.Equals(Settings.Default.XMin));
                }
                else if (roundedValue > Settings.Default.XMax)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.OriginX = Settings.Default.XMax, () => ModelSettings.Default.OriginX.Equals(Settings.Default.XMax));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.OriginX = roundedValue, () => ModelSettings.Default.OriginX.Equals(roundedValue));
                }

                if (changed)
                {
                    if (deltaX >= 0)
                    {
                        this.RelativeXEnd = this.RelativeXEnd + deltaX;
                        this.RelativeXStart = this.RelativeXStart + deltaX;
                    }
                    else
                    {
                        this.RelativeXStart = this.RelativeXStart + deltaX;
                        this.RelativeXEnd = this.RelativeXEnd + deltaX;
                    }

                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double OriginY
        {
            get { return ModelSettings.Default.OriginY; }
            set
            {
                bool changed;
                double roundedValue = this.GetOneDigitRoundedValue(value);
                double deltaY = ModelSettings.Default.OriginY - roundedValue;

                if (roundedValue < Settings.Default.YMin)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.OriginY = Settings.Default.YMin, () => ModelSettings.Default.OriginY.Equals(Settings.Default.YMin));
                }
                else if (roundedValue > Settings.Default.YMax)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.OriginY = Settings.Default.YMax, () => ModelSettings.Default.OriginY.Equals(Settings.Default.YMax));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.OriginY = roundedValue, () => ModelSettings.Default.OriginY.Equals(roundedValue));
                }

                if (changed)
                {
                    if (deltaY >= 0)
                    {
                        this.RelativeYEnd = this.RelativeYEnd + deltaY;
                        this.RelativeYStart = this.RelativeYStart + deltaY;
                    }
                    else
                    {
                        this.RelativeYStart = this.RelativeYStart + deltaY;
                        this.RelativeYEnd = this.RelativeYEnd + deltaY;
                    }

                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double AbsoluteXStart
        {
            get { return this.OriginX + this.RelativeXStart; }
        }

        public double RelativeXStart
        {
            get { return ModelSettings.Default.XStart; }
            set
            {
                bool changed;
                double roundedValue = this.GetOneDigitRoundedValue(value);

                if (this.OriginX + roundedValue < Settings.Default.XMin)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.XStart = Settings.Default.XMin - this.OriginX, () => ModelSettings.Default.XStart.Equals(Settings.Default.XMin - this.OriginX));
                }
                else if (this.OriginX + roundedValue >= this.AbsoluteXEnd)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.XStart = this.AbsoluteXEnd - 1 - this.OriginX, () => ModelSettings.Default.XStart.Equals(this.AbsoluteXEnd - 1 - this.OriginX));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.XStart = roundedValue, () => ModelSettings.Default.XStart.Equals(roundedValue));
                }

                if (changed)
                {
                    this.RaisePropertyChanged(() => this.XDifference);
                    this.RaisePropertyChanged(() => this.AbsoluteXStart);
                    this.CalculateXSteps();
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double DeltaX
        {
            get { return ModelSettings.Default.DeltaX; }
            set
            {
                bool changed;

                if (value < 1)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.DeltaX = 1, () => ModelSettings.Default.DeltaX.Equals(1));
                }
                else if (value > this.XDifference)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.DeltaX = this.XDifference, () => ModelSettings.Default.DeltaX.Equals(this.XDifference));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.DeltaX = value, () => ModelSettings.Default.DeltaX.Equals(value));
                }

                if (changed)
                {
                    this.CalculateXSteps();
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double XSteps
        {
            get { return this._xSteps; }
            set
            {
                if (value < 2)
                {
                    value = 2;
                }

                this.SetProperty(ref this._xSteps, value);
                this.CalculateDeltaX();
            }
        }

        public double XDifference
        {
            get { return Math.Abs(this.RelativeXEnd - this.RelativeXStart); }
        }

        public double AbsoluteXEnd
        {
            get { return this.OriginX + this.RelativeXEnd; }
        }

        public double RelativeXEnd
        {
            get { return ModelSettings.Default.XEnd; }
            set
            {
                bool changed;
                double roundedValue = this.GetOneDigitRoundedValue(value);

                if (this.OriginX + roundedValue <= this.AbsoluteXStart)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.XEnd = this.AbsoluteXStart + 1 - this.OriginX, () => ModelSettings.Default.XEnd.Equals(this.AbsoluteXStart + 1 - this.OriginX));
                }
                else if (this.OriginX + roundedValue > Settings.Default.XMax)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.XEnd = Settings.Default.XMax - this.OriginX, () => ModelSettings.Default.XEnd.Equals(Settings.Default.XMax - this.OriginX));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.XEnd = roundedValue, () => ModelSettings.Default.XEnd.Equals(roundedValue));
                }

                if (changed)
                {
                    this.RaisePropertyChanged(() => this.XDifference);
                    this.RaisePropertyChanged(() => this.AbsoluteXEnd);
                    this.CalculateXSteps();
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double AbsoluteYStart
        {
            get { return this.OriginY + this.RelativeYStart; }
        }

        public double RelativeYStart
        {
            get { return ModelSettings.Default.YStart; }
            set
            {
                bool changed;
                double roundedValue = this.GetOneDigitRoundedValue(value);

                if (this.OriginY + roundedValue < Settings.Default.YMin)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.YStart = Settings.Default.YMin - this.OriginY, () => ModelSettings.Default.YStart.Equals(Settings.Default.YMin - this.OriginY));
                }
                else if (this.OriginY + roundedValue >= this.AbsoluteYEnd)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.YStart = this.AbsoluteYEnd - 1 - this.OriginY, () => ModelSettings.Default.YStart.Equals(this.AbsoluteYEnd - 1 - this.OriginY));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.YStart = roundedValue, () => ModelSettings.Default.YStart.Equals(roundedValue));
                }

                if (changed)
                {
                    this.RaisePropertyChanged(() => this.YDifference);
                    this.RaisePropertyChanged(() => this.AbsoluteYStart);
                    this.CalculateYSteps();
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double DeltaY
        {
            get { return ModelSettings.Default.DeltaY; }
            set
            {
                bool changed;

                if (value < 1)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.DeltaY = 1, () => ModelSettings.Default.DeltaY.Equals(1));
                }
                else if (value > this.YDifference)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.DeltaY = this.YDifference, () => ModelSettings.Default.DeltaY.Equals(this.YDifference));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.DeltaY = value, () => ModelSettings.Default.DeltaY.Equals(value));
                }

                if (changed)
                {
                    this.CalculateYSteps();
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double YSteps
        {
            get { return this._ySteps; }
            set
            {
                if (value < 2)
                {
                    value = 2;
                }

                this.SetProperty(ref this._ySteps, value);
                this.CalculateDeltaY();
            }
        }

        public double YDifference
        {
            get { return Math.Abs(this.RelativeYEnd - this.RelativeYStart); }
        }

        public double AbsoluteYEnd
        {
            get { return this.OriginY + this.RelativeYEnd; }
        }

        public double RelativeYEnd
        {
            get { return ModelSettings.Default.YEnd; }
            set
            {
                bool changed;
                double roundedValue = this.GetOneDigitRoundedValue(value);

                if (this.OriginY + roundedValue <= this.AbsoluteYStart)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.YEnd = this.AbsoluteYStart + 1 - this.OriginY, () => ModelSettings.Default.YEnd.Equals(this.AbsoluteYStart + 1 - this.OriginY));
                }
                else if (this.OriginY + roundedValue > Settings.Default.YMax)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.YEnd = Settings.Default.YMax - this.OriginY, () => ModelSettings.Default.YEnd.Equals(Settings.Default.YMax - this.OriginY));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.YEnd = roundedValue, () => ModelSettings.Default.YEnd.Equals(roundedValue));
                }

                if (changed)
                {
                    this.RaisePropertyChanged(() => this.YDifference);
                    this.RaisePropertyChanged(() => this.AbsoluteYEnd);
                    this.CalculateYSteps();
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double FreqStart
        {
            get { return ModelSettings.Default.FreqStart; }

            set
            {
                bool changed;

                if (value < 1)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.FreqStart = 1, () => ModelSettings.Default.FreqStart.Equals(1.0));
                }
                else if (value >= this.FreqEnd)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.FreqStart = this.FreqEnd / 1e1, () => ModelSettings.Default.FreqStart.Equals(this.FreqEnd / 1e1));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.FreqStart = value, () => ModelSettings.Default.FreqStart.Equals(value));
                }

                if (changed)
                {
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public double DeltaFreq
        {
            get { return (double)this.ResBW / 4; }
        }

        public double FreqEnd
        {
            get { return ModelSettings.Default.FreqEnd; }

            set
            {
                bool changed;

                if (value <= this.FreqStart)
                {
                    changed = this.SetProperty(() => ModelSettings.Default.FreqEnd = this.FreqStart * 1e1, () => ModelSettings.Default.FreqEnd.Equals(this.FreqStart * 1e1));
                }
                else
                {
                    changed = this.SetProperty(() => ModelSettings.Default.FreqEnd = value, () => ModelSettings.Default.FreqEnd.Equals(value));
                }

                if (changed)
                {
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public ResBW ResBW
        {
            get { return (ResBW)ModelSettings.Default.ResBWIndex; }
            set
            {
                if (this.SetProperty(() => ModelSettings.Default.ResBWIndex = (int)value, () => ModelSettings.Default.ResBWIndex == (int)value))
                {
                    this.RaisePropertyChanged(() => this.DeltaFreq);
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public int MeasureTime
        {
            get { return ModelSettings.Default.MeasureTime; }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }

                if (this.SetProperty(() => ModelSettings.Default.MeasureTime = value, () => ModelSettings.Default.MeasureTime == value))
                {
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public Orientation Orientation
        {
            get { return (Orientation)ModelSettings.Default.OrientationIndex; }
            set
            {
                if (this.SetProperty(() => ModelSettings.Default.OrientationIndex = (int)value, () => ModelSettings.Default.OrientationIndex == (int)value))
                {
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public SensorViewModel SelectedSensorViewModel
        {
            get { return this._selectedSensorViewModel; }
            set
            {
                if (this.SetProperty(ref this._selectedSensorViewModel, value))
                {
                    if (this._selectedSensorViewModel != null)
                    {
                        this.SensorType = this._selectedSensorViewModel.SensorType;
                        this.SensorImageStream = this._selectedSensorViewModel.SensorImageStream;
                        this.SensorName = this._selectedSensorViewModel.SensorName;
                    }
                    else
                    {
                        this.SensorType = null;
                        this.SensorName = null;
                        this.SensorImageStream = null;
                    }

                    // Update to see / delete error messages when under / over sensor frequency ranges
                    this.RaisePropertyChanged(() => this.FreqStart);
                    this.RaisePropertyChanged(() => this.FreqEnd);
                }
            }
        }

        public SensorType? SensorType
        {
            get
            {
                if (ModelSettings.Default.SensorTypeIndex == -1)
                {
                    return null;
                }

                return (SensorType)ModelSettings.Default.SensorTypeIndex;
            }
            private set
            {
                if (value == null)
                {
                    if (this.SetProperty(() => ModelSettings.Default.SensorTypeIndex = -1, () => ModelSettings.Default.SensorTypeIndex == -1))
                    {
                        this.SetValuesChangedAndUpdateCommands();
                    }
                }
                else
                {
                    if (this.SetProperty(() => ModelSettings.Default.SensorTypeIndex = (int)value, () => ModelSettings.Default.SensorTypeIndex == (int)value))
                    {
                        this.SetValuesChangedAndUpdateCommands();
                    }
                }
            }
        }

        public byte[] SensorImageStream
        {
            get
            {
                if (string.IsNullOrEmpty(ModelSettings.Default.SensorImageStream))
                {
                    return null;
                }

                return Convert.FromBase64String(ModelSettings.Default.SensorImageStream);
            }
            set
            {
                if (value == null)
                {
                    if (this.SetProperty(() => ModelSettings.Default.SensorImageStream = null, () => ModelSettings.Default.SensorImageStream == null))
                    {
                        this.SetValuesChangedAndUpdateCommands();
                    }
                }
                else
                {
                    if (this.SetProperty(() => ModelSettings.Default.SensorImageStream = Convert.ToBase64String(value), () => ModelSettings.Default.SensorImageStream == Convert.ToBase64String(value)))
                    {
                        this.SetValuesChangedAndUpdateCommands();
                    }
                }
            }
        }

        public string SensorName
        {
            get { return ModelSettings.Default.SensorName; }
            set
            {
                if (this.SetProperty(() => ModelSettings.Default.SensorName = value, () => ModelSettings.Default.SensorName == value))
                {
                    this.SetValuesChangedAndUpdateCommands();
                }
            }
        }

        public ObservableCollection<SensorViewModel> Sensors { get; set; }

        /// <summary>
        ///     Gets command to reload all sensors from the database.
        /// </summary>
        public RelayCommand ReloadSensorsCommand
        {
            get
            {
                if (this._reloadSensorsCommand == null)
                {
                    this._reloadSensorsCommand = new RelayCommand(this.ReloadSensors);
                }

                return this._reloadSensorsCommand;
            }
        }

        public RelayCommand ResetChangesCommand
        {
            get
            {
                if (this._resetChangesCommand == null)
                {
                    this._resetChangesCommand = new RelayCommand(this.ResetChanges, this.CanResetChanges);
                }

                return this._resetChangesCommand;
            }
        }

        public RelayCommand SaveChangesCommand
        {
            get
            {
                if (this._saveChangesCommand == null)
                {
                    this._saveChangesCommand = new RelayCommand(this.SaveChanges, this.CanSaveChanges);
                }

                return this._saveChangesCommand;
            }
        }

        private bool ValuesChanged { get; set; }

        private IEnumerable<PropertyInfo> PropertyInfos
        {
            get
            {
                if (this._propertyInfos == null)
                {
                    this._propertyInfos = this.GetType().GetProperties(BindingFlags.Instance |
                                                                       BindingFlags.Public |
                                                                       BindingFlags.DeclaredOnly);
                }

                return this._propertyInfos;
            }
        }

        #endregion



        #region IDataErrorInfo Members

        public string this[string propertyName]
        {
            get {
                if (propertyName == PropertyNameExtractor.GetPropertyName<DefaultParameterViewModel>(x => x.FreqStart))
                {
                    if (this.SelectedSensorViewModel != null && this.SelectedSensorViewModel.MinFrequency != null & this.FreqStart < this.SelectedSensorViewModel.MinFrequency)
                    {
                        return "Min frequency is outside specified sensor frequency range!";
                    }
                }
                else if (propertyName == PropertyNameExtractor.GetPropertyName<DefaultParameterViewModel>(x => x.FreqEnd))
                {
                    if (this.SelectedSensorViewModel != null && this.SelectedSensorViewModel.MaxFrequency != null & this.FreqEnd > this.SelectedSensorViewModel.MaxFrequency)
                    {
                        return "Max frequency is outside specified sensor frequency range!";
                    }
                }

                return string.Empty;
            }
        }

        public string Error
        {
            get { return null; }
        }

        #endregion



        private bool CanResetChanges()
        {
            return this.ValuesChanged;
        }

        private void ResetChanges()
        {
            // restore values before changes
            ModelSettings.Default.Reload();

            // update properties to see changes in view
            this.UpdateProperties();

            // necessary for updating steps in view
            this.CalculateXSteps();
            this.CalculateYSteps();

            // select default sensor from sensor list
            this.SelectedSensorViewModel = this.Sensors.FirstOrDefault(x => x.SensorName == this.SensorName && x.SensorType == this.SensorType);

            // after all values have been restored
            this.ResetValuesChangedAndUpdateCommands();
        }

        private bool CanSaveChanges()
        {
            return this.ValuesChanged;
        }

        private void SaveChanges()
        {
            ModelSettings.Default.Save();

            // after save changed state can be reset
            this.ResetValuesChangedAndUpdateCommands();

            this.UpdateSetupParameter();
        }

        private void UpdateSetupParameter()
        {
            Messenger.Default.Send(new NotificationMessage(Resources.Messenger_UpdateParameterValuesForSetupVM));
        }

        private void SetValuesChangedAndUpdateCommands()
        {
            this.ValuesChanged = true;
            this.UpdateCommandsEnableState();
        }

        private void ResetValuesChangedAndUpdateCommands()
        {
            this.ValuesChanged = false;
            this.UpdateCommandsEnableState();
        }

        /// <summary>
        ///     Loads all sensors from database and sets selected sensor to default sensor.
        /// </summary>
        private void InitSensors()
        {
            try
            {
                List<Sensor> sensors = this.repository.GetAll<Sensor>().ToList();

                // add all sensors to list
                foreach (Sensor sensor in sensors)
                {
                    this.Sensors.InsertOrderedBy(new SensorViewModel(sensor), x => x.SensorName);
                }

                // if default sensor in list, select it, otherwise select first in list
                if (!string.IsNullOrEmpty(this.SensorName) && this.Sensors.Count(x => x.SensorName == this.SensorName && x.SensorType == this.SensorType) != 0)
                {
                    this.SelectedSensorViewModel = this.Sensors.First(x => x.SensorName == this.SensorName && x.SensorType == this.SensorType);
                }
                else
                {
                    // select first sensor in list if exist
                    this.SelectedSensorViewModel = this.Sensors.FirstOrDefault();

                    if (this.SelectedSensorViewModel == null)
                    {
                        // database was empty, so default properties should be deleted
                        this.ClearSensorData();
                    }
                }
            }
            catch
            {
                // no connection to database
            }
        }

        /// <summary>
        ///     Clears all sensor values
        /// </summary>
        private void ClearSensorData()
        {
            this.SensorName = null;
            this.SensorType = null;
            this.SensorImageStream = null;
        }

        /// <summary>
        ///     Reloads all sensors from the database.
        /// </summary>
        private void ReloadSensors()
        {
            try
            {
                List<Sensor> sensors = this.repository.GetAll<Sensor>().ToList();

                foreach (Sensor sensor in sensors)
                {
                    // add sensors from database when not already in list
                    if (this.Sensors.Count(this.SensorViewModelSearchCriteria(sensor.SensorName, sensor.SensorType, sensor.SensorImageStream)) == 0)
                    {
                        this.Sensors.InsertOrderedBy(new SensorViewModel(sensor), x => x.SensorName);
                    }
                }

                for (int i = this.Sensors.Count - 1; i >= 0; i--)
                {
                    // delete sensors that do not exist in the database
                    if (sensors.Count(this.SensorSearchCriteria(this.Sensors[i].SensorName, this.Sensors[i].SensorType, this.Sensors[i].SensorImageStream)) == 0)
                    {
                        this.Sensors.Remove(this.Sensors[i]);
                    }

                    // now null when sensor that was not in database was selected
                    if (this.SelectedSensorViewModel == null)
                    {
                        // set to first one in sensor list
                        this.SelectedSensorViewModel = this.Sensors.FirstOrDefault();
                    }
                }
            }
            catch
            {
                // no connection to database
                this.Sensors.Clear();
            }
        }

        /// <summary>
        ///     Creates equal condition between two sensor view models.
        /// </summary>
        /// <param name="sensorName">sensor name</param>
        /// <param name="sensorType">sensor type</param>
        /// <param name="sensorImageStream">sensor image stream</param>
        /// <returns>condition that must be met</returns>
        private Func<SensorViewModel, bool> SensorViewModelSearchCriteria(string sensorName, SensorType? sensorType, byte[] sensorImageStream)
        {
            return x => x.SensorName == sensorName &&
                        x.SensorType == sensorType &&
                        (x.SensorImageStream == null && sensorImageStream == null ||
                         x.SensorImageStream != null && sensorImageStream != null && x.SensorImageStream.SequenceEqual(sensorImageStream));
        }

        /// <summary>
        ///     Creates equal condition between two sensors.
        /// </summary>
        /// <param name="sensorName">sensor name</param>
        /// <param name="sensorType">sensor type</param>
        /// <param name="sensorImageStream">sensor image stream</param>
        /// <returns>condition that must be met</returns>
        private Func<Sensor, bool> SensorSearchCriteria(string sensorName, SensorType? sensorType, byte[] sensorImageStream)
        {
            return x => x.SensorName == sensorName &&
                        x.SensorType == sensorType &&
                        (x.SensorImageStream == null && sensorImageStream == null ||
                         x.SensorImageStream != null && sensorImageStream != null && x.SensorImageStream.SequenceEqual(sensorImageStream));
        }

        private void CalculateXSteps()
        {
            this.XSteps = Math.Round(this.XDifference / this.DeltaX) + 1;
        }

        private void CalculateYSteps()
        {
            this.YSteps = Math.Round(this.YDifference / this.DeltaY) + 1;
        }

        private void CalculateDeltaX()
        {
            this.DeltaX = this.XDifference / (this.XSteps - 1);
        }

        private void CalculateDeltaY()
        {
            this.DeltaY = this.YDifference / (this.YSteps - 1);
        }

        private double GetOneDigitRoundedValue(double value)
        {
            return Math.Round(value, 1);
        }

        /// <summary>
        ///     Update command enable state for view.
        /// </summary>
        private void UpdateCommandsEnableState()
        {
            this.SaveChangesCommand.RaiseCanExecuteChanged();
            this.ResetChangesCommand.RaiseCanExecuteChanged();
        }

        private void UpdateProperties()
        {
            foreach (PropertyInfo propertyInfo in this.PropertyInfos)
            {
                this.RaisePropertyChanged(propertyInfo.Name);
            }
        }
    }
}