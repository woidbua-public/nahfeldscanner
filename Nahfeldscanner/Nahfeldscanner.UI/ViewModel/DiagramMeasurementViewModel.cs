﻿// ///////////////////////////////////
// File: DiagramMeasurementViewModel.cs
// Last Change: 30.01.2018  16:29
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Nahfeldscanner.Model.Database;
    using OxyPlot;



    public class DiagramMeasurementViewModel
    {
        #region Fields

        private readonly Measurement _measurement;

        #endregion



        #region Constructors / Destructor

        public DiagramMeasurementViewModel(Measurement measurement, double trackerFrequency)
        {
            this._measurement = measurement;
            this.InitData(trackerFrequency);
        }

        #endregion



        #region Properties

        public List<DataPoint> MaxDetectorDataPoints { get; private set; }

        public List<DataPoint> AVGDetectorDataPoints { get; private set; }

        public List<DataPoint> QPDetectorDataPoints { get; private set; }

        public ObservableCollection<DataPoint> TrackerDataPoints { get; private set; }

        public double MinIntensity { get; private set; }

        public double MaxIntensity { get; private set; }

        private bool HasQuasipeakMeasurements { get; set; }

        #endregion



        public void SetTrackerDataPointPosition(double trackerFrequency)
        {
            this.TrackerDataPoints[0] = new DataPoint(trackerFrequency, this.MinIntensity);
            this.TrackerDataPoints[1] = new DataPoint(trackerFrequency, this.MaxIntensity);
        }

        private void InitData(double trackerFrequency)
        {
            this.MaxDetectorDataPoints = new List<DataPoint>();
            this.AVGDetectorDataPoints = new List<DataPoint>();
            this.QPDetectorDataPoints = new List<DataPoint>();
            this.TrackerDataPoints = new ObservableCollection<DataPoint>();

            this.HasQuasipeakMeasurements = !this._measurement.MeasurementValues.All(x => x.IntensityQPDetector.Equals(0));

            foreach (MeasurementValue measurementValue in this._measurement.MeasurementValues)
            {
                this.MaxDetectorDataPoints.Add(new DataPoint(measurementValue.Frequency, measurementValue.IntensityMaxDetector));
                this.AVGDetectorDataPoints.Add(new DataPoint(measurementValue.Frequency, measurementValue.IntensityAVGDetector));

                if (this.HasQuasipeakMeasurements)
                {
                    this.QPDetectorDataPoints.Add(new DataPoint(measurementValue.Frequency, measurementValue.IntensityQPDetector));
                }
            }

            this.InitMinMaxIntensities();
            this.InitTrackerDataPoints(trackerFrequency);
        }

        private void InitMinMaxIntensities()
        {
            double[] minIntensities;
            double[] maxIntensities;

            if (this.HasQuasipeakMeasurements)
            {
                minIntensities = new[]
                                 {
                                         this.MaxDetectorDataPoints.Min(point => point.Y),
                                         this.AVGDetectorDataPoints.Min(point => point.Y),
                                         this.QPDetectorDataPoints.Min(point => point.Y)
                                 };

                maxIntensities = new[]
                                 {
                                         this.MaxDetectorDataPoints.Max(point => point.Y),
                                         this.AVGDetectorDataPoints.Max(point => point.Y),
                                         this.QPDetectorDataPoints.Max(point => point.Y)
                                 };
            }
            else
            {
                minIntensities = new[]
                                 {
                                         this.MaxDetectorDataPoints.Min(point => point.Y),
                                         this.AVGDetectorDataPoints.Min(point => point.Y)
                                 };

                maxIntensities = new[]
                                 {
                                         this.MaxDetectorDataPoints.Max(point => point.Y),
                                         this.AVGDetectorDataPoints.Max(point => point.Y)
                                 };
            }

            this.MinIntensity = minIntensities.Min();
            this.MaxIntensity = maxIntensities.Max();
        }

        private void InitTrackerDataPoints(double trackerFrequency)
        {
            this.TrackerDataPoints.Add(new DataPoint(trackerFrequency, this.MinIntensity));
            this.TrackerDataPoints.Add(new DataPoint(trackerFrequency, this.MaxIntensity));
        }
    }
}