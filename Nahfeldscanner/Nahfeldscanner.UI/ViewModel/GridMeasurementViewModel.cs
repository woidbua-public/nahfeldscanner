﻿// ///////////////////////////////////
// File: GridMeasurementViewModel.cs
// Last Change: 17.01.2018  15:11
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using Nahfeldscanner.Model.Database;



    public class GridMeasurementViewModel : BindableViewModelBase
    {
        #region Fields

        private readonly Measurement _measurement;
        private MeasurementValueViewModel _measurementValueViewModel;

        #endregion



        #region Constructors / Destructor

        public GridMeasurementViewModel(Measurement measurement, int xIndex, int yIndex)
        {
            this._measurement = measurement;
            this.XIndex = xIndex;
            this.YIndex = yIndex;
        }

        #endregion



        #region Properties

        public double X
        {
            get { return this._measurement.X; }
        }

        public double XIndex { get; private set; }

        public double Y
        {
            get { return this._measurement.Y; }
        }

        public double YIndex { get; private set; }

        public MeasurementValueViewModel MeasurementValueViewModel
        {
            get { return this._measurementValueViewModel; }
            private set { this.SetProperty(ref this._measurementValueViewModel, value); }
        }

        #endregion



        public void SetMeasurementValue(MeasurementValue measurementValue)
        {
            this.MeasurementValueViewModel = new MeasurementValueViewModel(measurementValue);
        }
    }
}