﻿// ///////////////////////////////////
// File: ImageCommandViewModel.cs
// Last Change: 15.01.2018  14:02
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System.Drawing;
    using GalaSoft.MvvmLight.Command;



    public class ImageCommandViewModel : CommandViewModel
    {
        #region Constructors / Destructor

        public ImageCommandViewModel(Bitmap image, string displayName, RelayCommand relayCommand)
                : base(displayName, relayCommand)
        {
            this.Image = image;
        }

        #endregion



        #region Properties

        public Bitmap Image { get; set; }

        #endregion
    }
}