// ///////////////////////////////////
// File: MainViewModel.cs
// Last Change: 22.01.2018  11:25
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;



    public class MainViewModel : BindableViewModelBase, IDisposable
    {
        #region Fields

        private readonly IRepository repository;
        private readonly IDialogService dialogService;
        private readonly SerialPortViewModel serialPortViewModel;

        private bool _canChangeWorkspace = true;
        private WorkspaceViewModel _currentWorkspaceViewModel;

        private RelayCommand _createDatabaseCommand;
        private RelayCommand _loadDatabaseCommand;

        #endregion



        #region Constructors / Destructor

        public MainViewModel(IRepository repository, IDialogService dialogService)
        {
            this.repository = repository;
            this.dialogService = dialogService;
            this.serialPortViewModel = new SerialPortViewModel(this.dialogService);

            this.TryConnectAtStartup();
            this.InitWorkspaces();
            Messenger.Default.Send(new NotificationMessage(Resources.Messenger_LoadSensorsForSensorConfigurationVM));

            Messenger.Default.Register<NotificationMessage>(this, this.ExecuteNotificationMessage);
            Messenger.Default.Register<NotificationMessage<bool>>(this, this.ExecuteNotificationMessage);
        }

        #endregion



        #region Properties

        public bool CanChangeWorkspace
        {
            get { return this._canChangeWorkspace; }
            private set { this.SetProperty(ref this._canChangeWorkspace, value); }
        }

        public bool IsConnected
        {
            get { return this.repository.IsConnected; }
        }

        public string Title
        {
            get
            {
                if (string.IsNullOrEmpty(this.repository.DatabasePath))
                {
                    return Resources.Application_Title;
                }

                return Resources.Application_Title + " - " + this.repository.DatabasePath;
            }
        }

        public WorkspaceViewModel CurrentWorkspaceViewModel
        {
            get { return this._currentWorkspaceViewModel; }
            set
            {
                if (this.SetProperty(ref this._currentWorkspaceViewModel, value))
                {
                    if (this.CurrentWorkspaceViewModel.GetType() == typeof(SetupViewModel))
                    {
                        ((SetupViewModel)this.CurrentWorkspaceViewModel).SetSerialOriginToParameterOrigin();
                    }
                    else if (this.CurrentWorkspaceViewModel.GetType() == typeof(ScanViewModel))
                    {
                        ((ScanViewModel)this.CurrentWorkspaceViewModel).SetSerialOriginToParameterOrigin();
                    }
                }
            }
        }

        public ObservableCollection<WorkspaceViewModel> WorkspaceViewModels { get; private set; }

        public RelayCommand CreateDatabaseCommand
        {
            get
            {
                if (this._createDatabaseCommand == null)
                {
                    this._createDatabaseCommand = new RelayCommand(this.CreateDatabase);
                }

                return this._createDatabaseCommand;
            }
        }

        public RelayCommand LoadDatabaseCommand
        {
            get
            {
                if (this._loadDatabaseCommand == null)
                {
                    this._loadDatabaseCommand = new RelayCommand(this.LoadDatabase);
                }

                return this._loadDatabaseCommand;
            }
        }

        private ScanViewModel ScanViewModel { get; set; }

        private SearchViewModel SearchViewModel { get; set; }

        private SetupViewModel SetupViewModel { get; set; }

        private OptionViewModel OptionViewModel { get; set; }

        #endregion



        #region IDisposable Members

        public void Dispose()
        {
            if (this.serialPortViewModel != null)
            {
                this.serialPortViewModel.Dispose();
            }

            if (this.ScanViewModel != null)
            {
                this.ScanViewModel.Dispose();
            }

            Messenger.Default.Unregister(this);
        }

        #endregion



        private void UpdateProperties()
        {
            this.RaisePropertyChanged(() => this.Title);
            this.RaisePropertyChanged(() => this.IsConnected);
        }

        private void InitWorkspaces()
        {
            this.SetupViewModel = new SetupViewModel(this.repository, this.serialPortViewModel,
                                                     Resources.Workspace_Title_Setup, Resources.img_setup);

            this.ScanViewModel = new ScanViewModel(this.repository, this.dialogService, this.serialPortViewModel,
                                                   Resources.Workspace_Title_Scan, Resources.img_scan);

            this.SearchViewModel = new SearchViewModel(this.repository, this.dialogService,
                                                       Resources.Workspace_Title_Search, Resources.img_search);

            this.OptionViewModel = new OptionViewModel(this.repository, this.dialogService,
                                                       Resources.Workspace_Title_Options, Resources.img_options);

            this.WorkspaceViewModels = new ObservableCollection<WorkspaceViewModel>
                                       {
                                               this.SetupViewModel,
                                               this.ScanViewModel,
                                               this.SearchViewModel,
                                               this.OptionViewModel
                                       };

            this.CurrentWorkspaceViewModel = this.SetupViewModel;
        }

        private void CreateDatabase()
        {
            Settings.Default.DatabaseFilePath = this.dialogService.ShowSaveDatabaseFileDialog();

            if (string.IsNullOrEmpty(Settings.Default.DatabaseFilePath))
            {
                return;
            }

            try
            {
                this.repository.RecreateDatabase(Settings.Default.DatabaseFilePath);
                Settings.Default.Save();
                this.UpdateProperties();
                Messenger.Default.Send(new NotificationMessage(Resources.Messenger_LoadSensorsForSensorConfigurationVM));
                Messenger.Default.Send(new NotificationMessage(Resources.Messenger_ClearFoundAnalysesForSearchVM));
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    this.dialogService.ShowMessage(Resources.Exception_Message_CouldNotCreateDatabase, e.Message);
                }
                else
                {
                    this.dialogService.ShowMessage(Resources.Exception_Message_CouldNotCreateDatabase, e.Message, e.InnerException.ToString());
                }
            }
        }

        private void LoadDatabase()
        {
            Settings.Default.DatabaseFilePath = this.dialogService.ShowDatabaseFileDialog();

            if (string.IsNullOrEmpty(Settings.Default.DatabaseFilePath))
            {
                return;
            }

            try
            {
                this.repository.LoadDatabase(Settings.Default.DatabaseFilePath);
                Settings.Default.Save();
                this.UpdateProperties();
                Messenger.Default.Send(new NotificationMessage(Resources.Messenger_LoadSensorsForSensorConfigurationVM));
                Messenger.Default.Send(new NotificationMessage(Resources.Messenger_ClearFoundAnalysesForSearchVM));
                Messenger.Default.Send(new NotificationMessage(Resources.Messenger_ClearLoadedAnalysisForScanVM));
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    this.dialogService.ShowMessage(Resources.Exception_Message_CouldNotLoadDatabase, e.Message);
                }
                else
                {
                    this.dialogService.ShowMessage(Resources.Exception_Message_CouldNotLoadDatabase, e.Message, e.InnerException.ToString());
                }
            }
        }

        private void TryConnectAtStartup()
        {
            try
            {
                if (File.Exists(Settings.Default.DatabaseFilePath))
                {
                    this.repository.LoadDatabase(Settings.Default.DatabaseFilePath);
                    this.UpdateProperties();
                }
            }
            catch
            {
                // do nothing
            }
        }

        private void ExecuteNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Resources.Messenger_LoadScanVMForMainVM)
            {
                this.CurrentWorkspaceViewModel = this.ScanViewModel;
            }
        }

        private void ExecuteNotificationMessage(NotificationMessage<bool> message)
        {
            if (message.Notification == Resources.Messenger_ChangeWorkspaceEnableStateForMainVM)
            {
                this.CanChangeWorkspace = message.Content;
            }
        }
    }
}