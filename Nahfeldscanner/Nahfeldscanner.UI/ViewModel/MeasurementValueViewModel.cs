﻿// ///////////////////////////////////
// File: MeasurementValueViewModel.cs
// Last Change: 17.01.2018  15:08
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using Nahfeldscanner.Model.Database;



    public class MeasurementValueViewModel : BindableViewModelBase
    {
        #region Fields

        private readonly MeasurementValue _measurementValue;

        #endregion



        #region Constructors / Destructor

        public MeasurementValueViewModel(MeasurementValue measurementValue)
        {
            this._measurementValue = measurementValue;
        }

        #endregion



        #region Properties

        public double Frequency
        {
            get { return this._measurementValue.Frequency; }
        }

        public double IntensityMaxDetector
        {
            get { return this._measurementValue.IntensityMaxDetector; }
        }

        public double IntensityAVGDetector
        {
            get { return this._measurementValue.IntensityAVGDetector; }
        }

        public double IntensityQPDetector
        {
            get { return this._measurementValue.IntensityQPDetector; }
        }

        #endregion
    }
}