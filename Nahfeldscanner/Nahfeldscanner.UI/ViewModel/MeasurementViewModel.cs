﻿// ///////////////////////////////////
// File: MeasurementViewModel.cs
// Last Change: 23.01.2018  15:23
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;



    public class MeasurementViewModel : BindableViewModelBase
    {
        #region Fields

        private readonly Measurement _measurement;

        private double _currentFrequency;
        private double _currentIntensity;
        private double _minFrequency;
        private double _minIntensity;
        private double _maxFrequency;
        private double _maxIntensity;

        #endregion



        #region Constructors / Destructor

        /// <summary>
        ///     Create new instance with min, current and max measurement values.
        /// </summary>
        /// <param name="measurement">reference to parent measurement</param>
        /// <param name="currentMeasurementValue">meausurement value data for current frequency</param>
        /// <param name="minMeasurementValue">meausurement value data where min intensity occured</param>
        /// <param name="maxMeasurementValue">meausurement value data where max intensity occured</param>
        /// <param name="detectorType">the detector type that is currently selected</param>
        /// <param name="xDimension">horizontal position in overall measurement size</param>
        /// <param name="yDimension">vertical position in overall measurement size</param>
        public MeasurementViewModel(Measurement measurement, MeasurementValue currentMeasurementValue,
                                    MeasurementValue minMeasurementValue, MeasurementValue maxMeasurementValue,
                                    DetectorType detectorType, int xDimension, int yDimension)
        {
            this._measurement = measurement;
            this.SetMinMeasurementValue(minMeasurementValue, detectorType);
            this.SetMaxMeasurementValue(maxMeasurementValue, detectorType);
            this.SetCurrentMeasurementValue(currentMeasurementValue, detectorType);
            this.XDimension = xDimension;
            this.YDimension = yDimension;
        }

        #endregion



        #region Properties

        /// <summary>
        ///     Gets the id of the measurement.
        /// </summary>
        public int Id
        {
            get { return this._measurement.ID; }
        }

        /// <summary>
        ///     X position of the recorded measurement.
        /// </summary>
        public double X
        {
            get { return this._measurement.X; }
        }

        /// <summary>
        ///     Horizontal position in overall measurement size.
        /// </summary>
        public int XDimension { get; private set; }

        /// <summary>
        ///     Vertical position in overall measurement size.
        /// </summary>
        public int YDimension { get; private set; }

        /// <summary>
        ///     Y position of the recorded measurement.
        /// </summary>
        public double Y
        {
            get { return this._measurement.Y; }
        }

        /// <summary>
        ///     Gets the currently selected frequency.
        /// </summary>
        public double CurrentFrequency
        {
            get { return this._currentFrequency; }
            private set { this.SetProperty(ref this._currentFrequency, value); }
        }

        /// <summary>
        ///     Gets the intensity by the currently selected frequency and detector type.
        /// </summary>
        public double CurrentIntensity
        {
            get { return this._currentIntensity; }
            private set { this.SetProperty(ref this._currentIntensity, value); }
        }

        /// <summary>
        ///     Gets the frequency where the min intensity of the currently selected detector type occured.
        /// </summary>
        public double MinFrequency
        {
            get { return this._minFrequency; }
            private set { this.SetProperty(ref this._minFrequency, value); }
        }

        /// <summary>
        ///     Gets the min intensity that occured in this measurement by the currently selected detector type.
        /// </summary>
        public double MinIntensity
        {
            get { return this._minIntensity; }
            private set { this.SetProperty(ref this._minIntensity, value); }
        }

        /// <summary>
        ///     Gets the frequency where the max intensity of the currently selected detector type occured.
        /// </summary>
        public double MaxFrequency
        {
            get { return this._maxFrequency; }
            private set { this.SetProperty(ref this._maxFrequency, value); }
        }

        /// <summary>
        ///     Gets the max intensity that occured in this measurement by the currently selected detector type.
        /// </summary>
        public double MaxIntensity
        {
            get { return this._maxIntensity; }
            private set { this.SetProperty(ref this._maxIntensity, value); }
        }

        #endregion



        /// <summary>
        ///     Sets current frequency and intensity value to new measurement value properties.
        /// </summary>
        /// <param name="measurementValue">new measurement value</param>
        /// <param name="detectorType">currently selected detector type</param>
        public void SetCurrentMeasurementValue(MeasurementValue measurementValue, DetectorType detectorType)
        {
            this.CurrentFrequency = measurementValue.Frequency;

            switch (detectorType)
            {
                case DetectorType.Max_Peak:
                    this.CurrentIntensity = measurementValue.IntensityMaxDetector;
                    break;
                case DetectorType.Average:
                    this.CurrentIntensity = measurementValue.IntensityAVGDetector;
                    break;
                default:
                    this.CurrentIntensity = measurementValue.IntensityQPDetector;
                    break;
            }
        }

        /// <summary>
        ///     Sets min frequency and intensity value to new measurement value properties.
        /// </summary>
        /// <param name="measurementValue">new measurement value</param>
        /// <param name="detectorType">currently selected detector type</param>
        public void SetMinMeasurementValue(MeasurementValue measurementValue, DetectorType detectorType)
        {
            this.MinFrequency = measurementValue.Frequency;

            switch (detectorType)
            {
                case DetectorType.Max_Peak:
                    this.MinIntensity = measurementValue.IntensityMaxDetector;
                    break;
                case DetectorType.Average:
                    this.MinIntensity = measurementValue.IntensityAVGDetector;
                    break;
                default:
                    this.MinIntensity = measurementValue.IntensityQPDetector;
                    break;
            }
        }

        /// <summary>
        ///     Sets max frequency and intensity value to new measurement value properties.
        /// </summary>
        /// <param name="measurementValue">new measurement value</param>
        /// <param name="detectorType">currently selected detector type</param>
        public void SetMaxMeasurementValue(MeasurementValue measurementValue, DetectorType detectorType)
        {
            this.MaxFrequency = measurementValue.Frequency;

            switch (detectorType)
            {
                case DetectorType.Max_Peak:
                    this.MaxIntensity = measurementValue.IntensityMaxDetector;
                    break;
                case DetectorType.Average:
                    this.MaxIntensity = measurementValue.IntensityAVGDetector;
                    break;
                default:
                    this.MaxIntensity = measurementValue.IntensityQPDetector;
                    break;
            }
        }

        /// <summary>
        ///     Sets the current measurement value to the min measurement value.
        /// </summary>
        public void SelectMinMeasurementValue()
        {
            this.CurrentFrequency = this.MinFrequency;
            this.CurrentIntensity = this.MinIntensity;
        }

        /// <summary>
        ///     Sets the current measurement value to the max measurement value.
        /// </summary>
        /// ///
        public void SelectMaxMeasurementValue()
        {
            this.CurrentFrequency = this.MaxFrequency;
            this.CurrentIntensity = this.MaxIntensity;
        }
    }
}