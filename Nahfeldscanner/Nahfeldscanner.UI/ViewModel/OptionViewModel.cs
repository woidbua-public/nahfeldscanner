﻿// ///////////////////////////////////
// File: OptionViewModel.cs
// Last Change: 22.01.2018  11:23
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System.Drawing;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.UI.Service;



    public class OptionViewModel : WorkspaceViewModel
    {
        #region Fields

        private readonly DefaultParameterViewModel _defaultParameterViewModel;
        private readonly SensorConfigurationViewModel _sensorConfigurationViewModel;

        #endregion



        #region Constructors / Destructor

        public OptionViewModel(IRepository repository, IDialogService dialogService, string title, Bitmap bitmap)
            : base(title, bitmap)
        {
            this._defaultParameterViewModel = new DefaultParameterViewModel(repository);
            this._sensorConfigurationViewModel = new SensorConfigurationViewModel(repository, dialogService);
        }

        #endregion



        #region Properties

        public DefaultParameterViewModel DefaultParameterViewModel
        {
            get { return this._defaultParameterViewModel; }
        }

        public SensorConfigurationViewModel SensorConfigurationViewModel
        {
            get { return this._sensorConfigurationViewModel; }
        }

        #endregion
    }
}