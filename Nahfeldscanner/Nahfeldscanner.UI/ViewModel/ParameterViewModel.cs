﻿// ///////////////////////////////////
// File: ParameterViewModel.cs
// Last Change: 19.02.2018  12:33
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Helper;
    using Nahfeldscanner.UI.Markup;
    using Nahfeldscanner.UI.Properties;



    public class ParameterViewModel : BindableViewModelBase, IDataErrorInfo
    {
        #region Fields

        private readonly Parameter parameter;
        private readonly IRepository repository;

        private bool _canEdit;

        private double? _xSteps;
        private double? _ySteps;
        private ObservableCollection<DeltaLine> _absoluteDeltaXLines;
        private ObservableCollection<DeltaLine> _absoluteDeltaYLines;
        private SensorViewModel _selectedSensorViewModel;

        private RelayCommand _reloadSensorsCommand;

        private PropertyInfo[] _propertyInfos;

        #endregion



        #region Constructors / Destructor

        public ParameterViewModel(IRepository repository, Parameter parameter)
        {
            this.repository = repository;
            this.parameter = parameter;
            this.Sensors = new ObservableCollection<SensorViewModel>();

            this.InitSensors();

            this.CalculateXSteps();
            this.CalculateYSteps();
            this.CalculateAbsoluteDeltaLines();
        }

        #endregion



        #region Properties

        /// <summary>
        ///     Indicates whether the properties can be edited.
        /// </summary>
        public bool CanEdit
        {
            get { return this._canEdit; }
            set { this.SetProperty(() => this._canEdit = value, () => this._canEdit == value); }
        }

        public bool ValuesSet
        {
            get { return this.parameter.ValuesSet(); }
        }

        public int ID
        {
            get { return this.parameter.ID; }
            set { this.SetProperty(() => this.parameter.ID = value, () => this.parameter.ID == value); }
        }

        public bool DoQuasipeakMeasurement
        {
            get { return this.parameter.DoQuasipeakMeasurement; }
            set { this.SetProperty(() => this.parameter.DoQuasipeakMeasurement = value, () => this.parameter.DoQuasipeakMeasurement == value); }
        }

        public double? OriginX
        {
            get { return this.parameter.OriginX; }
            set
            {
                bool changed = false;
                double roundedValue = value != null ? this.GetOneDigitRoundedValue((double)value) : 0;
                double? deltaX = this.parameter.OriginX - roundedValue;

                if (value == null)
                {
                    this.SetProperty(() => this.parameter.OriginX = null, () => this.parameter.OriginX == null);
                }
                else if (roundedValue < Settings.Default.XMin)
                {
                    changed = this.SetProperty(() => this.parameter.OriginX = Settings.Default.XMin, () => this.parameter.OriginX.Equals(Settings.Default.XMin));
                }
                else if (roundedValue > Settings.Default.XMax)
                {
                    changed = this.SetProperty(() => this.parameter.OriginX = Settings.Default.XMax, () => this.parameter.OriginX.Equals(Settings.Default.XMax));
                }
                else
                {
                    changed = this.SetProperty(() => this.parameter.OriginX = roundedValue, () => this.parameter.OriginX.Equals(roundedValue));
                }

                if (changed)
                {
                    this.SendOriginsToSerialPort();

                    if (deltaX >= 0)
                    {
                        this.RelativeXEnd = this.RelativeXEnd + deltaX;
                        this.RelativeXStart = this.RelativeXStart + deltaX;
                    }
                    else
                    {
                        this.RelativeXStart = this.RelativeXStart + deltaX;
                        this.RelativeXEnd = this.RelativeXEnd + deltaX;
                    }
                }
            }
        }

        public double? OriginY
        {
            get { return this.parameter.OriginY; }
            set
            {
                bool changed = false;
                double roundedValue = value != null ? this.GetOneDigitRoundedValue((double)value) : 0;
                double? deltaY = this.parameter.OriginY - roundedValue;

                if (value == null)
                {
                    this.SetProperty(() => this.parameter.OriginY = null, () => this.parameter.OriginY == null);
                }
                else if (roundedValue < Settings.Default.YMin)
                {
                    changed = this.SetProperty(() => this.parameter.OriginY = Settings.Default.YMin, () => this.parameter.OriginY.Equals(Settings.Default.YMin));
                }
                else if (roundedValue > Settings.Default.YMax)
                {
                    changed = this.SetProperty(() => this.parameter.OriginY = Settings.Default.YMax, () => this.parameter.OriginY.Equals(Settings.Default.YMax));
                }
                else
                {
                    changed = this.SetProperty(() => this.parameter.OriginY = roundedValue, () => this.parameter.OriginY.Equals(roundedValue));
                }

                if (changed)
                {
                    this.SendOriginsToSerialPort();

                    if (deltaY >= 0)
                    {
                        this.RelativeYEnd = this.RelativeYEnd + deltaY;
                        this.RelativeYStart = this.RelativeYStart + deltaY;
                    }
                    else
                    {
                        this.RelativeYStart = this.RelativeYStart + deltaY;
                        this.RelativeYEnd = this.RelativeYEnd + deltaY;
                    }
                }
            }
        }

        public double? AbsoluteXStart
        {
            get
            {
                if (this.OriginX == null || this.RelativeXStart == null)
                {
                    return null;
                }

                return this.OriginX + this.RelativeXStart;
            }
        }

        public double? RelativeXStart
        {
            get { return this.parameter.XStart; }
            set
            {
                bool changed = false;
                double roundedValue = value != null ? this.GetOneDigitRoundedValue((double)value) : 0;

                if (value == null)
                {
                    this.SetProperty(() => this.parameter.XStart = null, () => this.parameter.XStart == null);
                }
                else if (this.OriginX + roundedValue < Settings.Default.XMin)
                {
                    changed = this.SetProperty(() => this.parameter.XStart = Settings.Default.XMin - this.OriginX, () => this.parameter.XStart.Equals(Settings.Default.XMin - this.OriginX));
                }
                else if (this.AbsoluteXEnd == null && this.OriginX + roundedValue >= Settings.Default.XMax)
                {
                    changed = this.SetProperty(() => this.parameter.XStart = Settings.Default.XMax - 1 - this.OriginX, () => this.parameter.XStart.Equals(Settings.Default.XMax - 1 - this.OriginX));
                }
                else if (this.OriginX + roundedValue >= this.AbsoluteXEnd)
                {
                    changed = this.SetProperty(() => this.parameter.XStart = this.AbsoluteXEnd - 1 - this.OriginX, () => this.parameter.XStart.Equals(this.AbsoluteXEnd - 1 - this.OriginX));
                }
                else
                {
                    changed = this.SetProperty(() => this.parameter.XStart = roundedValue, () => this.parameter.XStart.Equals(roundedValue));
                }

                if (changed)
                {
                    this.RaisePropertyChanged(() => this.XDifference);
                    this.RaisePropertyChanged(() => this.AbsoluteXStart);
                    this.CalculateXSteps();
                    this.CalculateAbsoluteDeltaLines();
                }
            }
        }

        public double? DeltaX
        {
            get { return this.parameter.DeltaX; }
            set
            {
                bool changed;

                if (value < 1)
                {
                    changed = this.SetProperty(() => this.parameter.DeltaX = 1, () => this.parameter.DeltaX.Equals(1.0));
                }
                else if (value > this.XDifference)
                {
                    changed = this.SetProperty(() => this.parameter.DeltaX = this.XDifference, () => this.parameter.DeltaX.Equals(this.XDifference));
                }
                else
                {
                    changed = this.SetProperty(() => this.parameter.DeltaX = value, () => this.parameter.DeltaX.Equals(value));
                }

                if (changed)
                {
                    this.CalculateXSteps();
                    this.CalculateAbsoluteDeltaLines();
                }
            }
        }

        public double? XSteps
        {
            get { return this._xSteps; }
            set
            {
                if (value < 2)
                {
                    value = 2;
                }

                this.SetProperty(ref this._xSteps, value);
                this.CalculateDeltaX();
                this.CalculateAbsoluteDeltaLines();
            }
        }

        public ObservableCollection<DeltaLine> AbsoluteDeltaXLines
        {
            get
            {
                if (this._absoluteDeltaXLines == null)
                {
                    this._absoluteDeltaXLines = new ObservableCollection<DeltaLine>();
                }

                return this._absoluteDeltaXLines;
            }
        }

        public double? XDifference
        {
            get
            {
                if (this.RelativeXStart != null && this.RelativeXEnd != null)
                {
                    return Math.Abs((double)this.RelativeXEnd - (double)this.RelativeXStart);
                }

                return null;
            }
        }

        public double? AbsoluteXEnd
        {
            get
            {
                if (this.OriginX == null || this.RelativeXEnd == null)
                {
                    return null;
                }

                return this.OriginX + this.RelativeXEnd;
            }
        }

        public double? RelativeXEnd
        {
            get { return this.parameter.XEnd; }
            set
            {
                bool changed = false;
                double roundedValue = value != null ? this.GetOneDigitRoundedValue((double)value) : 0;

                if (value == null)
                {
                    this.SetProperty(() => this.parameter.XEnd = null, () => this.parameter.XEnd == null);
                }
                else if (this.AbsoluteXStart == null && this.OriginX + roundedValue <= Settings.Default.XMin)
                {
                    changed = this.SetProperty(() => this.parameter.XEnd = Settings.Default.XMin + 1 - this.OriginX, () => this.parameter.XEnd.Equals(Settings.Default.XMin + 1 - this.OriginX));
                }
                else if (this.OriginX + roundedValue <= this.AbsoluteXStart)
                {
                    changed = this.SetProperty(() => this.parameter.XEnd = this.AbsoluteXStart + 1 - this.OriginX, () => this.parameter.XEnd.Equals(this.AbsoluteXStart + 1 - this.OriginX));
                }
                else if (this.OriginX + roundedValue > Settings.Default.XMax)
                {
                    changed = this.SetProperty(() => this.parameter.XEnd = Settings.Default.XMax - this.OriginX, () => this.parameter.XEnd.Equals(Settings.Default.XMax - this.OriginX));
                }
                else
                {
                    changed = this.SetProperty(() => this.parameter.XEnd = roundedValue, () => this.parameter.XEnd.Equals(roundedValue));
                }

                if (changed)
                {
                    this.RaisePropertyChanged(() => this.XDifference);
                    this.RaisePropertyChanged(() => this.AbsoluteXEnd);
                    this.CalculateXSteps();
                    this.CalculateAbsoluteDeltaLines();
                }
            }
        }

        public double? AbsoluteYStart
        {
            get
            {
                if (this.OriginY == null || this.RelativeYStart == null)
                {
                    return null;
                }

                return this.OriginY + this.RelativeYStart;
            }
        }

        public double? RelativeYStart
        {
            get { return this.parameter.YStart; }
            set
            {
                bool changed = false;
                double roundedValue = value != null ? this.GetOneDigitRoundedValue((double)value) : 0;

                if (value == null)
                {
                    this.SetProperty(() => this.parameter.YStart = null, () => this.parameter.YStart == null);
                }
                else if (this.OriginY + roundedValue < Settings.Default.YMin)
                {
                    changed = this.SetProperty(() => this.parameter.YStart = Settings.Default.YMin - this.OriginY, () => this.parameter.YStart.Equals(Settings.Default.YMin - this.OriginY));
                }
                else if (this.AbsoluteYEnd == null && this.OriginY + roundedValue >= Settings.Default.YMax)
                {
                    changed = this.SetProperty(() => this.parameter.YStart = Settings.Default.YMax - 1 - this.OriginY, () => this.parameter.YStart.Equals(Settings.Default.YMax - 1 - this.OriginY));
                }
                else if (this.OriginY + roundedValue >= this.AbsoluteYEnd)
                {
                    changed = this.SetProperty(() => this.parameter.YStart = this.AbsoluteYEnd - 1 - this.OriginY, () => this.parameter.YStart.Equals(this.AbsoluteYEnd - 1 - this.OriginY));
                }
                else
                {
                    changed = this.SetProperty(() => this.parameter.YStart = roundedValue, () => this.parameter.YStart.Equals(roundedValue));
                }

                if (changed)
                {
                    this.RaisePropertyChanged(() => this.YDifference);
                    this.RaisePropertyChanged(() => this.AbsoluteYStart);
                    this.CalculateYSteps();
                    this.CalculateAbsoluteDeltaLines();
                }
            }
        }

        public double? DeltaY
        {
            get { return this.parameter.DeltaY; }
            set
            {
                bool changed;

                if (value < 1)
                {
                    changed = this.SetProperty(() => this.parameter.DeltaY = 1, () => this.parameter.DeltaY.Equals(1.0));
                }
                else if (value > this.YDifference)
                {
                    changed = this.SetProperty(() => this.parameter.DeltaY = this.YDifference, () => this.parameter.DeltaY.Equals(this.YDifference));
                }
                else
                {
                    changed = this.SetProperty(() => this.parameter.DeltaY = value, () => this.parameter.DeltaY.Equals(value));
                }

                if (changed)
                {
                    this.CalculateYSteps();
                    this.CalculateAbsoluteDeltaLines();
                }
            }
        }

        public double? YSteps
        {
            get { return this._ySteps; }
            set
            {
                if (value < 2)
                {
                    value = 2;
                }

                this.SetProperty(ref this._ySteps, value);
                this.CalculateDeltaY();
                this.CalculateAbsoluteDeltaLines();
            }
        }

        public ObservableCollection<DeltaLine> AbsoluteDeltaYLines
        {
            get
            {
                if (this._absoluteDeltaYLines == null)
                {
                    this._absoluteDeltaYLines = new ObservableCollection<DeltaLine>();
                }

                return this._absoluteDeltaYLines;
            }
        }

        public double? YDifference
        {
            get
            {
                if (this.RelativeYStart != null && this.RelativeYEnd != null)
                {
                    return Math.Abs((double)this.RelativeYEnd - (double)this.RelativeYStart);
                }

                return null;
            }
        }

        public double? AbsoluteYEnd
        {
            get
            {
                if (this.OriginY == null || this.RelativeYEnd == null)
                {
                    return null;
                }

                return this.OriginY + this.RelativeYEnd;
            }
        }

        public double? RelativeYEnd
        {
            get { return this.parameter.YEnd; }
            set
            {
                bool changed = false;
                double roundedValue = value != null ? this.GetOneDigitRoundedValue((double)value) : 0;

                if (value == null)
                {
                    this.SetProperty(() => this.parameter.YEnd = null, () => this.parameter.YEnd == null);
                }
                else if (this.AbsoluteYStart == null && this.OriginY + roundedValue <= Settings.Default.YMin)
                {
                    changed = this.SetProperty(() => this.parameter.YEnd = Settings.Default.YMin + 1 - this.OriginY, () => this.parameter.YEnd.Equals(Settings.Default.YMin + 1 - this.OriginY));
                }
                else if (this.OriginY + roundedValue <= this.AbsoluteYStart)
                {
                    changed = this.SetProperty(() => this.parameter.YEnd = this.AbsoluteYStart + 1 - this.OriginY, () => this.parameter.YEnd.Equals(this.AbsoluteYStart + 1 - this.OriginY));
                }
                else if (this.OriginY + roundedValue > Settings.Default.YMax)
                {
                    changed = this.SetProperty(() => this.parameter.YEnd = Settings.Default.YMax - this.OriginY, () => this.parameter.YEnd.Equals(Settings.Default.YMax - this.OriginY));
                }
                else
                {
                    changed = this.SetProperty(() => this.parameter.YEnd = roundedValue, () => this.parameter.YEnd.Equals(roundedValue));
                }

                if (changed)
                {
                    this.RaisePropertyChanged(() => this.YDifference);
                    this.RaisePropertyChanged(() => this.AbsoluteYEnd);
                    this.CalculateYSteps();
                    this.CalculateAbsoluteDeltaLines();
                }
            }
        }

        public double? FreqStart
        {
            get { return this.parameter.FreqStart; }

            set
            {
                if (value < 1)
                {
                    this.SetProperty(() => this.parameter.FreqStart = 1, () => this.parameter.FreqStart.Equals(1.0));
                }
                else if (value >= this.FreqEnd)
                {
                    this.SetProperty(() => this.parameter.FreqStart = this.FreqEnd / 1e1, () => this.parameter.FreqStart.Equals(this.FreqEnd / 1e1));
                }
                else
                {
                    this.SetProperty(() => this.parameter.FreqStart = value, () => this.parameter.FreqStart.Equals(value));
                }
            }
        }

        public double? DeltaFreq
        {
            get { return this.parameter.DeltaFreq; }
            private set { this.SetProperty(() => this.parameter.DeltaFreq = value, () => this.parameter.DeltaFreq.Equals(value)); }
        }

        public double? FreqEnd
        {
            get { return this.parameter.FreqEnd; }

            set
            {
                if (this.FreqStart == null && value < 1e1)
                {
                    this.SetProperty(() => this.parameter.FreqEnd = 1e1, () => this.parameter.FreqEnd.Equals(1e1));
                }
                else if (value <= this.FreqStart)
                {
                    this.SetProperty(() => this.parameter.FreqEnd = this.FreqStart * 1e1, () => this.parameter.FreqEnd.Equals(this.FreqStart * 1e1));
                }
                else
                {
                    this.SetProperty(() => this.parameter.FreqEnd = value, () => this.parameter.FreqEnd.Equals(value));
                }
            }
        }

        public ResBW? ResBW
        {
            get { return this.parameter.ResBW; }
            set
            {
                this.SetProperty(() => this.parameter.ResBW = value, () => this.parameter.ResBW == value);

                if (this.ResBW != null)
                {
                    this.DeltaFreq = (double)this.ResBW / 4;
                }
                else
                {
                    this.DeltaFreq = null;
                }
            }
        }

        public int? MeasureTime
        {
            get { return this.parameter.MeasureTime; }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }

                this.SetProperty(() => this.parameter.MeasureTime = value, () => this.parameter.MeasureTime == value);
            }
        }

        public Orientation? Orientation
        {
            get { return this.parameter.Orientation; }
            set { this.SetProperty(() => this.parameter.Orientation = value, () => this.parameter.Orientation == value); }
        }

        public SensorViewModel SelectedSensorViewModel
        {
            get { return this._selectedSensorViewModel; }
            set
            {
                if (this.SetProperty(ref this._selectedSensorViewModel, value))
                {
                    if (this._selectedSensorViewModel != null)
                    {
                        this.SensorType = this._selectedSensorViewModel.SensorType;
                        this.SensorImageStream = this._selectedSensorViewModel.SensorImageStream;
                        this.SensorName = this._selectedSensorViewModel.SensorName;
                    }
                    else
                    {
                        this.SensorType = null;
                        this.SensorName = null;
                        this.SensorImageStream = null;
                    }

                    // Update to see / delete error messages when under / over sensor frequency ranges
                    this.RaisePropertyChanged(() => this.FreqStart);
                    this.RaisePropertyChanged(() => this.FreqEnd);
                }
            }
        }

        public SensorType? SensorType
        {
            get { return this.parameter.SensorType; }
            private set { this.SetProperty(() => this.parameter.SensorType = value, () => this.parameter.SensorType == value); }
        }

        public byte[] SensorImageStream
        {
            get { return this.parameter.SensorImageStream; }
            private set { this.SetProperty(() => this.parameter.SensorImageStream = value, () => this.parameter.SensorImageStream != null && this.parameter.SensorImageStream.Equals(value)); }
        }

        public string SensorName
        {
            get { return this.parameter.SensorName; }
            private set { this.SetProperty(() => this.parameter.SensorName = value, () => this.parameter.SensorName == value); }
        }

        public ObservableCollection<SensorViewModel> Sensors { get; set; }

        /// <summary>
        ///     Gets command to reload all sensors from the database.
        /// </summary>
        public RelayCommand ReloadSensorsCommand
        {
            get
            {
                if (this._reloadSensorsCommand == null)
                {
                    this._reloadSensorsCommand = new RelayCommand(this.ReloadSensors);
                }

                return this._reloadSensorsCommand;
            }
        }

        private PropertyInfo[] PropertyInfos
        {
            get
            {
                if (this._propertyInfos == null)
                {
                    this._propertyInfos = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                }

                return this._propertyInfos;
            }
        }

        #endregion



        #region IDataErrorInfo Members

        public string this[string propertyName]
        {
            get
            {
                if (propertyName == PropertyNameExtractor.GetPropertyName<DefaultParameterViewModel>(x => x.FreqStart))
                {
                    if (this.SelectedSensorViewModel != null && (this.SelectedSensorViewModel.MinFrequency != null) & (this.FreqStart < this.SelectedSensorViewModel.MinFrequency))
                    {
                        return "Min frequency is outside specified sensor frequency range!";
                    }
                }
                else if (propertyName == PropertyNameExtractor.GetPropertyName<DefaultParameterViewModel>(x => x.FreqEnd))
                {
                    if (this.SelectedSensorViewModel != null && (this.SelectedSensorViewModel.MaxFrequency != null) & (this.FreqEnd > this.SelectedSensorViewModel.MaxFrequency))
                    {
                        return "Max frequency is outside specified sensor frequency range!";
                    }
                }

                return string.Empty;
            }
        }

        public string Error
        {
            get { return null; }
        }

        #endregion



        public void Clear()
        {
            this.parameter.Clear();
            this.SelectedSensorViewModel = null;
            this.XSteps = null;
            this.YSteps = null;
            this.AbsoluteDeltaXLines.Clear();
            this.AbsoluteDeltaYLines.Clear();

            this.UpdateProperties();
        }

        /// <summary>
        ///     Loads all sensors from database and sets selected sensor to default sensor.
        /// </summary>
        private void InitSensors()
        {
            try
            {
                List<Sensor> sensors = this.repository.GetAll<Sensor>().ToList();

                // add all sensors to list
                foreach (Sensor sensor in sensors)
                {
                    this.Sensors.InsertOrderedBy(new SensorViewModel(sensor), x => x.SensorName);
                }

                // if default sensor in list, select it, otherwise select first in list
                if (!string.IsNullOrEmpty(this.SensorName) && this.Sensors.Count(x => x.SensorName == this.SensorName && x.SensorType == this.SensorType) != 0)
                {
                    this.SelectedSensorViewModel = this.Sensors.First(x => x.SensorName == this.SensorName && x.SensorType == this.SensorType);
                }
                else
                {
                    // select first sensor in list if exist
                    this.SelectedSensorViewModel = this.Sensors.FirstOrDefault();

                    if (this.SelectedSensorViewModel == null)
                    {
                        // database was empty, so default properties should be deleted
                        this.ClearSensorData();
                    }
                }
            }
            catch
            {
                // no connection to database
            }
        }

        /// <summary>
        ///     Clears all values that depend the sensor
        /// </summary>
        private void ClearSensorData()
        {
            this.SensorName = null;
            this.SensorType = null;
            this.SensorImageStream = null;
        }

        /// <summary>
        ///     Reloads all sensors from the database.
        /// </summary>
        private void ReloadSensors()
        {
            try
            {
                List<Sensor> sensors = this.repository.GetAll<Sensor>().ToList();

                foreach (Sensor sensor in sensors)
                {
                    // add sensors from database when not already in list
                    if (this.Sensors.Count(this.SensorViewModelSearchCriteria(sensor.SensorName, sensor.SensorType, sensor.SensorImageStream)) == 0)
                    {
                        this.Sensors.InsertOrderedBy(new SensorViewModel(sensor), x => x.SensorName);
                    }
                }

                for (int i = this.Sensors.Count - 1; i >= 0; i--)
                {
                    // delete sensors that do not exist in the database
                    if (sensors.Count(this.SensorSearchCriteria(this.Sensors[i].SensorName, this.Sensors[i].SensorType, this.Sensors[i].SensorImageStream)) == 0)
                    {
                        this.Sensors.Remove(this.Sensors[i]);
                    }

                    // now null when sensor that was not in database was selected
                    if (this.SelectedSensorViewModel == null)
                    {
                        // set to first one in sensor list
                        this.SelectedSensorViewModel = this.Sensors.FirstOrDefault();
                    }
                }
            }
            catch
            {
                // no connection to database
                this.Sensors.Clear();
            }
        }

        /// <summary>
        ///     Creates equal condition between two sensor view models.
        /// </summary>
        /// <param name="sensorName">sensor name</param>
        /// <param name="sensorType">sensor type</param>
        /// <param name="sensorImageStream">sensor image stream</param>
        /// <returns>condition that must be met</returns>
        private Func<SensorViewModel, bool> SensorViewModelSearchCriteria(string sensorName, SensorType? sensorType, byte[] sensorImageStream)
        {
            return x => x.SensorName == sensorName &&
                        x.SensorType == sensorType &&
                        (x.SensorImageStream == null && sensorImageStream == null ||
                         x.SensorImageStream != null && sensorImageStream != null && x.SensorImageStream.SequenceEqual(sensorImageStream));
        }

        /// <summary>
        ///     Creates equal condition between two sensors.
        /// </summary>
        /// <param name="sensorName">sensor name</param>
        /// <param name="sensorType">sensor type</param>
        /// <param name="sensorImageStream">sensor image stream</param>
        /// <returns>condition that must be met</returns>
        private Func<Sensor, bool> SensorSearchCriteria(string sensorName, SensorType? sensorType, byte[] sensorImageStream)
        {
            return x => x.SensorName == sensorName &&
                        x.SensorType == sensorType &&
                        (x.SensorImageStream == null && sensorImageStream == null ||
                         x.SensorImageStream != null && sensorImageStream != null && x.SensorImageStream.SequenceEqual(sensorImageStream));
        }

        private void UpdateProperties()
        {
            foreach (PropertyInfo propertyInfo in this.PropertyInfos)
            {
                this.RaisePropertyChanged(propertyInfo.Name);
            }
        }

        private void CalculateXSteps()
        {
            if (this.DeltaX != null && this.XDifference != null)
            {
                this.XSteps = Math.Round((double)this.XDifference / (double)this.DeltaX) + 1;
            }
            else
            {
                this.XSteps = null;
            }
        }

        private void CalculateYSteps()
        {
            if (this.DeltaY != null && this.YDifference != null)
            {
                this.YSteps = Math.Round((double)this.YDifference / (double)this.DeltaY) + 1;
            }
            else
            {
                this.YSteps = null;
            }
        }

        private void CalculateDeltaX()
        {
            this.DeltaX = this.XDifference / (this.XSteps - 1);
        }

        private void CalculateDeltaY()
        {
            this.DeltaY = this.YDifference / (this.YSteps - 1);
        }

        /// <summary>
        ///     Calculates the grid lines between the measurement positions.
        /// </summary>
        private void CalculateAbsoluteDeltaLines()
        {
            if (this.AbsoluteXStart == null || this.AbsoluteYEnd == null || this.XDifference == null ||
                this.AbsoluteYStart == null || this.AbsoluteYEnd == null || this.YDifference == null)
            {
                return;
            }

            this.AbsoluteDeltaXLines.Clear();
            double? currentAbsoluteValue = this.AbsoluteXStart;

            while (currentAbsoluteValue - this.AbsoluteXEnd <= Settings.Default.DoubleTolerance)
            {
                DeltaLine deltaLine = new DeltaLine(Settings.Default.StrokeSize, (double)this.YDifference, (double)currentAbsoluteValue, (double)this.AbsoluteYStart);
                this.AbsoluteDeltaXLines.Add(deltaLine);
                currentAbsoluteValue += this.DeltaX;
            }

            this.AbsoluteDeltaYLines.Clear();
            currentAbsoluteValue = this.AbsoluteYStart;

            while (currentAbsoluteValue - this.AbsoluteYEnd <= Settings.Default.DoubleTolerance)
            {
                DeltaLine deltaLine = new DeltaLine((double)this.XDifference, Settings.Default.StrokeSize, (double)this.AbsoluteXStart, (double)currentAbsoluteValue);
                this.AbsoluteDeltaYLines.Add(deltaLine);
                currentAbsoluteValue += this.DeltaY;
            }
        }

        private double GetOneDigitRoundedValue(double value)
        {
            return Math.Round(value, 1);
        }

        private void SendOriginsToSerialPort()
        {
            Messenger.Default.Send(new NotificationMessage<Tuple<double?, double?>>(new Tuple<double?, double?>(this.OriginX, this.OriginY), Resources.Messenger_SetOriginForSerialPortVM));
        }
    }
}