﻿// ///////////////////////////////////
// File: ScanViewModel.cs
// Last Change: 20.02.2018  10:50
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using Business;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Ivi.SpecAn;
    using Model;
    using Model.Database;
    using Model.Enum;
    using NHibernate.Criterion;
    using Properties;
    using Service;
    using State;



    // ReSharper disable CompareOfFloatsByEqualityOperator
    public class ScanViewModel : WorkspaceViewModel, IDisposable
    {
        #region Fields

        private readonly IRepository repository;
        private readonly IDialogService _dialogService;
        private readonly ManualResetEvent threadLocker;
        private readonly EsrTestReceiver testReceiver;

        private Analysis analysis;

        private IScanState _currentState;
        private List<IScanState> states;
        private PropertyInfo[] _propertyInfos;

        private SerialPortViewModel _serialPortViewModel;
        private AnalysisViewModel _analysisViewModel;
        private ParameterViewModel _parameterViewModel;
        private SpectrumViewModel _spectrumViewModel;

        private BackgroundWorker _measurementBackgroundWorker;
        private BackgroundWorker _goToPositionBackgroundWorker;

        private int _measurementProgress;
        private bool _isProgressVisible;

        private RelayCommand _editAnalysisCommand;
        private RelayCommand _goToGridPositionCommand;

        #endregion



        #region Constructors / Destructor

        public ScanViewModel(IRepository repository, IDialogService dialogService, SerialPortViewModel serialPortViewModel, string title, Bitmap bitmap)
                : base(title, bitmap)
        {
            this.repository = repository;
            this._dialogService = dialogService;
            this.threadLocker = new ManualResetEvent(false);
            this.testReceiver = new EsrTestReceiver();
            this.SerialPortViewModel = serialPortViewModel;

            this.InitStates();
            this.InitCommands();
            this.InitBackgroundWorker();

            this.ChangeToEmptyState();

            Messenger.Default.Register<NotificationMessage>(this, this.ExecuteNotificationMessage);
            Messenger.Default.Register<NotificationMessage<int>>(this, this.ExecuteNotificationMessage);
            Messenger.Default.Register<NotificationMessage<Parameter>>(this, this.ExecuteNotificationMessage);
        }

        #endregion



        #region Properties

        public SerialPortViewModel SerialPortViewModel
        {
            get { return this._serialPortViewModel; }
            private set { this.SetProperty(ref this._serialPortViewModel, value); }
        }

        public AnalysisViewModel AnalysisViewModel
        {
            get { return this._analysisViewModel; }
            private set { this.SetProperty(ref this._analysisViewModel, value); }
        }

        public ParameterViewModel ParameterViewModel
        {
            get { return this._parameterViewModel; }
            private set { this.SetProperty(ref this._parameterViewModel, value); }
        }

        public SpectrumViewModel SpectrumViewModel
        {
            get { return this._spectrumViewModel; }
            private set { this.SetProperty(ref this._spectrumViewModel, value); }
        }

        public List<ImageCommandViewModel> CRUDCommands { get; private set; }

        public ImageCommandViewModel CreateNewAnalysisCommand { get; private set; }

        public ImageCommandViewModel RepeatAnalysisCommand { get; private set; }

        public ImageCommandViewModel DeleteAnalysisCommand { get; private set; }

        public List<ImageCommandViewModel> TestExecutionCommands { get; private set; }

        public ImageCommandViewModel StartAnalysisCommand { get; private set; }

        public ImageCommandViewModel PauseAnalysisCommand { get; private set; }

        public ImageCommandViewModel StopAnalysisCommand { get; private set; }

        public IScanState CurrentState
        {
            get { return this._currentState; }
            private set
            {
                if (this._currentState == value)
                {
                    return;
                }

                this._currentState = value;
                this.UpdateViewModel();
                this.SendMainMenuState();
                this.GoToGridPositionCommand.RaiseCanExecuteChanged();
            }
        }

        public int MeasurementProgress
        {
            get { return this._measurementProgress; }
            private set { this.SetProperty(ref this._measurementProgress, value); }
        }

        public bool IsProgressVisible
        {
            get { return this._isProgressVisible; }
            private set { this.SetProperty(ref this._isProgressVisible, value); }
        }

        public RelayCommand EditAnalysisCommand
        {
            get
            {
                if (this._editAnalysisCommand == null)
                {
                    this._editAnalysisCommand = new RelayCommand(this.EditAnalysis, this.CanEditAnalysis);
                }

                return this._editAnalysisCommand;
            }
        }

        public RelayCommand GoToGridPositionCommand
        {
            get
            {
                if (this._goToGridPositionCommand == null)
                {
                    this._goToGridPositionCommand = new RelayCommand(this.GoToGridPosition, this.CanGoToGridPosition);
                }

                return this._goToGridPositionCommand;
            }
        }

        private bool CanEdit
        {
            get
            {
                if (this.CurrentState.GetType() == typeof(ScanCreationState))
                {
                    return true;
                }

                return false;
            }
        }

        private PropertyInfo[] PropertyInfos
        {
            get
            {
                if (this._propertyInfos == null)
                {
                    this._propertyInfos = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                }

                return this._propertyInfos;
            }
        }

        #endregion



        #region IDisposable Members

        public void Dispose()
        {
            if (this.threadLocker != null)
            {
                this.threadLocker.Dispose();
            }

            if (this._serialPortViewModel != null)
            {
                this._serialPortViewModel.Dispose();
            }

            if (this._measurementBackgroundWorker != null)
            {
                this._measurementBackgroundWorker.Dispose();
            }

            if (this._goToPositionBackgroundWorker != null)
            {
                this._goToPositionBackgroundWorker.Dispose();
            }

            Messenger.Default.Unregister(this);
        }

        #endregion



        public void SetSerialOriginToParameterOrigin()
        {
            if (this.ParameterViewModel != null && this.ParameterViewModel.OriginX != null && this.ParameterViewModel.OriginY != null)
            {
                this.SerialPortViewModel.OriginX = (double)this.ParameterViewModel.OriginX;
                this.SerialPortViewModel.OriginY = (double)this.ParameterViewModel.OriginY;
            }
        }

        private bool CanEditAnalysis()
        {
            if (this.CurrentState.GetType() == typeof(ScanLoadedState) ||
                this.CurrentState.GetType() == typeof(ScanEditState))
            {
                return true;
            }

            return false;
        }

        private void EditAnalysis()
        {
            if (this.CurrentState.GetType() == typeof(ScanLoadedState))
            {
                this.ChangeToEditState();
            }
            else if (this.CurrentState.GetType() == typeof(ScanEditState))
            {
                this.repository.SaveOrUpdate(this.analysis);
                this.ChangeToLoadedState();
            }
        }

        private void InitCommands()
        {
            this.CreateNewAnalysisCommand = new ImageCommandViewModel(Resources.img_new_analysis, Resources.CommandTitle_NewAnalysis,
                                                                      new RelayCommand(this.CreateNewAnalysis, this.CanCreateNewAnalysis));

            this.RepeatAnalysisCommand = new ImageCommandViewModel(Resources.img_repeat_analysis, Resources.CommandTitle_RepeatAnalysis,
                                                                   new RelayCommand(this.RepeatAnalysis, this.CanRepeatAnalysis));

            this.DeleteAnalysisCommand = new ImageCommandViewModel(Resources.img_delete_analysis, Resources.CommandTitle_DeleteAnalysis,
                                                                   new RelayCommand(this.DeleteAnalysis, this.CanDeleteAnalysis));

            this.CRUDCommands = new List<ImageCommandViewModel>
                                {
                                        this.CreateNewAnalysisCommand,
                                        this.RepeatAnalysisCommand,
                                        this.DeleteAnalysisCommand
                                };

            this.StartAnalysisCommand = new ImageCommandViewModel(Resources.img_play, Resources.CommandTitle_StartAnalysis,
                                                                  new RelayCommand(this.StartAnalysis, this.CanStartAnalysis));

            this.PauseAnalysisCommand = new ImageCommandViewModel(Resources.img_pause, Resources.CommandTitle_PauseAnalysis,
                                                                  new RelayCommand(this.PauseAnalysis, this.CanPauseAnalysis));

            this.StopAnalysisCommand = new ImageCommandViewModel(Resources.img_stop, Resources.CommandTitle_StopAnalysis,
                                                                 new RelayCommand(this.StopAnalysis, this.CanStopAnalysis));

            this.TestExecutionCommands = new List<ImageCommandViewModel>
                                         {
                                                 this.StartAnalysisCommand,
                                                 this.PauseAnalysisCommand,
                                                 this.StopAnalysisCommand
                                         };
        }

        private void InitStates()
        {
            this.states = new List<IScanState>
                          {
                                  new ScanCreationState(),
                                  new ScanLoadedState(),
                                  new ScanEditState(),
                                  new ScanPauseState(),
                                  new ScanRunState(),
                                  new ScanEmptyState()
                          };
        }

        private void LoadAnalysis(int id, bool loadSpectrumViewModel = true)
        {
            this.analysis = this.repository.GetEagerById<Analysis>(id, a => a.Measurements);
            this.LoadViewModels(loadSpectrumViewModel);
        }

        private void LoadAnalysis(Analysis newAnalysis = null, bool loadSpectrumViewModel = true)
        {
            this.analysis = newAnalysis;
            this.LoadViewModels(loadSpectrumViewModel);
        }

        private void Clear()
        {
            this.LoadAnalysis(null, false);
            this.ChangeToEmptyState();
        }

        private void LoadViewModels(bool loadSpectrumViewModel)
        {
            this.AnalysisViewModel = null;
            this.ParameterViewModel = null;
            this.SpectrumViewModel = null;
            GC.Collect();

            if (this.analysis != null)
            {
                this.AnalysisViewModel = new AnalysisViewModel(this.repository, this.analysis);
                this.ParameterViewModel = this.AnalysisViewModel.ParameterViewModel;

                if (loadSpectrumViewModel)
                {
                    this.SpectrumViewModel = new SpectrumViewModel(this.repository, this.analysis);
                }

                this.SetSerialOriginToParameterOrigin();
            }
        }

        private bool CanCreateNewAnalysis()
        {
            return this.CurrentState.CanCreate();
        }

        private void CreateNewAnalysis()
        {
            this.LoadAnalysis(new Analysis(), false);
            this.ChangeToCreationState();
        }

        private bool CanRepeatAnalysis()
        {
            return this.CurrentState.CanRepeat();
        }

        private void RepeatAnalysis()
        {
            this.LoadAnalysis(this.analysis.Clone(), false);
            this.ChangeToCreationState();
        }

        private bool CanDeleteAnalysis()
        {
            return this.CurrentState.CanDelete();
        }

        private async void DeleteAnalysis()
        {
            if (await this._dialogService.ShowDialogYesNo(Resources.Message_Title_Attention, Resources.Message_ShouldDeleteAnalysis))
            {
                if (this.CurrentState.GetType() != typeof(ScanCreationState))
                {
                    int id = this.analysis.ID;
                    this.repository.Delete(this.analysis);
                    Messenger.Default.Send(new NotificationMessage<int>(id, Resources.Messenger_RemoveDeletedAnalysisFromSearchListForSearchVM));
                }

                this.Clear();
            }
        }

        private bool CanStartAnalysis()
        {
            return this.CurrentState.CanRun();
        }

        private void StartAnalysis()
        {
            // check if connected to database and all values are set
            string executionMessage = this.GetExecutionErrorMessage();

            if (string.IsNullOrEmpty(executionMessage))
            {
                this.ResumeBackgroundWorker();

                if (this.SerialPortViewModel.IsOpen)
                {
                    this.ChangeToRunState();
                }
            }
            else
            {
                this._dialogService.ShowMessage(Resources.Message_Title_Attention, executionMessage);
            }
        }

        private string GetExecutionErrorMessage()
        {
            if (this.repository.IsConnected == false)
            {
                return Resources.Exception_Message_NoDatabaseConnection;
            }

            if (this.ParameterViewModel.ValuesSet == false)
            {
                return Resources.Exception_Message_ParameterHasEmptyValues;
            }

            return string.Empty;
        }

        private bool CanPauseAnalysis()
        {
            return this.CurrentState.CanPause();
        }

        private void PauseAnalysis()
        {
            this.PauseBackgroundWorker();
            this.ChangeToPauseState();
        }

        private bool CanStopAnalysis()
        {
            return this.CurrentState.CanStop();
        }

        private void StopAnalysis()
        {
            this.StopMeasurementBackgroundWorker();
        }

        private void ChangeToCreationState()
        {
            this.CurrentState = this.states.Find(x => x.GetType() == typeof(ScanCreationState));
        }

        private void ChangeToLoadedState()
        {
            this.CurrentState = this.states.Find(x => x.GetType() == typeof(ScanLoadedState));
        }

        private void ChangeToEditState()
        {
            this.CurrentState = this.states.Find(x => x.GetType() == typeof(ScanEditState));
        }

        private void ChangeToPauseState()
        {
            this.CurrentState = this.states.Find(x => x.GetType() == typeof(ScanPauseState));
        }

        private void ChangeToRunState()
        {
            this.CurrentState = this.states.Find(x => x.GetType() == typeof(ScanRunState));
        }

        private void ChangeToEmptyState()
        {
            this.CurrentState = this.states.Find(x => x.GetType() == typeof(ScanEmptyState));
        }

        private void InitBackgroundWorker()
        {
            // Measurement BackgroundWorker
            this._measurementBackgroundWorker = new BackgroundWorker();
            this._measurementBackgroundWorker.WorkerSupportsCancellation = true;
            this._measurementBackgroundWorker.WorkerReportsProgress = true;
            this._measurementBackgroundWorker.DoWork += this.MeasurementBackgroundWorker_DoWork;
            this._measurementBackgroundWorker.ProgressChanged += this.MeasurementBackgroundWorker_ProgressChanged;
            this._measurementBackgroundWorker.RunWorkerCompleted += this.MeasurementBackgroundWorker_RunWorkerCompleted;

            // Go to Position BackgroundWorker
            this._goToPositionBackgroundWorker = new BackgroundWorker();
            this._goToPositionBackgroundWorker.WorkerReportsProgress = true;
            this._goToPositionBackgroundWorker.DoWork += this.GoToPositionBackgroundWorker_DoWork;
            this._goToPositionBackgroundWorker.ProgressChanged += this.GoToPositionBackgroundWorker_ProgressChanged;
            this._goToPositionBackgroundWorker.RunWorkerCompleted += this.GoToPositionBackgroundWorker_RunWorkerCompleted;
        }

        private void ResumeBackgroundWorker()
        {
            this.ConnectToSerialPortForMeasurement();
            this.SetStartTimeIfNecessary();
            this.SetupBackgroundWorkerForMeasurement(this._measurementBackgroundWorker, true);
        }

        private void ConnectToSerialPortForMeasurement()
        {
            if (this.SerialPortViewModel.IsOpen == false)
            {
                this.SerialPortViewModel.OpenCloseConnectionCommand.Execute(null);
            }
        }

        private void SetStartTimeIfNecessary()
        {
            if (this.SerialPortViewModel.IsOpen && this._measurementBackgroundWorker.IsBusy == false)
            {
                this.AnalysisViewModel.StartTime = DateTime.Now.ToString(Resources.DateTime_Format, CultureInfo.InvariantCulture);
            }
        }

        private void SetupBackgroundWorkerForMeasurement(BackgroundWorker backgroundWorker, bool hasThreadLocker)
        {
            if (this.SerialPortViewModel.IsOpen)
            {
                if (backgroundWorker.IsBusy == false)
                {
                    backgroundWorker.RunWorkerAsync();
                }

                if (hasThreadLocker)
                {
                    this.threadLocker.Set();
                }
            }
        }

        private void PauseBackgroundWorker()
        {
            this.threadLocker.Reset();
        }

        private void StopMeasurementBackgroundWorker()
        {
            if (this._measurementBackgroundWorker.IsBusy)
            {
                this._measurementBackgroundWorker.CancelAsync();
            }

            this.threadLocker.Set();
        }

        private void MeasurementBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                this.SetupTestReceiverForMeasurement();
            }
            catch (Exception exception)
            {
                this._measurementBackgroundWorker.ReportProgress(Settings.Default.InvalidTestReceiverConfigurationProgressPercentage, Resources.Exception_Message_CouldNotConnectToTestReceiver + "\n\n(" + exception.Message + ")");
                e.Result = MotorState.Disconnected;
                return;
            }

            if (this.IsAruinoConnected() == false)
            {
                this._measurementBackgroundWorker.ReportProgress(Settings.Default.WrongCOMProgressPercentage);
                e.Result = MotorState.Disconnected;
                return;
            }

            this.InitHardware();
            this.SetParameterIfAlreadyExists();
            this.repository.SaveOrUpdate(this.analysis);
            this.IsProgressVisible = true;

            // ReSharper disable PossibleInvalidOperationException
            int columns = (int)((this.ParameterViewModel.RelativeXEnd - this.ParameterViewModel.RelativeXStart) / this.ParameterViewModel.DeltaX + 1);
            int rows = (int)((this.ParameterViewModel.RelativeYEnd - this.ParameterViewModel.RelativeYStart) / this.ParameterViewModel.DeltaY + 1);

            // ReSharper restore PossibleInvalidOperationException
            double deltaProgress = 100.0 / (columns * rows);
            double progress = 0;

            for (int yIndex = 0; yIndex < rows; yIndex++)
            {
                if (yIndex % 2 != 0)
                {
                    for (int xIndex = columns - 1; xIndex >= 0; xIndex--)
                    {
                        this.threadLocker.WaitOne();

                        if (this._measurementBackgroundWorker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }

                        this.MoveToMeasurementPosition(xIndex, yIndex);
                        this.ExecuteMeasurement(xIndex, yIndex);

                        progress += deltaProgress;
                        this._measurementBackgroundWorker.ReportProgress((int)progress);

                        // necessary to show whole progress changes
                        Thread.Sleep(Settings.Default.ShortThreadDelay);
                    }
                }
                else
                {
                    for (int xIndex = 0; xIndex < columns; xIndex++)
                    {
                        this.threadLocker.WaitOne();

                        if (this._measurementBackgroundWorker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }

                        this.MoveToMeasurementPosition(xIndex, yIndex);
                        this.ExecuteMeasurement(xIndex, yIndex);

                        progress += deltaProgress;
                        this._measurementBackgroundWorker.ReportProgress((int)progress);

                        // necessary to show whole progress changes
                        Thread.Sleep(Settings.Default.ShortThreadDelay);
                    }
                }
            }
        }

        private void MeasurementBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == Settings.Default.WrongCOMProgressPercentage)
            {
                this._dialogService.ShowMessage(Resources.Message_Title_Attention, Resources.Message_NoArduinoConnectedOnThisPort);
            }
            else if (e.ProgressPercentage == Settings.Default.InvalidTestReceiverConfigurationProgressPercentage)
            {
                this._dialogService.ShowMessage(Resources.Message_Title_Attention, e.UserState.ToString());
            }
            else if (e.ProgressPercentage == Settings.Default.ExceedMaxDbmProgressPercentage)
            {
                this._dialogService.ShowMessage(Resources.Message_Title_Attention, Resources.Message_ExceededMaxDbm);
            }
            else
            {
                this.MeasurementProgress = e.ProgressPercentage;
            }
        }

        private void MeasurementBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.SerialPortViewModel.GoOriginCommand.Execute(null);
            Thread.Sleep(Settings.Default.ThreadDelay);

            while (this.SerialPortViewModel.IsRunning)
            {
                Thread.Sleep(Settings.Default.ThreadDelay);
            }

            if (this.SerialPortViewModel.IsOpen)
            {
                this.SerialPortViewModel.OpenCloseConnectionCommand.Execute(null);
            }

            if (this.testReceiver.IsConnected)
            {
                this.testReceiver.Close();
            }

            this.IsProgressVisible = false;
            this.MeasurementProgress = 0;

            if (e.Cancelled == false && e.Result != null &&
                e.Result.GetType() == typeof(MotorState) && (MotorState)e.Result == MotorState.Disconnected)
            {
                this.AnalysisViewModel.StartTime = null;
                this.AnalysisViewModel.FinishedAnalysis = null;
                this.ChangeToCreationState();
                return;
            }

            if (e.Cancelled)
            {
                this.AnalysisViewModel.FinishedAnalysis = false;
            }
            else
            {
                this.AnalysisViewModel.FinishedAnalysis = true;
            }

            this.AnalysisViewModel.EndTime = DateTime.Now.ToString(Resources.DateTime_Format, CultureInfo.InvariantCulture);
            this.repository.SaveOrUpdate(this.analysis);
            this.LoadAnalysis(this.repository.GetEagerById<Analysis>(this.analysis.ID, a => a.Measurements));
            this.ChangeToLoadedState();
        }

        private bool CanGoToGridPosition()
        {
            // check if sensor can go to defined position
            return this.ParameterViewModel != null && this.SpectrumViewModel != null &&
                   this.ParameterViewModel.OriginX != null && this.ParameterViewModel.OriginY != null &&
                   this.SpectrumViewModel.SelectedMeasurementViewModel != null;
        }

        private void GoToGridPosition()
        {
            this.ConnectToSerialPortForMeasurement();
            this.SetupBackgroundWorkerForMeasurement(this._goToPositionBackgroundWorker, false);
        }

        private void GoToPositionBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // ReSharper disable PossibleInvalidOperationException
            double absoluteX = (double)this.ParameterViewModel.OriginX + this.SpectrumViewModel.SelectedMeasurementViewModel.X;
            double absoluteY = (double)this.ParameterViewModel.OriginY + this.SpectrumViewModel.SelectedMeasurementViewModel.Y;
            // ReSharper restore PossibleInvalidOperationException

            if (this.IsAruinoConnected() == false)
            {
                this._measurementBackgroundWorker.ReportProgress(Settings.Default.WrongCOMProgressPercentage);
                return;
            }

            this.InitHardware();

            this.SerialPortViewModel.MoveToAbsolutePosition(absoluteX, absoluteY);
            Thread.Sleep(Settings.Default.LongThreadDelay);

            while (this.SerialPortViewModel.IsRunning)
            {
                Thread.Sleep(Settings.Default.ThreadDelay);
            }
        }

        private void GoToPositionBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == Settings.Default.WrongCOMProgressPercentage)
            {
                this._dialogService.ShowMessage(Resources.Message_Title_Attention, Resources.Message_NoArduinoConnectedOnThisPort);
            }
        }

        private void GoToPositionBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this.SerialPortViewModel.IsOpen)
            {
                this.SerialPortViewModel.OpenCloseConnectionCommand.Execute(null);
            }
        }

        private void SetupTestReceiverForMeasurement()
        {
            if (this.testReceiver.IsConnected == false)
            {
                this.testReceiver.Connect();
            }

            this.testReceiver.SetDCCoupling();
            this.testReceiver.DeactivateCisprRbwCoupling();

            // ReSharper disable PossibleInvalidOperationException
            this.testReceiver.SetStartStopFrequency((double)this.ParameterViewModel.FreqStart, (double)this.ParameterViewModel.FreqEnd);

            if (this.ParameterViewModel.DoQuasipeakMeasurement)
            {
                this.testReceiver.SetDetectorModes();
                ResBW tempResBW;
                Enum.TryParse(this.testReceiver.GetResolutionBandwidth().ToString(CultureInfo.InvariantCulture), out tempResBW);
                this.ParameterViewModel.ResBW = tempResBW;
            }
            else
            {
                this.testReceiver.SetDetectorModesWithoutQuasipeak();
                this.testReceiver.SetResolutionBandwidth((double)this.ParameterViewModel.ResBW);

                if (this.testReceiver.GetResolutionBandwidth().Equals((double)this.ParameterViewModel.ResBW) == false)
                {
                    throw new Exception(Resources.Exception_Message_CouldNotSetBandwidth);
                }
            }

            this.testReceiver.SetSweepTime((int)this.ParameterViewModel.MeasureTime);
            this.testReceiver.SetAmplitudeUnit(AmplitudeUnits.dBmV);

            // ReSharper restore PossibleInvalidOperationException
        }

        private bool IsAruinoConnected()
        {
            Thread.Sleep(Settings.Default.LongThreadDelay);

            if (this.SerialPortViewModel.IsConnected)
            {
                return true;
            }

            return false;
        }

        private void InitHardware()
        {
            // Delays are necessary to read the received status changes via USB
            Thread.Sleep(Settings.Default.ThreadDelay);

            if (this.SerialPortViewModel.IsInitialized == false)
            {
                this.SerialPortViewModel.GoHomeCommand.Execute(null);
                Thread.Sleep(Settings.Default.ThreadDelay);
            }

            // wait till the sled moved to home position
            while (this.SerialPortViewModel.IsIdle == false)
            {
                Thread.Sleep(Settings.Default.ThreadDelay);
            }
        }

        private void MoveToMeasurementPosition(int xIndex, int yIndex)
        {
            // ReSharper disable PossibleInvalidOperationException
            this.SerialPortViewModel.MoveToAbsolutePosition((double)this.ParameterViewModel.AbsoluteXStart + xIndex * (double)this.ParameterViewModel.DeltaX,
                                                            (double)this.ParameterViewModel.AbsoluteYStart + yIndex * (double)this.ParameterViewModel.DeltaY);

            // ReSharper restore PossibleInvalidOperationException
            Thread.Sleep(Settings.Default.LongThreadDelay);

            while (this.SerialPortViewModel.IsRunning)
            {
                Thread.Sleep(Settings.Default.ThreadDelay);
            }
        }

        private void ExecuteMeasurement(int xIndex, int yIndex)
        {
            this.testReceiver.StartSingleRun();

            double[] maxDetectorIntensities = null;
            double[] avgDetectorIntensities = null;
            double[] qpDetectorIntensities = null;

            while (maxDetectorIntensities == null || avgDetectorIntensities == null)
            {
                if (this._measurementBackgroundWorker.CancellationPending)
                {
                    return;
                }

                try
                {
                    maxDetectorIntensities = this.testReceiver.GetMaxMeasurementValues();
                    avgDetectorIntensities = this.testReceiver.GetAVGMeasurementValues();

                    if (this.ParameterViewModel.DoQuasipeakMeasurement)
                    {
                        qpDetectorIntensities = this.testReceiver.GetQuasipeakMeasurementValues();
                    }
                }
                catch
                {
                    // do nothing
                }
            }

            int numberOfFrequencies = maxDetectorIntensities.Length;
            MeasurementValue[] measurementValues = new MeasurementValue[numberOfFrequencies];

            // ReSharper disable PossibleInvalidOperationException
            Measurement measurement = new Measurement
                                      {
                                              Analysis = this.analysis,
                                              X = (double)this.ParameterViewModel.RelativeXStart + xIndex * (double)this.ParameterViewModel.DeltaX,
                                              Y = (double)this.ParameterViewModel.RelativeYStart + yIndex * (double)this.ParameterViewModel.DeltaY
                                      };

            // ReSharper restore PossibleInvalidOperationException

            // save measurement before adding measurementvalues (by save the measurement gets an id)
            this.repository.SaveOrUpdate(measurement);

            // ReSharper disable PossibleInvalidOperationException
            for (int i = 0; i < numberOfFrequencies; i++)
            {
                measurementValues[i] = new MeasurementValue
                                       {
                                               Measurement = measurement,
                                               Frequency = (double)this.analysis.Parameter.FreqStart + i * (double)this.analysis.Parameter.DeltaFreq,
                                               IntensityMaxDetector = maxDetectorIntensities[i],
                                               IntensityAVGDetector = avgDetectorIntensities[i],
                                               IntensityQPDetector = qpDetectorIntensities == null ? 0 : qpDetectorIntensities[i]
                                       };
            }

            // ReSharper restore PossibleInvalidOperationException
            this.repository.BulkInsert(Settings.Default.DatabaseFilePath, measurementValues);

            // Check if dbm values exceed allowed max value
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (maxDetectorIntensities != null && maxDetectorIntensities.Max() > Settings.Default.MaxDbm ||
                avgDetectorIntensities != null && avgDetectorIntensities.Max() > Settings.Default.MaxDbm ||
                qpDetectorIntensities != null && qpDetectorIntensities.Max() > Settings.Default.MaxDbm)
            {
                // cancel measurement
                this._goToPositionBackgroundWorker.ReportProgress(Settings.Default.ExceedMaxDbmProgressPercentage);
                this.StopMeasurementBackgroundWorker();
            }
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        private void SetParameterIfAlreadyExists()
        {
            Conjunction conjunction = Restrictions.Conjunction();

            conjunction.Add(Restrictions.Where<Parameter>(p => p.DoQuasipeakMeasurement == this.ParameterViewModel.DoQuasipeakMeasurement));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.OriginX == this.ParameterViewModel.OriginX));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.OriginY == this.ParameterViewModel.OriginY));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.XStart == this.ParameterViewModel.RelativeXStart));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.DeltaX == this.ParameterViewModel.DeltaX));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.XEnd == this.ParameterViewModel.RelativeXEnd));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.YStart == this.ParameterViewModel.RelativeYStart));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.DeltaY == this.ParameterViewModel.DeltaY));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.YEnd == this.ParameterViewModel.RelativeYEnd));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.FreqStart == this.ParameterViewModel.FreqStart));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.DeltaFreq == this.ParameterViewModel.DeltaFreq));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.FreqEnd == this.ParameterViewModel.FreqEnd));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.ResBW == this.ParameterViewModel.ResBW));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.MeasureTime == this.ParameterViewModel.MeasureTime));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.SensorName == this.ParameterViewModel.SensorName));
            conjunction.Add(Restrictions.Where<Parameter>(p => p.SensorType == this.ParameterViewModel.SensorType));

            Parameter parameter = this.repository.GetByCriterion<Parameter>(conjunction);

            if (parameter != null)
            {
                this.analysis.Parameter = parameter;
            }
        }

        private void UpdateViewModel()
        {
            this.UpdateCommands();
            this.UpdateProperties();

            if (this.analysis != null)
            {
                this.AnalysisViewModel.CanEdit = this.CanEdit || this.CurrentState.GetType() == typeof(ScanEditState);
                this.ParameterViewModel.CanEdit = this.CanEdit;
            }
        }

        private void UpdateCommands()
        {
            foreach (ImageCommandViewModel command in this.CRUDCommands)
            {
                command.RelayCommand.RaiseCanExecuteChanged();
            }

            foreach (ImageCommandViewModel command in this.TestExecutionCommands)
            {
                command.RelayCommand.RaiseCanExecuteChanged();
            }
        }

        private void UpdateProperties()
        {
            foreach (PropertyInfo propertyInfo in this.PropertyInfos)
            {
                this.RaisePropertyChanged(propertyInfo.Name);
            }
        }

        private void SendMainMenuState()
        {
            bool enableState = !(this.CurrentState.GetType() == typeof(ScanCreationState) ||
                                 this.CurrentState.GetType() == typeof(ScanRunState) ||
                                 this.CurrentState.GetType() == typeof(ScanPauseState));

            Messenger.Default.Send(new NotificationMessage<bool>(enableState, Resources.Messenger_ChangeWorkspaceEnableStateForMainVM));
        }

        private void ExecuteNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Resources.Messenger_ClearLoadedAnalysisForScanVM)
            {
                this.LoadAnalysis(loadSpectrumViewModel: false);
            }
            else if (message.Notification == Resources.Message_UpdateGoToPositionCommandForScanVM)
            {
                this.GoToGridPositionCommand.RaiseCanExecuteChanged();
            }
        }

        private void ExecuteNotificationMessage(NotificationMessage<int> message)
        {
            if (message.Notification == Resources.Messenger_LoadAnalysisForScanVM)
            {
                this.LoadAnalysis(message.Content);
                this.ChangeToLoadedState();
            }
        }

        private void ExecuteNotificationMessage(NotificationMessage<Parameter> message)
        {
            if (message.Notification == Resources.Messenger_CreateNewAnalysisWithLoadedParameterForScanVM)
            {
                Analysis newAnalysis = new Analysis { Parameter = message.Content };

                this.LoadAnalysis(newAnalysis, false);
                this.ChangeToCreationState();
            }
        }
    }
}