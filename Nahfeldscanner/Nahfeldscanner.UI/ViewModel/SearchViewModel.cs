﻿// ///////////////////////////////////
// File: SearchViewModel.cs
// Last Change: 01.02.2018  14:10
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.Linq;
    using System.Linq.Expressions;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using NHibernate.Criterion;



    public class SearchViewModel : WorkspaceViewModel, IDisposable
    {
        #region Fields

        private readonly IRepository repository;
        private readonly IDialogService dialogService;

        private ObservableCollection<AnalysisViewModel> _foundAnalysis;
        private AnalysisViewModel _searchAnalysisViewModel;
        private AnalysisViewModel _selectedAnalysisViewModel;

        private RelayCommand _searchAnalysisCommand;
        private RelayCommand _clearFieldsCommand;
        private RelayCommand _sendLoadSelectedAnalysisMessageCommand;

        #endregion



        #region Constructors / Destructor

        public SearchViewModel(IRepository repository, IDialogService dialogService, string title, Bitmap bitmap)
                : base(title, bitmap)
        {
            this.repository = repository;
            this.dialogService = dialogService;
            this.ClearParameterValues();

            Messenger.Default.Register<NotificationMessage>(this, this.ExecuteNotificationMessage);
            Messenger.Default.Register<NotificationMessage<int>>(this, this.ExecuteNotificationMessage);
        }

        #endregion



        #region Properties

        public ObservableCollection<AnalysisViewModel> FoundAnalyses
        {
            get
            {
                if (this._foundAnalysis == null)
                {
                    this._foundAnalysis = new ObservableCollection<AnalysisViewModel>();
                }

                return this._foundAnalysis;
            }
        }

        public AnalysisViewModel SearchAnalysisViewModel
        {
            get
            {
                if (this._searchAnalysisViewModel == null)
                {
                    this._searchAnalysisViewModel = new AnalysisViewModel(this.repository, new Analysis());
                }

                return this._searchAnalysisViewModel;
            }
            private set { this.SetProperty(ref this._searchAnalysisViewModel, value); }
        }

        public AnalysisViewModel SelectedAnalysisViewModel
        {
            get { return this._selectedAnalysisViewModel; }
            set { this.SetProperty(ref this._selectedAnalysisViewModel, value); }
        }

        public RelayCommand SearchAnalysisCommand
        {
            get
            {
                if (this._searchAnalysisCommand == null)
                {
                    this._searchAnalysisCommand = new RelayCommand(this.LoadSearchedAnalysis);
                }

                return this._searchAnalysisCommand;
            }
        }

        public RelayCommand ClearFieldsCommand
        {
            get
            {
                if (this._clearFieldsCommand == null)
                {
                    this._clearFieldsCommand = new RelayCommand(this.ClearFields);
                }

                return this._clearFieldsCommand;
            }
        }

        public RelayCommand SendLoadSelectedAnalysisMessageCommand
        {
            get
            {
                if (this._sendLoadSelectedAnalysisMessageCommand == null)
                {
                    this._sendLoadSelectedAnalysisMessageCommand = new RelayCommand(this.SendLoadSelectedAnalysisMessage);
                }

                return this._sendLoadSelectedAnalysisMessageCommand;
            }
        }

        #endregion



        #region IDisposable Members

        public void Dispose()
        {
            Messenger.Default.Unregister(this);
        }

        #endregion



        private void ClearFields()
        {
            this.SearchAnalysisViewModel = new AnalysisViewModel(this.repository, new Analysis());
            this.ClearParameterValues();
        }

        private void ClearParameterValues()
        {
            this.SearchAnalysisViewModel.ParameterViewModel.Clear();
        }

        private void LoadSearchedAnalysis()
        {
            if (this.repository.IsConnected == false)
            {
                this.dialogService.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_NoDatabaseConnection);
                return;
            }

            Tuple<ICriterion, Expression<Func<Analysis, Parameter>>, ICriterion> tupleCriterion = this.GetSearchCriterion();

            this.FoundAnalyses.Clear();

            if (tupleCriterion.Item2 == null || tupleCriterion.Item3 == null)
            {
                foreach (Analysis analysis in this.repository.GetCollectionByCriterion<Analysis>(tupleCriterion.Item1, a => a.ID))
                {
                    this.FoundAnalyses.Insert(0, new AnalysisViewModel(this.repository, analysis));
                }
            }
            else
            {
                foreach (Analysis analysis in this.repository.GetCollectionByCriterion(tupleCriterion.Item1, tupleCriterion.Item2, tupleCriterion.Item3, a => a.ID))
                {
                    this.FoundAnalyses.Insert(0, new AnalysisViewModel(this.repository, analysis));
                }
            }
        }

        private Tuple<ICriterion, Expression<Func<Analysis, Parameter>>, ICriterion> GetSearchCriterion()
        {
            // analysis data
            Conjunction analysisConjunction = Restrictions.Conjunction();

            if (this.SearchAnalysisViewModel.ID != 0)
            {
                analysisConjunction.Add(Restrictions.Where<Analysis>(a => a.ID == this.SearchAnalysisViewModel.ID));
                return new Tuple<ICriterion, Expression<Func<Analysis, Parameter>>, ICriterion>(analysisConjunction, null, null);
            }

            if (!string.IsNullOrEmpty(this.SearchAnalysisViewModel.StartTime))
            {
                analysisConjunction.Add(Restrictions.Where<Analysis>(a => a.StartTime.IsLike(this.SearchAnalysisViewModel.StartTime, MatchMode.Anywhere)));
            }

            if (!string.IsNullOrEmpty(this.SearchAnalysisViewModel.EndTime))
            {
                analysisConjunction.Add(Restrictions.Where<Analysis>(a => a.EndTime.IsLike(this.SearchAnalysisViewModel.EndTime, MatchMode.Anywhere)));
            }

            if (!string.IsNullOrEmpty(this.SearchAnalysisViewModel.Description))
            {
                analysisConjunction.Add(Restrictions.Where<Analysis>(a => a.Description.IsLike(this.SearchAnalysisViewModel.Description, MatchMode.Anywhere)));
            }

            // parameter data
            Conjunction parameterConjunction = Restrictions.Conjunction();

            if (this.SearchAnalysisViewModel.ParameterViewModel.ID != 0)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.ID == this.SearchAnalysisViewModel.ParameterViewModel.ID));
                return new Tuple<ICriterion, Expression<Func<Analysis, Parameter>>, ICriterion>(analysisConjunction, a => a.Parameter, parameterConjunction);
            }

            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (this.SearchAnalysisViewModel.ParameterViewModel.OriginX != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.OriginX == this.SearchAnalysisViewModel.ParameterViewModel.OriginX));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.OriginY != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.OriginY == this.SearchAnalysisViewModel.ParameterViewModel.OriginY));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.RelativeXStart != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.XStart == this.SearchAnalysisViewModel.ParameterViewModel.RelativeXStart));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.DeltaX != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.DeltaX == this.SearchAnalysisViewModel.ParameterViewModel.DeltaX));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.RelativeXEnd != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.XEnd == this.SearchAnalysisViewModel.ParameterViewModel.RelativeXEnd));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.RelativeYStart != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.YStart == this.SearchAnalysisViewModel.ParameterViewModel.RelativeYStart));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.DeltaY != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.DeltaY == this.SearchAnalysisViewModel.ParameterViewModel.DeltaY));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.RelativeYEnd != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.YEnd == this.SearchAnalysisViewModel.ParameterViewModel.RelativeYEnd));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.FreqStart != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.FreqStart == this.SearchAnalysisViewModel.ParameterViewModel.FreqStart));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.FreqEnd != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.FreqEnd == this.SearchAnalysisViewModel.ParameterViewModel.FreqEnd));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.ResBW != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.ResBW == this.SearchAnalysisViewModel.ParameterViewModel.ResBW));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.MeasureTime != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.MeasureTime == this.SearchAnalysisViewModel.ParameterViewModel.MeasureTime));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.Orientation != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.Orientation == this.SearchAnalysisViewModel.ParameterViewModel.Orientation));
            }

            if (this.SearchAnalysisViewModel.ParameterViewModel.SensorName != null)
            {
                parameterConjunction.Add(Restrictions.Where<Parameter>(p => p.SensorName == this.SearchAnalysisViewModel.ParameterViewModel.SensorName));
            }

            return new Tuple<ICriterion, Expression<Func<Analysis, Parameter>>, ICriterion>(analysisConjunction, a => a.Parameter, parameterConjunction);

            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        private void SendLoadSelectedAnalysisMessage()
        {
            if (this.SelectedAnalysisViewModel != null)
            {
                Messenger.Default.Send(new NotificationMessage<int>(this.SelectedAnalysisViewModel.ID, Resources.Messenger_LoadAnalysisForScanVM));
                Messenger.Default.Send(new NotificationMessage(Resources.Messenger_LoadScanVMForMainVM));
            }
        }

        private void ExecuteNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Resources.Messenger_ClearFoundAnalysesForSearchVM)
            {
                this.FoundAnalyses.Clear();
            }
        }

        private void ExecuteNotificationMessage(NotificationMessage<int> message)
        {
            if (message.Notification == Resources.Messenger_RemoveDeletedAnalysisFromSearchListForSearchVM)
            {
                if (this.FoundAnalyses.Any(x => x.ID == message.Content))
                {
                    this.FoundAnalyses.Remove(this.FoundAnalyses.First(x => x.ID == message.Content));
                }
            }
        }
    }
}