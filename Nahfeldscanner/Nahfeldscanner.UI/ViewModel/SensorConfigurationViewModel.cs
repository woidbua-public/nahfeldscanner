﻿// ///////////////////////////////////
// File: SensorConfigurationViewModel.cs
// Last Change: 19.02.2018  12:45
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.Linq;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Markup;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;
    using NHibernate.Criterion;



    public class SensorConfigurationViewModel : BindableViewModelBase, IDisposable
    {
        #region Fields

        private readonly IRepository repository;
        private readonly IDialogService dialogService;

        private SensorViewModel _selectedSensorViewModel;
        private SensorViewModel _newSensorViewModel;

        private RelayCommand _addSensorCommand;

        private RelayCommand _loadImageCommand;

        #endregion



        #region Constructors / Destructor

        public SensorConfigurationViewModel(IRepository repository, IDialogService dialogService)
        {
            this.repository = repository;
            this.dialogService = dialogService;

            this.SensorViewModels = new ObservableCollection<SensorViewModel>();
            this._newSensorViewModel = new SensorViewModel(new Sensor());

            Messenger.Default.Register<NotificationMessage>(this, this.ExecuteNotificationMessage);
            Messenger.Default.Register<NotificationMessage<int>>(this, this.ExecuteNotificationMessage);
        }

        #endregion



        #region Properties

        public SensorViewModel SelectedSensorViewModel
        {
            get { return this._selectedSensorViewModel; }
            set { this.SetProperty(ref this._selectedSensorViewModel, value); }
        }

        public SensorViewModel NewSensorViewModel
        {
            get { return this._newSensorViewModel; }
            set { this.SetProperty(ref this._newSensorViewModel, value); }
        }

        public ObservableCollection<SensorViewModel> SensorViewModels { get; set; }

        public RelayCommand LoadImageCommand
        {
            get
            {
                if (this._loadImageCommand == null)
                {
                    this._loadImageCommand = new RelayCommand(this.LoadImage);
                }

                return this._loadImageCommand;
            }
        }

        public RelayCommand AddSensorCommand
        {
            get
            {
                if (this._addSensorCommand == null)
                {
                    this._addSensorCommand = new RelayCommand(this.AddSensor);
                }

                return this._addSensorCommand;
            }
        }

        #endregion



        #region IDisposable Members

        public void Dispose()
        {
            Messenger.Default.Unregister(this);
        }

        #endregion



        private void LoadImage()
        {
            string imagePath = this.dialogService.ShowImageFileDialog();

            if (string.IsNullOrEmpty(imagePath))
            {
                return;
            }

            ImageConverter imageConverter = new ImageConverter();

            try
            {
                this.NewSensorViewModel.SensorImageStream = (byte[])imageConverter.ConvertTo(new Bitmap(imagePath), typeof(byte[]));
            }
            catch (Exception e)
            {
                this.dialogService.ShowMessage(Resources.Message_Title_Attention, e.Message);
            }
        }

        private void LoadSensors()
        {
            if (this.repository.IsConnected)
            {
                this.SensorViewModels.Clear();

                foreach (Sensor sensor in this.repository.GetAll<Sensor>())
                {
                    this.SensorViewModels.InsertOrderedBy(new SensorViewModel(sensor), x => x.SensorName);
                }
            }
        }

        private void AddSensor()
        {
            if (this.repository.IsConnected == false)
            {
                this.dialogService.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_NoDatabaseConnection);
                return;
            }

            if (string.IsNullOrEmpty(this.NewSensorViewModel.SensorName) || string.IsNullOrWhiteSpace(this.NewSensorViewModel.SensorName))
            {
                this.dialogService.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_SensorNameRequired);
                return;
            }

            if (this.IsDuplicateSensor())
            {
                this.dialogService.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_SensorExistsAlready);
                return;
            }

            if (this.NewSensorViewModel.SaveOrUpdateToDatabase(this.repository))
            {
                this.SensorViewModels.InsertOrderedBy(new SensorViewModel(this.repository.GetById<Sensor>(this.NewSensorViewModel.ID)), x => x.SensorName);
                this.NewSensorViewModel = new SensorViewModel(new Sensor { SensorType = SensorType.E_Field });
            }
            else
            {
                this.dialogService.ShowMessage(Resources.Message_Title_Attention, Resources.Exception_Message_CouldNotSaveSensorToDatabase);
            }
        }

        private bool IsDuplicateSensor()
        {
            try
            {
                Sensor sensor = this.repository.GetByCriterion<Sensor>(new Conjunction().Add(Restrictions.Where<Sensor>(x => x.SensorName == this.NewSensorViewModel.SensorName)));
                return sensor != null;
            }
            catch (Exception e)
            {
                this.dialogService.ShowMessage(Resources.Message_Title_Attention, e.Message);
                return true;
            }
        }

        private async Task RemoveSensor(int id)
        {
            string exceptionMessage = null;

            try
            {
                Sensor sensor = this.repository.GetById<Sensor>(id);

                if (sensor != null)
                {
                    if (await this.ShouldRemoveSensor())
                    {
                        this.SensorViewModels.Remove(this.SensorViewModels.FirstOrDefault(x => x.ID == sensor.ID));
                        this.repository.Delete(sensor);
                    }
                }
            }
            catch (Exception e)
            {
                exceptionMessage = e.Message;
            }

            if (exceptionMessage != null)
            {
                await this.dialogService.ShowMessage(Resources.Message_Title_Attention, exceptionMessage);
            }
        }

        private async Task<bool> ShouldRemoveSensor()
        {
            if (await this.dialogService.ShowDialogYesNo(Resources.Message_Title_Attention, Resources.Message_ShouldDeleteAnalysis))
            {
                return true;
            }

            return false;
        }

        private void ExecuteNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Resources.Messenger_LoadSensorsForSensorConfigurationVM)
            {
                this.LoadSensors();
            }
        }

        private async void ExecuteNotificationMessage(NotificationMessage<int> message)
        {
            if (message.Notification == Resources.Messenger_RemoveSensorRequestForSensorConfiguratonVM)
            {
                await this.RemoveSensor(message.Content);
            }
        }
    }
}