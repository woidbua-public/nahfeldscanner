﻿// ///////////////////////////////////
// File: SensorViewModel.cs
// Last Change: 19.02.2018  11:12
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Properties;



    public class SensorViewModel : BindableViewModelBase
    {
        #region Fields

        private readonly Sensor sensor;
        private RelayCommand _sendDeleteRequestCommand;

        #endregion



        #region Constructors / Destructor

        public SensorViewModel(Sensor sensor)
        {
            this.sensor = sensor;
        }

        #endregion



        #region Properties

        public int ID
        {
            get { return this.sensor.ID; }
        }

        public byte[] SensorImageStream
        {
            get { return this.sensor.SensorImageStream; }
            set { this.SetProperty(() => this.sensor.SensorImageStream = value, () => this.sensor.SensorImageStream != null && this.sensor.SensorImageStream.Equals(value)); }
        }

        public SensorType? SensorType
        {
            get { return this.sensor.SensorType; }
            set { this.SetProperty(() => this.sensor.SensorType = value, () => this.sensor.SensorType == value); }
        }

        public string SensorName
        {
            get { return this.sensor.SensorName; }
            set { this.SetProperty(() => this.sensor.SensorName = value, () => this.sensor.SensorName == value); }
        }

        public double? MinFrequency
        {
            get { return this.sensor.MinFrequency; }
            set { this.SetProperty(() => this.sensor.MinFrequency = value, () => this.sensor.MinFrequency.Equals(value)); }
        }

        public double? MaxFrequency
        {
            get { return this.sensor.MaxFrequency; }
            set { this.SetProperty(() => this.sensor.MaxFrequency = value, () => this.sensor.MaxFrequency.Equals(value)); }
        }

        public RelayCommand SendDeleteRequestCommand
        {
            get
            {
                if (this._sendDeleteRequestCommand == null)
                {
                    this._sendDeleteRequestCommand = new RelayCommand(this.SendDeleteRequest);
                }

                return this._sendDeleteRequestCommand;
            }
        }

        #endregion



        public bool SaveOrUpdateToDatabase(IRepository repository)
        {
            try
            {
                repository.SaveOrUpdate(this.sensor);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void SendDeleteRequest()
        {
            Messenger.Default.Send(new NotificationMessage<int>(this.ID, Resources.Messenger_RemoveSensorRequestForSensorConfiguratonVM));
        }
    }
}