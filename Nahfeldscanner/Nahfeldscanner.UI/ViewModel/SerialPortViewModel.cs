﻿// ///////////////////////////////////
// File: SerialPortViewModel.cs
// Last Change: 15.01.2018  13:57
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Globalization;
    using System.IO.Ports;
    using System.Threading;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Properties;
    using Nahfeldscanner.UI.Service;



    public class SerialPortViewModel : BindableViewModelBase, IDisposable
    {
        #region Fields

        private readonly IDialogService _dialogService;
        private readonly SerialPort _serialPort;

        private string _serialBuffer;
        private ObservableCollection<string> _portNames;

        private MotorState _motorState;
        private double _originX;
        private double _originY;
        private double? _absoluteCurrentX;
        private double? _absoluteCurrentY;
        private double? _relativeCurrentX;
        private double? _relativeCurrentY;
        private double _absoluteTargetX;
        private double _absoluteTargetY;
        private double _goToX;
        private double _goToY;

        private BackgroundWorker _statusBackgroundWorker;

        private RelayCommand _updatePortNamesCommand;
        private RelayCommand _openCloseConnectionCommand;
        private RelayCommand _goHomeCommand;
        private RelayCommand _goToCommand;
        private RelayCommand _goOriginCommand;
        private RelayCommand _moveUpCommand;
        private RelayCommand _moveRightCommand;
        private RelayCommand _moveDownCommand;
        private RelayCommand _moveLeftCommand;
        private RelayCommand _setOriginCommand;

        private bool _isDisposed;

        #endregion



        #region Constructors / Destructor

        public SerialPortViewModel(IDialogService dialogService)
        {
            this._dialogService = dialogService;
            this._serialPort = new SerialPort { BaudRate = Settings.Default.BaudRate };

            this.InitErrorMessages();
            this.InitBackgroundWorker();

            this._serialPort.DataReceived += this.OnSerialDataReceived;
            Messenger.Default.Register<NotificationMessage<Tuple<double?, double?>>>(this, this.ExecuteNotificationMessage);
        }

        ~SerialPortViewModel()
        {
            this.Dispose(false);
        }

        #endregion



        #region Properties

        public bool IsOpen
        {
            get { return this._serialPort.IsOpen; }
        }

        public MotorState MotorState
        {
            get { return this._motorState; }
            private set
            {
                if (this._motorState == MotorState.Disconnected && value == MotorState.Idle &&
                    this.AbsoluteCurrentX != null && this.AbsoluteCurrentY != null)
                {
                    this.AbsoluteTargetX = (double)this.AbsoluteCurrentX;
                    this.AbsoluteTargetY = (double)this.AbsoluteCurrentY;
                }

                if (this.SetProperty(ref this._motorState, value))
                {
                    this.RaisePropertyChanged(() => this.IsOpen);
                    this.RaisePropertyChanged(() => this.IsConnected);
                    this.RaisePropertyChanged(() => this.IsInitialized);
                    this.RaisePropertyChanged(() => this.IsIdle);
                    this.RaisePropertyChanged(() => this.IsRunning);
                }
            }
        }

        public bool IsConnected
        {
            get { return this.MotorState != MotorState.Disconnected; }
        }

        public bool IsInitialized
        {
            get { return this.MotorState != MotorState.Alarm && this.MotorState != MotorState.Disconnected; }
        }

        public bool IsIdle
        {
            get { return this.MotorState == MotorState.Idle; }
        }

        public bool IsRunning
        {
            get { return this.MotorState == MotorState.Run; }
        }

        public virtual double OriginX
        {
            get { return this._originX; }
            set
            {
                bool changed;
                double roundedValue = this.GetOneDigitRoundedValue(value);
                double deltaX = this.OriginX - roundedValue;

                if (roundedValue < Settings.Default.XMin)
                {
                    changed = this.SetProperty(ref this._originX, Settings.Default.XMin);
                }
                else if (roundedValue > Settings.Default.XMax)
                {
                    changed = this.SetProperty(ref this._originX, Settings.Default.XMax);
                }
                else
                {
                    changed = this.SetProperty(ref this._originX, roundedValue);
                }

                if (changed)
                {
                    this.GoToX = this.GoToX + deltaX;
                }
            }
        }

        public virtual double OriginY
        {
            get { return this._originY; }
            set
            {
                bool changed;
                double roundedValue = this.GetOneDigitRoundedValue(value);
                double deltaY = this.OriginY - roundedValue;

                if (roundedValue < Settings.Default.YMin)
                {
                    changed = this.SetProperty(ref this._originY, Settings.Default.YMin);
                }
                else if (roundedValue > Settings.Default.YMax)
                {
                    changed = this.SetProperty(ref this._originY, Settings.Default.YMax);
                }
                else
                {
                    changed = this.SetProperty(ref this._originY, roundedValue);
                }

                if (changed)
                {
                    this.GoToY = this.GoToY + deltaY;
                }
            }
        }

        public virtual double? AbsoluteCurrentX
        {
            get { return this._absoluteCurrentX; }
            private set { this.SetProperty(ref this._absoluteCurrentX, value); }
        }

        public virtual double? AbsoluteCurrentY
        {
            get { return this._absoluteCurrentY; }
            private set { this.SetProperty(ref this._absoluteCurrentY, value); }
        }

        public virtual double? RelativeCurrentX
        {
            get { return this._relativeCurrentX; }
            private set { this.SetProperty(ref this._relativeCurrentX, value); }
        }

        public virtual double? RelativeCurrentY
        {
            get { return this._relativeCurrentY; }
            private set { this.SetProperty(ref this._relativeCurrentY, value); }
        }

        public double GoToX
        {
            get { return this._goToX; }
            set
            {
                if (this.OriginX + value < Settings.Default.XMin)
                {
                    this.SetProperty(ref this._goToX, Settings.Default.XMin - this.OriginX);
                }
                else if (this.OriginX + value > Settings.Default.XMax)
                {
                    this.SetProperty(ref this._goToX, Settings.Default.XMax - this.OriginX);
                }
                else
                {
                    this.SetProperty(ref this._goToX, value);
                }
            }
        }

        public double GoToY
        {
            get { return this._goToY; }
            set
            {
                if (this.OriginY + value < Settings.Default.XMin)
                {
                    this.SetProperty(ref this._goToY, Settings.Default.YMin - this.OriginY);
                }
                else if (this.OriginY + value > Settings.Default.YMax)
                {
                    this.SetProperty(ref this._goToY, Settings.Default.YMax - this.OriginY);
                }
                else
                {
                    this.SetProperty(ref this._goToY, value);
                }
            }
        }

        public double AbsoluteTargetX
        {
            get { return this._absoluteTargetX; }
            set
            {
                if (value < Settings.Default.XMin)
                {
                    this.SetProperty(ref this._absoluteTargetX, Settings.Default.XMin);
                }
                else if (value > Settings.Default.XMax)
                {
                    this.SetProperty(ref this._absoluteTargetX, Settings.Default.XMax);
                }
                else
                {
                    this.SetProperty(ref this._absoluteTargetX, value);
                }
            }
        }

        public double AbsoluteTargetY
        {
            get { return this._absoluteTargetY; }
            set
            {
                if (value < Settings.Default.YMin)
                {
                    this.SetProperty(ref this._absoluteTargetY, Settings.Default.YMin);
                }
                else if (value > Settings.Default.YMax)
                {
                    this.SetProperty(ref this._absoluteTargetY, Settings.Default.YMax);
                }
                else
                {
                    this.SetProperty(ref this._absoluteTargetY, value);
                }
            }
        }

        public double StepSize
        {
            get { return Settings.Default.StepSize; }
            set
            {
                if (this.SetProperty(() => Settings.Default.StepSize = value, () => Math.Abs(Settings.Default.StepSize - value) < Settings.Default.DoubleTolerance))
                {
                    Settings.Default.Save();
                }
            }
        }

        public string SelectedPortName
        {
            get { return this._serialPort.PortName; }
            set
            {
                if (string.IsNullOrEmpty(value) || this.PortNames.Contains(value) == false)
                {
                    return;
                }

                this.SetProperty(() => this._serialPort.PortName = value, () => this._serialPort.PortName == value);
            }
        }

        public ObservableCollection<string> PortNames
        {
            get
            {
                if (this._portNames == null)
                {
                    this._portNames = new ObservableCollection<string>();
                }

                return this._portNames;
            }
        }

        public int BaudRate
        {
            get { return this._serialPort.BaudRate; }
        }

        public RelayCommand UpdatePortNamesCommand
        {
            get
            {
                if (this._updatePortNamesCommand == null)
                {
                    this._updatePortNamesCommand = new RelayCommand(this.UpdatePortNames);
                }

                return this._updatePortNamesCommand;
            }
        }

        public RelayCommand OpenCloseConnectionCommand
        {
            get
            {
                if (this._openCloseConnectionCommand == null)
                {
                    this._openCloseConnectionCommand = new RelayCommand(this.OpenCloseConnection);
                }

                return this._openCloseConnectionCommand;
            }
        }

        public RelayCommand GoHomeCommand
        {
            get
            {
                if (this._goHomeCommand == null)
                {
                    this._goHomeCommand = new RelayCommand(this.GoHome);
                }

                return this._goHomeCommand;
            }
        }

        public RelayCommand GoToCommand
        {
            get
            {
                if (this._goToCommand == null)
                {
                    this._goToCommand = new RelayCommand(this.GoTo);
                }

                return this._goToCommand;
            }
        }

        public RelayCommand GoOriginCommand
        {
            get
            {
                if (this._goOriginCommand == null)
                {
                    this._goOriginCommand = new RelayCommand(this.GoOrigin);
                }

                return this._goOriginCommand;
            }
        }

        public RelayCommand MoveUpCommand
        {
            get
            {
                if (this._moveUpCommand == null)
                {
                    this._moveUpCommand = new RelayCommand(this.MoveUp);
                }

                return this._moveUpCommand;
            }
        }

        public RelayCommand MoveRightCommand
        {
            get
            {
                if (this._moveRightCommand == null)
                {
                    this._moveRightCommand = new RelayCommand(this.MoveRight);
                }

                return this._moveRightCommand;
            }
        }

        public RelayCommand MoveDownCommand
        {
            get
            {
                if (this._moveDownCommand == null)
                {
                    this._moveDownCommand = new RelayCommand(this.MoveDown);
                }

                return this._moveDownCommand;
            }
        }

        public RelayCommand MoveLeftCommand
        {
            get
            {
                if (this._moveLeftCommand == null)
                {
                    this._moveLeftCommand = new RelayCommand(this.MoveLeft);
                }

                return this._moveLeftCommand;
            }
        }

        public RelayCommand SetOriginToCurrentPositionCommand
        {
            get
            {
                if (this._setOriginCommand == null)
                {
                    this._setOriginCommand = new RelayCommand(this.SetOriginToCurrentPosition);
                }

                return this._setOriginCommand;
            }
        }

        public bool IsDisposed
        {
            get { return this._isDisposed; }
            private set { this._isDisposed = value; }
        }

        private int ErrorCode { get; set; }

        private Dictionary<int, string> ErrorMessages { get; set; }

        #endregion



        #region IDisposable Members

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion



        public void MoveToAbsolutePosition(double x, double y)
        {
            if (this.IsConnected)
            {
                this.AbsoluteTargetX = x;
                this.AbsoluteTargetY = y;
                this.GoTarget();
            }
        }

        private void Dispose(bool disposeManagedRessources)
        {
            if (this.IsDisposed)
            {
                return;
            }

            if (this.IsOpen)
            {
                this.OpenCloseConnection();
            }

            if (disposeManagedRessources)
            {
                this._serialPort.DataReceived -= this.OnSerialDataReceived;
                this._serialPort.Dispose();
            }

            this.IsDisposed = true;
        }

        private void InitErrorMessages()
        {
            this.ErrorMessages = new Dictionary<int, string>();
            this.ErrorMessages.Add(1, "G-code words consist of a letter and a value. Letter was not found.");
            this.ErrorMessages.Add(2, "Numeric value format is not valid or missing an expected value.");
            this.ErrorMessages.Add(3, "Grbl '$' system command was not recognized or supported.");
            this.ErrorMessages.Add(4, "Negative value received for an expected positive value.");
            this.ErrorMessages.Add(5, "Homing cycle is not enabled via settings.");
            this.ErrorMessages.Add(6, "Minimum step pulse time must be greater than 3usec");
            this.ErrorMessages.Add(7, "EEPROM read failed. Reset and restored to default values.");
            this.ErrorMessages.Add(8, "Grbl '$' command cannot be used unless Grbl is IDLE. Ensures smooth operation during a job.");
            this.ErrorMessages.Add(9, "G-code locked out during alarm or jog state.");
            this.ErrorMessages.Add(10, "Soft limits cannot be enabled without homing also enabled.");
            this.ErrorMessages.Add(11, "Max characters per line exceeded. Line was not processed and executed.");
            this.ErrorMessages.Add(12, "(Compile Option) Grbl '$' setting value exceeds the maximum step rate supported.");
            this.ErrorMessages.Add(13, "Safety door detected as opened and door state initiated.");
            this.ErrorMessages.Add(14, "(Grbl-Mega Only) Build info or startup line exceeded EEPROM line length limit.");
            this.ErrorMessages.Add(15, "Jog target exceeds machine travel. Command ignored.");
            this.ErrorMessages.Add(16, "Jog command with no '=' or contains prohibited g-code.");
            this.ErrorMessages.Add(17, "Laser mode requires PWM output.");
            this.ErrorMessages.Add(20, "Unsupported or invalid g-code command found in block.");
            this.ErrorMessages.Add(21, "More than one g-code command from same modal group found in block.");
            this.ErrorMessages.Add(22, "Feed rate has not yet been set or is undefined.");
            this.ErrorMessages.Add(23, "G-code command in block requires an integer value.");
            this.ErrorMessages.Add(24, "Two G-code commands that both require the use of the XYZ axis words were detected in the block.");
            this.ErrorMessages.Add(25, "A G-code word was repeated in the block.");
            this.ErrorMessages.Add(26, "A G-code command implicitly or explicitly requires XYZ axis words in the block, but none were detected.");
            this.ErrorMessages.Add(27, "N line number value is not within the valid range of 1 - 9,999,999.");
            this.ErrorMessages.Add(28, "A G-code command was sent, but is missing some required P or L value words in the line.");
            this.ErrorMessages.Add(29, "Grbl supports six work coordinate systems G54-G59. G59.1, G59.2, and G59.3 are not supported.");
            this.ErrorMessages.Add(30, "The G53 G-code command requires either a G0 seek or G1 feed motion mode to be active. A different motion was active.");
            this.ErrorMessages.Add(31, "There are unused axis words in the block and G80 motion mode cancel is active.");
            this.ErrorMessages.Add(32, "A G2 or G3 arc was commanded but there are no XYZ axis words in the selected plane to trace the arc.");
            this.ErrorMessages.Add(33, "The motion command has an invalid target. G2, G3, and G38.2 generates this error, if the arc is impossible to generate or if the probe target is the current position.");
            this.ErrorMessages.Add(34, "A G2 or G3 arc, traced with the radius definition, had a mathematical error when computing the arc geometry. Try either breaking up the arc into semi-circles or quadrants, or redefine them with the arc offset definition.");
            this.ErrorMessages.Add(35, "A G2 or G3 arc, traced with the offset definition, is missing the IJK offset word in the selected plane to trace the arc.");
            this.ErrorMessages.Add(36, "There are unused, leftover G-code words that aren't used by any command in the block.");
            this.ErrorMessages.Add(37, "The G43.1 dynamic tool length offset command cannot apply an offset to an axis other than its configured axis. The Grbl default axis is the Z-axis.");
        }

        private void InitBackgroundWorker()
        {
            this._statusBackgroundWorker = new BackgroundWorker();
            this._statusBackgroundWorker.WorkerSupportsCancellation = true;
            this._statusBackgroundWorker.WorkerReportsProgress = true;
            this._statusBackgroundWorker.DoWork += this.StatusBackgroundWorker_DoWork;
            this._statusBackgroundWorker.ProgressChanged += this.StatusBackgroundWorker_ProgressChanged;
            this._statusBackgroundWorker.RunWorkerCompleted += this.StatusBackgroundWorker_RunWorkerCompleted;
        }

        private void StatusBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int lastErrorCode = this.ErrorCode;

            while (!this._statusBackgroundWorker.CancellationPending)
            {
                Thread.Sleep(Settings.Default.ThreadDelay);
                this.SendStatusRequest();

                if (this.ErrorCode != lastErrorCode)
                {
                    lastErrorCode = this.ErrorCode;
                    this._statusBackgroundWorker.ReportProgress(0);
                }
            }

            Thread.Sleep(Settings.Default.ThreadDelay);
            e.Cancel = true;
        }

        private void StatusBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string errorMessage = "Error " + this.ErrorCode + ": " + this.ErrorMessages[this.ErrorCode];
            this._dialogService.ShowMessage(Resources.Exception_Message_SerialErrorState, errorMessage);
        }

        private void StatusBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                this._serialPort.Close();
                this.RaisePropertyChanged(() => this.IsOpen);
                this.MotorState = MotorState.Disconnected;
            }
            else if (e.Error != null)
            {
                try
                {
                    this._serialPort.Close();
                }
                catch
                {
                    // do nothing, must be called to change isOpen state
                }

                this.MotorState = MotorState.Disconnected;
                this.UpdatePortNames();
                this._dialogService.ShowMessage(Resources.Exception_Message_SerialConnectionLost, e.Error.Message);
            }
        }

        private void OpenCloseConnection()
        {
            if (this.IsOpen)
            {
                this._statusBackgroundWorker.CancelAsync();
            }
            else
            {
                try
                {
                    this._serialPort.Open();
                    this.UpdatePortNames();
                    this._statusBackgroundWorker.RunWorkerAsync();
                }
                catch (Exception e)
                {
                    if (this._statusBackgroundWorker.IsBusy)
                    {
                        this._statusBackgroundWorker.CancelAsync();
                    }

                    this._dialogService.ShowMessage(Resources.Exception_Message_CouldNotOpenSerialPortConnection, e.Message);
                }
            }

            this.RaisePropertyChanged(() => this.IsOpen);
        }

        private void UpdatePortNames()
        {
            this.PortNames.Clear();

            foreach (string portName in SerialPort.GetPortNames())
            {
                this.PortNames.Add(portName);
            }

            if (this.PortNames.Contains(this.SelectedPortName))
            {
                // selected port name remains when still in list after update
                this.RaisePropertyChanged(() => this.SelectedPortName);
            }
        }

        private void GoHome()
        {
            if (this.IsConnected)
            {
                this.SendCommandToHardware("$H");
                this.AbsoluteTargetX = Settings.Default.XMin;
                this.AbsoluteTargetY = Settings.Default.YMin;
            }
        }

        private void GoTo()
        {
            if (this.IsConnected)
            {
                this.AbsoluteTargetX = this.GoToX + this.OriginX;
                this.AbsoluteTargetY = this.GoToY + this.OriginY;
                this.GoTarget();
            }
        }

        private void GoOrigin()
        {
            if (this.IsConnected)
            {
                this.AbsoluteTargetX = this.OriginX;
                this.AbsoluteTargetY = this.OriginY;
                this.GoTarget();
            }
        }

        private void GoTarget()
        {
            this.SendCommandToHardware("G90 X" + this.AbsoluteTargetX.ToString("N1", CultureInfo.InvariantCulture) + " Y" + this.AbsoluteTargetY.ToString("N1", CultureInfo.InvariantCulture));
        }

        private void MoveUp()
        {
            if (this.IsConnected)
            {
                this.AbsoluteTargetY += this.StepSize;
                this.GoTarget();
            }
        }

        private void MoveRight()
        {
            if (this.IsConnected)
            {
                this.AbsoluteTargetX += this.StepSize;
                this.GoTarget();
            }
        }

        private void MoveDown()
        {
            if (this.IsConnected)
            {
                this.AbsoluteTargetY -= this.StepSize;
                this.GoTarget();
            }
        }

        private void MoveLeft()
        {
            if (this.IsConnected)
            {
                this.AbsoluteTargetX -= this.StepSize;
                this.GoTarget();
            }
        }

        private void SetOriginToCurrentPosition()
        {
            if (this.IsConnected && this.AbsoluteCurrentX != null && this.AbsoluteCurrentY != null)
            {
                this.OriginX = (double)this.AbsoluteCurrentX;
                this.OriginY = (double)this.AbsoluteCurrentY;
            }
        }

        private void SendStatusRequest()
        {
            this.SendCommandToHardware("?");
        }

        private void SendCommandToHardware(string commandCode)
        {
            this._serialPort.WriteLine(commandCode);
        }

        private void OnSerialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string dataBuffer = this._serialPort.ReadExisting();

            if (!dataBuffer.Contains("\r"))
            {
                this._serialBuffer += dataBuffer;
            }
            else
            {
                string subDataBuffer = dataBuffer.Substring(0, dataBuffer.IndexOf("\r", StringComparison.Ordinal));
                this._serialBuffer += subDataBuffer;

                try
                {
                    if (this._serialBuffer.StartsWith("<"))
                    {
                        string[] positions = this._serialBuffer.Substring(this._serialBuffer.IndexOf(':') + 1, this._serialBuffer.IndexOf('|', this._serialBuffer.IndexOf(':')) - this._serialBuffer.IndexOf(':') - 1).Split(',');
                        this.AbsoluteCurrentX = Convert.ToDouble(positions[0], CultureInfo.InvariantCulture);
                        this.AbsoluteCurrentY = Convert.ToDouble(positions[1], CultureInfo.InvariantCulture);
                        this.RelativeCurrentX = this.AbsoluteCurrentX - this.OriginX;
                        this.RelativeCurrentY = this.AbsoluteCurrentY - this.OriginY;

                        string motorState = this._serialBuffer.Substring(1, this._serialBuffer.IndexOf('|') - 1);
                        MotorState tempMotorState;
                        Enum.TryParse(motorState, out tempMotorState);
                        this.MotorState = tempMotorState;
                    }
                    else if (this._serialBuffer.StartsWith("error"))
                    {
                        string errorCode = this._serialBuffer.Substring(this._serialBuffer.IndexOf(':') + 1, this._serialBuffer.Length - (this._serialBuffer.IndexOf(':') + 1));
                        this.ErrorCode = Convert.ToInt32(errorCode);
                    }
                }
                catch (FormatException formatException)
                {
                    this._dialogService.ShowMessage(Resources.Exception_Message_SerialDataConversionFailed, formatException.Message);
                }
                catch (ArgumentOutOfRangeException outOfRangeException)
                {
                    this._dialogService.ShowMessage(Resources.Exception_Message_SerialDataConversionFailed, outOfRangeException.Message);
                }

                this._serialBuffer = string.Empty; // reset for next read
            }
        }

        private void ExecuteNotificationMessage(NotificationMessage<Tuple<double?, double?>> message)
        {
            if (message.Notification == Resources.Messenger_SetOriginForSerialPortVM)
            {
                if (message.Content.Item1 != null && message.Content.Item2 != null)
                {
                    this.OriginX = (double)message.Content.Item1;
                    this.OriginY = (double)message.Content.Item2;
                }
            }
        }

        private double GetOneDigitRoundedValue(double value)
        {
            return Math.Round(value, 1);
        }
    }
}