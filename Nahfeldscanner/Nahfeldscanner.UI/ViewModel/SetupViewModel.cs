﻿// ///////////////////////////////////
// File: SetupViewModel.cs
// Last Change: 20.02.2018  08:23
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Drawing;
    using Business;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Model.Database;
    using Properties;
    using ModelProperties = Model.Properties;



    public class SetupViewModel : WorkspaceViewModel, IDisposable
    {
        #region Fields

        private readonly IRepository repository;

        private SerialPortViewModel _serialPortViewModel;
        private Parameter parameter;
        private ParameterViewModel _parameterViewModel;

        private RelayCommand _setOriginToCurrentPositionCommand;
        private RelayCommand _setStartPositionToCurrentPositionCommand;
        private RelayCommand _setEndPositionToCurrentPositionCommand;
        private RelayCommand _sendParameterAndSwitchToScanViewModelCommand;
        private RelayCommand _setTopLeftCornerPositionCommand;
        private RelayCommand _setTopRightCornerPositionCommand;
        private RelayCommand _setBottomLeftCornerPositionCommand;
        private RelayCommand _setBottomRightCornerPositionCommand;

        #endregion



        #region Constructors / Destructor

        public SetupViewModel(IRepository repository, SerialPortViewModel serialPortViewModel, string title, Bitmap bitmap)
                : base(title, bitmap)
        {
            this.repository = repository;
            this.SerialPortViewModel = serialPortViewModel;

            this.CreateNewParameter();
            Messenger.Default.Register<NotificationMessage>(this, this.ExecuteNotificationMessage);
        }

        #endregion



        #region Properties

        public string TestReceiverIP
        {
            get { return ModelProperties.Settings.Default.TestReceiverIP; }
            set
            {
                this.SetProperty(() => ModelProperties.Settings.Default.TestReceiverIP = value,
                                 () => ModelProperties.Settings.Default.TestReceiverIP == value);

                ModelProperties.Settings.Default.Save();
            }
        }

        public SerialPortViewModel SerialPortViewModel
        {
            get { return this._serialPortViewModel; }
            private set { this.SetProperty(ref this._serialPortViewModel, value); }
        }

        public ParameterViewModel ParameterViewModel
        {
            get { return this._parameterViewModel; }
            private set { this.SetProperty(ref this._parameterViewModel, value); }
        }

        public RelayCommand SetOriginToCurrentPositionCommand
        {
            get
            {
                if (this._setOriginToCurrentPositionCommand == null)
                {
                    this._setOriginToCurrentPositionCommand = new RelayCommand(this.SetOriginToCurrentPosition);
                }

                return this._setOriginToCurrentPositionCommand;
            }
        }

        public RelayCommand SetStartPositionToCurrentPositionCommand
        {
            get
            {
                if (this._setStartPositionToCurrentPositionCommand == null)
                {
                    this._setStartPositionToCurrentPositionCommand = new RelayCommand(this.SetStartPositionToCurrentPosition);
                }

                return this._setStartPositionToCurrentPositionCommand;
            }
        }

        public RelayCommand SetEndPositionToCurrentPositionCommand
        {
            get
            {
                if (this._setEndPositionToCurrentPositionCommand == null)
                {
                    this._setEndPositionToCurrentPositionCommand = new RelayCommand(this.SetEndPositionToCurrentPosition);
                }

                return this._setEndPositionToCurrentPositionCommand;
            }
        }

        public RelayCommand SendParameterAndSwitchToScanViewModelCommand
        {
            get
            {
                if (this._sendParameterAndSwitchToScanViewModelCommand == null)
                {
                    this._sendParameterAndSwitchToScanViewModelCommand = new RelayCommand(this.SendParameterAndSwitchToScanViewModel);
                }

                return this._sendParameterAndSwitchToScanViewModelCommand;
            }
        }

        public RelayCommand SetTopLeftCornerPositionCommand
        {
            get
            {
                if (this._setTopLeftCornerPositionCommand == null)
                {
                    this._setTopLeftCornerPositionCommand = new RelayCommand(this.SetTopLeftCornerPosition);
                }

                return this._setTopLeftCornerPositionCommand;
            }
        }

        public RelayCommand SetTopRightCornerPositionCommand
        {
            get
            {
                if (this._setTopRightCornerPositionCommand == null)
                {
                    this._setTopRightCornerPositionCommand = new RelayCommand(this.SetTopRightCornerPosition);
                }

                return this._setTopRightCornerPositionCommand;
            }
        }

        public RelayCommand SetBottomLeftCornerPositionCommand
        {
            get
            {
                if (this._setBottomLeftCornerPositionCommand == null)
                {
                    this._setBottomLeftCornerPositionCommand = new RelayCommand(this.SetBottomLeftCornerPosition);
                }

                return this._setBottomLeftCornerPositionCommand;
            }
        }

        public RelayCommand SetBottomRightCornerPositionCommand
        {
            get
            {
                if (this._setBottomRightCornerPositionCommand == null)
                {
                    this._setBottomRightCornerPositionCommand = new RelayCommand(this.SetBottomRightCornerPosition);
                }

                return this._setBottomRightCornerPositionCommand;
            }
        }

        #endregion



        #region IDisposable Members

        public void Dispose()
        {
            Messenger.Default.Unregister(this);
        }

        #endregion



        public void SetSerialOriginToParameterOrigin()
        {
            if (this.ParameterViewModel.OriginX != null && this.ParameterViewModel.OriginY != null)
            {
                this.SerialPortViewModel.OriginX = (double)this.ParameterViewModel.OriginX;
                this.SerialPortViewModel.OriginY = (double)this.ParameterViewModel.OriginY;
            }
        }

        private void SetTopLeftCornerPosition()
        {
            this.SetGotoPosition(this.ParameterViewModel.RelativeXStart, this.ParameterViewModel.RelativeYEnd);
        }

        private void SetTopRightCornerPosition()
        {
            this.SetGotoPosition(this.ParameterViewModel.RelativeXEnd, this.ParameterViewModel.RelativeYEnd);
        }

        private void SetBottomLeftCornerPosition()
        {
            this.SetGotoPosition(this.ParameterViewModel.RelativeXStart, this.ParameterViewModel.RelativeYStart);
        }

        private void SetBottomRightCornerPosition()
        {
            this.SetGotoPosition(this.ParameterViewModel.RelativeXEnd, this.ParameterViewModel.RelativeYStart);
        }

        private void SetGotoPosition(double? x, double? y)
        {
            // set to values, if null to 0
            this.SerialPortViewModel.GoToX = x ?? 0;
            this.SerialPortViewModel.GoToY = y ?? 0;
        }

        private void SetOriginToCurrentPosition()
        {
            if (this.SerialPortViewModel.AbsoluteCurrentX == null || this.SerialPortViewModel.AbsoluteCurrentY == null)
            {
                return;
            }

            this.ParameterViewModel.OriginX = this.GetOneDigitRoundedValue((double)this.SerialPortViewModel.AbsoluteCurrentX);
            this.ParameterViewModel.OriginY = this.GetOneDigitRoundedValue((double)this.SerialPortViewModel.AbsoluteCurrentY);
        }

        private void SetStartPositionToCurrentPosition()
        {
            if (this.SerialPortViewModel.RelativeCurrentX == null || this.SerialPortViewModel.RelativeCurrentY == null)
            {
                return;
            }

            this.ParameterViewModel.RelativeXStart = this.GetOneDigitRoundedValue((double)this.SerialPortViewModel.RelativeCurrentX);
            this.ParameterViewModel.RelativeYStart = this.GetOneDigitRoundedValue((double)this.SerialPortViewModel.RelativeCurrentY);
        }

        private void SetEndPositionToCurrentPosition()
        {
            if (this.SerialPortViewModel.RelativeCurrentX == null || this.SerialPortViewModel.RelativeCurrentY == null)
            {
                return;
            }

            this.ParameterViewModel.RelativeXEnd = this.GetOneDigitRoundedValue((double)this.SerialPortViewModel.RelativeCurrentX);
            this.ParameterViewModel.RelativeYEnd = this.GetOneDigitRoundedValue((double)this.SerialPortViewModel.RelativeCurrentY);
        }

        private void SendParameterAndSwitchToScanViewModel()
        {
            this.SendParameterConfiguration();
            this.SendSwitchToScanViewModel();

            if (this.SerialPortViewModel.IsOpen)
            {
                this.SerialPortViewModel.OpenCloseConnectionCommand.Execute(null);
            }
        }

        private void SendParameterConfiguration()
        {
            Messenger.Default.Send(new NotificationMessage<Parameter>(this.parameter,
                                                                      Resources.Messenger_CreateNewAnalysisWithLoadedParameterForScanVM));

            // Create new parameter (otherwise existing reference)
            this.CreateCloneParameter();
        }

        private void CreateNewParameter()
        {
            this.parameter = new Parameter();
            this.ParameterViewModel = new ParameterViewModel(this.repository, this.parameter);
        }

        private void CreateCloneParameter()
        {
            this.parameter = this.parameter.ClonePositions();
            this.ParameterViewModel = new ParameterViewModel(this.repository, this.parameter);
        }

        private void SendSwitchToScanViewModel()
        {
            Messenger.Default.Send(new NotificationMessage(Resources.Messenger_LoadScanVMForMainVM));
        }

        private double GetOneDigitRoundedValue(double value)
        {
            return Math.Round(value, 1);
        }

        private void ExecuteNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Resources.Messenger_UpdateParameterValuesForSetupVM)
            {
                this.CreateNewParameter();
            }
        }
    }
}