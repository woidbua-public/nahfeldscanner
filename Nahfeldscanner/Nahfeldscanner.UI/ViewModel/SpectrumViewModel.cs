﻿// ///////////////////////////////////
// File: SpectrumViewModel.cs
// Last Change: 16.02.2018  13:39
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Linq.Expressions;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.Model.Enum;
    using Nahfeldscanner.UI.Properties;
    using NHibernate.Criterion;



    public class SpectrumViewModel : BindableViewModelBase
    {
        #region Fields

        private readonly Analysis _analysis;
        private readonly IRepository _repository;
        private DetectorType _selectedDetectorType;
        private int _selectedFrequencyIndex;
        private int _maxFrequencyIndex;
        private double _minIntensityIndicator;
        private double _maxIntensityIndicator;
        private MeasurementViewModel _selectedMeasurementViewModel;
        private MeasurementViewModel _overallMinMeasurementViewModel;
        private MeasurementViewModel _overallMaxMeasurementViewModel;
        private DiagramMeasurementViewModel _diagramMeasurementViewModel;
        private RelayCommand _selectSmallerIndexCommand;
        private RelayCommand _selectGreaterIndexCommand;
        private RelayCommand _selectFrequencyIndexOfSelectedMeasurementWhereMinIntensityCommand;
        private RelayCommand _selectFrequencyIndexOfSelectedMeasurementWhereMaxIntensityCommand;
        private RelayCommand _selectOverallMinIntensityCommand;
        private RelayCommand _selectOverallMaxIntensityCommand;
        private RelayCommand _selectAllMinIntensitiesCommand;
        private RelayCommand _selectAllMaxIntensitiesCommand;
        private RelayCommand _resetMinIntensityIndicatorCommand;
        private RelayCommand _resetMaxIntensityIndicatorCommand;
        private RelayCommand _setCurrentMinIntensityIndicatorCommand;
        private RelayCommand _setCurrentMaxIntensityIndicatorCommand;

        #endregion



        #region Constructors / Destructor

        public SpectrumViewModel(IRepository repository, Analysis analysis)
        {
            this._repository = repository;
            this._analysis = analysis;

            this.MeasurementViewModels = new ObservableCollection<MeasurementViewModel>();

            if (this._analysis.Measurements.Count != 0)
            {
                this.CalculateMaxFrequencyIndex();
                this.CalculateMeasurementDimensions();
                this.InitMeasurements();
                this.InitIntensityIndicators();
            }
        }

        #endregion



        #region Properties

        /// <summary>
        ///     Gets or sets the min intensity for coloring.
        /// </summary>
        public double MinIntensityIndicator
        {
            get { return this._minIntensityIndicator; }
            set { this.SetProperty(ref this._minIntensityIndicator, value); }
        }

        /// <summary>
        ///     Gets or sets the max intensity for coloring.
        /// </summary>
        public double MaxIntensityIndicator
        {
            get { return this._maxIntensityIndicator; }
            set { this.SetProperty(ref this._maxIntensityIndicator, value); }
        }

        /// <summary>
        ///     Gets the horizontal size of a single measurement.
        /// </summary>
        public double XMeasurementSize { get; private set; }

        /// <summary>
        ///     Gets the vertical size of a single measurement.
        /// </summary>
        public double YMeasurementSize { get; private set; }

        /// <summary>
        ///     Gets the horizontal size of a measurment row.
        /// </summary>
        public double XOverallMeasurementSize { get; private set; }

        /// <summary>
        ///     Gets the vertical size of a measurement column.
        /// </summary>
        public double YOverallMeasurementSize { get; private set; }

        public double? XMin
        {
            get { return this._analysis.Parameter.XStart; }
        }

        public double? XMax
        {
            get { return this._analysis.Parameter.XEnd; }
        }

        public double? YMin
        {
            get { return this._analysis.Parameter.YStart; }
        }

        public double? YMax
        {
            get { return this._analysis.Parameter.YEnd; }
        }

        /// <summary>
        ///     Gets the min frequency of the analysis.
        /// </summary>
        public double MinFrequency
        {
            get { return Convert.ToDouble(this._analysis.Parameter.FreqStart); }
        }

        /// <summary>
        ///     Indicates whether the analysis has quasipeak measurements.
        /// </summary>
        public bool HasQuasipeakMeasurements
        {
            get { return this._analysis.Parameter.DoQuasipeakMeasurement; }
        }

        /// <summary>
        ///     Gets the currently selected detector type
        /// </summary>
        public DetectorType SelectedDetectorType
        {
            get { return this._selectedDetectorType; }
            set
            {
                if (this.SetProperty(ref this._selectedDetectorType, value))
                {
                    // set the measurement values depending on the selected detector type
                    if (this._analysis.Measurements.Count != 0)
                    {
                        this.UpdateMinMaxMeasurementValues(this.MeasurementViewModels[0].Id,
                                                           this.MeasurementViewModels[this.MeasurementViewModels.Count - 1].Id);
                        this.UpdateOverallMinMaxMeasurementValue();
                        this.UpdateCurrentMeasurementValues();
                        this.InitIntensityIndicators();
                    }
                }
            }
        }

        /// <summary>
        ///     Gets the currently selected frequency index
        /// </summary>
        public int SelectedFrequencyIndex
        {
            get { return this._selectedFrequencyIndex; }
            set
            {
                if (this.SetProperty(ref this._selectedFrequencyIndex, value))
                {
                    // raise manually, dependent on selected frequency index
                    this.RaisePropertyChanged(() => this.SelectedFrequency);

                    // Set current measurement values to new frequency and update tracker position in diagram
                    if (this._analysis.Measurements.Count != 0)
                    {
                        this.UpdateCurrentMeasurementValues();
                        this.UpdateTrackerInDiagram();
                    }
                }
            }
        }

        /// <summary>
        ///     Gets the max index of the selectable frequencies.
        /// </summary>
        public int MaxFrequencyIndex
        {
            get { return this._maxFrequencyIndex; }
            private set { this.SetProperty(ref this._maxFrequencyIndex, value); }
        }

        /// <summary>
        ///     Gets the currently selected measurement value
        /// </summary>
        public MeasurementViewModel SelectedMeasurementViewModel
        {
            get { return this._selectedMeasurementViewModel; }
            set
            {
                if (this.SetProperty(ref this._selectedMeasurementViewModel, value))
                {
                    // selected measurement could be set to null, so check necessary
                    if (this._selectedMeasurementViewModel != null)
                    {
                        this.SetNewDiagramMeasurementViewModel();
                    }

                    // update the go to position command for the ScanViewModel
                    Messenger.Default.Send(new NotificationMessage(Resources.Message_UpdateGoToPositionCommandForScanVM));
                }
            }
        }

        /// <summary>
        ///     Gets the measurement that contains the min measurement value
        ///     depending on detector type of the whole analysis.
        /// </summary>
        public MeasurementViewModel OverallMinMeasurementViewModel
        {
            get { return this._overallMinMeasurementViewModel; }
            private set { this.SetProperty(ref this._overallMinMeasurementViewModel, value); }
        }

        /// <summary>
        ///     Gets the measurement that contains the max measurement value
        ///     depending on detector type of the whole analysis.
        /// </summary>
        public MeasurementViewModel OverallMaxMeasurementViewModel
        {
            get { return this._overallMaxMeasurementViewModel; }
            private set { this.SetProperty(ref this._overallMaxMeasurementViewModel, value); }
        }

        /// <summary>
        ///     Gets the measurement for the visualisation in the diagram.
        /// </summary>
        public DiagramMeasurementViewModel DiagramMeasurementViewModel
        {
            get { return this._diagramMeasurementViewModel; }
            private set { this.SetProperty(ref this._diagramMeasurementViewModel, value); }
        }

        /// <summary>
        ///     Gets all measurement view models in the analysis
        /// </summary>
        public ObservableCollection<MeasurementViewModel> MeasurementViewModels { get; private set; }

        /// <summary>
        ///     Gets the command to decrement the currently selected frequency index by one.
        /// </summary>
        public RelayCommand SelectSmallerIndexCommand
        {
            get
            {
                if (this._selectSmallerIndexCommand == null)
                {
                    this._selectSmallerIndexCommand = new RelayCommand(this.SelectSmallerIndex);
                }

                return this._selectSmallerIndexCommand;
            }
        }

        /// <summary>
        ///     Gets the command to increment the currently selected frequency index by one.
        /// </summary>
        public RelayCommand SelectGreaterIndexCommand
        {
            get
            {
                if (this._selectGreaterIndexCommand == null)
                {
                    this._selectGreaterIndexCommand = new RelayCommand(this.SelectGreaterIndex);
                }

                return this._selectGreaterIndexCommand;
            }
        }

        /// <summary>
        ///     Gets the command to select the frequency index where the min intensity of the selected measurement is.
        /// </summary>
        public RelayCommand SelectFrequencyIndexOfSelectedMeasurementWhereMinIntensityCommand
        {
            get
            {
                if (this._selectFrequencyIndexOfSelectedMeasurementWhereMinIntensityCommand == null)
                {
                    this._selectFrequencyIndexOfSelectedMeasurementWhereMinIntensityCommand = new RelayCommand(this.SelectFrequencyIndexOfSelectedMeasurementWhereMinIntensity);
                }

                return this._selectFrequencyIndexOfSelectedMeasurementWhereMinIntensityCommand;
            }
        }

        /// <summary>
        ///     Gets the command to select the frequency index where the max intensity of the selected measurement is.
        /// </summary>
        public RelayCommand SelectFrequencyIndexOfSelectedMeasurementWhereMaxIntensityCommand
        {
            get
            {
                if (this._selectFrequencyIndexOfSelectedMeasurementWhereMaxIntensityCommand == null)
                {
                    this._selectFrequencyIndexOfSelectedMeasurementWhereMaxIntensityCommand = new RelayCommand(this.SelectFrequencyIndexOfSelectedMeasurementWhereMaxIntensity);
                }

                return this._selectFrequencyIndexOfSelectedMeasurementWhereMaxIntensityCommand;
            }
        }

        /// <summary>
        ///     Gets the command to select the measurement where the overall min intensity occured.
        /// </summary>
        public RelayCommand SelectOverallMinIntensityCommand
        {
            get
            {
                if (this._selectOverallMinIntensityCommand == null)
                {
                    this._selectOverallMinIntensityCommand = new RelayCommand(this.SelectOverallMinIntensity);
                }

                return this._selectOverallMinIntensityCommand;
            }
        }

        /// <summary>
        ///     Gets the command to select the measurement where the overall max intensity occured.
        /// </summary>
        public RelayCommand SelectOverallMaxIntensityCommand
        {
            get
            {
                if (this._selectOverallMaxIntensityCommand == null)
                {
                    this._selectOverallMaxIntensityCommand = new RelayCommand(this.SelectOverallMaxIntensity);
                }

                return this._selectOverallMaxIntensityCommand;
            }
        }

        /// <summary>
        ///     Gets the command to select in every measurement the measurement values where the min intensity occured.
        /// </summary>
        public RelayCommand SelectAllMinIntensitiesCommand
        {
            get
            {
                if (this._selectAllMinIntensitiesCommand == null)
                {
                    this._selectAllMinIntensitiesCommand = new RelayCommand(this.SelectAllMinIntensities);
                }

                return this._selectAllMinIntensitiesCommand;
            }
        }

        /// <summary>
        ///     Gets the command to select in every measurement the measurement values where the max intensity occured.
        /// </summary>
        public RelayCommand SelectAllMaxIntensitiesCommand
        {
            get
            {
                if (this._selectAllMaxIntensitiesCommand == null)
                {
                    this._selectAllMaxIntensitiesCommand = new RelayCommand(this.SelectAllMaxIntensities);
                }

                return this._selectAllMaxIntensitiesCommand;
            }
        }

        /// <summary>
        ///     Sets the min intensity indicator to the overall min value
        /// </summary>
        public RelayCommand ResetMinIntensityIndicatorCommand
        {
            get
            {
                if (this._resetMinIntensityIndicatorCommand == null)
                {
                    this._resetMinIntensityIndicatorCommand = new RelayCommand(this.ResetMinIntensityIndicator);
                }

                return this._resetMinIntensityIndicatorCommand;
            }
        }

        /// <summary>
        ///     Sets the max intensity indicator to the overall max value
        /// </summary>
        public RelayCommand ResetMaxIntensityIndicatorCommand
        {
            get
            {
                if (this._resetMaxIntensityIndicatorCommand == null)
                {
                    this._resetMaxIntensityIndicatorCommand = new RelayCommand(this.ResetMaxIntensityIndicator);
                }

                return this._resetMaxIntensityIndicatorCommand;
            }
        }

        /// <summary>
        ///     Sets the max intensity indicator to the max value of the current frequency
        /// </summary>
        public RelayCommand SetCurrentMinIntensityIndicatorCommand
        {
            get
            {
                if (this._setCurrentMinIntensityIndicatorCommand == null)
                {
                    this._setCurrentMinIntensityIndicatorCommand = new RelayCommand(this.SetCurrentMinIntensityIndicator);
                }

                return this._setCurrentMinIntensityIndicatorCommand;
            }
        }

        /// <summary>
        ///     Sets the min intensity indicator to the min value of the current frequency
        /// </summary>
        public RelayCommand SetCurrentMaxIntensityIndicatorCommand
        {
            get
            {
                if (this._setCurrentMaxIntensityIndicatorCommand == null)
                {
                    this._setCurrentMaxIntensityIndicatorCommand = new RelayCommand(this.SetCurrentMaxIntensityIndicator);
                }

                return this._setCurrentMaxIntensityIndicatorCommand;
            }
        }

        /// <summary>
        ///     Gets the currently selected frequency
        /// </summary>
        public double SelectedFrequency
        {
            get { return Convert.ToDouble(this._analysis.Parameter.FreqStart + this.SelectedFrequencyIndex * this._analysis.Parameter.DeltaFreq); }
        }

        #endregion



        private void ResetMaxIntensityIndicator()
        {
            this.MaxIntensityIndicator = this.OverallMaxMeasurementViewModel.MaxIntensity;
        }

        private void ResetMinIntensityIndicator()
        {
            this.MinIntensityIndicator = this.OverallMinMeasurementViewModel.MinIntensity;
        }

        private void SetCurrentMinIntensityIndicator()
        {
            this.MinIntensityIndicator = this.MeasurementViewModels.Min(x => x.CurrentIntensity);
        }

        private void SetCurrentMaxIntensityIndicator()
        {
            this.MaxIntensityIndicator = this.MeasurementViewModels.Max(x => x.CurrentIntensity);
        }

        /// <summary>
        ///     Calculates the number of recorded measuring points and sets the max index
        ///     that represents the last measure point.
        /// </summary>
        private void CalculateMaxFrequencyIndex()
        {
            this.MaxFrequencyIndex = Convert.ToInt32((this._analysis.Parameter.FreqEnd - this._analysis.Parameter.FreqStart) / this._analysis.Parameter.DeltaFreq);
        }

        /// <summary>
        ///     Initializes all measurements (current / min / max values) and the overall min / max
        ///     measurement values.
        /// </summary>
        private void InitMeasurements()
        {
            ICollection<MeasurementValue> currentMeasurementValues = this.GetCurrentMeasurementValues();
            ICollection<MeasurementValue> minMeasurementValues = this.GetMinMeasurementValues(currentMeasurementValues.ElementAt(0).Measurement.ID,
                                                                                              currentMeasurementValues.ElementAt(currentMeasurementValues.Count - 1).Measurement.ID);
            ICollection<MeasurementValue> maxMeasurementValues = this.GetMaxMeasurementValues(currentMeasurementValues.ElementAt(0).Measurement.ID,
                                                                                              currentMeasurementValues.ElementAt(currentMeasurementValues.Count - 1).Measurement.ID);

            for (int i = 0; i < this._analysis.Measurements.Count; i++)
            {
                // ReSharper disable PossibleInvalidOperationException
                int xDimension = Convert.ToInt32(((this._analysis.Measurements[i].X - (double)this._analysis.Parameter.XStart) / (double)this._analysis.Parameter.DeltaX) * this.XMeasurementSize);
                int yDimension = Convert.ToInt32(((this._analysis.Measurements[i].Y - (double)this._analysis.Parameter.YStart) / (double)this._analysis.Parameter.DeltaY) * this.YMeasurementSize);

                // ReSharper restore PossibleInvalidOperationException

                this.MeasurementViewModels.Add(new MeasurementViewModel(this._analysis.Measurements[i],
                                                                        currentMeasurementValues.ElementAt(i),
                                                                        minMeasurementValues.ElementAt(i),
                                                                        maxMeasurementValues.ElementAt(i),
                                                                        this.SelectedDetectorType, xDimension, yDimension)
                                              );
            }

            this.UpdateOverallMinMaxMeasurementValue();
        }

        private void CalculateMeasurementDimensions()
        {
            double? xDiff = this._analysis.Parameter.XEnd - this._analysis.Parameter.XStart;
            double? yDiff = this._analysis.Parameter.YEnd - this._analysis.Parameter.YStart;

            int columns = Convert.ToInt32(xDiff / this._analysis.Parameter.DeltaX + 1);
            int rows = Convert.ToInt32(yDiff / this._analysis.Parameter.DeltaY + 1);

            // ReSharper disable PossibleInvalidOperationException
            if (xDiff >= yDiff)
            {
                this.XMeasurementSize = Settings.Default.SpectrumImageWidth / columns;
                this.YMeasurementSize = this.XMeasurementSize * (double)this._analysis.Parameter.DeltaY / (double)this._analysis.Parameter.DeltaX;
            }
            else
            {
                this.YMeasurementSize = Settings.Default.SpectrumImageHeight / rows;
                this.XMeasurementSize = this.YMeasurementSize * (double)this._analysis.Parameter.DeltaX / (double)this._analysis.Parameter.DeltaY;
            }

            this.XOverallMeasurementSize = this.XMeasurementSize * columns;
            this.YOverallMeasurementSize = this.YMeasurementSize * rows;
        }

        /// <summary>
        ///     Sets the overall min / max measurement values for the currently selected detector type.
        /// </summary>
        private void UpdateOverallMinMaxMeasurementValue()
        {
            this.OverallMinMeasurementViewModel = this.MeasurementViewModels.FirstOrDefault(x => this.MeasurementViewModels.Min(y => y.MinIntensity).Equals(x.MinIntensity));
            this.OverallMaxMeasurementViewModel = this.MeasurementViewModels.FirstOrDefault(x => this.MeasurementViewModels.Max(y => y.MaxIntensity).Equals(x.MaxIntensity));
        }

        /// <summary>
        ///     Replaces the currently selected measurement values with the values for the new frequency.
        /// </summary>
        private void UpdateCurrentMeasurementValues()
        {
            ICollection<MeasurementValue> currentMeasurementValues = this.GetCurrentMeasurementValues();

            for (int i = 0; i < this.MeasurementViewModels.Count; i++)
            {
                this.MeasurementViewModels[i].SetCurrentMeasurementValue(currentMeasurementValues.ElementAt(i), this.SelectedDetectorType);
            }
        }

        /// <summary>
        ///     Replaces the min and max measurement values of the measurements in the list
        ///     depending on the currently selected detector type.
        /// </summary>
        private void UpdateMinMaxMeasurementValues(int minMeasurementId, int maxMeasurementId)
        {
            ICollection<MeasurementValue> minMeasurementValues = this.GetMinMeasurementValues(minMeasurementId, maxMeasurementId);
            ICollection<MeasurementValue> maxMeasurementValues = this.GetMaxMeasurementValues(minMeasurementId, maxMeasurementId);

            for (int i = 0; i < this.MeasurementViewModels.Count; i++)
            {
                this.MeasurementViewModels[i].SetMinMeasurementValue(minMeasurementValues.ElementAt(i), this.SelectedDetectorType);
                this.MeasurementViewModels[i].SetMaxMeasurementValue(maxMeasurementValues.ElementAt(i), this.SelectedDetectorType);
            }
        }

        /// <summary>
        ///     Initializes the intensity indicators to the min and max values.
        /// </summary>
        private void InitIntensityIndicators()
        {
            this.MinIntensityIndicator = this.OverallMinMeasurementViewModel.MinIntensity;
            this.MaxIntensityIndicator = this.OverallMaxMeasurementViewModel.MaxIntensity;
        }

        /// <summary>
        ///     Sets the tracker position of the diagram to the current frequency
        /// </summary>
        private void UpdateTrackerInDiagram()
        {
            if (this.DiagramMeasurementViewModel != null)
            {
                this.DiagramMeasurementViewModel.SetTrackerDataPointPosition(this.SelectedFrequency);
            }
        }

        private ICollection<MeasurementValue> GetMinMeasurementValues(int minMeasurementId, int maxMeasurementId)
        {
            ICriterion criterion =
                    Restrictions.And(Restrictions.Le(Projections.Property<MeasurementValue>(x => x.Measurement.ID), maxMeasurementId),
                                     Restrictions.Ge(Projections.Property<MeasurementValue>(x => x.Measurement.ID), minMeasurementId));

            return this._repository.GetMinMeasuremntValueCollectionByCriterion(criterion, this.GetDetectorTypeProperty());
        }

        private ICollection<MeasurementValue> GetMaxMeasurementValues(int minMeasurementId, int maxMeasurementId)
        {
            ICriterion criterion =
                    Restrictions.And(Restrictions.Le(Projections.Property<MeasurementValue>(x => x.Measurement.ID), maxMeasurementId),
                                     Restrictions.Ge(Projections.Property<MeasurementValue>(x => x.Measurement.ID), minMeasurementId));

            return this._repository.GetMaxMeasuremntValueCollectionByCriterion(criterion, this.GetDetectorTypeProperty());
        }

        /// <summary>
        ///     Gets all measurement values of the currently selected frequency and analysis.
        /// </summary>
        /// <returns>all measurement values with currently selected frequency</returns>
        private ICollection<MeasurementValue> GetCurrentMeasurementValues()
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            ICriterion measurementValueCriterion = Restrictions.Where<MeasurementValue>(x => x.Frequency == this.SelectedFrequency);
            ICriterion measurementCriterion = Restrictions.Where<Measurement>(x => x.Analysis.ID == this._analysis.ID);

            return this._repository.GetCollectionByCriterion<MeasurementValue, Measurement>(measurementValueCriterion, x => x.Measurement, measurementCriterion, x => x.Measurement.ID);
        }

        /// <summary>
        ///     Gets the expression for the intensity of the desired detector type.
        /// </summary>
        /// <returns>intensity expression for the currently selected detector type</returns>
        private Expression<Func<MeasurementValue, object>> GetDetectorTypeProperty()
        {
            switch (this.SelectedDetectorType)
            {
                case DetectorType.Max_Peak:
                    return mv => mv.IntensityMaxDetector;
                case DetectorType.Average:
                    return mv => mv.IntensityAVGDetector;
                default:
                    return mv => mv.IntensityQPDetector;
            }
        }

        /// <summary>
        ///     Decrements the currently selected frequency index by one.
        /// </summary>
        private void SelectSmallerIndex()
        {
            if (this.SelectedFrequencyIndex > 0)
            {
                this.SelectedFrequencyIndex--;
            }
        }

        /// <summary>
        ///     Increments the currently selected frequency index by one.
        /// </summary>
        private void SelectGreaterIndex()
        {
            if (this.SelectedFrequencyIndex < this.MaxFrequencyIndex)
            {
                this.SelectedFrequencyIndex++;
            }
        }

        /// <summary>
        ///     Selects the frequency index where the min intensity of the selected measurement is.
        /// </summary>
        private void SelectFrequencyIndexOfSelectedMeasurementWhereMinIntensity()
        {
            if (this.SelectedMeasurementViewModel != null)
            {
                this.SelectedFrequencyIndex = Convert.ToInt32((this.SelectedMeasurementViewModel.MinFrequency - this._analysis.Parameter.FreqStart) / this._analysis.Parameter.DeltaFreq);
            }
        }

        /// <summary>
        ///     Selects the frequency index where the max intensity of the selected measurement is.
        /// </summary>
        private void SelectFrequencyIndexOfSelectedMeasurementWhereMaxIntensity()
        {
            if (this.SelectedMeasurementViewModel != null)
            {
                this.SelectedFrequencyIndex = Convert.ToInt32((this.SelectedMeasurementViewModel.MaxFrequency - this._analysis.Parameter.FreqStart) / this._analysis.Parameter.DeltaFreq);
            }
        }

        /// <summary>
        ///     Selects the measurement where the overall min intensity occured.
        /// </summary>
        private void SelectOverallMinIntensity()
        {
            if (this.OverallMinMeasurementViewModel != null)
            {
                this.SelectedMeasurementViewModel = this.OverallMinMeasurementViewModel;
                this.SelectFrequencyIndexOfSelectedMeasurementWhereMinIntensity();
            }
        }

        /// <summary>
        ///     Selects the measurement where the overall max intensity occured.
        /// </summary>
        private void SelectOverallMaxIntensity()
        {
            if (this.OverallMaxMeasurementViewModel != null)
            {
                this.SelectedMeasurementViewModel = this.OverallMaxMeasurementViewModel;
                this.SelectFrequencyIndexOfSelectedMeasurementWhereMaxIntensity();
            }
        }

        /// <summary>
        ///     Selects in every measurement the measurement values where the min intensity occured.
        /// </summary>
        private void SelectAllMinIntensities()
        {
            foreach (MeasurementViewModel measurementViewModel in this.MeasurementViewModels)
            {
                measurementViewModel.SelectMinMeasurementValue();
            }
        }

        /// <summary>
        ///     Selects in every measurement the measurement values where the max intensity occured.
        /// </summary>
        private void SelectAllMaxIntensities()
        {
            foreach (MeasurementViewModel measurementViewModel in this.MeasurementViewModels)
            {
                measurementViewModel.SelectMaxMeasurementValue();
            }
        }

        /// <summary>
        ///     Creates a new diagram measurement out of the selected measurement.
        /// </summary>
        private void SetNewDiagramMeasurementViewModel()
        {
            this.DiagramMeasurementViewModel = new DiagramMeasurementViewModel(this._repository.GetEagerById<Measurement>(this.SelectedMeasurementViewModel.Id,
                                                                                                                          m => m.MeasurementValues), this.SelectedFrequency);
        }
    }
}