﻿// ///////////////////////////////////
// File: TestData_SensorViewModel.cs
// Last Change: 22.01.2018  11:24
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System.Drawing;
    using Nahfeldscanner.Model.Database;
    using Nahfeldscanner.UI.Properties;



    // TODO: delete test class
    public class TestData_SensorViewModel : SensorViewModel
    {
        #region Constructors / Destructor

        public TestData_SensorViewModel(Sensor sensor) : base(sensor)
        {
            Bitmap bitmap = Resources.img_home;
            ImageConverter imageConverter = new ImageConverter();

            this.SensorType = Model.Enum.SensorType.E_Field;
            this.SensorImageStream = (byte[])imageConverter.ConvertTo(bitmap, typeof(byte[]));
            this.SensorName = "Testsensor XCPE34";
        }

        #endregion
    }
}