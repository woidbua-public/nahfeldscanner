// ///////////////////////////////////
// File: ViewModelLocator.cs
// Last Change: 22.01.2018  11:23
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using GalaSoft.MvvmLight.Ioc;
    using Microsoft.Practices.ServiceLocation;
    using Nahfeldscanner.Business;
    using Nahfeldscanner.DataAccess;
    using Nahfeldscanner.UI.Service;



    public class ViewModelLocator
    {
        #region Constructors / Destructor

        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<ISessionManager, SqLiteSessionManager>();
            SimpleIoc.Default.Register<IRepository, SqLiteRepository>();
            SimpleIoc.Default.Register<IDialogService, DialogService>();
            SimpleIoc.Default.Register<MainViewModel>();
        }

        #endregion



        #region Properties

        public MainViewModel Main
        {
            get { return ServiceLocator.Current.GetInstance<MainViewModel>(); }
        }

        #endregion



        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}