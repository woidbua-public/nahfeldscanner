﻿// ///////////////////////////////////
// File: WorkspaceViewModel.cs
// Last Change: 22.01.2018  11:22
// Author: Andre Multerer
// ///////////////////////////////////



namespace Nahfeldscanner.UI.ViewModel
{
    using System.Drawing;



    public class WorkspaceViewModel : BindableViewModelBase
    {
        #region Constructors / Destructor

        public WorkspaceViewModel(string title, Bitmap bitmap)
        {
            this.Title = title;
            this.Bitmap = bitmap;
        }

        #endregion



        #region Properties

        public string Title { get; private set; }

        public Bitmap Bitmap { get; private set; }

        #endregion
    }
}