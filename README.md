<!-- markdownlint-disable MD030-->

# Nahfeldscanner

## Beschreibung

Bau und Programmierung eines EMV-Nahfeldscanners zur Lokalisierung von Komponenten, die EMV Störungen verursachen.

## Aufgabenstellung

-   Konstruktion und Aufbau einer XY-Einheit zur Positionierung der Nahfeldsonde über dem Prüfling
-   Entwicklung und Aufbau der elektrischen Ansteuerung
-   Software zur Ansteuerung der XY-Einheit zum Abfahren parametrierbarer Rasterpunkte
-   Ansteuerung eines Spektrumanalysators zur Konfiguration und Rücklesung der Messwerte
-   Software zur Aufbereitung und Darstellung der Messergebnisse in einer farbigen 2D-Darstellung

## Verwendete Software / Technologie

-   Arduino
-   C# / WPF
-   SQLite

## Ergebnis

### Aufbau

![Nahfeldscanner Aufbau](docs/img/nfsc_1.jpg)

### Konfigurationsmenü

Manuelle Steuerung des Schlitten über Pfeiltasten + Einstellen der Scanfläche.
![Konfigurationsmenü](docs/img/nfsc_2.png)

### Scanmenü

Visuelle Darstellung der auftretenden Pegelwerte über den Frequenzspektrum.
![Scanmenü](docs/img/nfsc_3.png)

### Suchmenü

Durchsuchen der Datenbank nach bestimmten Analysen mit bestimmten Schlagwörtern.
![Suchmenü](docs/img/nfsc_4.png)

### Optionenmenü

Einstellen von Default Parametern + Anlegen verschiedener Sensortypen
![Optionenmenü](docs/img/nfsc_5.png)
