Ben�tigte Software, um SQLite-Datenbank �ffnen zu k�nnen:
http://sqlitebrowser.org/

Befehle, um alle Tabellen au�er Sensoren zu resetten (Beachte Reihenfolge!):
DROP TABLE IF EXISTS MeasurementValue;
DROP TABLE IF EXISTS Measurement;
DROP TABLE IF EXISTS Analysis;
DROP TABLE IF EXISTS Parameter;
(optional)DROP TABLE IF EXISTS Sensor;